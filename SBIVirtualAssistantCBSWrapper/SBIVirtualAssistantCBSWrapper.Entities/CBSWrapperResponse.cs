﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SBIVirtualAssistantCBSWrapper.Entities
{
    public class CBSWrapperResponse
    {
        public string ResponseCode { get; set; }
        public string ErrorCode { get; set; }
        public string ErrorDescription { get; set; }
    }
}