﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBIVirtualAssistantCBSWrapper.Entities
{
    class INBAccountStatementResponse: BaseAPIResponse
    {

        public string ReferenceNumber { get; set; }
        public Statementdetail[] StatementDetails { get; set; }
    }

    public class Statementdetail
    {
        public string Amount { get; set; }
        public string Balance { get; set; }
        public string BranchNumber { get; set; }
        public string ChequeNumber { get; set; }
        public string Filler { get; set; }
        public string INBStatement { get; set; }
        public string Narration { get; set; }
        public string PostTime { get; set; }
        public string Statement { get; set; }
        public string TodaysDate { get; set; }
        public string TransferStatement { get; set; }
        public string ValueDate { get; set; }
    }

}
