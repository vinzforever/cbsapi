﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBIVirtualAssistantCBSWrapper.Entities
{
    public class ChequeBookResponse
    {
        public string ReferenceNumber { get; set; }
        public string CIFnumber { get; set; }
        public string AccountNumber { get; set; }
        public string JournalNumber { get; set; }
        public string BranchCode { get; set; }
        public string Status { get; set; }
        public string Data { get; set; }
        public string ErrorCode { get; set; }
        public string ErrorDescription { get; set; }
    }

}
