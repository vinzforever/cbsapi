﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBIVirtualAssistantCBSWrapper.Entities
{
   public  class AccountInformationRequest
    {
       public string ReferenceNumber { get; set; }
        public String RequestData { get; set; }
        public int Bank_Code { get; set; }
    }
}
