﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBIVirtualAssistantCBSWrapper.Entities
{
   public class INBAccountStatementRequest
    {

        public string AccountNumber { get; set; }
        public string FromAmount { get; set; }
        public string FromDate { get; set; }
        public string ToAmount { get; set; }
        public string ToDate { get; set; }
        public string TransactionNumber { get; set; }
    }

}
