﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBIVirtualAssistantCBSWrapper.Entities
{
    public class IFSCEnquiryRequest
    {
        public string ProductCode { get; set; }
        public string IFSCCode { get; set; }
    }

}
