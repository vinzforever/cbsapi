﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBIVirtualAssistantCBSWrapper.Entities
{
    public class SMSResponse : BaseAPIResponse
    {
        public string data{ get; set; }
    }
}
