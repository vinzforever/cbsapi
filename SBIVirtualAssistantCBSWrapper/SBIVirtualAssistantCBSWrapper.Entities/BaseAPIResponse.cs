﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBIVirtualAssistantCBSWrapper.Entities
{
    public class BaseAPIResponse
    {
        public string ErrorCode { get; set; }
        public string ErrorDesc { get; set; }
        public string SpeechEnglish { get; set; }
        public string SpeechHindi { get; set; }
        public string DisplayEnglish { get; set; }
        public string DisplayHindi { get; set; }

    }
}
