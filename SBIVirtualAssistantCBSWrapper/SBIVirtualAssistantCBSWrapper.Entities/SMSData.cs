﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SBIVirtualAssistantCBSWrapper.Entities
{
    public class SMSData
    {
        public string MobileNumber {get;set;}
        public string MessageData {get;set;}
        public string ProductCode {get;set;}
        public string BankCode  {get;set;}
    }
}