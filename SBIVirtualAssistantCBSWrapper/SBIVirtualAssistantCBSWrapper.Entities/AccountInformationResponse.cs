﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBIVirtualAssistantCBSWrapper.Entities
{
    public class AccountInformationResponse
    {
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string ReferenceNumber { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string AccountNumber { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string AccountName { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string CustomerName { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string CustomerNumber { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string AccountStatus { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string CustomerStatus { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string AccountHomeBranch { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string AccountType { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string AccountDescription { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string ModeofOperation { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string Visheshflag { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string vipcode { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string Currency { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string Shadowfilebal { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string AvailableBalance { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string Acctopendate { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string Micrcode { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string ACCTYPE { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string Interest_Category { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string ERROR_DESC { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string ERROR_CODE { get; set; }
    }

}
