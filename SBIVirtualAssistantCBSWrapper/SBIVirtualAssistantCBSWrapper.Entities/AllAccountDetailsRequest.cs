﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBIVirtualAssistantCBSWrapper.Entities
{
    public class AllAccountDetailsRequest
    {
        public string ReferenceNumber { get; set; }
        public string Branch_Code { get; set; }
        public string Request { get; set; }
        public string AccountType { get; set; }
    }

}
