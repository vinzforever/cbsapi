﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBIVirtualAssistantCBSWrapper.Entities
{
   public class CIFNumberResponse
    {
        public string ReferenceNumber { get; set; }
        public string ErrorDesc { get; set; }
        public string ErrorCode { get; set; }
        public string Account { get; set; }
        public List<string> ALLCIFInfo { get; set; }
    }
}
