﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBIVirtualAssistantCBSWrapper.Entities
{
    public class FDCreationBotRequest
    {
        public string TRANSFER_ACCOUNT_NUMBER { get; set; }
        public string MATURITY_DATE { get; set; }
        public string INTEREST_PAYMENT_METHOD { get; set; }
        public string MATURITY_INSTRUCTION { get; set; }
        public string TERM_VALUE_DEPOSITED { get; set; }
    }
}
