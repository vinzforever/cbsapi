﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBIVirtualAssistantCBSWrapper.Entities
{
    public class SBITransferResponse: BaseAPIResponse
    {

        public string ReferenceNumber { get; set; }
        public string CBSReferenceNumber { get; set; }
        public string Status { get; set; }
        public string JournalNumber { get; set; }
        public string GeneratedReconNumber { get; set; }

    }

}
