﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBIVirtualAssistantCBSWrapper.Entities
{
    public class NEFTResponse: BaseAPIResponse
    {

        public string ReferenceNumber { get; set; }
        public string JournalNumber { get; set; }
        public string UTRno { get; set; }
        public string DateOfTransaction { get; set; }
        public string CBSReferenceNumber { get; set; }
    }

}
