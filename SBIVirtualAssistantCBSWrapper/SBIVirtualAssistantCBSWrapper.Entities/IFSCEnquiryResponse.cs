﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBIVirtualAssistantCBSWrapper.Entities
{
    class IFSCEnquiryResponse
    {
        public string ReferenceNumber { get; set; }
        public string BankName { get; set; }
        public string BranchAddress { get; set; }
        public string BranchName { get; set; }
        public string IFSCCode { get; set; }
        public string ErrorCode { get; set; }
        public string ErrorDescription { get; set; }
    }

}
