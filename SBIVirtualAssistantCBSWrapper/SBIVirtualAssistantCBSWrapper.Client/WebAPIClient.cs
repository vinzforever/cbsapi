﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SBIVirtualAssistantCBSWrapper.Common.Resources;
using Newtonsoft.Json;

namespace SBIVirtualAssistantCBSWrapper.Client
{
    class WebAPIClient : HttpBaseClient, IWebAPIClient 
    {
        public WebAPIClient(string baseURL) : base(baseURL) { }
        //public TestWebApiClient(string baseURL) : base(baseURL) { }

        private string _Test_GetData = URLs.SMS_API;

        public string GetData()
        {
            string retVal = this.ExecuteHttpRequest(_Test_GetData, "GET", null);
            var result = JsonConvert.DeserializeObject<string>(retVal);
            return result;
        }
    }
}
