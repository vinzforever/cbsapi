﻿//using SBISi.Framework.Helpers;
using System;
using System.Net;
using System.Net.Http;
using System.Text;
namespace SBIVirtualAssistantCBSWrapper.Client
{
    public class HttpBaseClient
    {
        //private ApplicationContextEntity _context;

        private string _baseURL = string.Empty;

        //public HttpBaseClient(string baseURL, ApplicationContextEntity context)
        //{
        //    this._baseURL = baseURL;
        //    this._context = context;
        //}
        public HttpBaseClient(string baseURL)
        {
            this._baseURL = baseURL;
        }

        public string ExecuteHttpRequest(string appURI, string method, HttpContent data)
        {






            string result = string.Empty;
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder = stringBuilder.Append(this._baseURL).Append(appURI);
            using (HttpClient httpClient = new HttpClient())
            {





                String clientId = System.Configuration.ConfigurationSettings.AppSettings.Get("CLIENT.ID");
                String clientSecret = System.Configuration.ConfigurationSettings.AppSettings.Get("CLIENT.SECRET");




                if (!String.IsNullOrEmpty(clientId))
                    httpClient.DefaultRequestHeaders.Add("x-ibm-client-id", clientId);
                //if (!String.IsNullOrEmpty(clientSecret))
                //    httpClient.DefaultRequestHeaders.Add("x-ibm-client-secret", clientSecret);

                System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;


                httpClient.Timeout = new TimeSpan(0, 0,   Convert.ToInt32(  System.Configuration.ConfigurationSettings.AppSettings.Get("API:RequestTimeout")));
                HttpResponseMessage httpResponseMessage = null;
                if (method != null)
                {
                    if (!(method == "GET"))
                    {
                        if (!(method == "POST"))
                        {
                            if (!(method == "PUT"))
                            {
                                if (method == "DELETE")
                                {
                                    httpResponseMessage = httpClient.DeleteAsync(stringBuilder.ToString()).Result;
                                }
                            }
                            else
                            {
                                httpResponseMessage = httpClient.PutAsync(stringBuilder.ToString(), data).Result;
                            }
                        }
                        else
                        {
                            httpResponseMessage = httpClient.PostAsync(stringBuilder.ToString(), data).Result;
                        }
                    }
                    else
                    {
                        httpResponseMessage = httpClient.GetAsync(stringBuilder.ToString()).Result;
                    }
                }

                HttpContent content = httpResponseMessage.Content;
                result = content.ReadAsStringAsync().Result;
            }
            return result;
        }

        //public string ExecuteHttpRequest1(string appURI, string method, HttpContent data)
        //{
        //    string result = string.Empty;
        //    StringBuilder stringBuilder = new StringBuilder();
        //    stringBuilder = stringBuilder.Append(this._baseURL).Append(appURI);
        //    using (HttpClient httpClient = new HttpClient())
        //    {
        //        //httpClient.Timeout = new TimeSpan(0, 0, ConfigurationHelper.GetAppSettingValue<int>("API:RequestTimeout", 30));
        //        httpClient.Timeout = new TimeSpan(0, 0, 30);
        //        HttpResponseMessage httpResponseMessage = null;
        //        //httpClient.DefaultRequestHeaders.Add("AppContext", JsonConvert.SerializeObject(this._context));
        //        try
        //        {
        //            if (method != null)
        //            {
        //                if (!(method == "GET"))
        //                {
        //                    if (!(method == "POST"))
        //                    {
        //                        if (!(method == "PUT"))
        //                        {
        //                            if (method == "DELETE")
        //                            {
        //                                httpResponseMessage = httpClient.DeleteAsync(stringBuilder.ToString()).Result;
        //                            }
        //                        }
        //                        else
        //                        {
        //                            httpResponseMessage = httpClient.PutAsync(stringBuilder.ToString(), data).Result;
        //                        }
        //                    }
        //                    else
        //                    {
        //                        httpResponseMessage = httpClient.PostAsync(stringBuilder.ToString(), data).Result;
        //                    }
        //                }
        //                else
        //                {
        //                    httpResponseMessage = httpClient.GetAsync(stringBuilder.ToString()).Result;
        //                }
        //            }
        //        }
        //        catch (AggregateException ex)
        //        {
        //            if (ex.InnerException != null && ex.InnerException.GetType() == typeof(TaskCanceledException))
        //            {
        //                throw new TechnicalException(10006, Guid.NewGuid(), ErrorMessages.get_E10006(), ex);
        //            }
        //            throw;
        //        }
        //        if (!httpResponseMessage.IsSuccessStatusCode)
        //        {
        //            throw new TechnicalException(20001, Guid.NewGuid(), ErrorMessages.get_E20001(), new Exception(string.Format("Status Code: {0}, Reason: {1}", httpResponseMessage.StatusCode.ToString(), httpResponseMessage.ReasonPhrase)));
        //        }
        //        HttpContent content = httpResponseMessage.Content;
        //        result = content.ReadAsStringAsync().Result;
        //    }
        //    return result;
        //}
    }
}
