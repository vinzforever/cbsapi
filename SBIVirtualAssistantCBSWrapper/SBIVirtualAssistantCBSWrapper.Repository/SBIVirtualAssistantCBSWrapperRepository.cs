﻿using SBIVirtualAssistantCBSWrapper.Common.Utility;
using SBIVirtualAssistantCBSWrapper.Repository.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBIVirtualAssistantCBSWrapper.Repository
{
    class SBIVirtualAssistantCBSWrapperRepository :   ISBIVirtualAssistantCBSWrapperRepository
    {

         private IBaseRepository _BaseRepository;

         public SBIVirtualAssistantCBSWrapperRepository(IBaseRepository BaseRepository)
        {
            _BaseRepository = BaseRepository;
        }



        public string getReferenceNumber(string prifix)
        {
            var data = _BaseRepository.GetSingleRecord<decimal>(DBQueryID.GETSEQUENCE);
            return prifix + data.ToString().PadLeft(12,'0');
        }
    }
}
