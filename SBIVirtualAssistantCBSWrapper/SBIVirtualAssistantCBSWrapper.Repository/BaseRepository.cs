﻿using Dapper;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Xml.Linq;
using SBIVirtualAssistantCBSWrapper.Common.Utility;
using SBIVirtualAssistantCBSWrapper.Repository.Contracts;


namespace SBIVirtualAssistantCBSWrapper.Repository
{
    public class BaseRepository : IBaseRepository
    //<T>
    //where T : class , new()
    {
        //public readonly Entities _db = null;
        private string _BankCode = string.Empty;
        private string _querypath = string.Empty;
        public BaseRepository()
        {
            //this._db = db;
            _BankCode = "0";
            
            _querypath = HttpContext.Current.ApplicationInstance.Server.MapPath("~/App_Data/") + ConfigurationSettings.AppSettings["DBQueryFileName"];
        }

        #region Generic Methods
        public List<T> GetAll<T>(string queryID, CommandType commandType = CommandType.Text)
        {

            List<T> Tlist = new List<T>();
            string DBQuery = GetDBQuery(queryID);

            using (IDbConnection db = GetConnection())
            {
                if (!string.IsNullOrWhiteSpace(DBQuery))
                {
                    Tlist = db.Query<T>(DBQuery, commandType: commandType).ToList();
                }
            }

            return Tlist;
        }



        public List<T> GetAllbyQry<T>(string query, CommandType commandType = CommandType.Text)
        {

            List<T> Tlist = new List<T>();
            string DBQuery = query;

            using (IDbConnection db = GetConnection())
            {
                if (!string.IsNullOrWhiteSpace(DBQuery))
                {
                    Tlist = db.Query<T>(DBQuery, commandType: commandType).ToList();
                }
            }

            return Tlist;
        }



        public List<T> GetAllbyFilter<T>(string queryID, DynamicParameters param, CommandType commandType = CommandType.Text)
        {
            List<T> Tlist = new List<T>();
            Tlist = RunByQuery<T>(queryID, param, commandType);

            return Tlist;
        }

        public T GetSingleRecord<T>(string queryID, CommandType commandType = CommandType.Text)
        {
            List<T> Tlist = RunByQuery<T>(queryID, commandType);
            return Tlist.FirstOrDefault();
        }

        public T GetSingleRecordbyFilter<T>(string queryID, DynamicParameters param, CommandType commandType = CommandType.Text)
        {
            List<T> Tlist = RunByQuery<T>(queryID, param, commandType);
            return Tlist.FirstOrDefault();
        }

        public bool Save(string querID, DynamicParameters param, CommandType commandType = CommandType.Text)
        {
            try
            {
                RunByExecute(querID, param, commandType);

            }
            catch (Exception)
            {
                return false;
            }

            return true;
        }

        #endregion

        private void RunByExecute(string querID, DynamicParameters param, CommandType commandType)
        {
            string DBQuery = GetDBQuery(querID);

            using (IDbConnection db = GetConnection())
            {
                if (db.State == ConnectionState.Closed) db.Open();
                using (IDbTransaction tran = db.BeginTransaction())
                {
                    if (!string.IsNullOrWhiteSpace(DBQuery))
                    {
                        List<string> queryList = DBQuery.Split(new char[] { DBQueryID.QuerySeparator }, StringSplitOptions.RemoveEmptyEntries).ToList();

                        for (int i = 0; i < queryList.Count; i++)
                        {
                            int result = db.Execute(queryList[i], param, commandType: commandType);
                        }

                        tran.Commit();
                    }
                }

            }
        }

        private void RunByExecuteList(List<string> _queryList, DynamicParameters param, CommandType commandType)
        {
            string DBQuery = "";
            using (IDbConnection db = GetConnection())
            {
                if (db.State == ConnectionState.Closed) db.Open();
                using (IDbTransaction tran = db.BeginTransaction())
                {
                    if (_queryList.Count > 0)
                    {
                        List<string> queryList = _queryList;// DBQuery.Split(new char[] { DBQueryID.QuerySeparator }, StringSplitOptions.RemoveEmptyEntries).ToList();

                        for (int i = 0; i < queryList.Count; i++)
                        {
                            DBQuery = GetDBQuery(queryList[i]);

                            int result = db.Execute(DBQuery, param, commandType: commandType);


                        }

                        tran.Commit();
                    }
                }

            }
        }

        private List<T> RunByQuery<T>(string queryID, DynamicParameters param, CommandType commandType)
        {
            string DBQuery = GetDBQuery(queryID);
            List<T> Tlist = new List<T>();
            using (IDbConnection db = GetConnection())
            {
                if (db.State == ConnectionState.Closed) db.Open();
                //using (IDbTransaction tran = db.BeginTransaction())
                //{
                if (!string.IsNullOrWhiteSpace(DBQuery))
                {
                    List<string> queryList = DBQuery.Split(new char[] { DBQueryID.QuerySeparator }, StringSplitOptions.RemoveEmptyEntries).ToList();

                    for (int i = 0; i < queryList.Count; i++)
                    {
                        Tlist = db.Query<T>(queryList[i], param, commandType: commandType).ToList();
                    }
                }
                //}
            }
            return Tlist;
        }

        private List<T> RunByQuery<T>(string queryID, CommandType commandType)
        {
            string DBQuery = GetDBQuery(queryID);
            List<T> Tlist = new List<T>();
            using (IDbConnection db = GetConnection())
            {
                if (db.State == ConnectionState.Closed) db.Open();
                //using (IDbTransaction tran = db.BeginTransaction())
                //{
                if (!string.IsNullOrWhiteSpace(DBQuery))
                {
                    List<string> queryList = DBQuery.Split(new char[] { DBQueryID.QuerySeparator }, StringSplitOptions.RemoveEmptyEntries).ToList();

                    for (int i = 0; i < queryList.Count; i++)
                    {
                        Tlist = db.Query<T>(queryList[i], commandType: commandType).ToList();
                    }
                }
                //}
            }
            return Tlist;
        }






        public string GetDBQuery(string queryID)
        {
            var text = File.ReadAllText(_querypath);
            XDocument xDoc = XDocument.Parse(text);
            //XDocument xDoc = new XDocument();
            //StreamReader sr = new StreamReader(queryPath);

            //xDoc = XDocument.Load(sr, LoadOptions.PreserveWhitespace);
            string DBQuery = string.Empty;

            if (xDoc != null) DBQuery = xDoc.Elements("SBISIMIS").Elements("QUERY").Where(t => t.Attribute("NAME").Value == queryID).FirstOrDefault().Value;
            return DBQuery;
        }




        private OracleConnection GetConnection()
        {
            return new OracleConnection(System.Configuration.ConfigurationSettings.AppSettings.Get("Bank" + _BankCode).ToString());
        }
        public bool Delete(int id)
        {
            return true;
        }
        public T GetByID<T>(T data, int id)
        {
            return data;
        }
        public bool Save<T>(T data)
        {
            return true;
        }

        string IBaseRepository.getReferenceNumber(string queryID)
        {
            string DBQuery = GetDBQuery(queryID);
            double referenceNo =0;

            using (IDbConnection db = GetConnection())
            {
                if (db.State == ConnectionState.Closed) db.Open();
                //using (IDbTransaction tran = db.BeginTransaction())
                //{
                if (!string.IsNullOrWhiteSpace(DBQuery))
                {
                    var resp = db.Query(DBQuery);
                    
                }
                //}
            }
            return referenceNo.ToString().PadLeft(12,'0');
        }




    }
}
