﻿using System.Collections.Generic;
using System.Data;
using Dapper;

namespace SBIVirtualAssistantCBSWrapper.Repository.Contracts
{
    public interface IBaseRepository
    //where T : class, new()
    {
        string getReferenceNumber(string queryID);
        List<T> GetAll<T>(string QuerID, CommandType commandType = CommandType.Text);
        List<T> GetAllbyFilter<T>(string QuerID, DynamicParameters param, CommandType commandType = CommandType.Text);
        bool Save(string QuerID, DynamicParameters param, CommandType commandType = CommandType.Text);
        T GetSingleRecord<T>(string queryID, CommandType commandType = CommandType.Text);
        T GetSingleRecordbyFilter<T>(string queryID, DynamicParameters param, CommandType commandType = CommandType.Text);

        bool Delete(int id);
        T GetByID<T>(T data, int id);
        bool Save<T>(T data);
        string GetDBQuery(string queryID);
        List<T> GetAllbyQry<T>(string query, CommandType commandType = CommandType.Text);
    }
}
