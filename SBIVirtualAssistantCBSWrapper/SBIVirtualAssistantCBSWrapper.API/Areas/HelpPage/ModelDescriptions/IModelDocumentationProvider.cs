using System;
using System.Reflection;

namespace SBIVirtualAssistantCBSWrapper.API.Areas.HelpPage.ModelDescriptions
{
    public interface IModelDocumentationProvider
    {
        string GetDocumentation(MemberInfo member);

        string GetDocumentation(Type type);
    }
}