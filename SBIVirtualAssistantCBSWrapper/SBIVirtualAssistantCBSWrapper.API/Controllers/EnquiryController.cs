﻿using SBIVirtualAssistantCBSWrapper.Domain.Contracts;
using SBIVirtualAssistantCBSWrapper.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace SBIVirtualAssistantCBSWrapper.API.Controllers
{
    public class EnquiryController : ApiController
    {

        private ISBIVirtualAssistantCBSWrapperDomain _domain;

        public EnquiryController(ISBIVirtualAssistantCBSWrapperDomain domain)
        {
            this._domain = domain;
        }
        [HttpPost]
        [Route("Enquiry/AllAccountDetails")]
        public AllAccountDetailsResponse Post(AllAccountDetailsRequest allAccountDetailsRequest)
        {
            AllAccountDetailsResponse allAccountDetailsResponse = new AllAccountDetailsResponse();

            try
            {
                allAccountDetailsResponse = _domain.AllAccountDetails(allAccountDetailsRequest);

            }
            catch (AggregateException ex)
            {

                string resp = ex.Flatten().Message;
            }

            catch (Exception e)
            {

                string resp = e.Message;
            }

            return allAccountDetailsResponse;

        }


        [HttpPost]
        [Route("Enquiry/AllAccountDetailsByType")]
        public AllAccountDetailsResponse AllAccountDetailsByType(AllAccountDetailsRequest allAccountDetailsRequest)
        {
            AllAccountDetailsResponse allAccountDetailsResponse = new AllAccountDetailsResponse();


            try
            {
                allAccountDetailsResponse = _domain.AllAccountDetails(allAccountDetailsRequest);

                allAccountDetailsResponse.ac_details = getSelectedAccountListByAccountCategory(allAccountDetailsResponse, allAccountDetailsRequest.AccountType);

            }
            catch (AggregateException ex)
            {

                string resp = ex.Flatten().Message;
            }

            catch (Exception e)
            {

                string resp = e.Message;
            }

            return allAccountDetailsResponse;

        }





        [HttpPost]
        [Route("Enquiry/AllTermDepositAccountDetails")]
        public AllAccountDetailsResponse AllTermDepositAccountDetails(AllAccountDetailsRequest allAccountDetailsRequest)
        {
            AllAccountDetailsResponse allAccountDetailsResponse = getAllAccounts(allAccountDetailsRequest);

            allAccountDetailsResponse.ac_details = getSelectedAccountListByAccountType(ref allAccountDetailsResponse, "TDR");
            allAccountDetailsResponse.calculateNetBalance();


            return allAccountDetailsResponse;

        }

        private AllAccountDetailsResponse getAllAccounts(AllAccountDetailsRequest allAccountDetailsRequest)
        {
            AllAccountDetailsResponse allAccountDetailsResponse = new AllAccountDetailsResponse();

            try
            {
                allAccountDetailsResponse = _domain.AllAccountDetails(allAccountDetailsRequest);

            }
            catch (AggregateException ex)
            {

                string resp = ex.Flatten().Message;
            }

            catch (Exception e)
            {

                string resp = e.Message;
            }

            return allAccountDetailsResponse;
        }

        [HttpPost]
        [Route("Enquiry/AllFixedDepositAccountDetails")]
        public AllAccountDetailsResponse AllFixedDepositAccountDetails(AllAccountDetailsRequest allAccountDetailsRequest)
        {

            AllAccountDetailsResponse allAccountDetailsResponse = getAllAccounts(allAccountDetailsRequest);
            allAccountDetailsResponse.ac_details = getSelectedAccountListByAccountType(ref allAccountDetailsResponse, "TDR");


            allAccountDetailsResponse.calculateNetBalance();
            return allAccountDetailsResponse;

        }



        [HttpPost]
        [Route("Enquiry/AllRecurringDepositAccountDetails")]
        public AllAccountDetailsResponse AllRecurringDepositAccountDetails(AllAccountDetailsRequest allAccountDetailsRequest)
        {
            AllAccountDetailsResponse allAccountDetailsResponse = getAllAccounts(allAccountDetailsRequest);


            allAccountDetailsResponse.ac_details = getSelectedAccountListByAccountType(ref allAccountDetailsResponse, "RD");
            allAccountDetailsResponse.calculateNetBalance();
            return allAccountDetailsResponse;

        }


        [HttpPost]
        [Route("Enquiry/AllOverdraftAccountDetails")]
        public AllAccountDetailsResponse AllOverdraftAccountDetails(AllAccountDetailsRequest allAccountDetailsRequest)
        {
            AllAccountDetailsResponse allAccountDetailsResponse = getAllAccounts(allAccountDetailsRequest);


            allAccountDetailsResponse.ac_details = getSelectedAccountListByAccountType(ref allAccountDetailsResponse, "OD");
            allAccountDetailsResponse.calculateNetBalance();
            return allAccountDetailsResponse;

        }




        [HttpPost]
        [Route("Enquiry/AllLoanAccountDetails")]
        public AllAccountDetailsResponse AllLoanAccountDetails(AllAccountDetailsRequest allAccountDetailsRequest)
        {
            AllAccountDetailsResponse allAccountDetailsResponse = getAllAccounts(allAccountDetailsRequest);


            allAccountDetailsResponse.ac_details = getSelectedAccountListByAccountCategory(allAccountDetailsResponse, "L");
            return allAccountDetailsResponse;

        }





        private Ac_Details[] getSelectedAccountListByAccountType(ref AllAccountDetailsResponse allAccountDetailsResponse, string accountType)
        {
            Ac_Details[] TDRAccountArray = null;
            List<Ac_Details> TDRAccountList = new List<Ac_Details>();

            bool accountFound = false;

            for (int i = 0; i < allAccountDetailsResponse.ac_details.Length; i++)
            {

                if (allAccountDetailsResponse.ac_details[i].AccountType.Contains(accountType) && !allAccountDetailsResponse.ac_details[i].AccountStatus.ToUpper().Contains("CLOS"))
                {
                    TDRAccountList.Add(allAccountDetailsResponse.ac_details[i]);
                    accountFound = true;
                }

            }


            if (!accountFound)
            {
                allAccountDetailsResponse.ErrorCode = "ACTYPEERROR";
                allAccountDetailsResponse.ErrorDescription = "No " + accountType + " found";
            }
            else
            {

                TDRAccountArray = TDRAccountList.ToArray();
            }


            return TDRAccountArray;

        }




        private Ac_Details[] getSelectedAccountListByAccountCategory(AllAccountDetailsResponse allAccountDetailsResponse, string accountCategory)
        {
            Ac_Details[] TDRAccountArray = null;
            List<Ac_Details> TDRAccountList = new List<Ac_Details>();
            for (int i = 0; i < allAccountDetailsResponse.ac_details.Length; i++)
            {

                if (allAccountDetailsResponse.ac_details[i].AccountCategory.Contains(accountCategory))
                {

                    TDRAccountList.Add(allAccountDetailsResponse.ac_details[i]);
                }

            }


            TDRAccountArray = TDRAccountList.ToArray();

            return TDRAccountArray;

        }




        [HttpPost]
        [Route("Enquiry/getAccountBalance")]
        public BalanceEnquiryResponse getAccountBalance(AccountBalanceRequest accountBalanceRequest)
        {
            BalanceEnquiryResponse accountBalanceResponse = new BalanceEnquiryResponse();

            try
            {

                accountBalanceResponse = _domain.getAccountBalance(accountBalanceRequest);

            }
            catch (Exception e)
            {
                accountBalanceResponse.ErrorCode = "2000";
                accountBalanceResponse.ErrorDesc = e.Message;

            }
            return accountBalanceResponse;
        }



        /// <summary>
        /// Get CIF Details with all account details
        /// </summary>
        /// <param name="cbsAccountRequest"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("Enquiry/GetAccountDetails")]
        public AccountListResponse GetAccountDetails(CBSAccountRequest cbsAccountRequest)
        {
            AccountListResponse accountBalanceResponse = new AccountListResponse();

            try
            {

                accountBalanceResponse = _domain.GetAccountDetails(cbsAccountRequest);

            }
            catch (Exception e)
            {
                accountBalanceResponse.ErrorCode = "2000";
                accountBalanceResponse.ErrorDesc = e.Message;

            }
            return accountBalanceResponse;
        }







        /// <summary>
        /// Get CIF Details with all account details
        /// </summary>
        /// <param name="cbsAccountRequest"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("Enquiry/AccountStatement")]
        public INBAccountStatementDetails AccountStatement(AccountStatementRequest accountStatementRequest)
        {
            INBAccountStatementDetails inbAccountStatementDetails = new INBAccountStatementDetails();

            try
            {

                inbAccountStatementDetails = _domain.AccountStatement(accountStatementRequest);

            }
            catch (Exception e)
            {
                inbAccountStatementDetails.ErrorCode = "2000";
                inbAccountStatementDetails.ErrorDescription = e.Message;

            }
            return inbAccountStatementDetails;
        }



        /// <summary>
        /// Get CIF Number from account Number
        /// </summary>
        /// <param name="cbsAccountRequest"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("Enquiry/getCIFNumber")]
        public CIFNumberResponse getCIFNumber(CIFNumberRequest cifNumberRequest)
        {
            CIFNumberResponse cifNumberResponse = new CIFNumberResponse();

            try
            {

                cifNumberResponse = _domain.getCIFNumber(cifNumberRequest);

            }
            catch (Exception e)
            {
                cifNumberResponse.ErrorCode = "2000";
                cifNumberResponse.ErrorDesc = e.Message;

            }
            return cifNumberResponse;
        }



        private static String getAccountCategory(String AccountType)
        {
            String accountCategory = String.Empty;

            if (AccountType == "LOAN") accountCategory = "L";
            else if (AccountType == "FD") accountCategory = "FD";
            else if (AccountType == "OD") accountCategory = "OD";
            else if (AccountType == "LOAN") accountCategory = "L";
            else accountCategory = "L";
            return accountCategory;


        }


    }
}
