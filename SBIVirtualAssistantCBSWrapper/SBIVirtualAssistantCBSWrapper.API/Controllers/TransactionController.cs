﻿using SBIVirtualAssistantCBSWrapper.Domain.Contracts;
using SBIVirtualAssistantCBSWrapper.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace SBIVirtualAssistantCBSWrapper.API.Controllers
{
    public class TransactionController : ApiController
    {

        private ISBIVirtualAssistantCBSWrapperDomain _domain;




        public TransactionController(ISBIVirtualAssistantCBSWrapperDomain domain)
        {
            this._domain = domain;
        }

        [HttpPost]
        [Route("Transaction/RTGSTransfer")]
        public RTGSResponse Post(RTGSRequest rtgsRequest)
        {
            RTGSResponse rtgsResponse = new RTGSResponse();

            try
            {
                rtgsResponse = _domain.RTGStransfer(rtgsRequest);

            }
            catch (Exception e)
            {

                string resp = e.Message;
            }

            return rtgsResponse;

        }

        [HttpPost]
        [Route("Transaction/NEFTTransfer")]
        public NEFTResponse Post(NEFTRequest neftRequest)
        {
            NEFTResponse neftResponse = new NEFTResponse();

            try
            {
                neftResponse = _domain.NEFTtransfer(neftRequest);

            }
            catch (Exception e)
            {

                string resp = e.Message;
            }

            return neftResponse;

        }


        [HttpPost]
        [Route("Transaction/SBITransfer")]
        public SBITransferResponse Post(SBITransferRequest sbiTransferRequest)
        {
            SBITransferResponse sbiResponse = new SBITransferResponse();

            try
            {
                sbiResponse = _domain.SBITransfer(sbiTransferRequest);

            }
            catch (Exception e)
            {

                string resp = e.Message;
            }

            return sbiResponse;

        }



        //[HttpPost]
        //[Route("Transaction/FDCreation")]
        //public FDCreationResponse Post(FDCreationRequest fdCreationRequest)
        //{
        //    FDCreationResponse fdCreationResponse = new FDCreationResponse();

        //    try
        //    {
        //        fdCreationResponse = _domain.FDCreation(fdCreationRequest);

        //    }
        //    catch (Exception e)
        //    {

        //        string resp = e.Message;
        //    }

        //    return fdCreationResponse;

        //}




        [HttpPost]
        [Route("Transaction/FDCreationDefault")]
        public FDCreationResponse FDCreationDefault(FDCreationBotRequest fdCreationBotRequest)
        {
            FDCreationResponse fdCreationResponse = new FDCreationResponse();

            String check = checkDefaultFDCreationRequest(fdCreationBotRequest);
            if (check != "SUCCESS" && fdCreationBotRequest.TRANSFER_ACCOUNT_NUMBER != "NNN")
            {
                fdCreationResponse.ErrorDesc = fdCreationResponse.ERROR_DESCRIPTION = check;
                fdCreationResponse.ErrorCode = "API01";
            }
            else
            {
                fdCreationResponse.ACCOUNT_NO = "30002056982";
                fdCreationResponse.BANK_CODE = "SBI";
                fdCreationResponse.BRANCH_CODE = "20985";
                fdCreationResponse.JOURNAL_NO = "12320985";
                fdCreationResponse.RESPONSE_STATUS = "SUCCESS";
                fdCreationResponse.RESP_DATE = DateTime.Now.ToString();
            }


            return fdCreationResponse;

        }




        [HttpPost]
        [Route("Transaction/FDCreation")]
        public FDCreationResponse FDCreationD(FDCreationBotRequest fdCreationBotRequest)
        {

            FDCreationRequest fdCreationRequest = GetFDCreationRequestObject(fdCreationBotRequest);

            FDCreationResponse fdCreationResponse = new FDCreationResponse();

            try
            {
                fdCreationResponse = _domain.FDCreation(fdCreationRequest);

            }
            catch (Exception e)
            {

                string resp = e.Message;
            }

            return fdCreationResponse;

        }



        private FDCreationRequest GetFDCreationRequestObject(FDCreationBotRequest fdCreationBotRequest)
        {
            FDCreationRequest fdCreationRequest = new FDCreationRequest();



            AllAccountDetailsRequest allAccountDetailsRequest = new AllAccountDetailsRequest();

            allAccountDetailsRequest.Request = fdCreationBotRequest.TRANSFER_ACCOUNT_NUMBER;


            AllAccountDetailsResponse allAccountDetailsResponse = new AllAccountDetailsResponse();

            try
            {
                allAccountDetailsResponse = _domain.AllAccountDetails(allAccountDetailsRequest);

            }

            catch (Exception e)
            {

                throw;
            }





            Ac_Details AccountDetails = null;
            List<Ac_Details> AccountList = new List<Ac_Details>();



            for (int i = 0; i < allAccountDetailsResponse.ac_details.Length; i++)
            {
                if (allAccountDetailsResponse.ac_details[i].AccountNumber.Contains(fdCreationBotRequest.TRANSFER_ACCOUNT_NUMBER))
                    AccountDetails = allAccountDetailsResponse.ac_details[i];
            }


            if (AccountDetails == null)
            {
                allAccountDetailsResponse.ErrorCode = "ACTYPEERROR";
                allAccountDetailsResponse.ErrorDescription = "No Account found";
            }
            else
            {


                fdCreationRequest.BANK_CODE = "0";
                fdCreationRequest.BRANCH_CODE = AccountDetails.HomeBranch;
                fdCreationRequest.CUSTOMER_NAME = allAccountDetailsResponse.main_ac.CIFname;
                fdCreationRequest.Language_Code = "01";
                fdCreationRequest.INTEREST_FREQUENCY = "M";
                fdCreationRequest.NATIONALITY = "IN";
                fdCreationRequest.SECURITY_INDICATOR = "Z";
                fdCreationRequest.TRANSFER_ACCOUNT_NUMBER = fdCreationBotRequest.INTEREST_PAYMENT_METHOD == "R" ? fdCreationBotRequest.TRANSFER_ACCOUNT_NUMBER : "";
                fdCreationRequest.ACCT_SGMT_CODE = "0706";
                fdCreationRequest.MATURITY_DATE = fdCreationBotRequest.MATURITY_DATE;
                fdCreationRequest.TIME_BAND = "ZZ";
                fdCreationRequest.TERM_LOCATION = "29";
                fdCreationRequest.CUSTOMER_NO = allAccountDetailsResponse.main_ac.CIFnumber;
                fdCreationRequest.TYPE = "11";
                fdCreationRequest.CUSTOMER_RISK_DOMESTIC_RISK = "ZZ";
                fdCreationRequest.CUSTOMER_CATEGORY = "1";
                fdCreationRequest.ADDRESS_1 = allAccountDetailsResponse.main_ac.Address1;
                fdCreationRequest.ADDRESS_2 = allAccountDetailsResponse.main_ac.Address2;
                fdCreationRequest.PRODUCT = "25";
                fdCreationRequest.RD_INSTL_FREQ = "0";
                fdCreationRequest.INTEREST_PAYMENT_METHOD = fdCreationBotRequest.INTEREST_PAYMENT_METHOD;
                fdCreationRequest.ACCOUNT_CROSS_BORDER_RISK = "ZZ";
                fdCreationRequest.MATURITY_INSTRUCTION = fdCreationBotRequest.MATURITY_INSTRUCTION;
                fdCreationRequest.TERM_BASIS = "D";
                fdCreationRequest.TERM_VALUE_DEPOSITED = fdCreationBotRequest.TERM_VALUE_DEPOSITED;
                fdCreationRequest.TERM_MONTHS = "";
                fdCreationRequest.MOBILISER = "RMPB5";
                fdCreationRequest.CURRENCY = "INR";
                fdCreationRequest.POST_CODE = allAccountDetailsResponse.main_ac.PinCode;
                fdCreationRequest.CROSS_BORDER_RISK = "ZZ";
                fdCreationRequest.FORM60_61 = "form 60";
                fdCreationRequest.PAN_NO = "0000000000";
                fdCreationRequest.TERM_YEARS = "";
                fdCreationRequest.RD_EXPECTED_INSTALLMENT = "0";
                fdCreationRequest.CURRENCY_CODE = "1";
                fdCreationRequest.Term_Days = "";
                fdCreationRequest.TERM_LENGTH = "0460";

            }

            return fdCreationRequest;

        }



        [HttpPost]
        [Route("Transaction/RDCreation")]
        public RDCreationResponse Post(RDCreationRequest rdCreationRequest)
        {
            RDCreationResponse rdCreationResponse = new RDCreationResponse();

            try
            {
                rdCreationResponse = _domain.RDCreation(rdCreationRequest);

            }
            catch (Exception e)
            {

                string resp = e.Message;
            }

            return rdCreationResponse;

        }


        [HttpPost]
        [Route("Transaction/RDCreationDefault")]
        public RDCreationResponse RDCreationDefault(RDCreationBotRequest rdCreationBotRequest)
        {
            RDCreationRequest rdCreationRequest = GetRDCreationRequestObject(rdCreationBotRequest);

            RDCreationResponse rdCreationResponse = new RDCreationResponse();

            try
            {
                rdCreationResponse = _domain.RDCreation(rdCreationRequest);

            }
            catch (Exception e)
            {

                string resp = e.Message;
            }



            
            String check = checkDefaultRDCreationRequest(rdCreationBotRequest);

            if (check != "SUCCESS" && rdCreationBotRequest.TRANSFER_ACCOUNT_NUMBER != "NNN")
            {
                rdCreationResponse.ErrorDesc = rdCreationResponse.ERROR_DESCRIPTION = check;
                rdCreationResponse.ErrorCode = "API01";
            }
            else
            {
                rdCreationResponse.ACCOUNT_NO = "30002056982";
                rdCreationResponse.BANK_CODE = "SBI";
                rdCreationResponse.BRANCH_CODE = "20985";
                rdCreationResponse.JOURNAL_NO = "12320985";
                rdCreationResponse.RESPONSE_STATUS = "SUCCESS";
                rdCreationResponse.RESP_DATE = DateTime.Now.ToString();
            }


            return rdCreationResponse;







            return rdCreationResponse;

        }

        private RDCreationRequest GetRDCreationRequestObject(RDCreationBotRequest rdCreationBotRequest)
        {


            RDCreationRequest rdCreationRequest = new RDCreationRequest();



            AllAccountDetailsRequest allAccountDetailsRequest = new AllAccountDetailsRequest();

            allAccountDetailsRequest.Request = rdCreationBotRequest.TRANSFER_ACCOUNT_NUMBER;


            AllAccountDetailsResponse allAccountDetailsResponse = new AllAccountDetailsResponse();

            try
            {
                allAccountDetailsResponse = _domain.AllAccountDetails(allAccountDetailsRequest);

            }

            catch (Exception e)
            {

                throw;
            }





            Ac_Details AccountDetails = null;
            List<Ac_Details> AccountList = new List<Ac_Details>();



            for (int i = 0; i < allAccountDetailsResponse.ac_details.Length; i++)
            {
                if (allAccountDetailsResponse.ac_details[i].AccountNumber.Contains(rdCreationBotRequest.TRANSFER_ACCOUNT_NUMBER))
                    AccountDetails = allAccountDetailsResponse.ac_details[i];
            }


            if (AccountDetails == null)
            {
                allAccountDetailsResponse.ErrorCode = "ACTYPEERROR";
                allAccountDetailsResponse.ErrorDescription = "No Account found";
            }
            else
            {


                rdCreationRequest.BANK_CODE = "0";
                rdCreationRequest.BRANCH_CODE = AccountDetails.HomeBranch;
                rdCreationRequest.CUSTOMER_NAME = allAccountDetailsResponse.main_ac.CIFname;
                rdCreationRequest.Language_Code = "01";
                rdCreationRequest.INTEREST_FREQUENCY = "M";
                rdCreationRequest.SECURITY_INDICATOR = "Z";
                rdCreationRequest.TRANSFER_ACCOUNT_NUMBER = rdCreationBotRequest.INTEREST_PAYMENT_METHOD == "R" ? rdCreationBotRequest.TRANSFER_ACCOUNT_NUMBER : "";
                rdCreationRequest.ACCT_SGMT_CODE = "0706";
                rdCreationRequest.MATURITY_DATE = rdCreationBotRequest.MATURITY_DATE;
                rdCreationRequest.TIME_BAND = "ZZ";
                rdCreationRequest.TERM_LOCATION = "29";
                rdCreationRequest.CUSTOMER_NO = allAccountDetailsResponse.main_ac.CIFnumber;
                rdCreationRequest.TYPE = "11";
                rdCreationRequest.CUSTOMER_RISK_DOMESTIC_RISK = "ZZ";
                rdCreationRequest.CUSTOMER_CATEGORY = "1";
                rdCreationRequest.ADDRESS_1 = allAccountDetailsResponse.main_ac.Address1;
                rdCreationRequest.ADDRESS_2 = allAccountDetailsResponse.main_ac.Address2;
                rdCreationRequest.PRODUCT = "25";
                rdCreationRequest.RD_INSTL_FREQ = "0";
                rdCreationRequest.INTEREST_PAYMENT_METHOD = rdCreationBotRequest.INTEREST_PAYMENT_METHOD;
                rdCreationRequest.ACCOUNT_CROSS_BORDER_RISK = "ZZ";
                rdCreationRequest.MATURITY_INSTRUCTION = rdCreationBotRequest.MATURITY_INSTRUCTION;
                rdCreationRequest.TERM_BASIS = "D";
                rdCreationRequest.TERM_VALUE_DEPOSITED = rdCreationBotRequest.TERM_VALUE_DEPOSITED;
                rdCreationRequest.TERM_MONTHS = "";
                rdCreationRequest.MOBILISER = "RMPB5";
                rdCreationRequest.CURRENCY = "INR";
                rdCreationRequest.POST_CODE = allAccountDetailsResponse.main_ac.PinCode;
                rdCreationRequest.CROSS_BORDER_RISK = "ZZ";
                rdCreationRequest.FORM60_61 = "form 60";
                rdCreationRequest.PAN_NO = "0000000000";
                rdCreationRequest.TERM_YEARS = "";
                rdCreationRequest.RD_EXPECTED_INSTALLMENT = "0";
                rdCreationRequest.CURRENCY_CODE = "1";
                rdCreationRequest.Term_Days = "";
                rdCreationRequest.TERM_LENGTH = "0460";

            }

            return rdCreationRequest;

        }

        [HttpPost]
        [Route("Transaction/RequestChequeBook")]
        public ChequeBookResponse Post(ChequeBookRequest chequeBookRequest)
        {
            ChequeBookResponse chequeBookResponse = new ChequeBookResponse();

            try
            {
                chequeBookResponse = _domain.RequestChequeBook(chequeBookRequest);

            }
            catch (Exception e)
            {

                string resp = e.Message;
            }

            return chequeBookResponse;

        }


        private String checkDefaultFDCreationRequest(FDCreationBotRequest fdCreationBotRequest)
        {

            String response = String.Empty;
            DateTime dateTime;

            if (!DateTime.TryParseExact(fdCreationBotRequest.MATURITY_DATE, "ddmmyyyy", null, System.Globalization.DateTimeStyles.None, out dateTime))
                response = "Invalid Maturity Date";

            else if (String.IsNullOrEmpty(fdCreationBotRequest.TRANSFER_ACCOUNT_NUMBER)) response = "Invalid Transfer Account Number";
            else if (String.IsNullOrEmpty(fdCreationBotRequest.MATURITY_DATE)) response = "Invalid Maturity Date";
            else if (String.IsNullOrEmpty(fdCreationBotRequest.INTEREST_PAYMENT_METHOD)) response = "Invalid Interest payment Method";
            else if (String.IsNullOrEmpty(fdCreationBotRequest.MATURITY_INSTRUCTION)) response = "Invalid Maturity Instruction";
            else if (String.IsNullOrEmpty(fdCreationBotRequest.TERM_VALUE_DEPOSITED)) response = "Invalid Term Value deposited";
            else response = "SUCCESS";



            return response;

        }


        private String checkDefaultRDCreationRequest(RDCreationBotRequest rdCreationBotRequest)
        {

            String response = String.Empty;
            DateTime dateTime;

            if (!DateTime.TryParseExact(rdCreationBotRequest.MATURITY_DATE, "ddmmyyyy", null, System.Globalization.DateTimeStyles.None, out dateTime))
                response = "Invalid Maturity Date";

            else if (String.IsNullOrEmpty(rdCreationBotRequest.TRANSFER_ACCOUNT_NUMBER)) response = "Invalid Transfer Account Number";
            else if (String.IsNullOrEmpty(rdCreationBotRequest.MATURITY_DATE)) response = "Invalid Maturity Date";
            else if (String.IsNullOrEmpty(rdCreationBotRequest.INTEREST_PAYMENT_METHOD)) response = "Invalid Interest payment Method";
            else if (String.IsNullOrEmpty(rdCreationBotRequest.MATURITY_INSTRUCTION)) response = "Invalid Maturity Instruction";
            else if (String.IsNullOrEmpty(rdCreationBotRequest.TERM_VALUE_DEPOSITED)) response = "Invalid Term Value deposited";
            else response = "SUCCESS";



            return response;

        }

    }
}
