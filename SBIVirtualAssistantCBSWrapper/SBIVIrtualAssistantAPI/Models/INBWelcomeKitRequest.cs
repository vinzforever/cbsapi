﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LotusWebAPI.Models
{
    public class INBWelcomeKitRequest
    {
        public string REQUEST_REF_NO { get; set; }
        public string BRANCH_CODE { get; set; }
        public string BANK_CODE { get; set; }
        public string CIF_NUMBER { get; set; }
        public string NAME { get; set; }
        public string CUSTOMER_SHORT_NAME { get; set; }
        public string INCOME_TAX_BASIS { get; set; }
        public string INCOME_TAX_BRANCH { get; set; }
        public string COMMUNE_CODE { get; set; }
        public string SCOPE_OF_FIRM { get; set; }
        public string E_MAIL_ADDRESS_1 { get; set; }
        public string E_MAIL_ADDRESS_2 { get; set; }
        public string CREDITOR_CODE { get; set; }
        public string EDUCATION_CODE { get; set; }
        public string ACCESS_CHANNEL_11 { get; set; }
        public string ACCESS_CHANNEL_12 { get; set; }
        public string ACCESS_CHANNEL_13 { get; set; }
        public string ACCESS_CHANNEL_14 { get; set; }
        public string ACCESS_CHANNEL_15 { get; set; }
        public string ACCESS_CHANNEL_16 { get; set; }
        public string ACCESS_CHANNEL_17 { get; set; }
        public string ACCESS_CHANNEL_18 { get; set; }
        public string ACCESS_CHANNEL_19 { get; set; }
        public string ACCESS_CHANNEL_20 { get; set; }
        public string ACCESS_CHANNEL_21 { get; set; }
        public string ACCESS_CHANNEL_22 { get; set; }
        public string ACCESS_CHANNEL_23 { get; set; }
        public string ACCESS_CHANNEL_24 { get; set; }
        public string ACCESS_CHANNEL_25 { get; set; }
        public string ACCESS_CHANNEL_26 { get; set; }
        public string ACCESS_CHANNEL_27 { get; set; }
        public string ACCESS_CHANNEL_28 { get; set; }
        public string ACCESS_CHANNEL_29 { get; set; }
        public string ACCESS_CHANNEL_30 { get; set; }
        public string STATEMENT_FREQUENCY { get; set; }
        public string CYCLE { get; set; }
        public string DAY { get; set; }
        public string DELIVERY_THROUGH_E_MAIL { get; set; }
        public string VILLAGE_CODE { get; set; }
        public string INTERNET_BANKING_KIT_NUMBER { get; set; }
        public string ADDRESS_1 { get; set; }
        public string ADDRESS_2 { get; set; }
        public string ADDRESS_3 { get; set; }
        public string ADDRESS_4 { get; set; }
        public string STATE_NAME { get; set; }
        public string CITY_NAME { get; set; }
        public string COUNTRY_NAME { get; set; }
        public string POST_CODE { get; set; }
        public string PHONE_HOME { get; set; }
        public string PHONE_BUSINESS { get; set; }
        public string MOBILE_NUMBER { get; set; } 
        public string RESIDENT_STATUS { get; set; }
        public string CUSTOMER_TYPE { get; set; }            
    }

    public class INBWelcomeKitInherit : INBWelcomeKitRequest
    {
        public string REQUEST_CODE = "CBS.CE020";

        public string REQUEST_AUTH_ID = "0";

        public string REQUEST_TELLER_ID = "0";

        public string SOURCE_ID = "SBISI";

        public string REQUEST_DATE_TIME = DateTime.Now.ToString("yyyy-MM-ddThh:mm:ss");
    }
}