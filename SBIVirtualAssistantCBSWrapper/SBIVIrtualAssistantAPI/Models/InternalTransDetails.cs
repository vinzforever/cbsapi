﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LotusWebAPI.Models
{
    public class InternalTransDetails
    {
        public string SI_Reference_No { get; set; }
        public string Input { get; set; }
        public string Output { get; set; }

        public InternalTransDetails(string SIReferenceNo,string input,string output)
        {
            SI_Reference_No = SIReferenceNo;
            Input = input;
            Output = output;
        }

        public string TostringContent()
        {
            return Newtonsoft.Json.JsonConvert.SerializeObject(this);
        }

    }
}