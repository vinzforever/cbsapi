﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LotusWebAPI.Models
{
    public class LoanAccountStatementRequest
    {
        public string ReferenceNumber { get; set; } 
        public string BankCode { get; set; }
        public string AccountNumber { get; set; }
        public string TransactionFlag { get; set; }
        //public string FromDate { get; set; }
        //public string ToDate { get; set; }
    }
}