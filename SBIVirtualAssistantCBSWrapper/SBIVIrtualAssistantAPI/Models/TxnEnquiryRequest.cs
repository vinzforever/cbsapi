﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LotusWebAPI.Models
{
    public class TxnEnquiryRequest
    {
        public string ReferenceNumber { get; set; }
        public int BankCode { get; set; }
        public string CBSReferenceNumber { get; set; }

        public string TostringContent()
        {
            return Newtonsoft.Json.JsonConvert.SerializeObject(this);
        }
    }
}