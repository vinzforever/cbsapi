﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LotusWebAPI.Models
{
    public class RDPreMatureClosureRequest
    {
        public string REQUEST_REF_NO { get; set; }

        public string BANK_CODE { get; set; }
        public string BRANCH_CODE { get; set; }

        public string ACCOUNT_NO { get; set; }

    }

    public class RDPreMatureClosureRequest_Inherit : RDPreMatureClosureRequest
    {

        public string REQUEST_CODE = "CBS.IF007";

        public string REQUEST_DATE_TIME = DateTime.Now.ToString("yyyy-MM-ddThh:mm:ss");

        public string SOURCE_ID = "SBISI";

        public string REQUEST_TELLER_ID = "0";

        public string REQUEST_AUTH_ID = "0";
    }
}