﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace LotusWebAPI.Models
{
    public class FDCreationRequest
    {
       
        public string REQUEST_REF_NO { get; set; }
     public string BANK_CODE { get; set; }
        public string BRANCH_CODE { get; set; }
     public string CUSTOMER_NAME{ get; set; }
        public string Language_Code{ get; set; }
        public string INTEREST_FREQUENCY{ get; set; }
        public string NATIONALITY{ get; set; }
        public string SECURITY_INDICATOR{ get; set; }
        public string TRANSFER_ACCOUNT_NUMBER{ get; set; }
        public string ACCT_SGMT_CODE{ get; set; }
        public string MATURITY_DATE{ get; set; }
        public string TIME_BAND{ get; set; }
        public string TERM_LOCATION{ get; set; }
        public string CUSTOMER_NO{ get; set; }
        public string TYPE{ get; set; }
        public string CUSTOMER_RISK_DOMESTIC_RISK{ get; set; }
        public string CUSTOMER_CATEGORY{ get; set; }
        public string ADDRESS_1{ get; set; }
        public string ADDRESS_2{ get; set; }
        public string PRODUCT{ get; set; }
        public string RD_INSTL_FREQ{ get; set; }
        public string INTEREST_PAYMENT_METHOD{ get; set; }
        public string ACCOUNT_CROSS_BORDER_RISK{ get; set; }
        public string MATURITY_INSTRUCTION{ get; set; }
        public string TERM_BASIS{ get; set; }
        public string TERM_VALUE_DEPOSITED{ get; set; }
        public string TERM_MONTHS{ get; set; }
        public string MOBILISER{ get; set; }
        public string CURRENCY{ get; set; }
        public string POST_CODE{ get; set; }
        public string CROSS_BORDER_RISK{ get; set; }
        public string FORM60_61{ get; set; }
        public string PAN_NO{ get; set; }
        public string TERM_YEARS{ get; set; }
        public string RD_EXPECTED_INSTALLMENT{ get; set; }
        public string CURRENCY_CODE{ get; set; }
        public string Term_Days{ get; set; }
        public string TERM_LENGTH{ get; set; }

    }

    public class FDCreationRequest_Inherit :FDCreationRequest
    {
       
        public string REQUEST_CODE = "CBS.AO001";
       
        public string REQUEST_DATE_TIME = DateTime.Now.ToString("yyyy-MM-ddThh:mm:ss");
       
        public string SOURCE_ID = "SBISI";
       
        public string REQUEST_TELLER_ID = "0";
       
        public string REQUEST_AUTH_ID = "0";
    }
}