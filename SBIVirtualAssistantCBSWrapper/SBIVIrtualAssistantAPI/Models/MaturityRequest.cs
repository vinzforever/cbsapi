﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace LotusWebAPI.Models
{
    public class MaturityRequest
    {
      
        public string BRANCH_CODE { get; set; }
        public string BANK_CODE { get; set; }
     
        public string REQUEST_REF_NO { get; set; }
      
        public string TERM_LENGTH { get; set; }
        public string DAYS { get; set; }
        public string INTEREST_FREQUENCY { get; set; }
        public string PRODUCT { get; set; }
        public string INTEREST_CATEGORY { get; set; }
        public string INTEREST_PAYMENT_METHOD { get; set; }
        public string MONTHS { get; set; }
        public string AMOUNT { get; set; }
        public string RECURRING_FREQUENCY { get; set; }
        public string TERM_BASIS { get; set; }
        public string STARTDATE { get; set; }
        public string MATURITY_DATE { get; set; }
        public string YEARS { get; set; }
        public string RECCURING_AMOUNT { get; set; }
        public string TYPE { get; set; }

    }

    public class MaturityRequest_Inherit : MaturityRequest
    {
       
        public string REQUEST_AUTH_ID = "0";
       
        public string REQUEST_TELLER_ID = "0";
       
        public string SOURCE_ID = "SBISI";
       
        public string REQUEST_DATE_TIME = DateTime.Now.ToString("yyyy-MM-ddThh:mm:ss");
       
        public string REQUEST_CODE = "CBS.AO002";
    }
}