﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LotusWebAPI.Models
{
    public class WelcomeKitAccountNumberWithCheckResponse
    {
        public String ReferenceNumber { get; set; }
        public string ACCOUNT_NUMBER_WITH_CHECKDIGIT { get; set; }
        public string ERROR_DESCRIPTION { get; set; }
    }
}