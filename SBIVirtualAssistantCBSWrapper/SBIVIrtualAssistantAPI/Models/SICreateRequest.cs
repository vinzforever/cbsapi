﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace LotusWebAPI.Models
{
    public class SICreateRequest
    {
      
        public string REQUEST_REF_NO { get; set; }
       
        public string BANK_CODE { get; set; }
        public string BRANCH_CODE { get; set; }
    
        public string Account_Number { get; set; }
        public string Amount { get; set; }
        public string Frequency { get; set; }
        public string Frequency_Code { get; set; }
        public string To_Account { get; set; }
        public string From_Account_Comments { get; set; }
        public string Start_Date { get; set; }
        public string Currency { get; set; }
        public string Security_Code { get; set; }
        public string Priority_Code { get; set; }
        public string To_Account_Comments { get; set; }
        public string System { get; set; }
        public string End_Date { get; set; }
        public string Auto_Chase_Days { get; set; }
        public string Payment_Type { get; set; }
        public string Purpose { get; set; }
        public string Hold_Required { get; set; }
        public string Apply_Fees { get; set; }
        public string Purpose_Of_SI { get; set; }
        public string Lon_Chase_Days { get; set; }
        public string Other_Chase_Days { get; set; }
    }

    public class SICreateRequest_Inherit : SICreateRequest
    {
       
        public string REQUEST_CODE = "CBS000900";
       
        public string REQUEST_DATE_TIME = DateTime.Now.ToString("yyyy-MM-ddThh:mm:ss");
       
        public string SOURCE_ID = "SBISI";
       
        public string REQUEST_TELLER_ID = "0";
       
        public string REQUEST_AUTH_ID = "0";
    }
}