﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LotusWebAPI.Models
{
    public class CustomerDetails_Account
    {
        public string ReferenceNumber { get; set; }
        public string ErrorDesc { get; set; }
        public string ErrorCode { get; set; }
        public string CIF { get; set; }

        public string CIFNumber { get; set; }
        public string CustomerType { get; set; }
        public string Salutation { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string RelativeIndicator { get; set; }
        public string RelativeName { get; set; }
        public string DateOFBirth { get; set; }
        public string PANNumber { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string AddressLine3 { get; set; }
        public string State { get; set; }
        public string District { get; set; }
        public string SubDistrict { get; set; }
        public string City { get; set; }
        public string PinCode { get; set; }
        public string MobileNumber { get; set; }
        public string LandLineNumber { get; set; }
        public string UIDNumber { get; set; }
        public string IDType { get; set; }
        public string IDNumber { get; set; }
        public string IDIssuedAt { get; set; }
        public string IDIssuedDate { get; set; }
        public string PhoneBusiness { get; set; }
        public string Nationality { get; set; }
        public string VIPCode { get; set; }
        public string Status { get; set; }
        public string HomeBranch { get; set; }
        public string Country { get; set; }
        public string FatherName { get; set; }
        public string MotherName { get; set; }
        public string DomesticCountry { get; set; }
        public string ResidentialStatus { get; set; }
        public string GenderCode { get; set; }
        public string MaritalStatus { get; set; }
        public string VISAIssuedBy { get; set; }
        public string VISAIssueDate { get; set; }
        public string branchcode { get; set; }
        public string VISAExpiryDate { get; set; }
        public string VISADetails { get; set; }
        public string NPATrade { get; set; }
        public string NPANonTrade { get; set; }
        public string Email1 { get; set; }
        public string Email2 { get; set; }
        public string VillageCode { get; set; }
        public string PersBanker { get; set; }
        public string BusinessCustomerName { get; set; }
        public string TANNumber { get; set; }
        public string FAXNumber { get; set; }
        public string Occupancy { get; set; }
        public string DomesticRisk { get; set; }
        public string CrossBorderRisk { get; set; }
        public string LockerHolder { get; set; }
        public string TFNIndicator { get; set; }
        public string CustomerLimitIndicator { get; set; }
        public string CustomerPALimitIndicator { get; set; }
        public string IndustryLimitIndicator { get; set; }
        public string SectorLimitIndicator { get; set; }
        public string IndustryCode { get; set; }
        public string CountryRisk { get; set; }
        public string GroupCode { get; set; }
        public string BusinessSectorCode { get; set; }
        public string CISOrganisationCode { get; set; }
        public string OrganisationCode { get; set; }
        public string DateEstablish { get; set; }
        public string DateCommencement { get; set; }
        public string DateLiquiditation { get; set; }
        public string DatePaymentReturn { get; set; }
        public string TradeCustomer { get; set; }
        public string VoterID { get; set; }
        public string IDRemark { get; set; }
        public string CustomerEvolutionFlag { get; set; }
        public string AccountMgmtGroupID { get; set; }
        public string Threshold { get; set; }
        public string CustomerRisk { get; set; }
        public string FPWMFlag { get; set; }
        public string PFNumber { get; set; }
        public string KYCUpdateDAte { get; set; }
        public string RiskUpdateDate { get; set; }
        public string VisualFlag { get; set; }
        public string VISAFlag { get; set; }
        public string WealthFlag { get; set; } 
    }
}