﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LotusWebAPI.Models
{
    public class ATMPinValidationResponse
    {
        public string ReferenceNumber { get; set; }
        public string Response { get; set; }
        public string ERROR_DESCRIPTION { get; set; }
    }
}