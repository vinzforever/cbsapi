﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace LotusWebAPI.Models
{
     
    public class CardIssuanceRequest
    {
     
        public string ACCOUNT_NUMBER { get; set; }
      
        public string NAME_ON_CARD { get; set; }
       
      
        public string REFERENCE_NUMBER;
   
        public string CARD_TYPE { get; set; }
    }

    public class CardIssuanceRequestInhrt
    {

        public string ACCOUNT_NUMBER { get; set; }

        public string NAME_ON_CARD { get; set; }
 

        public string CARD_TYPE { get; set; }
    }

}