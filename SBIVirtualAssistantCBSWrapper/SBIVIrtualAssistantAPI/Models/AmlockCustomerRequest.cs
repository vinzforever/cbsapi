﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LotusWebAPI.Models
{
    public class AmlockCustomerRequest
    {
        public string REQUEST_REF_NO { get; set; }
        public string BranchCode { get; set; }
        public string TellerID { get; set; }
        public string Name { get; set; }
    }

    public class AmlockCustomerRequestInherit
    {
        public string NAME { get; set; }
        public string BRANCHCODE { get; set; }
        public string TELLERID { get; set; }
       
    }
}