﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LotusWebAPI.Models
{
    public class AmlockDetailsUpdateResponse
    {
        public string REFERENCE_NUMBER { get; set; }
        public string RESPONSECODE{ get; set; }
        public string RESPONSEDESCRIPTION{ get; set; }
        public string REQUESTID { get; set; }
        public string ERRORDESCRIPTION { get; set; }
        public string ERRORCODE { get; set; }
    }
}