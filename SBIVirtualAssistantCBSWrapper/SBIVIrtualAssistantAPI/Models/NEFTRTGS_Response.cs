﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LotusWebAPI.Models
{
    public class NEFTRTGS_Response
    {
        public string ReferenceNumber { get; set; }
        public string JournalNumber { get; set; }
        public string UTRno { get; set; }
        public string DateOfTransaction { get; set; }
        public string ErrorDesc { get; set; }
        public string CBSReferenceNumber { get; set; }
        public string ErrorCode { get; set; }

        public string TostringContent()
        {
            return Newtonsoft.Json.JsonConvert.SerializeObject(this);
        }
    }
}