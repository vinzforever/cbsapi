﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LotusWebAPI.Models
{
    public class Cif_MobilePanRequest
    {
        public string ReferenceNumber { get; set; }
        public String RequestData { get; set; }
        public int Bank_Code { get; set; }
        public string RequestType { get; set; }
    }
}