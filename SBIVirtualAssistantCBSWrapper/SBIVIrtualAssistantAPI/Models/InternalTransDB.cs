﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LotusWebAPI.Models
{
    public class InternalTransDB
    {
        public string Channel_Name { get; set; }
        public string SI_Reference_No { get; set; }
        public string Reference_No { get; set; }
        public string Req_Time { get; set; }
        public string Res_Time { get; set; }
        public string Error_Code { get; set; }
        public string Error_Desc { get; set; }
        public string Method_Name { get; set; }
        public DateTime ReqTime { get; set; }
        public DateTime ResTime { get; set; }

        public InternalTransDB(string ChannelName, string ReferenceNo, string SIReferenceNo, DateTime ReqTime, DateTime ResTime, string ErrorCode, string ErrorDesc, string MethodName)
        {
            Channel_Name = ChannelName;
            Reference_No = ReferenceNo;
            SI_Reference_No = SIReferenceNo;
            Req_Time = ReqTime.ToString("yyyy-MM-dd HH:mm:ss");
            Res_Time = ResTime.ToString("yyyy-MM-dd HH:mm:ss");
            Error_Code = ErrorCode;
            Error_Desc = ErrorDesc;
            Method_Name = MethodName;
        }

        public string TostringContent()
        {
            return Newtonsoft.Json.JsonConvert.SerializeObject(this);
        }

    }
}