﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LotusWebAPI.Models
{
    public class CustomerMiscAmmendmentRequest
    {
        public string BRANCH_CODE { get; set; }
        public string REQUEST_REF_NO { get; set; }
        public string CUSTOMER_NO { get; set; }
        public string CUSTOMER_NAME { get; set; }
        public string CUSTOMER_SHORT_NAME { get; set; }
        public string INCOME_TAX_BASIS { get; set; }
        public string INCOME_TAX_BRANCH { get; set; }
        public string COMMUNICATION_CODE { get; set; }
        public string SCOPE_OF_FIRM { get; set; }
        public string EMAIL_ID1 { get; set; }
        public string EMAIL_ID2 { get; set; }
        public string CREDITOR_CODE { get; set; }
        public string EDUCATION_CODE { get; set; }
        public string ACCESS_CHANNEL1 { get; set; }
        public string ACCESS_CHANNEL2 { get; set; }
        public string ACCESS_CHANNEL3 { get; set; }
        public string ACCESS_CHANNEL4 { get; set; }
        public string ACCESS_CHANNEL5 { get; set; }
        public string ACCESS_CHANNEL6 { get; set; }
        public string ACCESS_CHANNEL7 { get; set; }
        public string ACCESS_CHANNEL8 { get; set; }
        public string ACCESS_CHANNEL9 { get; set; }
        public string ACCESS_CHANNEL10 { get; set; }
        public string ACCESS_CHANNEL11 { get; set; }
        public string ACCESS_CHANNEL12 { get; set; }
        public string ACCESS_CHANNEL13 { get; set; }
        public string ACCESS_CHANNEL14 { get; set; }
        public string ACCESS_CHANNEL15 { get; set; }
        public string ACCESS_CHANNEL16 { get; set; }
        public string ACCESS_CHANNEL17 { get; set; }
        public string ACCESS_CHANNEL18 { get; set; }
        public string ACCESS_CHANNEL19 { get; set; }
        public string ACCESS_CHANNEL20 { get; set; }
        public string STATEMENT_REQ_FLAG { get; set; }
        public string STATEMENT_CYCLE { get; set; }
        public string STATEMENT_DAY { get; set; }
        public string MODE_OF_DELIVERY { get; set; }
        public string VILLAGE_CODE { get; set; }
        public string INB_KIT_NO { get; set; }
        public string CUSTOMER_ADD1 { get; set; }
        public string CUSTOMER_ADD2 { get; set; }
        public string CUSTOMER_ADD3 { get; set; }
        public string CUSTOMER_ADD4 { get; set; }
        public string STATE_CODE { get; set; }
        public string CITY_CODE { get; set; }
        public string COUNTRY_CODE { get; set; }
        public string POSTAL_CODE { get; set; }
        public string HOME_PHONE_NO { get; set; }
        public string BUSINESS_PHONE_NO { get; set; }
        public string MOBILE_NO { get; set; }
        public string CUSTOMER_TYPE { get; set; }
        public string REQUEST_TELLER_ID { get; set; }
    }

    public class CustomerMiscAmmendmentRequestInherit : CustomerMiscAmmendmentRequest
    {
        public string REQUEST_AUTH_ID = "0000000";       

        public string BANK_CODE = "0";
        public string SOURCE_ID = "PSG";

        public string REQUEST_DATE_TIME = DateTime.Now.ToString("yyyy-MM-ddThh:mm:ss");

        public string REQUEST_CODE = "CBS.CM012";

    }
}