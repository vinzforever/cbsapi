﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LotusWebAPI.Models
{
    public class CustomerGeneralAmmendmentResponse
    {
        public string RESPONSE_STATUS { get; set; }

        public string REQUEST_REF_NO { get; set; }


        public string JOURNAL_NUMBER { get; set; }
        public string ERROR_DESCRIPTION { get; set; }
    }
}