﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace LotusWebAPI.Models
{
    
    public class AmlockDetailsUpdateRequest
    {
       
        public string REFERENCE_NUMBER { get; set; }
        public string ACTION { get; set; }
        public string CIF { get; set; }
        public string REQUESTID { get; set; }
        public string MATCHED { get; set; }
        public string BRANCHCODE { get; set; }
      
      
        public string LISTINFO { get; set; }
    }
    public class AmlockDetailsUpdateRequestInhrt  

    {
        public string ACTION { get; set; }
        public string CIF { get; set; }
        public string REQUESTID { get; set; }
        public string MATCHED { get; set; }
        public string BRANCHCODE { get; set; }
        
       
        public string LISTINFORMATION { get; set; }
        public string MAKERID = "0";
        public string CHECKERID = "0";
        public string SCREENNUMBER = "064554";
        
    }
}