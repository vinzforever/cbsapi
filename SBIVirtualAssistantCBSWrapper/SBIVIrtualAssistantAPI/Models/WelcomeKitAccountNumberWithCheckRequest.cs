﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LotusWebAPI.Models
{
    public class WelcomeKitAccountNumberWithCheckRequest
    {
        public string REQUEST_REF_NO { get; set; }
        public string ACCOUNT_NUMBER { get; set; }
        public string  BRANCH_CODE { get; set; }
        public string BANK_CODE { get; set; }
        public string REQUEST_TELLER_ID { get; set; }
    }

    public class WelcomeKitAccountNumberWithCheckRequest_Inherit : WelcomeKitAccountNumberWithCheckRequest
    {

        public string REQUEST_CODE = "CBS.AE050";

        public string REQUEST_AUTH_ID = "0000000";

       

        public string SOURCE_ID = "PSG";

        public string REQUEST_DATE_TIME = DateTime.Now.ToString("yyyy-MM-ddThh:mm:ss");
    }
}