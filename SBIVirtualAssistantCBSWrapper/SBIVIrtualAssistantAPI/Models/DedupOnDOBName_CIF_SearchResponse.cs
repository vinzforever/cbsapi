﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LotusWebAPI.Models
{
    public class DedupOnDOBName_CIF_SearchResponse
    {             
        public string RESPONSE_STATUS { get; set; }
             
        public string BRANCH_CODE { get; set; }
        public string BANK_CODE { get; set; }
        public string REQUEST_REF_NO { get; set; }
        public string RESP_DATE { get; set; }

        public List<CIF_DETAILS> DETAILS { get; set; }
        public string ERROR_DESCRIPTION { get; set; }
    }

    public class CIF_DETAILS
    {
         public string CIF { get; set; }
         public string FIRST_NAME { get; set; }
         public string LAST_NAME { get; set; }
         public string FATHER_NAME { get; set; }
         public string PINCODE { get; set; }
         public string PAN_ID { get; set; }
         public string UID_ID { get; set; }
         public string PASS_ID { get; set; }
         public string VOTER_ID { get; set; }
         public string DRIVING_ID { get; set; }
         public string NREGA_ID { get; set; }
         public string PPO_ID { get; set; }
         public string MOBILE_NO { get; set; }

    }
}