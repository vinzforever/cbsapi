﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LotusWebAPI.Models
{
    public class FDPreMatureClosureResponse
    {
        public string RESPONSE_STATUS { get; set; }

        public string BRANCH_CODE { get; set; }
        public string BANK_CODE { get; set; }
        public string REQUEST_REF_NO { get; set; }
        public string RESP_DATE { get; set; }


        public string ACCOUNT_NO { get; set; }
        public string BREAK_OUT_VALUE { get; set; }
        public string PAYABLE_AMOUNT { get; set; }
        public string PENALTY_AMOUNT { get; set; }
        public string EFECTIVE_INTEREST_RATE { get; set; }
        public string NUMBER_OF_TIMES_TDR_ADVICE_PRINTED { get; set; }
        public string DATE_OF_CLOSING { get; set; }

        public string ERROR_DESCRIPTION { get; set; }
    }
}