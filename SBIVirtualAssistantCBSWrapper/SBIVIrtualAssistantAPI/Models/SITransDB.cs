﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LotusWebAPI.Models
{
    public class SITransDB
    {
        public string Reference_No { get; set; }
        public string Product_Code { get; set; }
        public DateTime Req_Time { get; set; }
        public string Res_Time { get; set; }
        public string Error_Code { get; set; }
        public string Error_Desc { get; set; }
        public string Function_Name { get; set; }
        public string Req_Ip { get; set; }

        public string TostringContent()
        {
            return Newtonsoft.Json.JsonConvert.SerializeObject(this);
        }

        public SITransDB(string ReferenceNo, string Productcode, DateTime ReqTime, string ResTime, string ErrorCode, string ErrorDesc, string FunctionName, string ReqIP)
        {
            Reference_No = ReferenceNo;
            Product_Code = Productcode;
            Req_Time = ReqTime;
            Res_Time = ResTime;
            Error_Code = ErrorCode;
            Error_Desc = ErrorDesc;
            Function_Name = FunctionName;
            Req_Ip = ReqIP;
        }
    }
}