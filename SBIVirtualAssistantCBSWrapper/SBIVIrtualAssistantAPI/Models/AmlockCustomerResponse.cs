﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LotusWebAPI.Models
{
    public class AmlockCustomerResponse
    {
        public string Reference_Number { get; set; }
        public string RESPONSECODE{ get; set; }
        public string RESPONSE_DESCRIPTION { get; set; }
        public string REQUESTID { get; set; }
        public string LISTINFORMATION { get; set; }
    }
}