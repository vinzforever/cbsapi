﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace LotusWebAPI.Models
{
    public class SICancelRequest
    {
       
        public string REQUEST_REF_NO { get; set; }
       
        public string BANK_CODE { get; set; }
        public string BRANCH_CODE { get; set; }
       
        public string FromOrOwner_Account_Number { get; set; }
        public string Amount { get; set; }
        public string Frequency { get; set; }
        public string Frequency_Code { get; set; }
        public string Order_Date { get; set; }
        public string To_Account { get; set; }
        public string From_Account_Comments { get; set; }
        public string Start_Date { get; set; }
        public string Currency { get; set; }
        public string To_Account_Comments { get; set; }
        public string System { get; set; }
        public string End_Date { get; set; }
        public string Record { get; set; }
        public string SDV_Account_Number { get; set; }
        public string Receipt_Number { get; set; }
        public string Purpose_Of_SI { get; set; }
        public string Reason_Code { get; set; }
    }

    public class SICancelRequest_Inherit : SICancelRequest
    {
       
        public string REQUEST_CODE = "CBS000970";
       
        public string REQUEST_DATE_TIME = DateTime.Now.ToString("yyyy-MM-ddThh:mm:ss");
       
        public string SOURCE_ID = "SBISI";
       
        public string REQUEST_TELLER_ID = "0";
       
        public string REQUEST_AUTH_ID = "0";
    }
}