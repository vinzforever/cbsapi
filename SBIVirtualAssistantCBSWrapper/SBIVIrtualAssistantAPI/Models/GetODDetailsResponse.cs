﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LotusWebAPI.Models
{
    public class GetODDetailsResponse
    {

        public string ReferenceNumber { get; set; }

        public string AccountNumber { get; set; }        
        public string BookBalance { get; set; }
        public string LimitAmount { get; set; }
        public string DrawingPower { get; set; }
        public string AvailableBalance { get; set; }

        public string ERROR_DESC { get; set; }
        
        public string ERROR_CODE { get; set; }
    }
}