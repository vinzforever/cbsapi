﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LotusWebAPI.Models
{
    public class ReferenceEnquiryRequest
    {
        public string ReferenceNumber { get; set; }
        public string Function_Name { get; set; }

    }
}