﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace LotusWebAPI.Models
{
    public class AccountListResponse
    {        
        public string ReferenceNumber { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Address3 { get; set; }
        public string Address4 { get; set; }
        public string CIFname { get; set; }
        public string CIFnumber { get; set; }
        public string EmailID { get; set; }
        public string MobileNumber { get; set; }
        public string PFId { get; set; }
        public string PinCode { get; set; }
        public string SomeDate { get; set; }
        public string ErrorDesc { get; set; }
        public string ErrorCode { get; set; }
        public List<AccountDetail> AccDetailsList { get; set; }
    }

    public class AccountDetail
    {
        public string AccountCategory { get; set; }
        public string AccountNumber { get; set; }
        public string AccountOpeningDate { get; set; }
        public string AccountStatus { get; set; }
        public string AccountType { get; set; }
        public string AccountTypeCode { get; set; }
        public string ApprovedSanctionedAmount { get; set; }
        public string AvailableBalance { get; set; }
        public string Currency { get; set; }
        public string DPAvailable { get; set; }
        public string HomeBranch { get; set; }
        public string IntCategory { get; set; }
        public string InterestRate { get; set; }
        public string Link { get; set; }
        public string MaturityAmount { get; set; }
        public string MaturityDate { get; set; }
        public string PrincipleAmount { get; set; }
        public string TermPeriod { get; set; }
        public string TotalBalance { get; set; }
       
    }
}