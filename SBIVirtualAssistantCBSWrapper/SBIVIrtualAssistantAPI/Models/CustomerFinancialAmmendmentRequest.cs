﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LotusWebAPI.Models
{
    public class CustomerFinancialAmmendmentRequest
    {
        public string REQUEST_REF_NO{ get; set; }


        public string NAME { get; set; }
        public string BRANCH_CODE { get; set; }
        public string CIF_NUMBER { get; set; }
        public string INCOME_AMOUNT { get; set; }
        public string FREQUENCY { get; set; }
        public string RENTAL_INCOME { get; set; }
        public string FREQUENCY1 { get; set; }
        public string OTHER_INCOME { get; set; }
        public string FREQUENCY2 { get; set; }
        public string MORTGAGES { get; set; }
        public string BALANCE_OUTSTANDING1 { get; set; }
        public string MORTGAGE_REPAY { get; set; }
        public string FREQUENCY3 { get; set; }
        public string FREQUENCY4 { get; set; }
        public string INSURER { get; set; }
        public string INSURER_PREM { get; set; }
        public string FREQUENCY5 { get; set; }
        public string LIVING_EXPENSE { get; set; }
        public string FREQUENCY6 { get; set; }
        public string OTHER_EXPENSE { get; set; }
        public string FREQUENCY7 { get; set; }
        public string VALUE_OF_HOME { get; set; }
        public string EXTENDED_DEPOSITS { get; set; }
        public string EXT_INST { get; set; }
        public string CREDIT_RATING { get; set; }
        public string OTHER_ASSETS { get; set; }
        public string OTHER_CODE { get; set; }
        public string DIVIDENT_INTEREST { get; set; }
        public string FREQUENCY8 { get; set; }
        public string OTHER_LOAN { get; set; }
        public string FREQUENCY9 { get; set; }
        public string OTHER_LOANS { get; set; }
        public string BALANCE_OUTSTANDING2 { get; set; }
        public string OTHER_LIABILITIES { get; set; }
        public string BALANCE_OUTSTANDING3 { get; set; }
        public string TOTAL_LIABILITIES { get; set; }
        public string BALANCE_OUTSTANDING4 { get; set; }
        public string GUARANTEES { get; set; }
        public string BALANCE_OUTSTANDING5 { get; set; }
        public string CASH_BANK_BALANCE { get; set; }
        public string JEWELLERY { get; set; }
        public string QUOTED_SHARES_DEBENTURES { get; set; }
        public string TOTAL_ASSETS { get; set; }
        public string NET_WORTH { get; set; }
        public string SOURCE_OF_FUND { get; set; }
        public string ANNUAL_INCOME { get; set; }
        public string OPINION_DATE { get; set; }
        public string ASSET_LIABILITY_STATEMENT_DATE { get; set; }
    }

    public class CustomerFinancialAmmendmentRequestInherit : CustomerFinancialAmmendmentRequest
    {
        public string REQUEST_AUTH_ID = "0";

        public string REQUEST_TELLER_ID = "0";

        public string SOURCE_ID = "SBISI";

        public string REQUEST_DATE_TIME = DateTime.Now.ToString("yyyy-MM-ddThh:mm:ss");


        public string REQUEST_CODE = "CBS.CM024";
    }
}