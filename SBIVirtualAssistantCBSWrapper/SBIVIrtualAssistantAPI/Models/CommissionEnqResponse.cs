﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LotusWebAPI.Models
{
    public class CommissionEnqResponse
    {
        public string ReferenceNumber { get; set; }
        public string Error_desc { get; set; }
        public string Commission { get; set; }
    }
}