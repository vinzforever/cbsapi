﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LotusWebAPI.Models
{
    public class NEFTRTGS_Request
    {  
        public string BranchCode { get; set; }
        public string ReferenceNumber { get; set; }

        public int BankCode { get; set; }
        public string EMAILID { get; set; }
        public string MOBNUMBER { get; set; }
        public string TXNAMT { get; set; }
        public string COMMAMT { get; set; }
        public string REMTACCTNO { get; set; }
        public string REMNAME { get; set; }
        public string REMADD1 { get; set; }
        public string REMADD2 { get; set; }
        public string REMADD3 { get; set; }
        public string BENFACCTNO { get; set; }
        public string BENFNAME { get; set; }
        public string BENFADD1 { get; set; }
        public string BENFADD2 { get; set; }
        public string BENFADD3 { get; set; }
        public string RECBNKIFSC { get; set; }
        public string SNDIFSC { get; set; }

        public string ORDINSTNME { get; set; }
        public string ORDINSTADD1 { get; set; }
        public string ORDINSTADD2 { get; set; }
        public string ORDINSTADD3 { get; set; }

        public string InstructionPriority { get; set; }
        public string PurposeCode { get; set; }

        public string TostringContent()
        {
            return Newtonsoft.Json.JsonConvert.SerializeObject(this);
        }
    }
}