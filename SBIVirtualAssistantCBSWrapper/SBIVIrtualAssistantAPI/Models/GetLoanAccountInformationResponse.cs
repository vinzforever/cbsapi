﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LotusWebAPI.Models
{
    public class GetLoanAccountInformationResponse
    {
        public string ReferenceNumber { get; set; }
        public string AccountNumber { get; set; }      
        public string ERROR_DESC { get; set; }
        public string ERROR_CODE { get; set; }
        public string Purpose { get; set; }
        public string LoanAmount { get; set; }
        public string Tenure { get; set; }
        public string RateOfInterest { get; set; }
        public string DateOfSanction { get; set; }
        public string RepaymentStartDate { get; set; }
        public string OutstandingAmount { get; set; }
        public string DisbursementDate { get; set; }
        public string IrregularAmount { get; set; }
        public string IrregularityDate { get; set; }
        public string IRACStatus { get; set; }
        public string InstallmentDueDate { get; set; }
        public string EMIAmount { get; set; }
        public string AccountName { get; set; }
        public string CustomerName { get; set; }
        public string GLClassCode { get; set; }
        public string AppliedAmount { get; set; }
        public string AdvanceAmount { get; set; }
        public string AddLoan { get; set; }
        public string AccountTypeChange { get; set; }
        public string branchcode { get; set; }
        public string Insu { get; set; }
        public string RepaymentDate { get; set; }
        public string LastMaintenanceDate { get; set; }
        public string ApprovalDate { get; set; }
        public string RepaymentRate { get; set; }
        public string LastFinDate { get; set; }
        public string RepayOption { get; set; }
        public string LastAdvDate { get; set; }
        public string IntPrePayAmount { get; set; }
        public string Theo { get; set; }
        public string OldIracStatus { get; set; }
        public string StatementFrequency { get; set; }
        public string StatementCycle { get; set; }
        public string CurrYearTaxDeduction { get; set; }
        public string PrevYearTaxDeduction { get; set; }
        public string AdvanceValue { get; set; }
        public string SecurityAmount { get; set; }
      
    }
}