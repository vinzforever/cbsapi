﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LotusWebAPI.Models
{
    public class FDPreMatureClosureRequest
    {
        public string REQUEST_REF_NO { get; set; }

        public string BANK_CODE { get; set; }
        public string BRANCH_CODE { get; set; }

        public string ACCOUNT_NO { get; set; }
        public string DATE_OF_CLOSURE { get; set; }
    }

    public class FDPreMatureClosureRequest_Inherit : FDPreMatureClosureRequest
    {

        public string REQUEST_CODE = "CBS.IF006";

        public string REQUEST_DATE_TIME = DateTime.Now.ToString("yyyy-MM-ddThh:mm:ss");

        public string SOURCE_ID = "SBISI";

        public string REQUEST_TELLER_ID = "0";

        public string REQUEST_AUTH_ID = "0";
    }
}