﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace LotusWebAPI.Models
{
    public class AcntClosureThroughTrnsfrRequest
    {
        public string ASF_RECOVERED { get; set; }
      
      
        public string BRANCH_CODE { get; set; }
        public string BANK_CODE { get; set; }
       
    
        public string REQUEST_REF_NO { get; set; }
     
        public string USE_SPECIAL_INTEREST_RATE { get; set; }
        public string FROM_ACCOUNT { get; set; }
        public string REASON_FOR_CLOSURE { get; set; }
        public string ACCOUNT_BALANCE { get; set; }
        public string PROMO_NUMBER { get; set; }
        public string TO_ACCOUNT { get; set; }
        public string EXEMPT_FROM_PENALTY { get; set; }
        public string FROM_ACCOUNT_CURRENCY_CODE { get; set; }
        public string AMOUNT { get; set; }
        public string SPECIAL_INTEREST_RATE { get; set; }
        public string TO_ACCOUNT_CURRENCY_CODE { get; set; }
        public string BASE_CURRENCY_AMOUNT { get; set; }
        public string COMMISSION { get; set; }
        public string CHANGE { get; set; }
        public string STATEMENT_NARRATIVE { get; set; }
        public string RATE_TYPE { get; set; }
        public string BRANCH_PENALTY { get; set; }

        public string BOOKING_NUMBER { get; set; }
        public string TREASURY_PENALTY { get; set; }

    }


    public class AcntClosureThroughTrnsfrRequest_Inherit : AcntClosureThroughTrnsfrRequest
    {
       
        public string REQUEST_CODE = "CBS.AM017";
       
        public string REQUEST_AUTH_ID = "0";
       
        public string REQUEST_TELLER_ID = "0";
       
        public string SOURCE_ID = "SBISI";
       
        public string REQUEST_DATE_TIME = DateTime.Now.ToString("yyyy-MM-ddThh:mm:ss");
    }
}