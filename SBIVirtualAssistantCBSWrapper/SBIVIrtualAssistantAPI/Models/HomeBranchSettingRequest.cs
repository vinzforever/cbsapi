﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace LotusWebAPI.Models
{
    public class HomeBranchSettingRequest
    {
       
        public string BRANCH_CODE { get; set; }
        public string BANK_CODE { get; set; }
        public string REQUEST_REF_NO { get; set; }
      
        public string Value_Flag { get; set; }
        public string Description { get; set; }
        public string Product { get; set; }
        public string Seg_code { get; set; }
        public string Account_Number { get; set; }
        public string Auto_Sweep { get; set; }
        public string Option_Selected { get; set; }
        public string Name { get; set; }
        public string Home_branch { get; set; }
        public string Branch_code { get; set; }
        public string Account_type { get; set; }
        public string Sub_Category { get; set; }
        public string Currency { get; set; }
        public string Segment_Code { get; set; }
    }

    public class HomeBranchSettingRequest_Inherit :HomeBranchSettingRequest
    {
       
        public string SOURCE_ID = "SBISI";
       
        public string REQUEST_TELLER_ID = "0";
       
        public string REQUEST_CODE = "CBS.AE005";
       
        public string REQUEST_AUTH_ID = "0";
       
        public string REQUEST_DATE_TIME = DateTime.Now.ToString("yyyy-MM-ddThh:mm:ss");
    }
}