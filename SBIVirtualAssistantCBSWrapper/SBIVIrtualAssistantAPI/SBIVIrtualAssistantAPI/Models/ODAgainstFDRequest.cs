﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LotusWebAPI.Models
{
    public class ODAgainstFDRequest
    {
        public string TO_DEPOSIT_ACCOUNTNUMBER { get; set; }
        public string REFERENCE_NUMBER { get; set; }
        public string BRANCH_CODE { get; set; }
        public string CUSTOMER_NUMBER { get; set; }
        public string PRODUCT { get; set; }
        public string SEGMENT_CODE { get; set; }
        public string MOBILISER { get; set; }
        public string LIMIT_AMOUNT { get; set; }
        public string EXPIRY_DATE { get; set; }
        public string RATE { get; set; }
        public string TYPE { get; set; }
        public string EXPIRY_RATE_TYPE { get; set; }
        public string EXPIRY_RATE_DETAILS { get; set; }
        public string ACTIVITY_CODE { get; set; }
        public string SBSD_SCHEME_CODE { get; set; }
        public string PURPOSE_CODE { get; set; }
        public string TRANSACTION_DATE { get; set; }
        public string COLLATERAL_SUB_TYPE { get; set; }
        public string COLLATERAL_SHORT_DESCRIPTION { get; set; }
        public string TDR_ACCOUNT_DESCRIPTION_1 { get; set; }
        public string HOLD_FLAG_1 { get; set; }
        public string HOLD_VALUE_1 { get; set; }
        public string MARGIN_1 { get; set; }
        public string TDR_ACCOUNT_NUMBER_1 { get; set; }
        public string MATURITY_VALUE_1 { get; set; }
    }

    
}