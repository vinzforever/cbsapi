﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LotusWebAPI.Models
{
    public class ODAgainstFDEnquiryResponse
    {
        public string REFERENCE_NUMBER { get; set; }
        public string CCOD_ACCOUNT_NUMBER { get; set; }
        public string COLLATERAL_NUMBER { get; set; }
        public string RESPONSE_STATUS { get; set; }
        public string ERROR_DESCRIPTION { get; set; }
    }
}