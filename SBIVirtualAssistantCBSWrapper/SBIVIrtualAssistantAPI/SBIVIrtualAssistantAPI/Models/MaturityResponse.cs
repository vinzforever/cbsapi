﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace LotusWebAPI.Models
{
    public class MaturityResponse
    {
        public string ERROR_DESC { get; set; }
        public string RATE { get; set; }
        public string MATURITY_DATE { get; set; }
        public string MATURITY_AMOUNT { get; set; }
        public string PAYOUT_AMOUNT { get; set; }
        
        public string RESPONSE_STATUS { get; set; }
        
      
        public string BRANCH_CODE { get; set; }
        public string BANK_CODE { get; set; }
        public string REQUEST_REF_NO { get; set; }
        public string RESP_DATE { get; set; }
        
    
    }
}