﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LotusWebAPI.Models
{
    public class SITransDetails
    {
        public string Reference_no { get; set; }
        public string Product_Code { get; set; }
        public string Input { get; set; }
        public string Output { get; set; }
        public string Function_Name { get; set; }

        public SITransDetails(string ReferenceNo,string ProductCode,String input,string output,string FunctionName)
        {
            Reference_no = ReferenceNo;
            Product_Code = ProductCode;
            Input = input;
            Output = output;
            Function_Name = FunctionName;
        }
        
        public string TostringContent()
        {
            return Newtonsoft.Json.JsonConvert.SerializeObject(this);
        }

    }
}