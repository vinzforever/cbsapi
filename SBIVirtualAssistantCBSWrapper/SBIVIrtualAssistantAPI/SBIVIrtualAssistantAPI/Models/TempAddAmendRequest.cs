﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LotusWebAPI.Models
{
    public class TempAddAmendRequest
    {
        public string REQUEST_REF_NO { get; set; }
       
        public string BANK_CODE { get; set; }
        public string BRANCH_CODE { get; set; }

        public string EFFECTIVE_DATE { get; set; }
        public string OPTION { get; set; }
        public string BUSINESS_CONTACT_NO { get; set; }
        public string LAST_NAME { get; set; }
        public string EXPIRY_DATE { get; set; }
        public string CUSTOMER_ADD4 { get; set; }
        public string FIRST_NAME { get; set; }
        public string CUSTOMER_ADD1 { get; set; }
        public string LANDLINE_NO { get; set; }
        public string MOBILE_NO { get; set; }
        public string CUSTOMER_ADD3 { get; set; }
        public string CURR_COUNTRY { get; set; }
        public string CUSTOMER_ADD2 { get; set; }
        public string CUSTOMER_NO { get; set; }
        public string FAX_NO { get; set; }
        public string POSTAL_CODE { get; set; }
        public string TITLE_CODE { get; set; }
    }

    public class TempAddAmendRequest_Inherit : TempAddAmendRequest
    {

        public string REQUEST_CODE = "CBS.CM005";
       
        public string REQUEST_DATE_TIME = DateTime.Now.ToString("yyyy-MM-ddThh:mm:ss");
       
        public string SOURCE_ID = "SBISI";
       
        public string REQUEST_TELLER_ID = "0";
       
        public string REQUEST_AUTH_ID = "0";
    }

}