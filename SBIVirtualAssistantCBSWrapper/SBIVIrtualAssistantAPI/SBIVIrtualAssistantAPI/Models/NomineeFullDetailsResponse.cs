﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LotusWebAPI.Models
{
    public class NomineeFullDetailsResponse
    {
        public string RESPONSE_STATUS { get; set; }

        public string BRANCH_CODE { get; set; }
        public string BANK_CODE { get; set; }
        public string REQUEST_REF_NO { get; set; }
        public string RESP_DATE { get; set; }


        public string ACCOUNT_NUMBER { get; set; }
        public string RECORD_NUMBER { get; set; }
        public string NOMINEE_NUMBER { get; set; }
        public string NAME_OF_NOMINEE { get; set; }

        public string ADDRESS_OF_NOMINEE { get; set; }
        public string RELATION { get; set; }
        public string MINOR { get; set; }
        public string DOB_OF_NOMINEE { get; set; }

        public string ERROR_DESCRIPTION { get; set; }
    }
}