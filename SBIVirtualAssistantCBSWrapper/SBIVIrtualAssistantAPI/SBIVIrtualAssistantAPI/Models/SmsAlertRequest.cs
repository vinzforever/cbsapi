﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LotusWebAPI.Models
{
    public class SmsAlertRequest
    {
        public string AccountNumber { get; set; }
        public int Bank_Code { get; set; }
        public string ChequeIssueAlert { get; set; }
        public string ChequeReturnAlert { get; set; }
        public string ChequeStopAlert { get; set; }
        public string CountryCode { get; set; }
        public string CreditThreshold { get; set; }
        public string DebitThreshold { get; set; }
        public string MobileNumber { get; set; }
        public string POSTransaction { get; set; }
        public string ReferenceNumber { get; set; }
        public string RemoveHold { get; set; }
        public string SetHold { get; set; }
        public string SMSFlag { get; set; }
        public string ThresholdBalance { get; set; }

        public string TostringContent()
        {
            return Newtonsoft.Json.JsonConvert.SerializeObject(this);
        }

    }
}