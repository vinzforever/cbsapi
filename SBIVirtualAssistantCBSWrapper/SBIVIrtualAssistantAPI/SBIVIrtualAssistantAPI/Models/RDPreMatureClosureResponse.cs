﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LotusWebAPI.Models
{
    public class RDPreMatureClosureResponse
    {
        public string RESPONSE_STATUS { get; set; }

        public string BRANCH_CODE { get; set; }
        public string BANK_CODE { get; set; }
        public string REQUEST_REF_NO { get; set; }
        public string RESP_DATE { get; set; }

        public string ACCOUNT_NO { get; set; }
        public string ACCOUNT_DESC { get; set; }
        public string PAYMENT_DATE { get; set; }
        public string FINAL_INT_ADJUSTMENT { get; set; }
        public string NET_AMOUNT_PAYABLE_ROUNDED_OFF { get; set; }
        public string SERVICE_CHARGES { get; set; }
        public string STATUS { get; set; } 
        public string MATURITY_VALUE_OF_RD { get; set; }
        public string NET_AMOUNT_PAYABLE { get; set; }
        public string RUN_PERIOD_OF_DEPOSIT_IN_DAYS { get; set; }
        public string RUN_PERIOD_OF_SECOND_INSTALLMENT_IN_DAYS { get; set; }
        public string MATURITY_DATE { get; set; }
        public string ADV_INST_PAID { get; set; }
        public string OPEN_DATE { get; set; }
        public string RUN_PERIOD_OF_DEPOSIT_IN_MONTHS { get; set; }
        public string RUN_PERIOD_OF_SECOND_INSTALLMENT_IN_MONTHS { get; set; }
        public string EXPIRY_DATE_OF_CONTRACT { get; set; }
        public string INSTALLMENT_AMT { get; set; }
        public string RUN_PERIOD_OF_FIRST_INSTALLMENT_IN_DAYS { get; set; }
        public string MATURITY_VALUE_OF_STDR_FOR_RD_AMT { get; set; }
        public string RUN_PERIOD_OF_FIRST_INSTALLMENT_IN_MONTHS { get; set; }
        public string PERIOD_BTN_OPENDT_DEPTDT_FOR_LAST_COMPLETED_QUARTER { get; set; }
        public string ACC_TYPE { get; set; }
        public string INT_CAT { get; set; }
        public string MATURITY_VALUE_OF_STDR_FOR_RUN_PERIOD_OF_FIRST_INST { get; set; }
        public string INTEREST_AVAILABLE_ROUNDED_OFF { get; set; }
        public string MATURITY_VALUE_OF_STDR_FOR_RUN_PERIOD_OF_SECOND_INST { get; set; }
        public string PENALTY { get; set; }
        public string CURRENT_BALANCE_ROUNDED_OFF { get; set; }
        public string DATE_OF_DEPOSIT_OF_SECOND_INST_AFTER_COMPLETED_QUARTER { get; set; }
        public string INT_RATE_ON_OPEN_DATE { get; set; }
        public string DATE_OF_DEPOSIT_OF_INST_FOR_LAST_COMPLETED_QUARTER { get; set; }
        public string DATE_OF_DEPOSIT_OF_FIRST_INST_AFTER_COMPLETED_QUARTER { get; set; }      
        
        public string ERROR_DESCRIPTION { get; set; }
    }
}