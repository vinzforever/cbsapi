﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace LotusWebAPI.Models
{
    public class TermDepositEnquiryResponse
    {
         
      
        public string RESPONSE_STATUS { get; set; }
         
        
        public string BRANCH_CODE { get; set; }
        public string BANK_CODE { get; set; }
        public string REQUEST_REF_NO { get; set; }
        public string RESP_DATE { get; set; }
         
       
        public string Interest_Frequency { get; set; }
        public string Interest_Payable { get; set; }
        public string Status { get; set; }
        public string Branch_Number { get; set; }
        public string Product_Code { get; set; }
        public string Receipt_Number { get; set; }
        public string Currency { get; set; }
        public string Interest_Rate { get; set; }
        public string Tax_Deducted { get; set; }
        public string Printed_Maturity_Value { get; set; }
        public string Interest_Applied { get; set; }
        public string RD_Installment_Due_Day { get; set; }
        public string Transfer_Account_Number { get; set; }
        public string Term_Value { get; set; }
        public string Account_Number { get; set; }
        public string Fees_Applied { get; set; }
        public string Maturity_Date { get; set; }
        public string Current_Balance { get; set; }
        public string Installation_Frequency { get; set; }
        public string Total_Number_of_Installment_Paid { get; set; }
        public string Installment_Amount { get; set; }
        public string ERROR_DESCRIPTION { get; set; }
    }
}