﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LotusWebAPI.Models
{
    public class SetMaturityInstructionRequest
    {
        public string REQUEST_REF_NO { get; set; }
        public string BRANCH_CODE { get; set; }
        public string BANK_CODE { get; set; }
        public string ACCOUNT_NUMBER { get; set; }
        public string INTEREST_FREQUENCY { get; set; }
        public string TRANSFER_ACCOUNT_NUMBER { get; set; }
        public string TERM_LENGTH { get; set; }
        public string TERM_DAYS { get; set; }
        public string TERM_MONTHS { get; set; }
        public string TERM_YEARS { get; set; }
    }

    public class SetMaturityInstruction_Inherit : SetMaturityInstructionRequest
    {
        public string REQUEST_AUTH_ID = "0";

        public string REQUEST_TELLER_ID = "0";

        public string SOURCE_ID = "SBISI";

        public string REQUEST_DATE_TIME = DateTime.Now.ToString("yyyy-MM-ddThh:mm:ss");

        public string REQUEST_CODE = "CBS.AM010";   
    }
}