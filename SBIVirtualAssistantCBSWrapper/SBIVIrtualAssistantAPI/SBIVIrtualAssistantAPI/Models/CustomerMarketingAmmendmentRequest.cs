﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LotusWebAPI.Models
{
    public class CustomerMarketingAmmendmentRequest
    {
        public string BRANCH_CODE { get; set; }
        public string REQUEST_REF_NO { get; set; }
        public string CUSTOMER_NO { get; set; }
        public string CUSTOMER_NAME { get; set; }
        public string MARKETING_INDICATOR { get; set; }
        public string EXPIRY_DATE { get; set; }
    }

    public class CustomerMarketingAmmendmentRequestInherit : CustomerMarketingAmmendmentRequest
    {
        public string REQUEST_AUTH_ID = "0";

        public string REQUEST_TELLER_ID = "0";

        public string SOURCE_ID = "SBISI";

        public string REQUEST_DATE_TIME = DateTime.Now.ToString("yyyy-MM-ddThh:mm:ss");

        public string REQUEST_CODE = "CBS.CM010";
    }


}