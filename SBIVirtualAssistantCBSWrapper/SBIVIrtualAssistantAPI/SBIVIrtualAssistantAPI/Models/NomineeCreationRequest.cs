﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace LotusWebAPI.Models
{
    public class NomineeCreationRequest
    {
      
        public string BRANCH_CODE { get; set; }
        public string BANK_CODE { get; set; }
        public string REQUEST_REF_NO { get; set; }
       
        public string ACCOUNT_NUMBER { get; set; }
        public string NOMINEE_NAME { get; set; }
        public string NOMINEE_ADDRESS { get; set; }
        public string RELATIONSHIP { get; set; }
        public string MINOR_FLAG { get; set; }
        public string DOB_OF_NOMINEE { get; set; }
        public string ACTION { get; set; }
        public string NAME_TO_BE_PRINTED_FLAG { get; set; }
        public string GUARDIAN_NAME { get; set; }
        public string GUARDIAN_ADDRESS1 { get; set; }
        public string GUARDIAN_ADDRESS2 { get; set; }
        public string GUARDIAN_AGE { get; set; }
        public string NOMINEE_SEQ { get; set; }


    }

    public class NomineeCreationRequest_Inherit  : NomineeCreationRequest
    {
        
        public string SOURCE_ID = "SBISI";
        
        public string REQUEST_TELLER_ID = "0";
        
        public string REQUEST_CODE = "CBS.AM009";
        
        public string REQUEST_AUTH_ID = "0";
        
        public string REQUEST_DATE_TIME = DateTime.Now.ToString("yyyy-MM-ddThh:mm:ss");
    }
}