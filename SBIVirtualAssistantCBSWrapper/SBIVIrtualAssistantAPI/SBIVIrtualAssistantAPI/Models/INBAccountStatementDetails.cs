﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LotusWebAPI.Models
{
    public class INBAccountStatementDetails
    {
        public string ReferenceNumber { get; set; }
        public string AccountNumber { get; set; }
        public string ErrorDescription { get; set; }
        public string ErrorCode { get; set; }
        public List<INBAccountStatement> AccountStatement { get; set; }
    }
    public class INBAccountStatement
    {
        public string ValueDate { get; set; }
        public string TodaysDate { get; set; }
        public string Amount { get; set; }
        public string Balance { get; set; }
        public string Narration { get; set; }
        public string Statement { get; set; }
        public string ChequeNumber { get; set; }
        public string INBStatement { get; set; }
        public string TransferStatement { get; set; }
        public string BranchNumber { get; set; }
        public string PostTime { get; set; }
    }
}