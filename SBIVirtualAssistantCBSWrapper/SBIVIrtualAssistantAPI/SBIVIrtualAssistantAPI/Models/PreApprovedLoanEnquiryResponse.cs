﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LotusWebAPI.Models
{
    public class PreApprovedLoanEnquiryResponse
    {
        public string LOAN_ACCOUNT_NUMBER { get; set; }
        public string RESPONSE_STATUS { get; set; }
        public string ERROR_DESCRIPTION { get; set; }
    }
}