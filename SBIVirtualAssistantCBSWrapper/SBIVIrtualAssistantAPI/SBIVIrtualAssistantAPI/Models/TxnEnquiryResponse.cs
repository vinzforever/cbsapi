﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LotusWebAPI.Models
{
    public class TxnEnquiryResponse
    {
        public string ReferenceNumber { get; set; }
        public string CBSReferenceNumber { get; set; }
        public string Status { get; set; }
        public string JournalNumber { get; set; }
        public string GeneratedReconNumber { get; set; }
        public string ErrorCode { get; set; }
        public string ErrorDesc { get; set; }

        public string TostringContent()
        {
            return Newtonsoft.Json.JsonConvert.SerializeObject(this);
        }
    }
}