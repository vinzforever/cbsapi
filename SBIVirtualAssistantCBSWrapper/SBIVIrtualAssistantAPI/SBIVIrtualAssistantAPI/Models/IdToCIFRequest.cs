﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace LotusWebAPI.Models
{
    public class IdToCIFRequest
    {
        
        public string REQUEST_REF_NO { get; set; }
         public string BANK_CODE { get; set; }
        public string BRANCH_CODE { get; set; }
           public string IDENTITY_NUMBER { get; set; }
        public string ID_TYPE { get; set; }
      
    }

    public class IdToCIFRequest_Inherit : IdToCIFRequest
    {
      
        public string REQUEST_CODE = "CBS060465";

        public string REQUEST_DATE_TIME = DateTime.Now.ToString("yyyy-MM-ddThh:mm:ss");

        public string SOURCE_ID = "SBISI";

        public string REQUEST_TELLER_ID = "0";

        public string REQUEST_AUTH_ID = "0";

    }
}

