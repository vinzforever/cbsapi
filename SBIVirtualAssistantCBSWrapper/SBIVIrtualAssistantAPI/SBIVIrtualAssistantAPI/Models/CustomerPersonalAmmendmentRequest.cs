﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LotusWebAPI.Models
{
    public class CustomerPersonalAmmendmentRequest
    {
        public string BRANCH_CODE { get; set; }
        public string REQUEST_REF_NO { get; set; }
        public string CUSTOMER_NO { get; set; }
        public string CUSTOMER_NAME { get; set; }
        public string DATE_OF_BIRTH { get; set; }
        public string GENDER { get; set; }
        public string MARITAL_STATUS { get; set; }
        public string DEPENDENT_PERSON { get; set; }
        public string COUNTRY_CODE { get; set; }
        public string EMPLOYED_DATE { get; set; }
        public string EMPLOYER_ADD { get; set; }
        public string OCCUPATION_DESCRIPTION { get; set; }
        public string OCCUPATION_CODE { get; set; }
        public string PLACE_OF_BIRTH { get; set; }
        public string EMPLOYER_ADD2 { get; set; }
        public string POSTAL_CODE { get; set; }
        public string DATE_OF_DEATH { get; set; }
        public string EMPLOYER_PAN { get; set; }
        public string ECONOMIC_NO { get; set; }
        public string CSP_FLAG { get; set; }
        public string JOURNAL_NO { get; set; }
    }

    public class CustomerPersonalAmmendmentRequestInherit : CustomerPersonalAmmendmentRequest
    {
        public string REQUEST_AUTH_ID = "0";

        public string REQUEST_TELLER_ID = "0";

        public string SOURCE_ID = "SBISI";

        public string REQUEST_DATE_TIME = DateTime.Now.ToString("yyyy-MM-ddThh:mm:ss");
        public string REQUEST_CODE = "CBS.CM008";
    }
}