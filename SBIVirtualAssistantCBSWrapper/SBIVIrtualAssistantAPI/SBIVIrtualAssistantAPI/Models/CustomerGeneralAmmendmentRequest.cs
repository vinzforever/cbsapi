﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LotusWebAPI.Models
{
    public class CustomerGeneralAmmendmentRequest
    {
        public string BRANCH_CODE { get; set; }
        public string NON_NPA_TRADE { get; set; }


        public string REQUEST_REF_NO{ get; set; }
      public string CUSTOMER_NO{ get; set; }
    public string CUSTOMER_TYPE{ get; set; }
    public string TITLE{ get; set; }
    public string FIRST_NAME{ get; set; }
    public string LAST_NAME { get; set; }
    public string THRESHOLD_LIMIT{ get; set; }
    public string PF_INDEX{ get; set; }
    public string CUSTOMER_ADD1{ get; set; }
    public string CUSTOMER_ADD2{ get; set; }
    public string CUSTOMER_ADD3{ get; set; }
    public string DISTRICT_DETAILS{ get; set; }
    public string ISD_CODE{ get; set; }
    public string TAN{ get; set; }
    public string DISCTRICT_CODE{ get; set; }
    public string SUB_DISTRICT_CODE{ get; set; }
    public string VILLAGE_CODE{ get; set; }
    public string PIN_CODE{ get; set; }
    public string COUNTRY_CODE{ get; set; }
    public string FAX_NO{ get; set; }
    public string BUSINESS_CONTACT_NO{ get; set; }
    public string MOBILE_NO{ get; set; }
    public string NATIONALITY{ get; set; }
    public string OCCUPANCY_CODE{ get; set; }
    public string IDENTIFICATION_NO{ get; set; }
    public string ISSUE_DATE{ get; set; }
    public string ID_ISSUED_AT{ get; set; }
    public string ID_TYPE{ get; set; }
    public string DOMESTIC_RISK{ get; set; }
    public string CROSS_BORDER_RISK{ get; set; }
    public string HNI_CODE{ get; set; }
    public string VIP_CODE{ get; set; }
    public string CIF_STATUS{ get; set; }
    public string TFN_INDICATOR{ get; set; }
    public string HOME_BRANCH{ get; set; }
    public string CUSTOMER_LIMIT_FLAG{ get; set; }
    public string NPA_NON_TRADE{ get; set; }
    public string INDUSTRY{ get; set; }
    public string COUNTRY{ get; set; }
    public string SECTOR{ get; set; }
    public string INDUSTRY_CLASSIFICATION{ get; set; }
    public string COUNTRY_CODE_TWO{ get; set; }
    public string AFFILIATE_CODE{ get; set; }
    public string BUSINESS_SECTOR_CODE{ get; set; }
    public string MIDDLE_NAME{ get; set; }
    public string FATHER_OR_SPOUSE_NAME{ get; set; }
    public string MOTHER_MAIDEN_NAME{ get; set; }
    public string RELATIVE_CODE{ get; set; }
    public string RESIDENT_STATUS{ get; set; }
    public string CIS_ORGANIZATION_CODE{ get; set; }
    public string BSR_ORGANIZATION_CODE{ get; set; }
    public string DATE_OF_BIRTH{ get; set; }
    public string GENDER_CODE{ get; set; }
    public string MARITAL_STATUS{ get; set; }
    public string DATE_OF_ESTABLISHMENT{ get; set; }
    public string DATE_OF_COMMENCEMENT{ get; set; }
    public string CUSTOMER_TYPE_TWO{ get; set; }
    public string VISA_ISSUED_BY{ get; set; }
    public string VISA_ISSUE_DATE{ get; set; }
    public string VISA_EXPIRY_DATE{ get; set; }
    public string VISA_DETAILS { get; set; }
    public string DATE_OF_LIQUIDATION{ get; set; }
    public string PAN_ID{ get; set; }
    public string NPA_TRADE{ get; set; }
    public string TRADE_CUSOTMER{ get; set; }
    public string COUNTRY_CODE_THREE{ get; set; }
    public string STATE_CODE{ get; set; }
    public string CITY_CODE{ get; set; }
    public string VOTER_ID{ get; set; }
    public string APPLY_TDS_FLAG{ get; set; }
    public string ID_ISSUE_REMARK{ get; set; }
    public string CUST_EVALUATION_REQ_FLAG{ get; set; }
    public string SECOND_ID{ get; set; }
    public string SECOND_ID_NO{ get; set; }
    public string RMPB_ID{ get; set; }
    public string CUSTOMER_RISK{ get; set; }
    public string KYC_UPDATED_DATE{ get; set; }
    public string RISK_CATEGORY_UPFDATED_DATE{ get; set; }
    public string PREMIER_BANKING_SERVICE_FLAG{ get; set; }
    public string VISUALLY_IMPAIRED{ get; set; }
    public string ADHAAR_NO{ get; set; }
    public string REQUEST_TELLER_ID { get; set; }
    }
    public class CustomerGeneralAmmendmentRequestInherit : CustomerGeneralAmmendmentRequest
    {
        public string REQUEST_AUTH_ID = "0000000";        

        public string BANK_CODE = "0";

        public string SOURCE_ID = "PSG";

        public string REQUEST_DATE_TIME = DateTime.Now.ToString("yyyy-MM-ddThh:mm:ss");

        public string REQUEST_CODE = "CBS.CM004";
    }

}