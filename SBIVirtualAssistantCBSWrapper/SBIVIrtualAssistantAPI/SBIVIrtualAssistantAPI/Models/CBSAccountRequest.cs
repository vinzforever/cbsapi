﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace LotusWebAPI.Models
{
    public class CBSAccountRequest
    {
        public string ReferenceNumber { get; set; }
        
        private string Teller_ID = "";
        
        private string Checker_ID = "";
        public int Bank_Code { get; set; }
        public string Branch_Code { get; set; }
        public string Request { get; set; }

        public string TostringContent()
        {
            return Newtonsoft.Json.JsonConvert.SerializeObject(this);
        }
    }
}