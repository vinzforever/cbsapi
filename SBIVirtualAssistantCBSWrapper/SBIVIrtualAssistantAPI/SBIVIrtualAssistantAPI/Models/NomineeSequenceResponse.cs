﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LotusWebAPI.Models
{
    public class NomineeSequenceResponse
    {
        public string REQUEST_REF_NO { get; set; }
        public string NOMINEE_SEQ { get; set; }
        public string ACCOUNT_NUMBER { get; set; }
        public string BRANCH_CODE { get; set; }
        public string BANK_CODE { get; set; }
        public string RESPONSE_STATUS { get; set; }     
        public string RESP_DATE { get; set; }
        public string ERROR_DESCRIPTION { get; set; }

    }
}