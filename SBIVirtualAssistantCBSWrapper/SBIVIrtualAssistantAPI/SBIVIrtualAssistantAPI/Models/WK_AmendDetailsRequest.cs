﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LotusWebAPI.Models
{
    public class WK_AmendDetailsRequest
    {
        public string REQUEST_REF_NO { get; set; }
        public string BANK_CODE { get; set; }
        public string BRANCH_CODE { get; set; }
        [JsonIgnore]
        public string REQUEST_CODE { get; set; }
        public string ACCOUNT_NUMBER { get; set; }
        public string CUSTOMER_NUMBER { get; set; }
        public string ID_NO { get; set; }
        public string CUSTOMER_NAME { get; set; }
        public string ADDRESS_LINE1 { get; set; }
        public string ADDRESS_LINE2 { get; set; }
        public string PAN_NUMBER { get; set; }
        public string FORM_60_61 { get; set; }
        public string TAX_FILE_NUMBER_INDICATOR { get; set; }
        public string ADDRESS_LINE3 { get; set; }
        public string NATIONALITY { get; set; }
        public string ADDRESS_LINE4 { get; set; }
        public string POST_CODE { get; set; }
        public string PRODUCT { get; set; }
        public string TYPE { get; set; }
        public string INTEREST_CATEGORY { get; set; }
        public string LIMITED_ACCESS { get; set; }
        public string INTEREST_PAYMENT_METHOD { get; set; }
        public string TRANSFER_ACCOUNT_NUMBER { get; set; }
        public string CHARGE_EXEMPTION_PROFILE { get; set; }
        public string LANGUAGE_CODE { get; set; }
        public string FREQUENCY { get; set; }
        public string CONSOLIDATED_STATEMENT_PROCESSING { get; set; }
        public string DATE_INDICATOR { get; set; }
        public string MAIL_INDICATOR { get; set; }
        public string NOTICE_INDICATOR { get; set; }
        public string NOTICE_CUSTOMER { get; set; }
        public string VARIABLE_INTEREST_RATE { get; set; }
        public string STOP_CHQ_PRES_NFT { get; set; }
        public string STOP_CHQ_PRES_AMT_VALIDATION { get; set; }
        public string INTEREST_FREQUENCY { get; set; }
        public string CYCLE { get; set; }
        public string DAY { get; set; }
        public string AGENT_CODE { get; set; }
        public string INVESTMENT_TYPE { get; set; }
        public string DISTRICT { get; set; }
        public string NEXT_DR_INT_APPLN_DATE { get; set; }
        public string GROUP_IND { get; set; }
        public string FACILITY_NUMBER { get; set; }
        public string APPLICATION_IDENTIFIER { get; set; }
        public string DATE { get; set; }
        public string CURRENCY { get; set; }
        public string ACCOUNT_MGMT_GRP { get; set; }
        public string VISA_FLAG { get; set; }
        public string VISA_SECURITY_CODE { get; set; }
        public string VISA_CREDIT_LIMIT { get; set; }
        public string CREDIT_RATING { get; set; }
        public string TERM_LENGTH { get; set; }
        public string TERM_BASIS { get; set; }
        public string TERM_VALUE_DEPOSITED { get; set; }
        public string ACCOUNT_SUB_TYPE { get; set; }
        public string CUSTOMER_RISK_DOMESTIC_RISK { get; set; }
        public string CROSS_BORDER_RISK { get; set; }
        public string ACCOUNT_CROSS_BORDER_RISK { get; set; }
        public string SECURITY_INDICATOR { get; set; }
        public string TIME_BAND { get; set; }
        public string VOSTRO_ACCOUNT_BANK_ID { get; set; }
        public string GL_CLASS_CODE { get; set; }
        public string COPIES { get; set; }
        public string NOTICE_PROCESS_INDICATOR { get; set; }
        public string ACCOUNT_EXTRACTION_IND { get; set; }
        public string FUNDS_TRANSFER_PRICING_RATE { get; set; }
        public string FCY_CLAUSE { get; set; }
        public string NOMINATED_HEDGE_CURRENCY { get; set; }
        public string RATE_CHANGE_NOTICE_ON_STATEMENT { get; set; }
        public string OVERDRAFT_REVIEW_INDICATOR { get; set; }
        public string PRIMARY_ACCOUNT_NO { get; set; }
        public string BOOKING_NO { get; set; }
        public string TPR { get; set; }
        public string SMS_REQD { get; set; }
        public string SAVING_PLUS_TERM_LENGTH_DAYS { get; set; }
        public string CIRCLE_CODE { get; set; }
        public string ADF { get; set; }
        public string PAL { get; set; }
        public string RMPB { get; set; }
        public string AGENT_ID { get; set; }
        public string VENDOR_ID { get; set; }
        public string TERM_DAYS { get; set; }
        public string TERM_MONTHS { get; set; }
        public string TERM_YEARS { get; set; }
        public string MATURITY_DATE { get; set; }
        public string SAVINGS_PLUS { get; set; }
        public string SAVING_PLUS_ACCT_TYPE { get; set; }
        public string SAVING_PLUS_TERM_LENGTH_MONTH { get; set; }
        public string SAVING_PLUS_TERM_BASIS { get; set; }
        public string SAVING_PLUS_INTEREST_FREQ { get; set; }
        public string RD_EXPECTED_INSTL { get; set; }
        public string RD_INSTL_FREQ { get; set; }
        public string RD_INSTALLMENT_DUE_DAY { get; set; }
        public string MOD { get; set; }
        public string LINK_ACTYPE { get; set; }
        public string LIFO_FIFO { get; set; }
        public string TAG_NO { get; set; }
        public string DDO_CODE { get; set; }
        public string MATURITY_INSTRUCTION { get; set; }
        public string BENEFICIARY_NAME { get; set; }
        public string BENEFICIARY_LAST_NAME { get; set; }
        public string ADDRESS1 { get; set; }
        public string ADDRESS2 { get; set; }
        public string PAYABLE_AT { get; set; }
        public string PRIMARY_ACCT_NO { get; set; }
        public string SAVINGS_PLUS_SUB_CATEGORY { get; set; }
        public string BKDT_INT_RECALC_REQ { get; set; }
        public string LINKED_BRANCH_CODE { get; set; }
        public string CENTRAL_GOVT_CODE { get; set; }
        public string CENTGOVT2 { get; set; }
        public string CENTGOVT3 { get; set; }
        public string CENTGOVT4 { get; set; }
        public string STATE_GOVT_CODE { get; set; }
        public string INSTALLMENT_COUNT { get; set; }
        public string AMOUNT_DEPOSITED { get; set; }
        public string IRREGULAR_COUNT { get; set; }
        public string NO_OF_EXTNS { get; set; }
        public string RECEIPT_TYPE { get; set; }
        public string PRIMARY_ACCT { get; set; }
        public string ADVICE_REQUIRED { get; set; }
        public string TYPE_OF_GOLD { get; set; }
        public string BACKDATED_RECALCULATION_IND { get; set; }
        public string WELCOME_KIT_ISSUED { get; set; }
        public string NOMINATION_REQD { get; set; }

        public string REQUEST_TELLER_ID { get; set; }
    }
    public class WK_AmendDetailsRequest_Inherit : WK_AmendDetailsRequest
    {


        public string REQUEST_CODE = "CBS.CE014";

        public string REQUEST_AUTH_ID = "0000000";        

        public string SOURCE_ID = "PSG";

        public string REQUEST_DATE_TIME = DateTime.Now.ToString("yyyy-MM-ddThh:mm:ss");
    }
}