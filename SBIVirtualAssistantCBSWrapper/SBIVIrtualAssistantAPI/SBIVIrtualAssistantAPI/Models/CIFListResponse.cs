﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LotusWebAPI.Models
{
    public class CIFListResponse
    {
        public string ReferenceNumber { get; set; }
        public string ErrorDesc { get; set; }
        public string ErrorCode { get; set; }
        public string Account { get; set; }
        public List<string> ALLCIFInfo { get; set; }
    }
}