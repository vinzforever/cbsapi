﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LotusWebAPI.Models
{
    public class DedupOnDOBName_CIF_SearchRequest
    {
        public string REQUEST_REF_NO { get; set; }

        public string BANK_CODE { get; set; }
        public string BRANCH_CODE { get; set; }

        public string FIRST_NAME { get; set; }
        public string LAST_NAME { get; set; }
        public string DATE_OF_BIRTH { get; set; }
    }

    public class DedupOnDOBName_CIF_SearchRequest_Inherit : DedupOnDOBName_CIF_SearchRequest
    {

        public string REQUEST_CODE = "CBS070660";

        public string REQUEST_DATE_TIME = DateTime.Now.ToString("yyyy-MM-ddThh:mm:ss");

        public string SOURCE_ID = "SBISI";

        public string REQUEST_TELLER_ID = "0";

        public string REQUEST_AUTH_ID = "0";
    }
}