﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LotusWebAPI.Models
{
    public class INBChannelEnquiryResponse
    {
        public string REQUEST_REF_NO { get; set; }
        public string RESPONSE_STATUS { get; set; }
        public string INTERNET_BANKING_KIT_NUMBER { get; set; }
        public string ERROR_DESCRIPTION { get; set; }   
    }
}