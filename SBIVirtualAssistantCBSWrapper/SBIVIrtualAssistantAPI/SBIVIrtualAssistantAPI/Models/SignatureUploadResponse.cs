﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LotusWebAPI.Models
{
    public class SignatureUploadResponse
    {
        public string ReferenceNumber { get; set; }
        public string Status { get; set; }
        
    }
}