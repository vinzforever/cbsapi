﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LotusWebAPI.Models
{
    public class ODAgainstMultipleFDRequest
    {
        public string REFERENCE_NUMBER { get; set; }

        public string TO_DEPOSIT_ACCOUNTNUMBER { get; set; }
        public string CUSTOMER_NUMBER { get; set; }
        public string PRODUCT { get; set; }
        public string SEGMENT_CODE { get; set; }
        public string MOBILISER { get; set; }
        public string LIMIT_AMOUNT { get; set; }
        public string EXPIRY_DATE { get; set; }
        public string RATE { get; set; }
        public string TYPE { get; set; }
        public string EXPIRY_RATE_TYPE { get; set; }
        public string EXPIRY_RATE_DETAILS { get; set; }
        public string ACTIVITY_CODE { get; set; }
        public string SBSD_SCHEME_CODE { get; set; }
        public string PURPOSE_CODE { get; set; }
        public string TRANSACTION_DATE { get; set; }
        public string COLLATERAL_SUB_TYPE { get; set; }
        public string COLLATERAL_SHORT_DESCRIPTION { get; set; }
        public string TDR_ACCOUNT_DESCRIPTION_1 { get; set; }
        public string TDR_ACCOUNT_DESCRIPTION_2 { get; set; }
        public string TDR_ACCOUNT_DESCRIPTION_3 { get; set; }
        public string TDR_ACCOUNT_DESCRIPTION_4 { get; set; }
        public string TDR_ACCOUNT_DESCRIPTION_5 { get; set; }
        public string HOLD_FLAG_1 { get; set; }
        public string HOLD_FLAG_2 { get; set; }
        public string HOLD_FLAG_3 { get; set; }
        public string HOLD_FLAG_4 { get; set; }
        public string HOLD_FLAG_5 { get; set; }
        public string HOLD_VALUE_1 { get; set; }
        public string HOLD_VALUE_2 { get; set; }
        public string HOLD_VALUE_3 { get; set; }
        public string HOLD_VALUE_4 { get; set; }
        public string HOLD_VALUE_5 { get; set; }
        public string MARGIN_1 { get; set; }
        public string MARGIN_2 { get; set; }
        public string MARGIN_3 { get; set; }
        public string MARGIN_4 { get; set; }
        public string MARGIN_5 { get; set; }
        public string TDR_ACCOUNT_NUMBER_1 { get; set; }
        public string TDR_ACCOUNT_NUMBER_2 { get; set; }
        public string TDR_ACCOUNT_NUMBER_3 { get; set; }
        public string TDR_ACCOUNT_NUMBER_4 { get; set; }
        public string TDR_ACCOUNT_NUMBER_5 { get; set; }
        public string MATURITY_VALUE_1 { get; set; }
        public string MATURITY_VALUE_2 { get; set; }
        public string MATURITY_VALUE_3 { get; set; }
        public string MATURITY_VALUE_4 { get; set; }
        public string MATURITY_VALUE_5 { get; set; } 
    }
}