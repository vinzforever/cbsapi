﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LotusWebAPI.Models
{
    public class ReferenceEnquiryResponse
    {
        public string ReferenceNumber { get; set; }
        public string Response { get; set; }
        public string ErrorDescription  { get; set; }
    }
}