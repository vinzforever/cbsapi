﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LotusWebAPI.Models
{
    public class PreApprovedLoanRequest
    {
       // public string OCCUPATION_CODE { get; set; }
       // public string DATE_OF_BIRTH { get; set; }
        public string BANKFACILITY { get; set; }
        public string REFERENCE_NUMBER { get; set; }
        public string CUSTOMER_NUMBER { get; set; }
        public string PRODUCT { get; set; }
        public string APPLICATION_AMOUNT { get; set; }
        public string LOAN_TERM { get; set; }
        public string LOAN_BASIS { get; set; }
        public string REPAY_DAY { get; set; }
        public string SEGMENT_CODE { get; set; }
        public string MOBILISER { get; set; }
        public string SCHEME_CODE { get; set; }
        public string ACTIVITY_CODE { get; set; }
        public string APPROVAL_DATE { get; set; }
        public string CURRENCY { get; set; }
        public string PRINCIPAL_REPAY_START { get; set; }
        public string INTEREST_REPAY_START { get; set; }
        public string MORATORIUM_TERM { get; set; }
        public string FREQUENCY { get; set; }
        public string TO_DEPOSIT_ACCOUNT { get; set; }
    }
}