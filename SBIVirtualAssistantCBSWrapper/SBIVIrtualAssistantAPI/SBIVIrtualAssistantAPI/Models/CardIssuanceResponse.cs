﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LotusWebAPI.Models
{
    public class CardIssuanceResponse
    {
        public string RESPONSE_STATUS { get; set; }
        public string RESPONSE_DESCRIPTION { get; set; }
        public string REFERENCE_NUMBER { get; set; }
    }
}