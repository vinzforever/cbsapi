﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LotusWebAPI.Models
{
    public class AccountStatementDetails
    {
        public string ReferenceNumber { get; set; }
        public string AccountNumber { get; set; }
        public string AccountName { get; set; }
        public string CurrentBalance { get; set; }
        public string Currency { get; set; }
        public string TotalTransactions { get; set; }
        public string ErrorDescription { get; set; }
        public string ErrorCode { get; set; }
        public List<AccountStatement> AccDetailsList { get; set; }
    }
    public class AccountStatement
    {
        public string DateOfTransaction { get; set; }
        public string TransactionAmount { get; set; }
        public string TransactionNarration { get; set; }
        public string AvailableBalance { get; set; }
        public string ValueDate { get; set; }
        public string Journal { get; set; }
        public string Teller { get; set; }
        public string BranchCode { get; set; }
        public string TxnNumber { get; set; }
        public string TxnType { get; set; }

    }
}