﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LotusWebAPI.Models
{
    public class AccountStatementRequest
    {
        public string ReferenceNumber { get; set; }
        public int Bank_Code { get; set; }
        public string AccountNumber { get; set; }       
        public string TransactionNumber { get; set; }
        public string FromDate { get; set; }
        public string ToDate { get; set; }
        public string FromAmount { get; set; }
        public string ToAmount { get; set; } 
    }
}