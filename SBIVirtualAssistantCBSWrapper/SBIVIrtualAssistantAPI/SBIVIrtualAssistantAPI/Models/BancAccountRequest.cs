﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LotusWebAPI.Models
{
    public class BancAccountRequest
    {
        public string ReferenceNumber { get; set; }
        public string Teller_ID { get; set; }
        public string Checker_ID { get; set; }
        public int Bank_Code { get; set; }
        public string Branch_Code { get; set; }
        public string Request { get; set; }

        public string TostringContent()
        {
            return Newtonsoft.Json.JsonConvert.SerializeObject(this);
        }
    }
}