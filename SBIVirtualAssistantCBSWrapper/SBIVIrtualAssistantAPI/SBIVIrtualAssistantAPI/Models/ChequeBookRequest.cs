﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LotusWebAPI.Models
{
    public class ChequeBookRequest
    {
        public string Branch_Code { get; set; }
        public string Checker_ID { get; set; }
        public string  Teller_ID { get; set; }
        public int Bank_Code { get; set; }
        public string ReferenceNumber { get; set; }
        public string AccountNumber { get; set; }
        public string InstrumentType { get; set; }
        public string InstrumentSubCategory { get; set; }
        public string LeavesPerBook { get; set; }
        public string PickupBranch { get; set; }
        public string NumberofChequeBooks { get; set; }
        public string SANType { get; set; }
        public string ChequeBookAmount { get; set; }
        public string ChequeBookType { get; set; }
        public string PsegFlag { get; set; }

        public string TostringContent()
        {
            return Newtonsoft.Json.JsonConvert.SerializeObject(this);
        }
    }

        
}