﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LotusWebAPI.Models
{
    public class Cif_MobilePanResponse
    {
        public string ReferenceNumber { get; set; }
        public string ErrorDesc { get; set; }
        public string ErrorCode { get; set; }
        public List<Individual_CIf_Details> Cif_Details_List { get; set; }
    }


    public class Individual_CIf_Details
    {
        public string CifNumber { get; set; }
        public string CustomerName { get; set; }
        public string BranchCode { get; set; }
        public string LinkedWith { get; set; }
    }
}