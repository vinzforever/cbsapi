﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LotusWebAPI.Models
{
    public class CustomerIdentificationAmmendmentRequest
    {
        public string BRANCH_CODE { get; set; }
        public string REQUEST_REF_NO { get; set; }
        public string CUSTOMER_NO { get; set; }
        public string ID_TYPE1 { get; set; }
        public string ID_NUMBER1 { get; set; }
        public string REMARKS1 { get; set; }
        public string EXPIRY_DATE1 { get; set; }
        public string FUNCTION_1 { get; set; }
        public string ISSUE_PLACE1 { get; set; }
        public string ID_TYPE2 { get; set; }
        public string ID_NUMBER2 { get; set; }
        public string REMARKS2 { get; set; }
        public string EXPIRY_DATE2 { get; set; }
        public string FUNCTION_2 { get; set; }
        public string ISSUE_PLACE2 { get; set; }
        public string ID_TYPE3 { get; set; }
        public string ID_NUMBER3 { get; set; }
        public string ID_ISSUE_DATE3 { get; set; }
        public string REMARKS3 { get; set; }
        public string EXPIRY_DATE3 { get; set; }
        public string FUNCTION_3 { get; set; }
        public string ISSUE_PLACE3 { get; set; }
        public string ID_TYPE4 { get; set; }
        public string ID_NUMBER4 { get; set; }
        public string REMARKS4 { get; set; }
        public string EXPIRY_DATE4 { get; set; }
        public string FUNCTION_4 { get; set; }
        public string ISSUE_PLACE4 { get; set; }
        public string ID_TYPE5 { get; set; }
        public string ID_NUMBER5 { get; set; }
        public string REMARKS5 { get; set; }
        public string EXPIRY_DATE5 { get; set; }
        public string FUNCTION_5 { get; set; }
        public string ISSUE_PLACE5 { get; set; }
    }

    public class CustomerIdentificationAmmendmentRequestInherit : CustomerIdentificationAmmendmentRequest
    {
        public string REQUEST_AUTH_ID = "0";

        public string REQUEST_TELLER_ID = "0";

        public string SOURCE_ID = "SBISI";

        public string REQUEST_DATE_TIME = DateTime.Now.ToString("yyyy-MM-ddThh:mm:ss");



        public string REQUEST_CODE = "CBS.CM007";
    }
}