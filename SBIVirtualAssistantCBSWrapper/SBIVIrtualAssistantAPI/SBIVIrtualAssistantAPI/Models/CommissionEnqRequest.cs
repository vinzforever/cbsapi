﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LotusWebAPI.Models
{
    public class CommissionEnqRequest
    {
        public string ReferenceNumber { get; set; }
        public string Account { get; set; }
        public string Amount { get; set; }
        public string Channel { get; set; }

        public string TostringContent()
        {
            return Newtonsoft.Json.JsonConvert.SerializeObject(this);
        }
    }
}