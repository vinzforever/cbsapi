﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LotusWebAPI.Models
{
    public class WelcomeKitCustomerResponse
    {
        public string Reference_No { get; set; }
        public string JOURNAL_NUMBER { get; set; }
        public string ERROR_DESCRIPTION { get; set; }
    }
}