﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace LotusWebAPI.Models
{
    public class SIEnquiryResponse
    {
          
      
        public string REQUEST_REF_NO { get; set; }
    
        public string RESPONSE_STATUS { get; set; }

        public string BANK_CODE { get; set; }
        public string BRANCH_CODE { get; set; }
          
       
        public string RESP_DATE { get; set; }
        
       
        public string Account_Number { get; set; }
        public string Currency { get; set; }
        public string System { get; set; }
        public string Purpose_Of_SI { get; set; }
        public string Enquiry_Option { get; set; }
        public string Number { get; set; }
        public string More_Records_To_Follow { get; set; }
        public List<Standing_Orders_Details> Standing_Orders { get; set; }
        public string ERROR_DESCRIPTION { get; set; }
    }

    public class Standing_Orders_Details
    {
        public string Delete_Reason_Code { get; set; }
        public string So_type { get; set; }
        public string Bank_ID { get; set; }
        public string Owner { get; set; }
        public string Freq_Code { get; set; }
        public string Amount { get; set; }
        public string Hold_Flag { get; set; }
        public string Order_Date { get; set; }
        public string ToOrFrom { get; set; }
        public string Account { get; set; }
        public string Purpose_Of_SI { get; set; }
        public string Priority { get; set; }
        public string Record { get; set; }
        public string Date { get; set; }
        public string StartOrNext_Date { get; set; }
        public string CodeORChase_Days { get; set; }
        public string Freq { get; set; }
        public string System { get; set; }
        public string Currency { get; set; }

    }
}