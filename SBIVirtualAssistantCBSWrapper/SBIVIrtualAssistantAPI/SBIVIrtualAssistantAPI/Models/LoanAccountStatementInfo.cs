﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LotusWebAPI.Models
{
    public class LoanAccountStatementInfo
    {
        public string ReferenceNumber { get; set; }
        public List<LoanAccountStatementResponse> StatementDetail { get; set; }
        public string ErrorCode { get; set; }
        public string ErrorDescription { get; set; }
    }
    public class LoanAccountStatementResponse
    {
       
        public string ValueDate { get; set; }
        public string PostDate { get; set; }
        public string Amount { get; set; }
        public string Balance { get; set; }
        public string Narration { get; set; }
        public string Statement { get; set; }
        public string BranchNumber { get; set; }
       
    }
}