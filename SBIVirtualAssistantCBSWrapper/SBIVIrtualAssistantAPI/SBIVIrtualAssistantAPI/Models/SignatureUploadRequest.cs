﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LotusWebAPI.Models
{
    public class SignatureUploadRequest
    {
        public string REQUEST_REF_NO { get; set; }
        public string BANK_CODE { get; set; }
        public string IMMASBYTES { get; set; }
        public string ACCOUNT_NO { get; set; }
        public string DATE_CRTD { get; set; }
        public string DATE_EXP { get; set; }
        public string DESCRIPTION { get; set; }
        public string CUSTOMER_NO { get; set; }
        public string IMAGE_ID { get; set; }
        public string MANDATE { get; set; }
    }
}