﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LotusWebAPI.Models
{
    public class TxnRequest
    {
        public int BankCode { get; set; }
        public string ReferenceNumber { get; set; }
        public string TransactionType { get; set; }
        public string BranchCode { get; set; }
        public string DebitAccount { get; set; }
        public string CreditAccount { get; set; }
        public string Amount { get; set; }
        public string Narration { get; set; }
        public string ReconReferenceNumber { get; set; }

        public string TostringContent()
        {
            return Newtonsoft.Json.JsonConvert.SerializeObject(this);
        }
    }
}