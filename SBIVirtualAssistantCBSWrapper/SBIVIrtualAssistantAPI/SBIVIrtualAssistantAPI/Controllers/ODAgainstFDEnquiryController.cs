﻿using LotusWebAPI.Models;
using LotusWebAPI.Provider;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using SBISI.CoreBanking.Common.Provider;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace LotusWebAPI.Controllers
{
    public class ODAgainstFDEnquiryController : ApiController
    {
        [TokenSecurityFilter.TokenSecurityFilter]
        [HttpPost]
        public ODAgainstFDEnquiryResponse AccountClosurePost(EnquiryRequestWithReference RequestObject)
        {
            if (RequestObject == null)
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound) { Content = new StringContent("SI011^Input string was not in a correct format."), ReasonPhrase = "SI011^Input string was not in a correct format." });

            if (RequestObject.REFERENCE_NUMBER.Substring(0, 5) != "SBILT")
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound) { Content = new StringContent("SI094^REFERENCE NUMBER INVALID. REFER DOCUMENT."), ReasonPhrase = "SI094^REFERENCE NUMBER INVALID. REFER DOCUMENT." });

            string ProductCode = "LOTUS";
            LoggerImplement LoggerImplementObj = new LoggerImplement(RequestObject.REFERENCE_NUMBER, ProductCode);
            LoggerImplementObj.LoggerObj.WriteLog(Newtonsoft.Json.JsonConvert.SerializeObject(RequestObject), "ODAgainstFDRequest");

            ODAgainstFDEnquiryResponse ResponseObject = new ODAgainstFDEnquiryResponse();

            List<InternalTransDB> ListInternalDB = new List<InternalTransDB>();
            List<InternalTransDetails> ListInternalDetails = new List<InternalTransDetails>();

            SITransDetails SIDetail = null;
            SITransDetails SIDetail1 = null;
            SITransDB TransDBObject = null;

            CBSTransaction GenericObject = new CBSTransaction();

            DatabaseOperation DBObject = new DatabaseOperation();
            string BankName = DatabaseOperation.GetBankName(0);

            int cnt = 1;
            TransDBObject = new SITransDB(RequestObject.REFERENCE_NUMBER, ProductCode, DateTime.Now, "", "", "", "ODAgainstFDEnquiry_" + cnt, IPAddressInfo.GetIP());
            ResponseObject.REFERENCE_NUMBER = RequestObject.REFERENCE_NUMBER;

            try
            {
                string DBResponse = LoggerImplementObj.InsertSITransDB(TransDBObject);
                while (DBResponse != "SUCCESS")
                {
                    if (DBResponse != "SUCCESS")
                        cnt = cnt + 1;

                    TransDBObject.Function_Name = "ODAgainstFDEnquiry_" + cnt;
                    DBResponse = LoggerImplementObj.InsertSITransDB(TransDBObject);
                }
                string MRReferenceNumber = DBObject.generateSBISIRRN(BankName + "TC", "");
                DateTime Request_date_time = DateTime.Now;

                var SICreationResponseString = GenericObject.sendJsonToSI("0", RequestObject.REFERENCE_NUMBER, "ODAgainstFDEnquiryPost");
                DateTime Response_Date_Time = DateTime.Now;

                JObject SICreationObj = JObject.Parse(SICreationResponseString.ToString());

                InternalTransDB InternalDB = new InternalTransDB("TC", RequestObject.REFERENCE_NUMBER, MRReferenceNumber, Request_date_time, Response_Date_Time, SICreationObj["RESPONSE_STATUS"].ToString(), "", "ODAgainstFDEnquiry");
                InternalTransDetails InternalDetail = new InternalTransDetails(MRReferenceNumber, Newtonsoft.Json.JsonConvert.SerializeObject(RequestObject), SICreationResponseString);

                ListInternalDB.Add(InternalDB);
                ListInternalDetails.Add(InternalDetail);

                ResponseObject.REFERENCE_NUMBER = RequestObject.REFERENCE_NUMBER;

                if (SICreationObj["RESPONSE_STATUS"].ToString() == "1")
                {
                    TransDBObject.Error_Desc = SICreationObj["ERROR_DESCRIPTION"].ToString();
                    TransDBObject.Res_Time = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");

                    foreach (JProperty propertyName in SICreationObj.Properties())
                    {
                        if (ResponseObject.GetType().GetProperty(propertyName.Name) != null)
                            ResponseObject.GetType().GetProperty(propertyName.Name).SetValue(ResponseObject, SICreationObj[propertyName.Name].ToString());
                    }

                    SIDetail = new SITransDetails(RequestObject.REFERENCE_NUMBER, ProductCode, Newtonsoft.Json.JsonConvert.SerializeObject(RequestObject), Newtonsoft.Json.JsonConvert.SerializeObject(ResponseObject), "ODAgainstFDEnquiry_" + cnt);
                    LoggerImplementObj.InsertInternalTransDBDetails(TransDBObject, SIDetail, ListInternalDB, ListInternalDetails);

                    ResponseObject.ERROR_DESCRIPTION = TransDBObject.Error_Desc;

                    return ResponseObject;
                }

                foreach (JProperty propertyName in SICreationObj.Properties())
                {
                    if (ResponseObject.GetType().GetProperty(propertyName.Name) != null)
                        ResponseObject.GetType().GetProperty(propertyName.Name).SetValue(ResponseObject, SICreationObj[propertyName.Name].ToString());
                }

                TransDBObject.Res_Time = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                SIDetail1 = new SITransDetails(RequestObject.REFERENCE_NUMBER, ProductCode, Newtonsoft.Json.JsonConvert.SerializeObject(RequestObject), JsonConvert.SerializeObject(ResponseObject), "ODAgainstFDEnquiry_" + cnt);

                LoggerImplementObj.InsertInternalTransDBDetails(TransDBObject, SIDetail1, ListInternalDB, ListInternalDetails);

                return ResponseObject;
            }
            catch (Exception ex)
            {
                LoggerImplementObj.LoggerObj.WriteExceptionLog(ex.Message, "ODAgainstFDEnquiryException");

                TransDBObject.Res_Time = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                TransDBObject.Error_Code = "SI001";
                TransDBObject.Error_Desc = ex.Message.Replace("'", "");
                ResponseObject.ERROR_DESCRIPTION = "Exception while processing the response";

                ResponseObject.REFERENCE_NUMBER = RequestObject.REFERENCE_NUMBER;

                SIDetail = new SITransDetails(RequestObject.REFERENCE_NUMBER, ProductCode, Newtonsoft.Json.JsonConvert.SerializeObject(RequestObject), Newtonsoft.Json.JsonConvert.SerializeObject(ResponseObject), "ODAgainstFDEnquiry_" + cnt);
                LoggerImplementObj.InsertInternalTransDBDetails(TransDBObject, SIDetail, ListInternalDB, ListInternalDetails);
            }
            finally
            {
                LoggerImplementObj.LoggerObj.WriteLog(Newtonsoft.Json.JsonConvert.SerializeObject(ResponseObject), "ODAgainstFDEnquiryResponse");
                LoggerImplementObj.InsertInternalTransDBDetails(TransDBObject, SIDetail, ListInternalDB, ListInternalDetails);
            }

            return ResponseObject;
        }
    }
}