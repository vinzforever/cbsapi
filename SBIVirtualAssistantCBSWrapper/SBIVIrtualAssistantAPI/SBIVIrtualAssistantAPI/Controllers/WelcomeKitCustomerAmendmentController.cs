﻿using LotusWebAPI.Models;
using LotusWebAPI.Provider;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using SBISI.CoreBanking.Common.Provider;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace LotusWebAPI.Controllers
{
    public class WelcomeKitCustomerAmendmentController : ApiController
    {
        [TokenSecurityFilter.TokenSecurityFilter]
        [HttpPost]
        public WelcomeKitCustomerResponse WelcomeKitCustomerAmendment(WelcomeKitCustomerRequest RequestObject)
        {
            if (RequestObject == null)
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound) { Content = new StringContent("SI011^Input string was not in a correct format."), ReasonPhrase = "SI011^Input string was not in a correct format." });
            if (RequestObject.REQUEST_REF_NO.Substring(0, 5) != "SBILT")
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound) { Content = new StringContent("SI094^REFERENCE NUMBER INVALID. REFER DOCUMENT."), ReasonPhrase = "SI094^REFERENCE NUMBER INVALID. REFER DOCUMENT." });

            string ProductCode = "LOTUS";
            LoggerImplement LoggerImplementObj = new LoggerImplement(RequestObject.REQUEST_REF_NO, ProductCode);
            LoggerImplementObj.LoggerObj.WriteLog(Newtonsoft.Json.JsonConvert.SerializeObject(RequestObject), "WelcomeKitCustomerRequest");

            WelcomeKitCustomerResponse ResponseObject = new WelcomeKitCustomerResponse();

            List<InternalTransDB> ListInternalDB = new List<InternalTransDB>();
            List<InternalTransDetails> ListInternalDetails = new List<InternalTransDetails>();

            SITransDetails SIDetail = null;
            SITransDetails SIDetail1 = null;
            SITransDB TransDBObject = null;

            CBSTransaction GenericObject = new CBSTransaction();

            DatabaseOperation DBObject = new DatabaseOperation();
            string BankName = DatabaseOperation.GetBankName(0);

            WelcomeKitCustomerRequest_Inherit WelcomeKitCustomerRequest_Inherit_InheritObj = new WelcomeKitCustomerRequest_Inherit();
            WelcomeKitCustomerRequest_Inherit_InheritObj.REQUEST_REF_NO = RequestObject.REQUEST_REF_NO;
            WelcomeKitCustomerRequest_Inherit_InheritObj.CUSTOMER_NO = RequestObject.CUSTOMER_NO;
            WelcomeKitCustomerRequest_Inherit_InheritObj.CUSTOMER_TYPE = RequestObject.CUSTOMER_TYPE;
            WelcomeKitCustomerRequest_Inherit_InheritObj.TITLE = RequestObject.TITLE;
            WelcomeKitCustomerRequest_Inherit_InheritObj.FIRST_NAME = RequestObject.FIRST_NAME;
            WelcomeKitCustomerRequest_Inherit_InheritObj.LAST_NAME = RequestObject.LAST_NAME;
            WelcomeKitCustomerRequest_Inherit_InheritObj.THRESHOLD_LIMIT = RequestObject.THRESHOLD_LIMIT;
            WelcomeKitCustomerRequest_Inherit_InheritObj.PF_INDEX = RequestObject.PF_INDEX;
            WelcomeKitCustomerRequest_Inherit_InheritObj.CUSTOMER_ADD1 = RequestObject.CUSTOMER_ADD1;
            WelcomeKitCustomerRequest_Inherit_InheritObj.CUSTOMER_ADD2 = RequestObject.CUSTOMER_ADD2;
            WelcomeKitCustomerRequest_Inherit_InheritObj.CUSTOMER_ADD3 = RequestObject.CUSTOMER_ADD3;
            WelcomeKitCustomerRequest_Inherit_InheritObj.DISTRICT_DETAILS = RequestObject.DISTRICT_DETAILS;
            WelcomeKitCustomerRequest_Inherit_InheritObj.ISD_CODE = RequestObject.ISD_CODE;
            WelcomeKitCustomerRequest_Inherit_InheritObj.TAN = RequestObject.TAN;
            WelcomeKitCustomerRequest_Inherit_InheritObj.DISCTRICT_CODE = RequestObject.DISCTRICT_CODE;
            WelcomeKitCustomerRequest_Inherit_InheritObj.SUB_DISTRICT_CODE = RequestObject.SUB_DISTRICT_CODE;
            WelcomeKitCustomerRequest_Inherit_InheritObj.VILLAGE_CODE = RequestObject.VILLAGE_CODE;
            WelcomeKitCustomerRequest_Inherit_InheritObj.PIN_CODE = RequestObject.PIN_CODE;
            WelcomeKitCustomerRequest_Inherit_InheritObj.COUNTRY_CODE = RequestObject.COUNTRY_CODE;
            WelcomeKitCustomerRequest_Inherit_InheritObj.FAX_NO = RequestObject.FAX_NO;
            WelcomeKitCustomerRequest_Inherit_InheritObj.BUSINESS_CONTACT_NO = RequestObject.BUSINESS_CONTACT_NO;
            WelcomeKitCustomerRequest_Inherit_InheritObj.MOBILE_NO = RequestObject.MOBILE_NO;
            WelcomeKitCustomerRequest_Inherit_InheritObj.NATIONALITY = RequestObject.NATIONALITY;
            WelcomeKitCustomerRequest_Inherit_InheritObj.OCCUPANCY_CODE = RequestObject.OCCUPANCY_CODE;
            WelcomeKitCustomerRequest_Inherit_InheritObj.IDENTIFICATION_NO = RequestObject.IDENTIFICATION_NO;
            WelcomeKitCustomerRequest_Inherit_InheritObj.ISSUE_DATE = RequestObject.ISSUE_DATE;
            WelcomeKitCustomerRequest_Inherit_InheritObj.ID_ISSUED_AT = RequestObject.ID_ISSUED_AT;
            WelcomeKitCustomerRequest_Inherit_InheritObj.ID_TYPE = RequestObject.ID_TYPE;
            WelcomeKitCustomerRequest_Inherit_InheritObj.DOMESTIC_RISK = RequestObject.DOMESTIC_RISK;
            WelcomeKitCustomerRequest_Inherit_InheritObj.CROSS_BORDER_RISK = RequestObject.CROSS_BORDER_RISK;
            WelcomeKitCustomerRequest_Inherit_InheritObj.HNI_CODE = RequestObject.HNI_CODE;
            WelcomeKitCustomerRequest_Inherit_InheritObj.CIF_STATUS = RequestObject.CIF_STATUS;
            WelcomeKitCustomerRequest_Inherit_InheritObj.VIP_CODE = RequestObject.VIP_CODE;
            WelcomeKitCustomerRequest_Inherit_InheritObj.TFN_INDICATOR = RequestObject.TFN_INDICATOR;
            WelcomeKitCustomerRequest_Inherit_InheritObj.HOME_BRANCH = RequestObject.HOME_BRANCH;
            WelcomeKitCustomerRequest_Inherit_InheritObj.CUSTOMER_LIMIT_FLAG = RequestObject.CUSTOMER_LIMIT_FLAG;
            WelcomeKitCustomerRequest_Inherit_InheritObj.NPA_NON_TRADE = RequestObject.NPA_NON_TRADE;
            WelcomeKitCustomerRequest_Inherit_InheritObj.INDUSTRY = RequestObject.INDUSTRY;
            WelcomeKitCustomerRequest_Inherit_InheritObj.COUNTRY = RequestObject.COUNTRY;
            WelcomeKitCustomerRequest_Inherit_InheritObj.SECTOR = RequestObject.SECTOR;
            WelcomeKitCustomerRequest_Inherit_InheritObj.INDUSTRY_CLASSIFICATION = RequestObject.INDUSTRY_CLASSIFICATION;
            WelcomeKitCustomerRequest_Inherit_InheritObj.COUNTRY_CODE_TWO = RequestObject.COUNTRY_CODE_TWO;
            WelcomeKitCustomerRequest_Inherit_InheritObj.BUSINESS_SECTOR_CODE = RequestObject.BUSINESS_SECTOR_CODE;
            WelcomeKitCustomerRequest_Inherit_InheritObj.AFFILIATE_CODE = RequestObject.AFFILIATE_CODE;
            WelcomeKitCustomerRequest_Inherit_InheritObj.MIDDLE_NAME = RequestObject.MIDDLE_NAME;
            WelcomeKitCustomerRequest_Inherit_InheritObj.FATHER_OR_SPOUSE_NAME = RequestObject.FATHER_OR_SPOUSE_NAME;
            WelcomeKitCustomerRequest_Inherit_InheritObj.MOTHER_MAIDEN_NAME = RequestObject.MOTHER_MAIDEN_NAME;
            WelcomeKitCustomerRequest_Inherit_InheritObj.RELATIVE_CODE = RequestObject.RELATIVE_CODE;
            WelcomeKitCustomerRequest_Inherit_InheritObj.RESIDENT_STATUS = RequestObject.RESIDENT_STATUS;
            WelcomeKitCustomerRequest_Inherit_InheritObj.CIS_ORGANIZATION_CODE = RequestObject.CIS_ORGANIZATION_CODE;
            WelcomeKitCustomerRequest_Inherit_InheritObj.BSR_ORGANIZATION_CODE = RequestObject.BSR_ORGANIZATION_CODE;
            WelcomeKitCustomerRequest_Inherit_InheritObj.DATE_OF_BIRTH = RequestObject.DATE_OF_BIRTH;
            WelcomeKitCustomerRequest_Inherit_InheritObj.GENDER_CODE = RequestObject.GENDER_CODE;
            WelcomeKitCustomerRequest_Inherit_InheritObj.MARITAL_STATUS = RequestObject.MARITAL_STATUS;
            WelcomeKitCustomerRequest_Inherit_InheritObj.DATE_OF_ESTABLISHMENT = RequestObject.DATE_OF_ESTABLISHMENT;
            WelcomeKitCustomerRequest_Inherit_InheritObj.DATE_OF_COMMENCEMENT = RequestObject.DATE_OF_COMMENCEMENT;
            WelcomeKitCustomerRequest_Inherit_InheritObj.CUSTOMER_TYPE_TWO = RequestObject.CUSTOMER_TYPE_TWO;
            WelcomeKitCustomerRequest_Inherit_InheritObj.VISA_ISSUED_BY = RequestObject.VISA_ISSUED_BY;
            WelcomeKitCustomerRequest_Inherit_InheritObj.VISA_ISSUE_DATE = RequestObject.VISA_ISSUE_DATE;
            WelcomeKitCustomerRequest_Inherit_InheritObj.VISA_EXPIRY_DATE = RequestObject.VISA_EXPIRY_DATE;
            WelcomeKitCustomerRequest_Inherit_InheritObj.VISA_DETAILS = RequestObject.VISA_DETAILS;
            WelcomeKitCustomerRequest_Inherit_InheritObj.DATE_OF_LIQUIDATION = RequestObject.DATE_OF_LIQUIDATION;
            WelcomeKitCustomerRequest_Inherit_InheritObj.NPA_TRADE = RequestObject.NPA_TRADE;
            WelcomeKitCustomerRequest_Inherit_InheritObj.NON_NPA_TRADE = RequestObject.NON_NPA_TRADE;
            WelcomeKitCustomerRequest_Inherit_InheritObj.TRADE_CUSOTMER = RequestObject.TRADE_CUSOTMER;
            WelcomeKitCustomerRequest_Inherit_InheritObj.COUNTRY_CODE_THREE = RequestObject.COUNTRY_CODE_THREE;
            WelcomeKitCustomerRequest_Inherit_InheritObj.STATE_CODE = RequestObject.STATE_CODE;
            WelcomeKitCustomerRequest_Inherit_InheritObj.CITY_CODE = RequestObject.CITY_CODE;
            WelcomeKitCustomerRequest_Inherit_InheritObj.PAN_ID = RequestObject.PAN_ID;
            WelcomeKitCustomerRequest_Inherit_InheritObj.APPLY_TDS_FLAG = RequestObject.APPLY_TDS_FLAG;
            WelcomeKitCustomerRequest_Inherit_InheritObj.ID_ISSUE_REMARK = RequestObject.ID_ISSUE_REMARK;
            WelcomeKitCustomerRequest_Inherit_InheritObj.CUST_EVALUATION_REQ_FLAG = RequestObject.CUST_EVALUATION_REQ_FLAG;
            WelcomeKitCustomerRequest_Inherit_InheritObj.SECOND_ID = RequestObject.SECOND_ID;
            WelcomeKitCustomerRequest_Inherit_InheritObj.SECOND_ID_NO = RequestObject.SECOND_ID_NO;
            WelcomeKitCustomerRequest_Inherit_InheritObj.KYC_UPDATED_DATE = RequestObject.KYC_UPDATED_DATE;
            WelcomeKitCustomerRequest_Inherit_InheritObj.RISK_CATEGORY_UPFDATED_DATE = RequestObject.RISK_CATEGORY_UPFDATED_DATE;
            WelcomeKitCustomerRequest_Inherit_InheritObj.RMPB_ID = RequestObject.RMPB_ID;
            WelcomeKitCustomerRequest_Inherit_InheritObj.CUSTOMER_RISK = RequestObject.CUSTOMER_RISK;
            WelcomeKitCustomerRequest_Inherit_InheritObj.PREMIER_BANKING_SERVICE_FLAG = RequestObject.PREMIER_BANKING_SERVICE_FLAG;
            WelcomeKitCustomerRequest_Inherit_InheritObj.VISUALLY_IMPAIRED = RequestObject.VISUALLY_IMPAIRED;
            WelcomeKitCustomerRequest_Inherit_InheritObj.ADHAAR_NO = RequestObject.ADHAAR_NO;
            WelcomeKitCustomerRequest_Inherit_InheritObj.REQUEST_TELLER_ID = RequestObject.REQUEST_TELLER_ID;

            TransDBObject = new SITransDB(RequestObject.REQUEST_REF_NO, ProductCode, DateTime.Now, "", "", "", "WelcomeKitCustomerAmendment", IPAddressInfo.GetIP());
            ResponseObject.Reference_No = RequestObject.REQUEST_REF_NO;

            try
            {
                string DBResponse = LoggerImplementObj.InsertSITransDB(TransDBObject);
                if (DBResponse != "SUCCESS")
                {
                    string ERROR_DESCRIPTION = string.Empty;
                    switch (DBResponse)
                    {
                        case "SI007": ERROR_DESCRIPTION = "DATABASE ERROR";
                            break;

                        case "SI087": ERROR_DESCRIPTION = "REFERENCE NUMBER MUST BE UNIQUE";
                            break;
                    }
                    ResponseObject.ERROR_DESCRIPTION = ERROR_DESCRIPTION;
                    ResponseObject.Reference_No = RequestObject.REQUEST_REF_NO;
                    return ResponseObject;
                }
                string MRReferenceNumber = DBObject.generateSBISIRRN(BankName + "TC", "");
                DateTime Request_date_time = DateTime.Now;

                var SICreationResponseString = GenericObject.sendJsonToSIPost("0", Newtonsoft.Json.JsonConvert.SerializeObject(WelcomeKitCustomerRequest_Inherit_InheritObj), "WelcomeKitCustomerAmendmentPost");
                DateTime Response_Date_Time = DateTime.Now;

                JObject SICreationObj = JObject.Parse(JObject.Parse(SICreationResponseString)["Response"].ToString());

                InternalTransDB InternalDB = new InternalTransDB("TC", RequestObject.REQUEST_REF_NO, MRReferenceNumber, Request_date_time, Response_Date_Time, SICreationObj["RESPONSE_STATUS"].ToString(), "", "WelcomeKitCustomerAmendment");
                InternalTransDetails InternalDetail = new InternalTransDetails(MRReferenceNumber, Newtonsoft.Json.JsonConvert.SerializeObject(WelcomeKitCustomerRequest_Inherit_InheritObj), SICreationResponseString);

                ListInternalDB.Add(InternalDB);
                ListInternalDetails.Add(InternalDetail);

                ResponseObject.Reference_No = RequestObject.REQUEST_REF_NO;

                if (SICreationObj["RESPONSE_STATUS"].ToString() == "1")
                {
                    TransDBObject.Error_Desc = SICreationObj["ERROR_DESCRIPTION"].ToString();
                    TransDBObject.Res_Time = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");

                    foreach (JProperty propertyName in SICreationObj.Properties())
                    {
                        if (ResponseObject.GetType().GetProperty(propertyName.Name) != null)
                            ResponseObject.GetType().GetProperty(propertyName.Name).SetValue(ResponseObject, SICreationObj[propertyName.Name].ToString());
                    }

                    SIDetail = new SITransDetails(RequestObject.REQUEST_REF_NO, ProductCode, Newtonsoft.Json.JsonConvert.SerializeObject(RequestObject), Newtonsoft.Json.JsonConvert.SerializeObject(ResponseObject), "WelcomeKitCustomerAmendment");
                    LoggerImplementObj.InsertInternalTransDBDetails(TransDBObject, SIDetail, ListInternalDB, ListInternalDetails);

                    ResponseObject.ERROR_DESCRIPTION = TransDBObject.Error_Desc;

                    return ResponseObject;
                }

                foreach (JProperty propertyName in SICreationObj.Properties())
                {
                    if (ResponseObject.GetType().GetProperty(propertyName.Name) != null)
                        ResponseObject.GetType().GetProperty(propertyName.Name).SetValue(ResponseObject, SICreationObj[propertyName.Name].ToString());
                }

                TransDBObject.Res_Time = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                SIDetail1 = new SITransDetails(RequestObject.REQUEST_REF_NO, ProductCode, Newtonsoft.Json.JsonConvert.SerializeObject(RequestObject), JsonConvert.SerializeObject(ResponseObject), "WelcomeKitCustomerAmendment");
                LoggerImplementObj.InsertInternalTransDBDetails(TransDBObject, SIDetail1, ListInternalDB, ListInternalDetails);

                return ResponseObject;
            }
            catch (Exception ex)
            {
                LoggerImplementObj.LoggerObj.WriteExceptionLog(ex.Message, "WelcomeKitCustomerAmendmentException");

                TransDBObject.Res_Time = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                TransDBObject.Error_Code = "SI001";
                TransDBObject.Error_Desc = ex.Message.Replace("'", "");
                ResponseObject.ERROR_DESCRIPTION = "Exception while processing the response";

                ResponseObject.Reference_No = RequestObject.REQUEST_REF_NO;

                SIDetail = new SITransDetails(RequestObject.REQUEST_REF_NO, ProductCode, Newtonsoft.Json.JsonConvert.SerializeObject(RequestObject), Newtonsoft.Json.JsonConvert.SerializeObject(ResponseObject), "WelcomeKitCustomerAmendment");
                LoggerImplementObj.InsertInternalTransDBDetails(TransDBObject, SIDetail, ListInternalDB, ListInternalDetails);
            }
            finally
            {
                LoggerImplementObj.LoggerObj.WriteLog(Newtonsoft.Json.JsonConvert.SerializeObject(ResponseObject), "WelcomeKitCustomerResponse");
                LoggerImplementObj.InsertInternalTransDBDetails(TransDBObject, SIDetail, ListInternalDB, ListInternalDetails);
            }

            return ResponseObject;
        }
    }
}