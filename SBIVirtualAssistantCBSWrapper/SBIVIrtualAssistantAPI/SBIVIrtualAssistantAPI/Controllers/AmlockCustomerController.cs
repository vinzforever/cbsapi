﻿using LotusWebAPI.Models;
using LotusWebAPI.Provider;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using SBISI.CoreBanking.Common.Provider;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace LotusWebAPI.Controllers
{
    public class AmlockCustomerController : ApiController
    {
        // [TokenSecurityFilter.TokenSecurityFilter]
        [HttpPost]
        public AmlockCustomerResponse AccountClosurePost(AmlockCustomerRequest RequestObject)
        {
            ServicePointManager.ServerCertificateValidationCallback += (sender, cert, chain, sslPolicyErrors) => true;

            if (RequestObject == null)
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound) { Content = new StringContent("SI011^Input string was not in a correct format."), ReasonPhrase = "SI011^Input string was not in a correct format." });
            if (RequestObject.REQUEST_REF_NO.Substring(0, 5) != "SBILT")
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound) { Content = new StringContent("SI094^REFERENCE NUMBER INVALID. REFER DOCUMENT."), ReasonPhrase = "SI094^REFERENCE NUMBER INVALID. REFER DOCUMENT." });

            string ProductCode = "LOTUS";
            LoggerImplement LoggerImplementObj = new LoggerImplement(RequestObject.REQUEST_REF_NO, ProductCode);
            LoggerImplementObj.LoggerObj.WriteLog(Newtonsoft.Json.JsonConvert.SerializeObject(RequestObject), "AmlockCustomerRequest");

            AmlockCustomerResponse ResponseObject = new AmlockCustomerResponse();
            CBSTransaction GenericObject = new CBSTransaction();

            List<InternalTransDB> ListInternalDB = new List<InternalTransDB>();
            List<InternalTransDetails> ListInternalDetails = new List<InternalTransDetails>();

            SITransDetails SIDetail = null;
            SITransDetails SIDetail1 = null;
            SITransDB TransDBObject = null;

            DatabaseOperation DBObject = new DatabaseOperation();
            string BankName = DatabaseOperation.GetBankName(0);

            AmlockCustomerRequestInherit AmlockCustomerRequestInherit_InheritObj = new AmlockCustomerRequestInherit();
            AmlockCustomerRequestInherit_InheritObj.BRANCHCODE = RequestObject.BranchCode;
            AmlockCustomerRequestInherit_InheritObj.NAME = RequestObject.Name;
            AmlockCustomerRequestInherit_InheritObj.TELLERID = RequestObject.TellerID;

            TransDBObject = new SITransDB(RequestObject.REQUEST_REF_NO, ProductCode, DateTime.Now, "", "", "", "AmlockCustomer", IPAddressInfo.GetIP());
            ResponseObject.Reference_Number = RequestObject.REQUEST_REF_NO;

            try
            {
                string DBResponse = LoggerImplementObj.InsertSITransDB(TransDBObject);
                if (DBResponse != "SUCCESS")
                {
                    string ERROR_DESCRIPTION = string.Empty;
                    switch (DBResponse)
                    {
                        case "SI007": ERROR_DESCRIPTION = "DATABASE ERROR";
                            break;

                        case "SI087": ERROR_DESCRIPTION = "REFERENCE NUMBER MUST BE UNIQUE";
                            break;
                    }
                    ResponseObject.RESPONSE_DESCRIPTION = ERROR_DESCRIPTION;
                    return ResponseObject;
                }
                string MRReferenceNumber = DBObject.generateSBISIRRN(BankName + "TC", "");
                DateTime Request_date_time = DateTime.Now;

                var SICreationResponseString = GenericObject.sendJsonToSINewPost("0", Newtonsoft.Json.JsonConvert.SerializeObject(AmlockCustomerRequestInherit_InheritObj), "AmlockCustomerPost");
                DateTime Response_Date_Time = DateTime.Now;

                JObject SICreationObj = JObject.Parse(SICreationResponseString);

                InternalTransDB InternalDB = new InternalTransDB("TC", RequestObject.REQUEST_REF_NO, MRReferenceNumber, Request_date_time, Response_Date_Time, "", "", "AmlockCustomer");
                InternalTransDetails InternalDetail = new InternalTransDetails(MRReferenceNumber, Newtonsoft.Json.JsonConvert.SerializeObject(AmlockCustomerRequestInherit_InheritObj), SICreationResponseString);

                ListInternalDB.Add(InternalDB);
                ListInternalDetails.Add(InternalDetail);

                ResponseObject.Reference_Number = RequestObject.REQUEST_REF_NO;

                if (SICreationObj["RESPONSECODE"].ToString() != "Success")
                {
                    TransDBObject.Error_Desc = SICreationObj["LISTINFORMATION"].ToString();
                    TransDBObject.Res_Time = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");

                    foreach (JProperty propertyName in SICreationObj.Properties())
                    {
                        if (ResponseObject.GetType().GetProperty(propertyName.Name) != null)
                            ResponseObject.GetType().GetProperty(propertyName.Name).SetValue(ResponseObject, SICreationObj[propertyName.Name].ToString());
                    }

                    SIDetail = new SITransDetails(RequestObject.REQUEST_REF_NO, ProductCode, Newtonsoft.Json.JsonConvert.SerializeObject(RequestObject), Newtonsoft.Json.JsonConvert.SerializeObject(ResponseObject), "AmlockCustomer");

                    LoggerImplementObj.InsertInternalTransDBDetails(TransDBObject, SIDetail, ListInternalDB, ListInternalDetails);
                    return ResponseObject;
                }

                foreach (JProperty propertyName in SICreationObj.Properties())
                {
                    if (ResponseObject.GetType().GetProperty(propertyName.Name) != null)
                        ResponseObject.GetType().GetProperty(propertyName.Name).SetValue(ResponseObject, SICreationObj[propertyName.Name].ToString());
                }

                TransDBObject.Res_Time = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                SIDetail1 = new SITransDetails(RequestObject.REQUEST_REF_NO, ProductCode, Newtonsoft.Json.JsonConvert.SerializeObject(RequestObject), JsonConvert.SerializeObject(ResponseObject), "AmlockCustomer");

                LoggerImplementObj.InsertInternalTransDBDetails(TransDBObject, SIDetail1, ListInternalDB, ListInternalDetails);
                return ResponseObject;
            }
            catch (Exception ex)
            {
                LoggerImplementObj.LoggerObj.WriteExceptionLog(ex.Message, "AmlockCustomerException");
                TransDBObject.Res_Time = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                TransDBObject.Error_Code = "SI001";
                TransDBObject.Error_Desc = ex.Message.Replace("'", "");
                ResponseObject.RESPONSE_DESCRIPTION = "Exception while processing the response";

                ResponseObject.Reference_Number = RequestObject.REQUEST_REF_NO;

                SIDetail = new SITransDetails(RequestObject.REQUEST_REF_NO, ProductCode, Newtonsoft.Json.JsonConvert.SerializeObject(RequestObject), Newtonsoft.Json.JsonConvert.SerializeObject(ResponseObject), "AmlockCustomer");

                LoggerImplementObj.InsertInternalTransDBDetails(TransDBObject, SIDetail, ListInternalDB, ListInternalDetails);
            }
            finally
            {
                LoggerImplementObj.LoggerObj.WriteLog(Newtonsoft.Json.JsonConvert.SerializeObject(ResponseObject), "AmlockCustomerResponse");
                LoggerImplementObj.InsertInternalTransDBDetails(TransDBObject, SIDetail, ListInternalDB, ListInternalDetails);
            }

            return ResponseObject;
        }
    }
}