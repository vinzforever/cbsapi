﻿using LotusWebAPI.Models;
using LotusWebAPI.Provider;
using Newtonsoft.Json.Linq;
using SBISI.CoreBanking.Common.Provider;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace LotusWebAPI.Controllers
{
    public class TempAddAmendController : ApiController
    {
        [TokenSecurityFilter.TokenSecurityFilter]
        [HttpPost]
        public TempAddAmendResponse TempAddAmendPost(TempAddAmendRequest RequestObject)
        {
            if (RequestObject == null)
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound) { Content = new StringContent("SI011^Input string was not in a correct format."), ReasonPhrase = "SI011^Input string was not in a correct format." });

            if (RequestObject.REQUEST_REF_NO.Substring(0, 5) != "SBILT")
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound) { Content = new StringContent("SI094^REFERENCE NUMBER INVALID. REFER DOCUMENT."), ReasonPhrase = "SI094^REFERENCE NUMBER INVALID. REFER DOCUMENT." });

            string ProductCode = "LOTUS";
            LoggerImplement LoggerImplementObj = new LoggerImplement(RequestObject.REQUEST_REF_NO, ProductCode);
            LoggerImplementObj.LoggerObj.WriteLog(Newtonsoft.Json.JsonConvert.SerializeObject(RequestObject), "TempAddAmendRequest");

            TempAddAmendResponse ResponseObject = new TempAddAmendResponse();

            List<InternalTransDB> ListInternalDB = new List<InternalTransDB>();
            List<InternalTransDetails> ListInternalDetails = new List<InternalTransDetails>();

            SITransDetails SIDetail = null;
            SITransDetails SIDetail1 = null;
            SITransDB TransDBObject = null;

            CBSTransaction GenericObject = new CBSTransaction();

            DatabaseOperation DBObject = new DatabaseOperation();
            string BankName = DatabaseOperation.GetBankName(Convert.ToInt32(RequestObject.BANK_CODE));

            TempAddAmendRequest_Inherit TempAddAmendRequest_InheritObj = new TempAddAmendRequest_Inherit();
            TempAddAmendRequest_InheritObj.REQUEST_REF_NO = RequestObject.REQUEST_REF_NO;
            TempAddAmendRequest_InheritObj.BANK_CODE = RequestObject.BANK_CODE;
            TempAddAmendRequest_InheritObj.BRANCH_CODE = RequestObject.BRANCH_CODE;
            TempAddAmendRequest_InheritObj.BUSINESS_CONTACT_NO = RequestObject.BUSINESS_CONTACT_NO;
            TempAddAmendRequest_InheritObj.CURR_COUNTRY = RequestObject.CURR_COUNTRY;
            TempAddAmendRequest_InheritObj.CUSTOMER_ADD1 = RequestObject.CUSTOMER_ADD1;
            TempAddAmendRequest_InheritObj.CUSTOMER_ADD2 = RequestObject.CUSTOMER_ADD2;
            TempAddAmendRequest_InheritObj.CUSTOMER_ADD3 = RequestObject.CUSTOMER_ADD3;
            TempAddAmendRequest_InheritObj.CUSTOMER_ADD4 = RequestObject.CUSTOMER_ADD4;
            TempAddAmendRequest_InheritObj.CUSTOMER_NO = RequestObject.CUSTOMER_NO;
            TempAddAmendRequest_InheritObj.EFFECTIVE_DATE = RequestObject.EFFECTIVE_DATE;
            TempAddAmendRequest_InheritObj.EXPIRY_DATE = RequestObject.EXPIRY_DATE;
            TempAddAmendRequest_InheritObj.FAX_NO = RequestObject.FAX_NO;
            TempAddAmendRequest_InheritObj.FIRST_NAME = RequestObject.FIRST_NAME;
            TempAddAmendRequest_InheritObj.LANDLINE_NO = RequestObject.LANDLINE_NO;
            TempAddAmendRequest_InheritObj.LAST_NAME = RequestObject.LAST_NAME;
            TempAddAmendRequest_InheritObj.MOBILE_NO = RequestObject.MOBILE_NO;
            TempAddAmendRequest_InheritObj.OPTION = RequestObject.OPTION;
            TempAddAmendRequest_InheritObj.POSTAL_CODE = RequestObject.POSTAL_CODE;
            TempAddAmendRequest_InheritObj.TITLE_CODE = RequestObject.TITLE_CODE;

            TransDBObject = new SITransDB(RequestObject.REQUEST_REF_NO, ProductCode, DateTime.Now, "", "", "", "TempAddAmend", IPAddressInfo.GetIP());

            ResponseObject.REQUEST_REF_NO = RequestObject.REQUEST_REF_NO;
            try
            {
                string DBResponse = LoggerImplementObj.InsertSITransDB(TransDBObject);
                if (DBResponse != "SUCCESS")
                {
                    string ERROR_DESCRIPTION = string.Empty;
                    switch (DBResponse)
                    {
                        case "SI007": ERROR_DESCRIPTION = "DATABASE ERROR";
                            break;

                        case "SI087": ERROR_DESCRIPTION = "REFERENCE NUMBER MUST BE UNIQUE";
                            break;
                    }
                    ResponseObject.ERROR_DESCRIPTION = ERROR_DESCRIPTION;
                    ResponseObject.REQUEST_REF_NO = RequestObject.REQUEST_REF_NO;
                    return ResponseObject;
                }

                var TempAddAmendResponseString = GenericObject.sendJsonToSIPost("0", Newtonsoft.Json.JsonConvert.SerializeObject(TempAddAmendRequest_InheritObj), "TempAddAmendPost");
                LoggerImplementObj.LoggerObj.WriteLog(TempAddAmendResponseString, "TempAddAmendResponseString");
                string MRReferenceNumber = DBObject.generateSBISIRRN(BankName + "TC", "");
                DateTime Request_date_time = DateTime.Now;
                DateTime Response_Date_Time = DateTime.Now;
                JObject TempAddAmendObj = JObject.Parse(JObject.Parse(TempAddAmendResponseString)["Response"].ToString());

                InternalTransDB InternalDB = new InternalTransDB("TC", RequestObject.REQUEST_REF_NO, MRReferenceNumber, Request_date_time, Response_Date_Time, TempAddAmendObj["RESPONSE_STATUS"].ToString(), "", "TempAddAmendnPost");
                InternalTransDetails InternalDetail = new InternalTransDetails(MRReferenceNumber, Newtonsoft.Json.JsonConvert.SerializeObject(TempAddAmendRequest_InheritObj), TempAddAmendResponseString);

                ListInternalDB.Add(InternalDB);
                ListInternalDetails.Add(InternalDetail);

                ResponseObject.REQUEST_REF_NO = RequestObject.REQUEST_REF_NO;

                if (TempAddAmendObj["RESPONSE_STATUS"].ToString() == "1")
                {
                    TransDBObject.Error_Desc = TempAddAmendObj["ERROR_DESCRIPTION"].ToString();
                    TransDBObject.Res_Time = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");

                    foreach (JProperty propertyName in TempAddAmendObj.Properties())
                    {
                        if (ResponseObject.GetType().GetProperty(propertyName.Name) != null)
                            ResponseObject.GetType().GetProperty(propertyName.Name).SetValue(ResponseObject, TempAddAmendObj[propertyName.Name].ToString());
                    }

                    SIDetail = new SITransDetails(RequestObject.REQUEST_REF_NO, ProductCode, Newtonsoft.Json.JsonConvert.SerializeObject(RequestObject), Newtonsoft.Json.JsonConvert.SerializeObject(ResponseObject), "TempAddAmend");
                    LoggerImplementObj.InsertInternalTransDBDetails(TransDBObject, SIDetail, ListInternalDB, ListInternalDetails);

                    ResponseObject.ERROR_DESCRIPTION = TransDBObject.Error_Desc;

                    return ResponseObject;
                }

                foreach (JProperty propertyName in TempAddAmendObj.Properties())
                {
                    if (ResponseObject.GetType().GetProperty(propertyName.Name) != null)
                        ResponseObject.GetType().GetProperty(propertyName.Name).SetValue(ResponseObject, TempAddAmendObj[propertyName.Name].ToString());
                }

                TransDBObject.Res_Time = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                SIDetail1 = new SITransDetails(RequestObject.REQUEST_REF_NO, ProductCode, Newtonsoft.Json.JsonConvert.SerializeObject(RequestObject), Newtonsoft.Json.JsonConvert.SerializeObject(ResponseObject), "TempAddAmend");
                LoggerImplementObj.InsertInternalTransDBDetails(TransDBObject, SIDetail1, ListInternalDB, ListInternalDetails);

                return ResponseObject;
            }
            catch (Exception ex)
            {
                LoggerImplementObj.LoggerObj.WriteExceptionLog(ex.Message, "TempAddAmendException");

                TransDBObject.Res_Time = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                TransDBObject.Error_Code = "SI001";
                TransDBObject.Error_Desc = ex.Message.Replace("'", "");
                ResponseObject.ERROR_DESCRIPTION = "Exception while processing the response";

                ResponseObject.REQUEST_REF_NO = RequestObject.REQUEST_REF_NO;

                SIDetail = new SITransDetails(RequestObject.REQUEST_REF_NO, ProductCode, Newtonsoft.Json.JsonConvert.SerializeObject(RequestObject), Newtonsoft.Json.JsonConvert.SerializeObject(ResponseObject), "TempAddAmend");
                LoggerImplementObj.InsertInternalTransDBDetails(TransDBObject, SIDetail, ListInternalDB, ListInternalDetails);
            }
            finally
            {
                LoggerImplementObj.LoggerObj.WriteLog(Newtonsoft.Json.JsonConvert.SerializeObject(ResponseObject), "TempAddAmendResponse");
                LoggerImplementObj.InsertInternalTransDBDetails(TransDBObject, SIDetail, ListInternalDB, ListInternalDetails);
            }

            return ResponseObject;
        }
    }
}