﻿using LotusWebAPI.Models;
using LotusWebAPI.Provider;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using SBISI.CoreBanking.Common.Provider;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace LotusWebAPI.Controllers
{
    public class AccountClosureThroughTransferController : ApiController
    {

        [ApiExplorerSettings(IgnoreApi = true)]
        //[TokenSecurityFilter.TokenSecurityFilter]
        [HttpPost]
        public AcntClosureThroughTrnsfrResponse AccountClosurePost(AcntClosureThroughTrnsfrRequest RequestObject)
        {
            if (RequestObject == null)
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound) { Content = new StringContent("SI011^Input string was not in a correct format."), ReasonPhrase = "SI011^Input string was not in a correct format." });

            if (RequestObject.REQUEST_REF_NO.Substring(0, 5) != "SBILT")
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound) { Content = new StringContent("SI094^REFERENCE NUMBER INVALID. REFER DOCUMENT."), ReasonPhrase = "SI094^REFERENCE NUMBER INVALID. REFER DOCUMENT." });

            string ProductCode = "LOTUS";
            LoggerImplement LoggerImplementObj = new LoggerImplement(RequestObject.REQUEST_REF_NO, ProductCode);
            LoggerImplementObj.LoggerObj.WriteLog(Newtonsoft.Json.JsonConvert.SerializeObject(RequestObject), "AcntClosureThroughTrnsfrRequest");

            AcntClosureThroughTrnsfrResponse ResponseObject = new AcntClosureThroughTrnsfrResponse();
            CBSTransaction GenericObject = new CBSTransaction();

            List<InternalTransDB> ListInternalDB = new List<InternalTransDB>();
            List<InternalTransDetails> ListInternalDetails = new List<InternalTransDetails>();

            SITransDetails SIDetail = null;
            SITransDetails SIDetail1 = null;
            SITransDB TransDBObject = null;

            DatabaseOperation DBObject = new DatabaseOperation();
            string BankName = DatabaseOperation.GetBankName(Convert.ToInt32(RequestObject.BANK_CODE));

            AcntClosureThroughTrnsfrRequest_Inherit AcntClosureThroughTrnsfrRequest_InheritObj = new AcntClosureThroughTrnsfrRequest_Inherit();
            AcntClosureThroughTrnsfrRequest_InheritObj.ASF_RECOVERED = RequestObject.ASF_RECOVERED;
            AcntClosureThroughTrnsfrRequest_InheritObj.BRANCH_CODE = RequestObject.BRANCH_CODE;
            AcntClosureThroughTrnsfrRequest_InheritObj.BANK_CODE = RequestObject.BANK_CODE;
            AcntClosureThroughTrnsfrRequest_InheritObj.REQUEST_REF_NO = RequestObject.REQUEST_REF_NO;
            AcntClosureThroughTrnsfrRequest_InheritObj.USE_SPECIAL_INTEREST_RATE = RequestObject.USE_SPECIAL_INTEREST_RATE;
            AcntClosureThroughTrnsfrRequest_InheritObj.FROM_ACCOUNT = RequestObject.FROM_ACCOUNT;
            AcntClosureThroughTrnsfrRequest_InheritObj.REASON_FOR_CLOSURE = RequestObject.REASON_FOR_CLOSURE;
            AcntClosureThroughTrnsfrRequest_InheritObj.ACCOUNT_BALANCE = RequestObject.ACCOUNT_BALANCE;
            AcntClosureThroughTrnsfrRequest_InheritObj.PROMO_NUMBER = RequestObject.PROMO_NUMBER;
            AcntClosureThroughTrnsfrRequest_InheritObj.TO_ACCOUNT = RequestObject.TO_ACCOUNT;
            AcntClosureThroughTrnsfrRequest_InheritObj.EXEMPT_FROM_PENALTY = RequestObject.EXEMPT_FROM_PENALTY;
            AcntClosureThroughTrnsfrRequest_InheritObj.FROM_ACCOUNT_CURRENCY_CODE = RequestObject.FROM_ACCOUNT_CURRENCY_CODE;
            AcntClosureThroughTrnsfrRequest_InheritObj.AMOUNT = RequestObject.AMOUNT;
            AcntClosureThroughTrnsfrRequest_InheritObj.SPECIAL_INTEREST_RATE = RequestObject.SPECIAL_INTEREST_RATE;
            AcntClosureThroughTrnsfrRequest_InheritObj.TO_ACCOUNT_CURRENCY_CODE = RequestObject.TO_ACCOUNT_CURRENCY_CODE;
            AcntClosureThroughTrnsfrRequest_InheritObj.BASE_CURRENCY_AMOUNT = RequestObject.BASE_CURRENCY_AMOUNT;
            AcntClosureThroughTrnsfrRequest_InheritObj.COMMISSION = RequestObject.COMMISSION;
            AcntClosureThroughTrnsfrRequest_InheritObj.CHANGE = RequestObject.CHANGE;
            AcntClosureThroughTrnsfrRequest_InheritObj.STATEMENT_NARRATIVE = RequestObject.STATEMENT_NARRATIVE;
            AcntClosureThroughTrnsfrRequest_InheritObj.RATE_TYPE = RequestObject.RATE_TYPE;
            AcntClosureThroughTrnsfrRequest_InheritObj.BRANCH_PENALTY = RequestObject.BRANCH_PENALTY;
            AcntClosureThroughTrnsfrRequest_InheritObj.BOOKING_NUMBER = RequestObject.BOOKING_NUMBER;
            AcntClosureThroughTrnsfrRequest_InheritObj.TREASURY_PENALTY = RequestObject.TREASURY_PENALTY;

            TransDBObject = new SITransDB(RequestObject.REQUEST_REF_NO, ProductCode, DateTime.Now, "", "", "", "AcntClosureThroughTrnsfr", IPAddressInfo.GetIP());
            ResponseObject.REQUEST_REF_NO = RequestObject.REQUEST_REF_NO;

            try
            {
                string DBResponse = LoggerImplementObj.InsertSITransDB(TransDBObject);
                if (DBResponse != "SUCCESS")
                {
                    string ERROR_DESCRIPTION = string.Empty;
                    switch (DBResponse)
                    {
                        case "SI007": ERROR_DESCRIPTION = "DATABASE ERROR";
                            break;

                        case "SI087": ERROR_DESCRIPTION = "REFERENCE NUMBER MUST BE UNIQUE";
                            break;
                    }
                    ResponseObject.ERROR_DESCRIPTION = ERROR_DESCRIPTION;
                    return ResponseObject;
                }
                string MRReferenceNumber = DBObject.generateSBISIRRN(BankName + "TC", "");
                DateTime Request_date_time = DateTime.Now;

                var SICreationResponseString = GenericObject.sendJsonToSIPost("0", Newtonsoft.Json.JsonConvert.SerializeObject(AcntClosureThroughTrnsfrRequest_InheritObj), "AccountClosurePost");
                DateTime Response_Date_Time = DateTime.Now;

                JObject SICreationObj = JObject.Parse(JObject.Parse(SICreationResponseString)["Response"].ToString());

                InternalTransDB InternalDB = new InternalTransDB("TC", RequestObject.REQUEST_REF_NO, MRReferenceNumber, Request_date_time, Response_Date_Time, SICreationObj["RESPONSE_STATUS"].ToString(), "", "AccountClosurePost");
                InternalTransDetails InternalDetail = new InternalTransDetails(MRReferenceNumber, Newtonsoft.Json.JsonConvert.SerializeObject(AcntClosureThroughTrnsfrRequest_InheritObj), SICreationResponseString);

                ListInternalDB.Add(InternalDB);
                ListInternalDetails.Add(InternalDetail);

                ResponseObject.REQUEST_REF_NO = RequestObject.REQUEST_REF_NO;

                if (SICreationObj["RESPONSE_STATUS"].ToString() == "1")
                {
                    TransDBObject.Error_Desc = SICreationObj["ERROR_DESCRIPTION"].ToString();
                    TransDBObject.Res_Time = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");

                    foreach (JProperty propertyName in SICreationObj.Properties())
                    {
                        if (ResponseObject.GetType().GetProperty(propertyName.Name) != null)
                            ResponseObject.GetType().GetProperty(propertyName.Name).SetValue(ResponseObject, SICreationObj[propertyName.Name].ToString());
                    }

                    SIDetail = new SITransDetails(RequestObject.REQUEST_REF_NO, ProductCode, Newtonsoft.Json.JsonConvert.SerializeObject(RequestObject), Newtonsoft.Json.JsonConvert.SerializeObject(ResponseObject), "AcntClosureThroughTrnsfr");

                    ResponseObject.ERROR_DESCRIPTION = TransDBObject.Error_Desc;
                    LoggerImplementObj.InsertInternalTransDBDetails(TransDBObject, SIDetail, ListInternalDB, ListInternalDetails);

                    return ResponseObject;
                }

                foreach (JProperty propertyName in SICreationObj.Properties())
                {
                    if (ResponseObject.GetType().GetProperty(propertyName.Name) != null)
                        ResponseObject.GetType().GetProperty(propertyName.Name).SetValue(ResponseObject, SICreationObj[propertyName.Name].ToString());
                }

                TransDBObject.Res_Time = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                SIDetail1 = new SITransDetails(RequestObject.REQUEST_REF_NO, ProductCode, Newtonsoft.Json.JsonConvert.SerializeObject(RequestObject), JsonConvert.SerializeObject(ResponseObject), "AcntClosureThroughTrnsfr");

                LoggerImplementObj.InsertInternalTransDBDetails(TransDBObject, SIDetail1, ListInternalDB, ListInternalDetails);
                return ResponseObject;
            }
            catch (Exception ex)
            {
                LoggerImplementObj.LoggerObj.WriteExceptionLog(ex.Message, "AcntClosureThroughTrnsfrException");
                TransDBObject.Res_Time = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                TransDBObject.Error_Code = "SI001";
                TransDBObject.Error_Desc = ex.Message.Replace("'", "");
                ResponseObject.ERROR_DESCRIPTION = "Exception while processing the response";

                ResponseObject.REQUEST_REF_NO = RequestObject.REQUEST_REF_NO;

                SIDetail = new SITransDetails(RequestObject.REQUEST_REF_NO, ProductCode, Newtonsoft.Json.JsonConvert.SerializeObject(RequestObject), Newtonsoft.Json.JsonConvert.SerializeObject(ResponseObject), "AcntClosureThroughTrnsfr");

                LoggerImplementObj.InsertInternalTransDBDetails(TransDBObject, SIDetail, ListInternalDB, ListInternalDetails);
            }
            finally
            {
                LoggerImplementObj.LoggerObj.WriteLog(Newtonsoft.Json.JsonConvert.SerializeObject(ResponseObject), "AcntClosureThroughTrnsfrResponse");
                LoggerImplementObj.InsertInternalTransDBDetails(TransDBObject, SIDetail, ListInternalDB, ListInternalDetails);
            }

            return ResponseObject;
        }
    }
}