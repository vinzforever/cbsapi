﻿using LotusWebAPI.Models;
using LotusWebAPI.Provider;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using SBISI.CoreBanking.Common.Provider;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace LotusWebAPI.Controllers
{
    public class CustomerFinancialAmmendmentController : ApiController
    {
        [TokenSecurityFilter.TokenSecurityFilter]
        [HttpPost]
        public CustomerFinancialAmmendmentResponse AccountClosurePost(CustomerFinancialAmmendmentRequest RequestObject)
        {
            if (RequestObject == null)
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound) { Content = new StringContent("SI011^Input string was not in a correct format."), ReasonPhrase = "SI011^Input string was not in a correct format." });

            if (RequestObject.REQUEST_REF_NO.Substring(0, 5) != "SBILT")
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound) { Content = new StringContent("SI094^REFERENCE NUMBER INVALID. REFER DOCUMENT."), ReasonPhrase = "SI094^REFERENCE NUMBER INVALID. REFER DOCUMENT." });

            string ProductCode = "LOTUS";
            LoggerImplement LoggerImplementObj = new LoggerImplement(RequestObject.REQUEST_REF_NO, ProductCode);
            LoggerImplementObj.LoggerObj.WriteLog(Newtonsoft.Json.JsonConvert.SerializeObject(RequestObject), "CustomerFinancialAmmendmentRequest");

            CustomerFinancialAmmendmentResponse ResponseObject = new CustomerFinancialAmmendmentResponse();

            List<InternalTransDB> ListInternalDB = new List<InternalTransDB>();
            List<InternalTransDetails> ListInternalDetails = new List<InternalTransDetails>();

            SITransDetails SIDetail = null;
            SITransDetails SIDetail1 = null;
            SITransDB TransDBObject = null;

            CBSTransaction GenericObject = new CBSTransaction();

            DatabaseOperation DBObject = new DatabaseOperation();
            string BankName = DatabaseOperation.GetBankName(0);

            CustomerFinancialAmmendmentRequestInherit CustomerFinancialAmmendmentRequestInherit_InheritObj = new CustomerFinancialAmmendmentRequestInherit();

            CustomerFinancialAmmendmentRequestInherit_InheritObj.BRANCH_CODE = RequestObject.BRANCH_CODE;
            CustomerFinancialAmmendmentRequestInherit_InheritObj.NAME = RequestObject.NAME;
            CustomerFinancialAmmendmentRequestInherit_InheritObj.CIF_NUMBER = RequestObject.CIF_NUMBER;
            CustomerFinancialAmmendmentRequestInherit_InheritObj.INCOME_AMOUNT = RequestObject.INCOME_AMOUNT;
            CustomerFinancialAmmendmentRequestInherit_InheritObj.FREQUENCY = RequestObject.FREQUENCY;
            CustomerFinancialAmmendmentRequestInherit_InheritObj.RENTAL_INCOME = RequestObject.RENTAL_INCOME;
            CustomerFinancialAmmendmentRequestInherit_InheritObj.FREQUENCY1 = RequestObject.FREQUENCY1;
            CustomerFinancialAmmendmentRequestInherit_InheritObj.OTHER_INCOME = RequestObject.OTHER_INCOME;
            CustomerFinancialAmmendmentRequestInherit_InheritObj.FREQUENCY2 = RequestObject.FREQUENCY2;
            CustomerFinancialAmmendmentRequestInherit_InheritObj.MORTGAGES = RequestObject.MORTGAGES;
            CustomerFinancialAmmendmentRequestInherit_InheritObj.BALANCE_OUTSTANDING1 = RequestObject.BALANCE_OUTSTANDING1;
            CustomerFinancialAmmendmentRequestInherit_InheritObj.MORTGAGE_REPAY = RequestObject.MORTGAGE_REPAY;
            CustomerFinancialAmmendmentRequestInherit_InheritObj.FREQUENCY3 = RequestObject.FREQUENCY3;
            CustomerFinancialAmmendmentRequestInherit_InheritObj.FREQUENCY4 = RequestObject.FREQUENCY4;
            CustomerFinancialAmmendmentRequestInherit_InheritObj.INSURER = RequestObject.INSURER;
            CustomerFinancialAmmendmentRequestInherit_InheritObj.INSURER_PREM = RequestObject.INSURER_PREM;
            CustomerFinancialAmmendmentRequestInherit_InheritObj.FREQUENCY5 = RequestObject.FREQUENCY5;
            CustomerFinancialAmmendmentRequestInherit_InheritObj.LIVING_EXPENSE = RequestObject.LIVING_EXPENSE;
            CustomerFinancialAmmendmentRequestInherit_InheritObj.FREQUENCY6 = RequestObject.FREQUENCY6;
            CustomerFinancialAmmendmentRequestInherit_InheritObj.OTHER_EXPENSE = RequestObject.OTHER_EXPENSE;
            CustomerFinancialAmmendmentRequestInherit_InheritObj.FREQUENCY7 = RequestObject.FREQUENCY7;
            CustomerFinancialAmmendmentRequestInherit_InheritObj.VALUE_OF_HOME = RequestObject.VALUE_OF_HOME;
            CustomerFinancialAmmendmentRequestInherit_InheritObj.EXTENDED_DEPOSITS = RequestObject.EXTENDED_DEPOSITS;
            CustomerFinancialAmmendmentRequestInherit_InheritObj.EXT_INST = RequestObject.EXT_INST;
            CustomerFinancialAmmendmentRequestInherit_InheritObj.CREDIT_RATING = RequestObject.CREDIT_RATING;
            CustomerFinancialAmmendmentRequestInherit_InheritObj.OTHER_ASSETS = RequestObject.OTHER_ASSETS;
            CustomerFinancialAmmendmentRequestInherit_InheritObj.OTHER_CODE = RequestObject.OTHER_CODE;
            CustomerFinancialAmmendmentRequestInherit_InheritObj.DIVIDENT_INTEREST = RequestObject.DIVIDENT_INTEREST;
            CustomerFinancialAmmendmentRequestInherit_InheritObj.FREQUENCY8 = RequestObject.FREQUENCY8;
            CustomerFinancialAmmendmentRequestInherit_InheritObj.OTHER_LOAN = RequestObject.OTHER_LOAN;
            CustomerFinancialAmmendmentRequestInherit_InheritObj.FREQUENCY9 = RequestObject.FREQUENCY9;
            CustomerFinancialAmmendmentRequestInherit_InheritObj.OTHER_LOANS = RequestObject.OTHER_LOANS;
            CustomerFinancialAmmendmentRequestInherit_InheritObj.BALANCE_OUTSTANDING2 = RequestObject.BALANCE_OUTSTANDING2;
            CustomerFinancialAmmendmentRequestInherit_InheritObj.OTHER_LIABILITIES = RequestObject.OTHER_LIABILITIES;
            CustomerFinancialAmmendmentRequestInherit_InheritObj.BALANCE_OUTSTANDING3 = RequestObject.BALANCE_OUTSTANDING3;
            CustomerFinancialAmmendmentRequestInherit_InheritObj.TOTAL_LIABILITIES = RequestObject.TOTAL_LIABILITIES;
            CustomerFinancialAmmendmentRequestInherit_InheritObj.BALANCE_OUTSTANDING4 = RequestObject.BALANCE_OUTSTANDING4;
            CustomerFinancialAmmendmentRequestInherit_InheritObj.GUARANTEES = RequestObject.GUARANTEES;
            CustomerFinancialAmmendmentRequestInherit_InheritObj.BALANCE_OUTSTANDING5 = RequestObject.BALANCE_OUTSTANDING5;
            CustomerFinancialAmmendmentRequestInherit_InheritObj.CASH_BANK_BALANCE = RequestObject.CASH_BANK_BALANCE;
            CustomerFinancialAmmendmentRequestInherit_InheritObj.JEWELLERY = RequestObject.JEWELLERY;
            CustomerFinancialAmmendmentRequestInherit_InheritObj.QUOTED_SHARES_DEBENTURES = RequestObject.QUOTED_SHARES_DEBENTURES;
            CustomerFinancialAmmendmentRequestInherit_InheritObj.TOTAL_ASSETS = RequestObject.TOTAL_ASSETS;
            CustomerFinancialAmmendmentRequestInherit_InheritObj.NET_WORTH = RequestObject.NET_WORTH;
            CustomerFinancialAmmendmentRequestInherit_InheritObj.SOURCE_OF_FUND = RequestObject.SOURCE_OF_FUND;
            CustomerFinancialAmmendmentRequestInherit_InheritObj.ANNUAL_INCOME = RequestObject.ANNUAL_INCOME;
            CustomerFinancialAmmendmentRequestInherit_InheritObj.OPINION_DATE = RequestObject.OPINION_DATE;
            CustomerFinancialAmmendmentRequestInherit_InheritObj.ASSET_LIABILITY_STATEMENT_DATE = RequestObject.ASSET_LIABILITY_STATEMENT_DATE;

            TransDBObject = new SITransDB(RequestObject.REQUEST_REF_NO, ProductCode, DateTime.Now, "", "", "", "CustomerFinancialAmmend", IPAddressInfo.GetIP());
            ResponseObject.REQUEST_REF_NO = RequestObject.REQUEST_REF_NO;

            try
            {
                string DBResponse = LoggerImplementObj.InsertSITransDB(TransDBObject);
                if (DBResponse != "SUCCESS")
                {
                    string ERROR_DESCRIPTION = string.Empty;
                    switch (DBResponse)
                    {
                        case "SI007": ERROR_DESCRIPTION = "DATABASE ERROR";
                            break;

                        case "SI087": ERROR_DESCRIPTION = "REFERENCE NUMBER MUST BE UNIQUE";
                            break;
                    }
                    ResponseObject.ERROR_DESCRIPTION = ERROR_DESCRIPTION;
                    ResponseObject.REQUEST_REF_NO = RequestObject.REQUEST_REF_NO;
                    return ResponseObject;
                }
                string MRReferenceNumber = DBObject.generateSBISIRRN(BankName + "TC", "");
                DateTime Request_date_time = DateTime.Now;

                var SICreationResponseString = GenericObject.sendJsonToSIPost("0", Newtonsoft.Json.JsonConvert.SerializeObject(CustomerFinancialAmmendmentRequestInherit_InheritObj), "CustomerFinancialAmmendmentPost");
                DateTime Response_Date_Time = DateTime.Now;

                JObject SICreationObj = JObject.Parse(JObject.Parse(SICreationResponseString)["Response"].ToString());

                InternalTransDB InternalDB = new InternalTransDB("TC", RequestObject.REQUEST_REF_NO, MRReferenceNumber, Request_date_time, Response_Date_Time, SICreationObj["RESPONSE_STATUS"].ToString(), "", "CustomerFinancialAmmend");
                InternalTransDetails InternalDetail = new InternalTransDetails(MRReferenceNumber, Newtonsoft.Json.JsonConvert.SerializeObject(CustomerFinancialAmmendmentRequestInherit_InheritObj), SICreationResponseString);

                ListInternalDB.Add(InternalDB);
                ListInternalDetails.Add(InternalDetail);

                ResponseObject.REQUEST_REF_NO = RequestObject.REQUEST_REF_NO;

                if (SICreationObj["RESPONSE_STATUS"].ToString() == "1")
                {
                    TransDBObject.Error_Desc = SICreationObj["ERROR_DESCRIPTION"].ToString();
                    TransDBObject.Res_Time = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");

                    foreach (JProperty propertyName in SICreationObj.Properties())
                    {
                        if (ResponseObject.GetType().GetProperty(propertyName.Name) != null)
                            ResponseObject.GetType().GetProperty(propertyName.Name).SetValue(ResponseObject, SICreationObj[propertyName.Name].ToString());
                    }

                    SIDetail = new SITransDetails(RequestObject.REQUEST_REF_NO, ProductCode, Newtonsoft.Json.JsonConvert.SerializeObject(RequestObject), Newtonsoft.Json.JsonConvert.SerializeObject(ResponseObject), "CustomerFinancialAmmend");
                    LoggerImplementObj.InsertInternalTransDBDetails(TransDBObject, SIDetail, ListInternalDB, ListInternalDetails);

                    ResponseObject.ERROR_DESCRIPTION = TransDBObject.Error_Desc;

                    return ResponseObject;
                }

                foreach (JProperty propertyName in SICreationObj.Properties())
                {
                    if (ResponseObject.GetType().GetProperty(propertyName.Name) != null)
                        ResponseObject.GetType().GetProperty(propertyName.Name).SetValue(ResponseObject, SICreationObj[propertyName.Name].ToString());
                }

                TransDBObject.Res_Time = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                SIDetail1 = new SITransDetails(RequestObject.REQUEST_REF_NO, ProductCode, Newtonsoft.Json.JsonConvert.SerializeObject(RequestObject), JsonConvert.SerializeObject(ResponseObject), "CustomerFinancialAmmend");
                LoggerImplementObj.InsertInternalTransDBDetails(TransDBObject, SIDetail1, ListInternalDB, ListInternalDetails);

                return ResponseObject;
            }
            catch (Exception ex)
            {
                LoggerImplementObj.LoggerObj.WriteExceptionLog(ex.Message, "CustomerFinancialAmmendmentException");

                TransDBObject.Res_Time = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                TransDBObject.Error_Code = "SI001";
                TransDBObject.Error_Desc = ex.Message.Replace("'", "");
                ResponseObject.ERROR_DESCRIPTION = "Exception while processing the response";

                ResponseObject.REQUEST_REF_NO = RequestObject.REQUEST_REF_NO;

                SIDetail = new SITransDetails(RequestObject.REQUEST_REF_NO, ProductCode, Newtonsoft.Json.JsonConvert.SerializeObject(RequestObject), Newtonsoft.Json.JsonConvert.SerializeObject(ResponseObject), "CustomerFinancialAmmend");
                LoggerImplementObj.InsertInternalTransDBDetails(TransDBObject, SIDetail1, ListInternalDB, ListInternalDetails);
            }
            finally
            {
                LoggerImplementObj.LoggerObj.WriteLog(Newtonsoft.Json.JsonConvert.SerializeObject(ResponseObject), "CustomerFinancialAmmendmentResponse");
                LoggerImplementObj.InsertInternalTransDBDetails(TransDBObject, SIDetail, ListInternalDB, ListInternalDetails);
            }

            return ResponseObject;
        }
    }
}