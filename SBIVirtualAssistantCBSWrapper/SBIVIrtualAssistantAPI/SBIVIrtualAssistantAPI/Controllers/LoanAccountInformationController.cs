﻿using LotusWebAPI.Models;
using LotusWebAPI.Provider;
using SBISI.CoreBanking.Common.Provider;
using SBISI.CoreBanking.Services.Models;
using SBISI.CoreBanking.Services.Providers;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace LotusWebAPI.Controllers
{
    public class LoanAccountInformationController : ApiController
    {
        [TokenSecurityFilter.TokenSecurityFilter]
        [HttpPost]
        public async Task<GetLoanAccountInformationResponse> GetLoanAccountInformation(GenericEnquiryRequest RequestObject)
        {
            if (RequestObject == null)
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound) { Content = new StringContent("SI011^Input string was not in a correct format."), ReasonPhrase = "SI011^Input string was not in a correct format." });

            if (string.IsNullOrEmpty(RequestObject.ReferenceNumber) || string.IsNullOrEmpty(RequestObject.RequestData))
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound) { Content = new StringContent("SI011^Input string was not in a correct format."), ReasonPhrase = "SI011^Input string was not in a correct format." });

            if (RequestObject.ReferenceNumber.Length != 25)
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound) { Content = new StringContent("SI004^INVALID/NOT-UNIQUE REFERENCE NUMBER"), ReasonPhrase = "SI004^INVALID/NOT-UNIQUE REFERENCE NUMBER" });

            if (RequestObject.Bank_Code > 7)
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound) { Content = new StringContent("SI006^Bank Code Is Not Proper"), ReasonPhrase = "SI006^Bank Code Is Not Proper" });

            if (RequestObject.ReferenceNumber.Substring(0, 5) != "SBILT")
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound) { Content = new StringContent("SI094^REFERENCE NUMBER INVALID. REFER DOCUMENT."), ReasonPhrase = "SI094^REFERENCE NUMBER INVALID. REFER DOCUMENT." });

            string ProductCode = "LOTUS";
            LoggerImplement LoggerImplementObj = new LoggerImplement(RequestObject.ReferenceNumber, ProductCode);
            LoggerImplementObj.LoggerObj.WriteLog(Newtonsoft.Json.JsonConvert.SerializeObject(RequestObject), "GenericEnquiryRequest");

            GetLoanAccountInformationResponse ResponseObject = new GetLoanAccountInformationResponse();

            List<InternalTransDB> ListInternalDB = new List<InternalTransDB>();
            List<InternalTransDetails> ListInternalDetails = new List<InternalTransDetails>();

            SITransDetails SIDetail = null;
            SITransDetails SIDetail1 = null;
            SITransDB TransDBObject = null;

            DatabaseOperation DBObject = new DatabaseOperation();
            string BankName = DatabaseOperation.GetBankName(RequestObject.Bank_Code);
            string MRReferenceNumber = DBObject.generateSBISIRRN(BankName + "MR", "");

            TransDBObject = new SITransDB(RequestObject.ReferenceNumber, ProductCode, DateTime.Now, "", "", "", "LoanAccountInformation", IPAddressInfo.GetIP());

            ResponseObject.ReferenceNumber = RequestObject.ReferenceNumber;
            try
            {
                string DBResponse = LoggerImplementObj.InsertSITransDB(TransDBObject);
                if (DBResponse != "SUCCESS")
                {
                    string ERROR_DESCRIPTION = string.Empty;
                    switch (DBResponse)
                    {
                        case "SI007": ERROR_DESCRIPTION = "DATABASE ERROR";
                            break;

                        case "SI087": ERROR_DESCRIPTION = "REFERENCE NUMBER MUST BE UNIQUE";
                            break;
                    }
                    ResponseObject.ERROR_DESC = ERROR_DESCRIPTION;
                    ResponseObject.ERROR_CODE = DBResponse;
                    ResponseObject.ReferenceNumber = RequestObject.ReferenceNumber;
                    return ResponseObject;
                }
                CoreBankingServices coreService = new CoreBankingServices();
                MultipleRemittenceAccountRequest CBSReuest = new MultipleRemittenceAccountRequest(MRReferenceNumber, Convert.ToInt64(RequestObject.RequestData), BankName);

                var responseobj = await coreService.GetAllLoanAccountInformation(CBSReuest);

                InternalTransDB InternalDB = new InternalTransDB("MR", RequestObject.ReferenceNumber, MRReferenceNumber, responseobj.Item2.RequestDateTime, responseobj.Item2.ResponseDateTime, responseobj.Item2.ErrorCode, responseobj.Item2.ErrorDescription, "GetAllLoanAccountInformation");
                InternalTransDetails InternalDetail = new InternalTransDetails(MRReferenceNumber, responseobj.Item2.RequestString, responseobj.Item2.ResponseString);

                ListInternalDB.Add(InternalDB);
                ListInternalDetails.Add(InternalDetail);

                if (responseobj.Item1 == null || !string.IsNullOrEmpty(responseobj.Item2.ErrorCode))
                {
                    TransDBObject.Error_Code = responseobj.Item2.ErrorCode;
                    TransDBObject.Error_Desc = responseobj.Item2.ErrorDescription;
                    TransDBObject.Res_Time = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                    SIDetail = new SITransDetails(RequestObject.ReferenceNumber, ProductCode, Newtonsoft.Json.JsonConvert.SerializeObject(RequestObject.RequestData), responseobj.Item2.ErrorCode + "^" + responseobj.Item2.ErrorDescription, "LoanAccountInformation");
                    LoggerImplementObj.InsertInternalTransDBDetails(TransDBObject, SIDetail, ListInternalDB, ListInternalDetails);

                    ResponseObject.AccountNumber = RequestObject.RequestData;
                    ResponseObject.ERROR_DESC = TransDBObject.Error_Desc;
                    ResponseObject.ERROR_CODE = TransDBObject.Error_Code;
                    LoggerImplementObj.LoggerObj.WriteLog(Newtonsoft.Json.JsonConvert.SerializeObject(ResponseObject), "GetLoanAccountInformationResponse");
                    return ResponseObject;
                }

                ResponseObject.AccountNumber = RequestObject.RequestData;
                ResponseObject.ReferenceNumber = RequestObject.ReferenceNumber;
                ResponseObject.Purpose = responseobj.Item1.Purpose;
                ResponseObject.LoanAmount = responseobj.Item1.LoanAmount;
                ResponseObject.Tenure = responseobj.Item1.Tenure;
                ResponseObject.RateOfInterest = responseobj.Item1.RateOfInterest;
                ResponseObject.DateOfSanction = responseobj.Item1.DateOfSanction;
                ResponseObject.RepaymentStartDate = responseobj.Item1.RepaymentStartDate;
                ResponseObject.OutstandingAmount = responseobj.Item1.Outstanding;
                ResponseObject.DisbursementDate = responseobj.Item1.DisbursementDate;
                ResponseObject.IrregularAmount = responseobj.Item1.IrregularAmount;
                ResponseObject.IrregularityDate = responseobj.Item1.IrregularityDate;
                ResponseObject.IRACStatus = responseobj.Item1.IRACStatus;
                ResponseObject.InstallmentDueDate = responseobj.Item1.InstallmentDueDate;
                ResponseObject.EMIAmount = responseobj.Item1.EMIAmount;
                ResponseObject.AccountName = responseobj.Item1.AccountName;
                ResponseObject.CustomerName = responseobj.Item1.CustomerName;
                ResponseObject.GLClassCode = responseobj.Item1.GLClassCode;
                ResponseObject.AppliedAmount = responseobj.Item1.AppliedAmount;
                ResponseObject.AdvanceAmount = responseobj.Item1.AdvanceAmount;
                ResponseObject.AddLoan = responseobj.Item1.AddLoan;
                ResponseObject.AccountTypeChange = responseobj.Item1.AccountTypeChg;
                ResponseObject.branchcode = responseobj.Item1.branchcode;
                ResponseObject.Insu = responseobj.Item1.Insu;
                ResponseObject.RepaymentDate = responseobj.Item1.RepaymentDate;
                ResponseObject.LastMaintenanceDate = responseobj.Item1.LastMaintenanceDate;
                ResponseObject.ApprovalDate = responseobj.Item1.ApprovalDate;
                ResponseObject.RepaymentRate = responseobj.Item1.RepaymentRate;
                ResponseObject.LastFinDate = responseobj.Item1.LastFinDate;
                ResponseObject.RepayOption = responseobj.Item1.RepayOption;
                ResponseObject.LastAdvDate = responseobj.Item1.LastAdvDate;
                ResponseObject.IntPrePayAmount = responseobj.Item1.IPAmount;
                ResponseObject.Theo = responseobj.Item1.Theo;
                ResponseObject.OldIracStatus = responseobj.Item1.OldIracStatus;
                ResponseObject.StatementFrequency = responseobj.Item1.StatementFrequency;
                ResponseObject.StatementCycle = responseobj.Item1.StatementCycle;
                ResponseObject.CurrYearTaxDeduction = responseobj.Item1.CYYTDIN;
                ResponseObject.PrevYearTaxDeduction = responseobj.Item1.PYYTDIN;
                ResponseObject.AdvanceValue = responseobj.Item1.AdvanceValue;
                ResponseObject.SecurityAmount = responseobj.Item1.SecurityAmount;

                LoggerImplementObj.LoggerObj.WriteLog(Newtonsoft.Json.JsonConvert.SerializeObject(ResponseObject), "GetLoanAccountInformationResponse");

                TransDBObject.Res_Time = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                SIDetail1 = new SITransDetails(RequestObject.ReferenceNumber, ProductCode, Newtonsoft.Json.JsonConvert.SerializeObject(RequestObject), Newtonsoft.Json.JsonConvert.SerializeObject(ResponseObject), "LoanAccountInformation");
                LoggerImplementObj.InsertInternalTransDBDetails(TransDBObject, SIDetail1, ListInternalDB, ListInternalDetails);

                return ResponseObject;
            }
            catch (Exception ex)
            {
                LoggerImplementObj.LoggerObj.WriteExceptionLog(ex.Message, "LoanAccountInformationException");

                TransDBObject.Res_Time = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                TransDBObject.Error_Code = "SI001";
                TransDBObject.Error_Desc = ex.Message.Replace("'", "");
                ResponseObject.ERROR_DESC = "Exception while processing the response";
                ResponseObject.AccountNumber = RequestObject.RequestData;

                SIDetail1 = new SITransDetails(RequestObject.ReferenceNumber, ProductCode, Newtonsoft.Json.JsonConvert.SerializeObject(RequestObject), Newtonsoft.Json.JsonConvert.SerializeObject(ResponseObject), "LoanAccountInformation");
                LoggerImplementObj.InsertInternalTransDBDetails(TransDBObject, SIDetail1, ListInternalDB, ListInternalDetails);
            }
            finally
            {
                LoggerImplementObj.LoggerObj.WriteLog(Newtonsoft.Json.JsonConvert.SerializeObject(ResponseObject), "GetLoanAccountInformationResponse");
                LoggerImplementObj.InsertInternalTransDBDetails(TransDBObject, SIDetail, ListInternalDB, ListInternalDetails);
            }

            return ResponseObject;
        }
    }
}