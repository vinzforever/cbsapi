﻿using LotusWebAPI.Models;
using LotusWebAPI.Provider;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using SBISI.CoreBanking.Common.Provider;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace LotusWebAPI.Controllers
{
    public class PreApprovedLoanController : ApiController
    {
        [TokenSecurityFilter.TokenSecurityFilter]
        [HttpPost]
        public PreApprovedLoanResponse PreApprovedLoanPost(PreApprovedLoanRequest RequestObject)
        {
            if (RequestObject == null)
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound) { Content = new StringContent("SI011^Input string was not in a correct format."), ReasonPhrase = "SI011^Input string was not in a correct format." });

            if (RequestObject.REFERENCE_NUMBER.Substring(0, 5) != "SBILT")
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound) { Content = new StringContent("SI094^REFERENCE NUMBER INVALID. REFER DOCUMENT."), ReasonPhrase = "SI094^REFERENCE NUMBER INVALID. REFER DOCUMENT." });

            string ProductCode = "LOTUS";
            LoggerImplement LoggerImplementObj = new LoggerImplement(RequestObject.REFERENCE_NUMBER, ProductCode);
            LoggerImplementObj.LoggerObj.WriteLog(Newtonsoft.Json.JsonConvert.SerializeObject(RequestObject), "PreApprovedLoanRequest");

            PreApprovedLoanResponse ResponseObject = new PreApprovedLoanResponse();

            List<InternalTransDB> ListInternalDB = new List<InternalTransDB>();
            List<InternalTransDetails> ListInternalDetails = new List<InternalTransDetails>();

            SITransDetails SIDetail = null;
            SITransDetails SIDetail1 = null;
            SITransDB TransDBObject = null;

            CBSTransaction GenericObject = new CBSTransaction();

            DatabaseOperation DBObject = new DatabaseOperation();
            string BankName = DatabaseOperation.GetBankName(0);

            PreApprovedLoanRequest RequestObjectPost = new PreApprovedLoanRequest();
            RequestObjectPost.REFERENCE_NUMBER = RequestObject.REFERENCE_NUMBER;
            //RequestObjectPost.OCCUPATION_CODE = RequestObject.OCCUPATION_CODE;
            //RequestObjectPost.DATE_OF_BIRTH = RequestObject.DATE_OF_BIRTH;
            RequestObjectPost.BANKFACILITY = RequestObject.BANKFACILITY;
            RequestObjectPost.ACTIVITY_CODE = RequestObject.ACTIVITY_CODE;
            RequestObjectPost.APPLICATION_AMOUNT = RequestObject.APPLICATION_AMOUNT;
            RequestObjectPost.APPROVAL_DATE = RequestObject.APPROVAL_DATE;
            RequestObjectPost.CUSTOMER_NUMBER = RequestObject.CUSTOMER_NUMBER;
            RequestObjectPost.FREQUENCY = RequestObject.FREQUENCY;
            RequestObjectPost.INTEREST_REPAY_START = RequestObject.INTEREST_REPAY_START;
            RequestObjectPost.LOAN_BASIS = RequestObject.LOAN_BASIS;
            RequestObjectPost.LOAN_TERM = RequestObject.LOAN_TERM;
            RequestObjectPost.MOBILISER = RequestObject.MOBILISER;
            RequestObjectPost.MORATORIUM_TERM = RequestObject.MORATORIUM_TERM;
            RequestObjectPost.PRINCIPAL_REPAY_START = RequestObject.PRINCIPAL_REPAY_START;
            RequestObjectPost.PRODUCT = RequestObject.PRODUCT;
            RequestObjectPost.REPAY_DAY = RequestObject.REPAY_DAY;
            RequestObjectPost.SCHEME_CODE = RequestObject.SCHEME_CODE;
            RequestObjectPost.SEGMENT_CODE = RequestObject.SEGMENT_CODE;
            RequestObjectPost.TO_DEPOSIT_ACCOUNT = RequestObject.TO_DEPOSIT_ACCOUNT;
            RequestObjectPost.CURRENCY = RequestObject.CURRENCY;

            TransDBObject = new SITransDB(RequestObject.REFERENCE_NUMBER, ProductCode, DateTime.Now, "", "", "", "PreApprovedLoanReference", IPAddressInfo.GetIP());
            ResponseObject.REFERENCE_NUMBER = RequestObject.REFERENCE_NUMBER;

            try
            {
                string DBResponse = LoggerImplementObj.InsertSITransDB(TransDBObject);
                if (DBResponse != "SUCCESS")
                {
                    string ERROR_DESCRIPTION = string.Empty;
                    switch (DBResponse)
                    {
                        case "SI007": ERROR_DESCRIPTION = "DATABASE ERROR";
                            break;

                        case "SI087": ERROR_DESCRIPTION = "REFERENCE NUMBER MUST BE UNIQUE";
                            break;
                    }
                    ResponseObject.ERROR_DESCRIPTION = ERROR_DESCRIPTION;
                    ResponseObject.REFERENCE_NUMBER = RequestObject.REFERENCE_NUMBER;
                    return ResponseObject;
                }
                string MRReferenceNumber = DBObject.generateSBISIRRN(BankName + "TC", "");
                DateTime Request_date_time = DateTime.Now;

                var SICreationResponseString = GenericObject.sendJsonToSIPost("0", Newtonsoft.Json.JsonConvert.SerializeObject(RequestObjectPost), "PreApprovedLoanReferencePost");
                DateTime Response_Date_Time = DateTime.Now;

                JObject SICreationObj = JObject.Parse(SICreationResponseString.ToString());

                InternalTransDB InternalDB = new InternalTransDB("TC", RequestObject.REFERENCE_NUMBER, MRReferenceNumber, Request_date_time, Response_Date_Time, SICreationObj["RESPONSE_STATUS"].ToString(), "", "PreApprovedLoanReferencePost");
                InternalTransDetails InternalDetail = new InternalTransDetails(MRReferenceNumber, Newtonsoft.Json.JsonConvert.SerializeObject(RequestObjectPost), SICreationResponseString);

                ListInternalDB.Add(InternalDB);
                ListInternalDetails.Add(InternalDetail);

                ResponseObject.REFERENCE_NUMBER = RequestObject.REFERENCE_NUMBER;

                if (SICreationObj["RESPONSE_STATUS"].ToString() == "1")
                {
                    TransDBObject.Error_Desc = SICreationObj["ERROR_DESCRIPTION"].ToString();
                    TransDBObject.Res_Time = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");

                    foreach (JProperty propertyName in SICreationObj.Properties())
                    {
                        if (ResponseObject.GetType().GetProperty(propertyName.Name) != null)
                            ResponseObject.GetType().GetProperty(propertyName.Name).SetValue(ResponseObject, SICreationObj[propertyName.Name].ToString());
                    }

                    SIDetail = new SITransDetails(RequestObject.REFERENCE_NUMBER, ProductCode, Newtonsoft.Json.JsonConvert.SerializeObject(RequestObject), Newtonsoft.Json.JsonConvert.SerializeObject(ResponseObject), "PreApprovedLoanReference");
                    LoggerImplementObj.InsertInternalTransDBDetails(TransDBObject, SIDetail, ListInternalDB, ListInternalDetails);

                    ResponseObject.ERROR_DESCRIPTION = TransDBObject.Error_Desc;

                    return ResponseObject;
                }

                foreach (JProperty propertyName in SICreationObj.Properties())
                {
                    if (ResponseObject.GetType().GetProperty(propertyName.Name) != null)
                        ResponseObject.GetType().GetProperty(propertyName.Name).SetValue(ResponseObject, SICreationObj[propertyName.Name].ToString());
                }

                TransDBObject.Res_Time = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                SIDetail1 = new SITransDetails(RequestObject.REFERENCE_NUMBER, ProductCode, Newtonsoft.Json.JsonConvert.SerializeObject(RequestObject), JsonConvert.SerializeObject(ResponseObject), "PreApprovedLoanReference");

                LoggerImplementObj.InsertInternalTransDBDetails(TransDBObject, SIDetail1, ListInternalDB, ListInternalDetails);

                return ResponseObject;
            }
            catch (Exception ex)
            {
                LoggerImplementObj.LoggerObj.WriteExceptionLog(ex.Message, "PreApprovedLoanException");

                TransDBObject.Res_Time = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                TransDBObject.Error_Code = "SI001";
                TransDBObject.Error_Desc = ex.Message.Replace("'", "");
                ResponseObject.ERROR_DESCRIPTION = "Exception while processing the response";

                ResponseObject.REFERENCE_NUMBER = RequestObject.REFERENCE_NUMBER;

                SIDetail = new SITransDetails(RequestObject.REFERENCE_NUMBER, ProductCode, Newtonsoft.Json.JsonConvert.SerializeObject(RequestObject), Newtonsoft.Json.JsonConvert.SerializeObject(ResponseObject), "PreApprovedLoanReference");
                LoggerImplementObj.InsertInternalTransDBDetails(TransDBObject, SIDetail, ListInternalDB, ListInternalDetails);
            }
            finally
            {
                LoggerImplementObj.LoggerObj.WriteLog(Newtonsoft.Json.JsonConvert.SerializeObject(ResponseObject), "PreApprovedLoanResponse");
                LoggerImplementObj.InsertInternalTransDBDetails(TransDBObject, SIDetail, ListInternalDB, ListInternalDetails);
            }

            return ResponseObject;
        }
    }
}