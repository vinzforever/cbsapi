﻿using LotusWebAPI.Models;
using LotusWebAPI.Provider;
using SBISI.CoreBanking.Common.Provider;
using SBISI.CoreBanking.Intouch.Providers;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace LotusWebAPI.Controllers
{
    public class PostingRestrictionController : ApiController
    {
        [TokenSecurityFilter.TokenSecurityFilter]
        [HttpPost]
        public async Task<BancsResponse> PostingRestrictionPost(BancAccountRequest RequestObject)
        {
            if (RequestObject == null)
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound) { Content = new StringContent("SI011^Input string was not in a correct format."), ReasonPhrase = "SI011^Input string was not in a correct format." });

            if (string.IsNullOrEmpty(RequestObject.Branch_Code) || string.IsNullOrEmpty(RequestObject.ReferenceNumber) || string.IsNullOrEmpty(RequestObject.Branch_Code) || string.IsNullOrEmpty(RequestObject.Checker_ID) || string.IsNullOrEmpty(RequestObject.Request) || string.IsNullOrEmpty(RequestObject.Teller_ID))
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound) { Content = new StringContent("SI011^Input string was not in a correct format."), ReasonPhrase = "SI011^Input string was not in a correct format." });

            if (RequestObject.ReferenceNumber.Length != 25)
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound) { Content = new StringContent("SI004^INVALID/NOT-UNIQUE REFERENCE NUMBER"), ReasonPhrase = "SI004^INVALID/NOT-UNIQUE REFERENCE NUMBER" });

            if (RequestObject.Bank_Code > 7)
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound) { Content = new StringContent("SI006^Bank Code Is Not Proper"), ReasonPhrase = "SI006^Bank Code Is Not Proper" });

            if (RequestObject.ReferenceNumber.Substring(0, 5) != "SBILT")
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound) { Content = new StringContent("SI094^REFERENCE NUMBER INVALID. REFER DOCUMENT."), ReasonPhrase = "SI094^REFERENCE NUMBER INVALID. REFER DOCUMENT." });

            string ProductCode = "LOTUS";
            LoggerImplement LoggerImplementObj = new LoggerImplement(RequestObject.ReferenceNumber, ProductCode);
            LoggerImplementObj.LoggerObj.WriteLog(Newtonsoft.Json.JsonConvert.SerializeObject(RequestObject), "BancAccountRequest");

            DatabaseOperation dbobject = new DatabaseOperation();
            string BankName = DatabaseOperation.GetBankName(RequestObject.Bank_Code);

            BancsResponse ResponseObject = new BancsResponse();

            List<InternalTransDB> ListInternalDB = new List<InternalTransDB>();
            List<InternalTransDetails> ListInternalDetails = new List<InternalTransDetails>();

            SITransDetails SIDetail = null;
            SITransDetails SIDetail1 = null;
            SITransDB TransDBObject = null;

            ResponseObject.ReferenceNumber = RequestObject.ReferenceNumber;
            try
            {
                TransDBObject = new SITransDB(RequestObject.ReferenceNumber, ProductCode, DateTime.Now, "", "", "", "PostingRestriction", IPAddressInfo.GetIP());
                string DBResponse = LoggerImplementObj.InsertSITransDB(TransDBObject);
                if (DBResponse != "SUCCESS")
                {
                    string ERROR_DESCRIPTION = string.Empty;
                    switch (DBResponse)
                    {
                        case "SI007": ERROR_DESCRIPTION = "DATABASE ERROR";
                            break;

                        case "SI087": ERROR_DESCRIPTION = "REFERENCE NUMBER MUST BE UNIQUE";
                            break;
                    }
                    ResponseObject.ErrorDescription = ERROR_DESCRIPTION;
                    ResponseObject.ErrorCode = DBResponse;
                    ResponseObject.ReferenceNumber = RequestObject.ReferenceNumber;
                    return ResponseObject;
                }
                CoreBankingIntouchServices coreService = new CoreBankingIntouchServices();
                string BancsReferenceNumber = dbobject.generateSBISIRRN(BankName + "BA", "");

                string PostingRequest = RequestObject.Request.PadLeft(17, '0') + "00000000000000000000000000000000                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  ";
                SBISI.CoreBanking.Intouch.Models.CBSGenericRequest CBSReuest = new SBISI.CoreBanking.Intouch.Models.CBSGenericRequest(BankName, RequestObject.ReferenceNumber.Substring(19), RequestObject.Branch_Code, RequestObject.Teller_ID, RequestObject.Checker_ID, PostingRequest);
                var NewCIFResponse = await coreService.PostingRestriction(CBSReuest);
                InternalTransDB InternalDB = new InternalTransDB("BA", RequestObject.ReferenceNumber, BancsReferenceNumber, NewCIFResponse.Item2.RequestDateTime, NewCIFResponse.Item2.ResponseDateTime, NewCIFResponse.Item2.ErrorCode, NewCIFResponse.Item2.ErrorDescription, "PostingRestriction");
                InternalTransDetails InternalDetail = new InternalTransDetails(BancsReferenceNumber, NewCIFResponse.Item2.RequestString, NewCIFResponse.Item2.ResponseString);

                ListInternalDB.Add(InternalDB);
                ListInternalDetails.Add(InternalDetail);

                if (NewCIFResponse.Item1 == null || !string.IsNullOrEmpty(NewCIFResponse.Item2.ErrorCode))
                {
                    TransDBObject.Error_Code = NewCIFResponse.Item2.ErrorCode;
                    TransDBObject.Error_Desc = NewCIFResponse.Item2.ErrorDescription;
                    ResponseObject.ErrorCode = NewCIFResponse.Item2.ErrorCode;
                    ResponseObject.ErrorDescription = NewCIFResponse.Item2.ErrorDescription;
                    TransDBObject.Res_Time = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                    SIDetail = new SITransDetails(RequestObject.ReferenceNumber, ProductCode, RequestObject.TostringContent(), Newtonsoft.Json.JsonConvert.SerializeObject(ResponseObject), "PostingRestriction");
                    LoggerImplementObj.InsertInternalTransDBDetails(TransDBObject, SIDetail, ListInternalDB, ListInternalDetails);

                    LoggerImplementObj.LoggerObj.WriteLog(Newtonsoft.Json.JsonConvert.SerializeObject(ResponseObject), "BancsResponse");

                    return ResponseObject;
                }
                ResponseObject.Data = NewCIFResponse.Item1.MainData;
                ResponseObject.JournalNumber = NewCIFResponse.Item1.JournalNumber;
                ResponseObject.BranchCode = NewCIFResponse.Item1.BranchCode;
                ResponseObject.Status = "O.K.";

                LoggerImplementObj.LoggerObj.WriteLog(Newtonsoft.Json.JsonConvert.SerializeObject(ResponseObject), "BancsResponse");

                TransDBObject.Res_Time = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                SIDetail1 = new SITransDetails(RequestObject.ReferenceNumber, ProductCode, RequestObject.TostringContent(), Newtonsoft.Json.JsonConvert.SerializeObject(ResponseObject), "PostingRestriction");
                LoggerImplementObj.InsertInternalTransDBDetails(TransDBObject, SIDetail1, ListInternalDB, ListInternalDetails);

                return ResponseObject;
            }
            catch (Exception ex)
            {
                LoggerImplementObj.LoggerObj.WriteExceptionLog(ex.Message, "PostingRestrictionException");

                TransDBObject.Res_Time = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                TransDBObject.Error_Code = "SI001";
                TransDBObject.Error_Desc = ex.Message.Replace("'", "");
                ResponseObject.ErrorDescription = "Exception while processing the response";

                SIDetail1 = new SITransDetails(RequestObject.ReferenceNumber, ProductCode, Newtonsoft.Json.JsonConvert.SerializeObject(RequestObject), Newtonsoft.Json.JsonConvert.SerializeObject(ResponseObject), "LoanAccountInformation");
                LoggerImplementObj.InsertInternalTransDBDetails(TransDBObject, SIDetail1, ListInternalDB, ListInternalDetails);
            }
            finally
            {
                LoggerImplementObj.LoggerObj.WriteLog(Newtonsoft.Json.JsonConvert.SerializeObject(ResponseObject), "GetLoanAccountInformationResponse");
                LoggerImplementObj.InsertInternalTransDBDetails(TransDBObject, SIDetail, ListInternalDB, ListInternalDetails);
            }

            return ResponseObject;
        }
    }
}