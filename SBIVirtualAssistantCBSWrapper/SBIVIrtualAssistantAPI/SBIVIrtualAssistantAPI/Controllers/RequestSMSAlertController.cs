﻿using LotusWebAPI.Models;
using LotusWebAPI.Provider;
using SBISI.CoreBanking.Common.Provider;
using SBISI.CoreBanking.Transaction.Services.Models;
using SBISI.CoreBanking.Transaction.Services.Provider;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace LotusWebAPI.Controllers
{
    public class RequestSMSAlertController : ApiController
    {
        [TokenSecurityFilter.TokenSecurityFilter]
        public async Task<BancsResponse> post(SmsAlertRequest RequestObject)
        {
            if (RequestObject == null)
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound) { Content = new StringContent("SI011^Input string was not in a correct format."), ReasonPhrase = "SI011^Input string was not in a correct format." });

            if (string.IsNullOrEmpty(RequestObject.ReferenceNumber))
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound) { Content = new StringContent("SI011^Input string was not in a correct format."), ReasonPhrase = "SI011^Input string was not in a correct format." });

            if (RequestObject.ReferenceNumber.Length != 25)
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound) { Content = new StringContent("SI004^INVALID/NOT-UNIQUE REFERENCE NUMBER"), ReasonPhrase = "SI004^INVALID/NOT-UNIQUE REFERENCE NUMBER" });

            if (RequestObject.Bank_Code > 7)
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound) { Content = new StringContent("SI006^Bank Code Is Not Proper"), ReasonPhrase = "SI006^Bank Code Is Not Proper" });

            if (RequestObject.ReferenceNumber.Substring(0, 5) != "SBILT")
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound) { Content = new StringContent("SI094^REFERENCE NUMBER INVALID. REFER DOCUMENT."), ReasonPhrase = "SI094^REFERENCE NUMBER INVALID. REFER DOCUMENT." });

            string ProductCode = "LOTUS";
            LoggerImplement LoggerImplementObj = new LoggerImplement(RequestObject.ReferenceNumber, ProductCode);
            LoggerImplementObj.LoggerObj.WriteLog(Newtonsoft.Json.JsonConvert.SerializeObject(RequestObject), "SmsAlertRequest");

            DatabaseOperation dbobject = new DatabaseOperation();
            string BankName = DatabaseOperation.GetBankName(RequestObject.Bank_Code);

            BancsResponse ResponseObject = new BancsResponse();

            List<InternalTransDB> ListInternalDB = new List<InternalTransDB>();
            List<InternalTransDetails> ListInternalDetails = new List<InternalTransDetails>();

            SITransDetails SIDetail = null;
            SITransDetails SIDetail1 = null;
            SITransDB TransDBObject = null;

            ResponseObject.ReferenceNumber = RequestObject.ReferenceNumber;
            try
            {
                TransDBObject = new SITransDB(RequestObject.ReferenceNumber, ProductCode, DateTime.Now, "", "", "", "RequestSMSAlert", IPAddressInfo.GetIP());

                string DBResponse = LoggerImplementObj.InsertSITransDB(TransDBObject);
                if (DBResponse != "SUCCESS")
                {
                    string ERROR_DESCRIPTION = string.Empty;
                    switch (DBResponse)
                    {
                        case "SI007": ERROR_DESCRIPTION = "DATABASE ERROR";
                            break;

                        case "SI087": ERROR_DESCRIPTION = "REFERENCE NUMBER MUST BE UNIQUE";
                            break;
                    }
                    ResponseObject.ErrorDescription = ERROR_DESCRIPTION;
                    ResponseObject.ErrorCode = DBResponse;
                    ResponseObject.ReferenceNumber = RequestObject.ReferenceNumber;
                    return ResponseObject;
                }

                CoreBankingTransactionServices coreService = new CoreBankingTransactionServices();
                string INBReferenceNumberUpd = dbobject.generateSBISIRRN(BankName + "IN", "");
                SMS7004UpdateRequest INBRequestObjectUpd = new SMS7004UpdateRequest();
                INBRequestObjectUpd.BankName = BankName;
                INBRequestObjectUpd.ReferenceNumber = RequestObject.ReferenceNumber;
                INBRequestObjectUpd.AccountNumber = RequestObject.AccountNumber;
                INBRequestObjectUpd.SetHold = RequestObject.SetHold;
                INBRequestObjectUpd.RemoveHold = RequestObject.RemoveHold;
                INBRequestObjectUpd.ChequeStopAlert = RequestObject.ChequeStopAlert;
                INBRequestObjectUpd.POSTransaction = RequestObject.POSTransaction;
                INBRequestObjectUpd.ChequeIssueAlert = RequestObject.ChequeIssueAlert;
                INBRequestObjectUpd.ChequeReturnAlert = RequestObject.ChequeReturnAlert;
                INBRequestObjectUpd.CreditThreshold = RequestObject.CreditThreshold;
                INBRequestObjectUpd.DebitThreshold = RequestObject.DebitThreshold;
                INBRequestObjectUpd.ThresholdBalance = RequestObject.ThresholdBalance;
                INBRequestObjectUpd.SMSFlag = RequestObject.SMSFlag;
                INBRequestObjectUpd.CountryCode = "IN";
                INBRequestObjectUpd.SetHold = RequestObject.SetHold;
                INBRequestObjectUpd.MobileNumber = RequestObject.MobileNumber.PadRight(12, ' ');

                var GetSMSAlertResponse = await coreService.UpdateSMSAlertInfo(INBRequestObjectUpd);

                InternalTransDB InternalDB = new InternalTransDB("IN", RequestObject.ReferenceNumber, INBReferenceNumberUpd, GetSMSAlertResponse.Item2.RequestDateTime, GetSMSAlertResponse.Item2.ResponseDateTime, GetSMSAlertResponse.Item2.ErrorCode, GetSMSAlertResponse.Item2.ErrorDescription, "UpdateSMSAlertInfo");
                InternalTransDetails InternalDetail = new InternalTransDetails(INBReferenceNumberUpd, GetSMSAlertResponse.Item2.RequestString, GetSMSAlertResponse.Item2.ResponseString);

                string responseString = string.Empty;

                ListInternalDB.Add(InternalDB);
                ListInternalDetails.Add(InternalDetail);

                if (GetSMSAlertResponse.Item1 == null || !string.IsNullOrEmpty(GetSMSAlertResponse.Item2.ErrorCode))
                {
                    TransDBObject.Error_Code = GetSMSAlertResponse.Item2.ErrorCode;
                    TransDBObject.Error_Desc = GetSMSAlertResponse.Item2.ErrorDescription;
                    TransDBObject.Res_Time = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");

                    ResponseObject.ErrorCode = GetSMSAlertResponse.Item2.ErrorCode;
                    ResponseObject.ErrorDescription = GetSMSAlertResponse.Item2.ErrorDescription;

                    SIDetail = new SITransDetails(RequestObject.ReferenceNumber, ProductCode, Newtonsoft.Json.JsonConvert.SerializeObject(RequestObject), Newtonsoft.Json.JsonConvert.SerializeObject(ResponseObject), "UpdateSMSAlertInformation");
                    LoggerImplementObj.InsertInternalTransDBDetails(TransDBObject, SIDetail, ListInternalDB, ListInternalDetails);

                    return ResponseObject;
                }

                ResponseObject.JournalNumber = GetSMSAlertResponse.Item1.JournalNumber;
                ResponseObject.BranchCode = GetSMSAlertResponse.Item1.BranchCode;
                ResponseObject.Status = "O.K.";

                LoggerImplementObj.LoggerObj.WriteLog(Newtonsoft.Json.JsonConvert.SerializeObject(ResponseObject), "BancsResponse");

                TransDBObject.Res_Time = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                SIDetail1 = new SITransDetails(RequestObject.ReferenceNumber, ProductCode, RequestObject.TostringContent(), Newtonsoft.Json.JsonConvert.SerializeObject(ResponseObject), "RequestSMSAlert");
                LoggerImplementObj.InsertInternalTransDBDetails(TransDBObject, SIDetail1, ListInternalDB, ListInternalDetails);

                return ResponseObject;
            }
            catch (Exception ex)
            {
                LoggerImplementObj.LoggerObj.WriteExceptionLog(ex.Message, "RequestSMSAlertException");

                TransDBObject.Res_Time = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                TransDBObject.Error_Code = "SI001";
                TransDBObject.Error_Desc = ex.Message.Replace("'", "");
                ResponseObject.ErrorDescription = "Exception while processing the response";
                //ResponseObject.AccountNumber = RequestObject.RequestData;

                SIDetail1 = new SITransDetails(RequestObject.ReferenceNumber, ProductCode, Newtonsoft.Json.JsonConvert.SerializeObject(RequestObject), Newtonsoft.Json.JsonConvert.SerializeObject(ResponseObject), "LoanAccountInformation");
                LoggerImplementObj.InsertInternalTransDBDetails(TransDBObject, SIDetail1, ListInternalDB, ListInternalDetails);
            }
            finally
            {
                LoggerImplementObj.LoggerObj.WriteLog(Newtonsoft.Json.JsonConvert.SerializeObject(ResponseObject), "BancsResponse");
                LoggerImplementObj.InsertInternalTransDBDetails(TransDBObject, SIDetail, ListInternalDB, ListInternalDetails);
            }

            return ResponseObject;
        }
    }
}