﻿using LotusWebAPI.Models;
using LotusWebAPI.Provider;
using SBISI.CoreBanking.Transaction.Services.Provider;
using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace LotusWebAPI.Controllers
{
    public class FundTransferController : ApiController
    {
        [TokenSecurityFilter.TokenSecurityFilter]
        [HttpPost]
        public async Task<TxnResponse> TransferAction(TxnRequest RequestObject)
        {
            if (RequestObject == null)
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound) { Content = new StringContent("SI011^Input string was not in a correct format."), ReasonPhrase = "SI011^Input string was not in a correct format." });

            if (string.IsNullOrEmpty(RequestObject.BranchCode) || string.IsNullOrEmpty(RequestObject.ReferenceNumber) || string.IsNullOrEmpty(RequestObject.Amount) || string.IsNullOrEmpty(RequestObject.CreditAccount) || string.IsNullOrEmpty(RequestObject.DebitAccount) || string.IsNullOrEmpty(RequestObject.TransactionType))
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound) { Content = new StringContent("SI011^Input string was not in a correct format."), ReasonPhrase = "SI011^Input string was not in a correct format." });

            if (RequestObject.ReferenceNumber.Length != 25)
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound) { Content = new StringContent("SI004^INVALID/NOT-UNIQUE REFERENCE NUMBER"), ReasonPhrase = "SI004^INVALID/NOT-UNIQUE REFERENCE NUMBER" });

            if (RequestObject.DebitAccount.Length < 10 || RequestObject.CreditAccount.Length < 10)
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound) { Content = new StringContent("SI023^INCORRECT CREDIT/DEBIT ACCOUNT"), ReasonPhrase = "SI023^INCORRECT CREDIT/DEBIT ACCOUNT" });

            if (RequestObject.BankCode > 7)
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound) { Content = new StringContent("SI006^Bank Code Is Not Proper"), ReasonPhrase = "SI006^Bank Code Is Not Proper" });

            if (RequestObject.ReferenceNumber.Substring(0, 5) != "SBILT")
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound) { Content = new StringContent("SI094^REFERENCE NUMBER INVALID. REFER DOCUMENT."), ReasonPhrase = "SI094^REFERENCE NUMBER INVALID. REFER DOCUMENT." });

            string ProductCode = "LOTUS";
            LoggerImplement LoggerImplementObj = new LoggerImplement(RequestObject.ReferenceNumber, ProductCode);
            LoggerImplementObj.LoggerObj.WriteLog(Newtonsoft.Json.JsonConvert.SerializeObject(RequestObject), "TxnRequest");

            TxnResponse txnObject = new TxnResponse();

            txnObject.ReferenceNumber = RequestObject.ReferenceNumber;
            RequestObject.ReconReferenceNumber = "";

            RequestObject.Amount = (Convert.ToDouble(RequestObject.Amount) * 100).ToString();
            CBSTransaction cbsObject = new CBSTransaction();
            CoreBankingTransactionServices coreService = new CoreBankingTransactionServices();

            switch (RequestObject.TransactionType)
            {
                case "C2B": txnObject = await cbsObject.C2BPost(RequestObject);

                    break;

                case "B2C": txnObject = await cbsObject.B2CPost(RequestObject);

                    break;

                case "B2B": txnObject = await cbsObject.B2BPost(RequestObject);

                    break;

                case "C2C": txnObject = await cbsObject.C2CPost(RequestObject);

                    break;

                default:
                    txnObject.ErrorCode = "SI005";
                    txnObject.ErrorDesc = "Invalid Transaction Type";
                    break;
            }
            txnObject.ReferenceNumber = RequestObject.ReferenceNumber;
            LoggerImplementObj.LoggerObj.WriteLog(Newtonsoft.Json.JsonConvert.SerializeObject(txnObject), "TxnResponse");

            return txnObject;
        }
    }
}