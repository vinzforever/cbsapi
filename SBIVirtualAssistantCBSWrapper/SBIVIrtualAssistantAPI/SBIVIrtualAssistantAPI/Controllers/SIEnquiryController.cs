﻿using LotusWebAPI.Models;
using LotusWebAPI.Provider;
using Newtonsoft.Json.Linq;
using SBISI.CoreBanking.Common.Provider;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace LotusWebAPI.Controllers
{
    public class SIEnquiryController : ApiController
    {
        [TokenSecurityFilter.TokenSecurityFilter]
        [HttpPost]
        public SIEnquiryResponse GetSIEnquiry(SIEnquiryRequest RequestObject)
        {
            if (RequestObject == null)
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound) { Content = new StringContent("SI011^Input string was not in a correct format."), ReasonPhrase = "SI011^Input string was not in a correct format." });
            if (RequestObject.REQUEST_REF_NO.Substring(0, 5) != "SBILT")
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound) { Content = new StringContent("SI094^REFERENCE NUMBER INVALID. REFER DOCUMENT."), ReasonPhrase = "SI094^REFERENCE NUMBER INVALID. REFER DOCUMENT." });

            string ProductCode = "LOTUS";
            LoggerImplement LoggerImplementObj = new LoggerImplement(RequestObject.REQUEST_REF_NO, ProductCode);
            LoggerImplementObj.LoggerObj.WriteLog(Newtonsoft.Json.JsonConvert.SerializeObject(RequestObject), "SIEnquiryRequest");

            SIEnquiryResponse ResponseObject = new SIEnquiryResponse();

            List<InternalTransDB> ListInternalDB = new List<InternalTransDB>();
            List<InternalTransDetails> ListInternalDetails = new List<InternalTransDetails>();

            SITransDetails SIDetail = null;
            SITransDetails SIDetail1 = null;
            SITransDB TransDBObject = null;

            CBSTransaction GenericObject = new CBSTransaction();

            DatabaseOperation DBObject = new DatabaseOperation();
            string BankName = DatabaseOperation.GetBankName(Convert.ToInt32(RequestObject.BANK_CODE));

            SIEnquiryRequest_Inherit SIEnquiryRequest_InheritObj = new SIEnquiryRequest_Inherit();
            SIEnquiryRequest_InheritObj.Account_Number = RequestObject.Account_Number;
            SIEnquiryRequest_InheritObj.BANK_CODE = RequestObject.BANK_CODE;
            SIEnquiryRequest_InheritObj.BRANCH_CODE = RequestObject.BRANCH_CODE;
            SIEnquiryRequest_InheritObj.SDVOrSC = RequestObject.SDVOrSC;
            SIEnquiryRequest_InheritObj.REQUEST_REF_NO = RequestObject.REQUEST_REF_NO;

            TransDBObject = new SITransDB(RequestObject.REQUEST_REF_NO, ProductCode, DateTime.Now, "", "", "", "SIEnquiry", IPAddressInfo.GetIP());
            ResponseObject.REQUEST_REF_NO = RequestObject.REQUEST_REF_NO;

            try
            {
                string DBResponse = LoggerImplementObj.InsertSITransDB(TransDBObject);
                if (DBResponse != "SUCCESS")
                {
                    string ERROR_DESCRIPTION = string.Empty;
                    switch (DBResponse)
                    {
                        case "SI007": ERROR_DESCRIPTION = "DATABASE ERROR";
                            break;

                        case "SI087": ERROR_DESCRIPTION = "REFERENCE NUMBER MUST BE UNIQUE";
                            break;
                    }
                    ResponseObject.ERROR_DESCRIPTION = ERROR_DESCRIPTION;
                    ResponseObject.REQUEST_REF_NO = RequestObject.REQUEST_REF_NO;
                    return ResponseObject;
                }

                var SIEnquiryDetailsString = GenericObject.sendJsonToSI("0", Newtonsoft.Json.JsonConvert.SerializeObject(SIEnquiryRequest_InheritObj), "GetSIEnquiry");
                LoggerImplementObj.LoggerObj.WriteLog(SIEnquiryDetailsString, "SIEnquiryDetailsString");

                JObject SIEnquiryDetailsObj = JObject.Parse(JObject.Parse(SIEnquiryDetailsString)["Response"].ToString());
                ResponseObject.REQUEST_REF_NO = RequestObject.REQUEST_REF_NO;

                string MRReferenceNumber = DBObject.generateSBISIRRN(BankName + "TC", "");
                DateTime Request_date_time = DateTime.Now;
                DateTime Response_Date_Time = DateTime.Now;

                InternalTransDB InternalDB = new InternalTransDB("TC", RequestObject.REQUEST_REF_NO, MRReferenceNumber, Request_date_time, Response_Date_Time, SIEnquiryDetailsObj["RESPONSE_STATUS"].ToString(), "", "GetSIEnquiry");
                InternalTransDetails InternalDetail = new InternalTransDetails(MRReferenceNumber, Newtonsoft.Json.JsonConvert.SerializeObject(SIEnquiryRequest_InheritObj), SIEnquiryDetailsString);

                ListInternalDB.Add(InternalDB);
                ListInternalDetails.Add(InternalDetail);

                if (SIEnquiryDetailsObj["RESPONSE_STATUS"].ToString() == "1")
                {
                    TransDBObject.Error_Desc = SIEnquiryDetailsObj["ERROR_DESCRIPTION"].ToString();
                    TransDBObject.Res_Time = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");

                    foreach (JProperty propertyName in SIEnquiryDetailsObj.Properties())
                    {
                        if (ResponseObject.GetType().GetProperty(propertyName.Name) != null)
                            ResponseObject.GetType().GetProperty(propertyName.Name).SetValue(ResponseObject, SIEnquiryDetailsObj[propertyName.Name].ToString());
                    }

                    SIDetail = new SITransDetails(RequestObject.REQUEST_REF_NO, ProductCode, Newtonsoft.Json.JsonConvert.SerializeObject(RequestObject), Newtonsoft.Json.JsonConvert.SerializeObject(ResponseObject), "SIEnquiry");
                    LoggerImplementObj.InsertInternalTransDBDetails(TransDBObject, SIDetail, ListInternalDB, ListInternalDetails);

                    ResponseObject.ERROR_DESCRIPTION = TransDBObject.Error_Desc;

                    return ResponseObject;
                }

                foreach (JProperty propertyName in SIEnquiryDetailsObj.Properties())
                {
                    if (propertyName.Name == "Standing_Orders")
                    {
                        if (SIEnquiryDetailsObj["Standing_Orders"].ToString() == "{}")
                        {
                            continue;
                        }
                        JArray NOMINEE_DETAILSObj = JArray.Parse(SIEnquiryDetailsObj["Standing_Orders"]["Standing_Orders_Details"].ToString());
                        ResponseObject.Standing_Orders = new List<Standing_Orders_Details>();
                        foreach (JObject Jobj in NOMINEE_DETAILSObj)
                        {
                            Standing_Orders_Details StandingOrder = new Standing_Orders_Details();
                            foreach (JProperty propName in Jobj.Properties())
                            {
                                if (StandingOrder.GetType().GetProperty(propName.Name) != null)
                                    StandingOrder.GetType().GetProperty(propName.Name).SetValue(StandingOrder, Jobj[propName.Name].ToString());
                            }
                            ResponseObject.Standing_Orders.Add(StandingOrder);
                        }
                    }
                    else
                    {
                        if (ResponseObject.GetType().GetProperty(propertyName.Name) != null)
                            ResponseObject.GetType().GetProperty(propertyName.Name).SetValue(ResponseObject, SIEnquiryDetailsObj[propertyName.Name].ToString());
                    }
                }

                TransDBObject.Res_Time = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                SIDetail1 = new SITransDetails(RequestObject.REQUEST_REF_NO, ProductCode, Newtonsoft.Json.JsonConvert.SerializeObject(RequestObject), Newtonsoft.Json.JsonConvert.SerializeObject(ResponseObject), "SIEnquiry");
                LoggerImplementObj.InsertInternalTransDBDetails(TransDBObject, SIDetail1, ListInternalDB, ListInternalDetails);

                return ResponseObject;
            }
            catch (Exception ex)
            {
                LoggerImplementObj.LoggerObj.WriteExceptionLog(ex.Message, "SIEnquiryException");

                TransDBObject.Res_Time = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                TransDBObject.Error_Code = "SI001";
                TransDBObject.Error_Desc = ex.Message.Replace("'", "");
                ResponseObject.ERROR_DESCRIPTION = "Exception while processing the response";

                ResponseObject.REQUEST_REF_NO = RequestObject.REQUEST_REF_NO;

                SIDetail = new SITransDetails(RequestObject.REQUEST_REF_NO, ProductCode, Newtonsoft.Json.JsonConvert.SerializeObject(RequestObject), Newtonsoft.Json.JsonConvert.SerializeObject(ResponseObject), "SIEnquiry");
                LoggerImplementObj.InsertInternalTransDBDetails(TransDBObject, SIDetail, ListInternalDB, ListInternalDetails);
            }
            finally
            {
                LoggerImplementObj.LoggerObj.WriteLog(Newtonsoft.Json.JsonConvert.SerializeObject(ResponseObject), "SIEnquiryResponse");
                LoggerImplementObj.InsertInternalTransDBDetails(TransDBObject, SIDetail, ListInternalDB, ListInternalDetails);
            }
            return ResponseObject;
        }
    }
}