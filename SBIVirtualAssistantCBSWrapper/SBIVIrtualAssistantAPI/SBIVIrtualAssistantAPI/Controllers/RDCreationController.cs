﻿using LotusWebAPI.Models;
using LotusWebAPI.Provider;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using SBISI.CoreBanking.Common.Provider;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace LotusWebAPI.Controllers
{
    public class RDCreationController : ApiController
    {
        [TokenSecurityFilter.TokenSecurityFilter]
        [HttpPost]
        public RDCreationResponse FDCreationPost(RDCreationRequest RequestObject)
        {
            if (RequestObject == null)
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound) { Content = new StringContent("SI011^Input string was not in a correct format."), ReasonPhrase = "SI011^Input string was not in a correct format." });

            if (RequestObject.REQUEST_REF_NO.Substring(0, 5) != "SBILT")
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound) { Content = new StringContent("SI094^REFERENCE NUMBER INVALID. REFER DOCUMENT."), ReasonPhrase = "SI094^REFERENCE NUMBER INVALID. REFER DOCUMENT." });

            string ProductCode = "LOTUS";
            LoggerImplement LoggerImplementObj = new LoggerImplement(RequestObject.REQUEST_REF_NO, ProductCode);
            LoggerImplementObj.LoggerObj.WriteLog(Newtonsoft.Json.JsonConvert.SerializeObject(RequestObject), "RDCreationRequest");

            RDCreationResponse ResponseObject = new RDCreationResponse();

            List<InternalTransDB> ListInternalDB = new List<InternalTransDB>();
            List<InternalTransDetails> ListInternalDetails = new List<InternalTransDetails>();

            SITransDetails SIDetail = null;
            SITransDetails SIDetail1 = null;
            SITransDB TransDBObject = null;

            CBSTransaction GenericObject = new CBSTransaction();

            DatabaseOperation DBObject = new DatabaseOperation();
            string BankName = DatabaseOperation.GetBankName(Convert.ToInt32(RequestObject.BANK_CODE));

            RDCreationRequest_Inherit RDCreationRequest_InheritObj = new RDCreationRequest_Inherit();
            RDCreationRequest_InheritObj.CUSTOMER_NAME = RequestObject.CUSTOMER_NAME;
            RDCreationRequest_InheritObj.Language_Code = RequestObject.Language_Code;
            RDCreationRequest_InheritObj.INTEREST_FREQUENCY = RequestObject.INTEREST_FREQUENCY;
            RDCreationRequest_InheritObj.NATIONALITY = RequestObject.NATIONALITY;
            RDCreationRequest_InheritObj.SECURITY_INDICATOR = RequestObject.SECURITY_INDICATOR;
            RDCreationRequest_InheritObj.TRANSFER_ACCOUNT_NUMBER = RequestObject.TRANSFER_ACCOUNT_NUMBER;
            RDCreationRequest_InheritObj.ACCT_SGMT_CODE = RequestObject.ACCT_SGMT_CODE;
            RDCreationRequest_InheritObj.MATURITY_DATE = RequestObject.MATURITY_DATE;
            RDCreationRequest_InheritObj.TIME_BAND = RequestObject.TIME_BAND;
            RDCreationRequest_InheritObj.TERM_LOCATION = RequestObject.TERM_LOCATION;
            RDCreationRequest_InheritObj.CUSTOMER_NO = RequestObject.CUSTOMER_NO;
            RDCreationRequest_InheritObj.TYPE = RequestObject.TYPE;
            RDCreationRequest_InheritObj.CUSTOMER_RISK_DOMESTIC_RISK = RequestObject.CUSTOMER_RISK_DOMESTIC_RISK;
            RDCreationRequest_InheritObj.CUSTOMER_CATEGORY = RequestObject.CUSTOMER_CATEGORY;
            RDCreationRequest_InheritObj.ADDRESS_1 = RequestObject.ADDRESS_1;
            RDCreationRequest_InheritObj.ADDRESS_2 = RequestObject.ADDRESS_2;
            RDCreationRequest_InheritObj.REQUEST_REF_NO = RequestObject.REQUEST_REF_NO;
            RDCreationRequest_InheritObj.PRODUCT = RequestObject.PRODUCT;
            RDCreationRequest_InheritObj.RD_INSTL_FREQ = RequestObject.RD_INSTL_FREQ;
            RDCreationRequest_InheritObj.INTEREST_PAYMENT_METHOD = RequestObject.INTEREST_PAYMENT_METHOD;
            RDCreationRequest_InheritObj.ACCOUNT_CROSS_BORDER_RISK = RequestObject.ACCOUNT_CROSS_BORDER_RISK;
            RDCreationRequest_InheritObj.MATURITY_INSTRUCTION = RequestObject.MATURITY_INSTRUCTION;
            RDCreationRequest_InheritObj.TERM_BASIS = RequestObject.TERM_BASIS;
            RDCreationRequest_InheritObj.TERM_VALUE_DEPOSITED = RequestObject.TERM_VALUE_DEPOSITED;
            RDCreationRequest_InheritObj.TERM_MONTHS = RequestObject.TERM_MONTHS;
            RDCreationRequest_InheritObj.MOBILISER = RequestObject.MOBILISER;
            RDCreationRequest_InheritObj.CURRENCY = RequestObject.CURRENCY;
            RDCreationRequest_InheritObj.POST_CODE = RequestObject.POST_CODE;
            RDCreationRequest_InheritObj.BRANCH_CODE = RequestObject.BRANCH_CODE;
            RDCreationRequest_InheritObj.BANK_CODE = RequestObject.BANK_CODE;
            RDCreationRequest_InheritObj.CROSS_BORDER_RISK = RequestObject.CROSS_BORDER_RISK;
            RDCreationRequest_InheritObj.FORM60_61 = RequestObject.FORM60_61;
            RDCreationRequest_InheritObj.PAN_NO = RequestObject.PAN_NO;
            RDCreationRequest_InheritObj.TERM_YEARS = RequestObject.TERM_YEARS;
            RDCreationRequest_InheritObj.RD_EXPECTED_INSTALLMENT = RequestObject.RD_EXPECTED_INSTALLMENT;
            RDCreationRequest_InheritObj.CURRENCY_CODE = RequestObject.CURRENCY_CODE;
            RDCreationRequest_InheritObj.Term_Days = RequestObject.Term_Days;
            RDCreationRequest_InheritObj.TERM_LENGTH = RequestObject.TERM_LENGTH;
            TransDBObject = new SITransDB(RequestObject.REQUEST_REF_NO, ProductCode, DateTime.Now, "", "", "", "RDCreation", IPAddressInfo.GetIP());

            ResponseObject.REQUEST_REF_NO = RequestObject.REQUEST_REF_NO;
            try
            {
                string DBResponse = LoggerImplementObj.InsertSITransDB(TransDBObject);
                if (DBResponse != "SUCCESS")
                {
                    string ERROR_DESCRIPTION = string.Empty;
                    switch (DBResponse)
                    {
                        case "SI007": ERROR_DESCRIPTION = "DATABASE ERROR";
                            break;

                        case "SI087": ERROR_DESCRIPTION = "REFERENCE NUMBER MUST BE UNIQUE";
                            break;
                    }
                    ResponseObject.ERROR_DESCRIPTION = ERROR_DESCRIPTION;
                    ResponseObject.REQUEST_REF_NO = RequestObject.REQUEST_REF_NO;
                    return ResponseObject;
                }

                var SICreationResponseString = GenericObject.sendJsonToSIPost("0", Newtonsoft.Json.JsonConvert.SerializeObject(RDCreationRequest_InheritObj), "RDCreationPost");

                LoggerImplementObj.LoggerObj.WriteLog(SICreationResponseString, "SICreationResponseString");
                JObject SICreationObj = JObject.Parse(JObject.Parse(SICreationResponseString)["Response"].ToString());
                ResponseObject.REQUEST_REF_NO = RequestObject.REQUEST_REF_NO;

                string MRReferenceNumber = DBObject.generateSBISIRRN(BankName + "TC", "");
                DateTime Request_date_time = DateTime.Now;
                DateTime Response_Date_Time = DateTime.Now;

                InternalTransDB InternalDB = new InternalTransDB("TC", RequestObject.REQUEST_REF_NO, MRReferenceNumber, Request_date_time, Response_Date_Time, SICreationObj["RESPONSE_STATUS"].ToString(), "", "RDCreationPost");
                InternalTransDetails InternalDetail = new InternalTransDetails(MRReferenceNumber, Newtonsoft.Json.JsonConvert.SerializeObject(RDCreationRequest_InheritObj), SICreationResponseString);

                ListInternalDB.Add(InternalDB);
                ListInternalDetails.Add(InternalDetail);

                if (SICreationObj["RESPONSE_STATUS"].ToString() == "1")
                {
                    TransDBObject.Error_Desc = SICreationObj["ERROR_DESCRIPTION"].ToString();
                    TransDBObject.Res_Time = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");

                    foreach (JProperty propertyName in SICreationObj.Properties())
                    {
                        if (ResponseObject.GetType().GetProperty(propertyName.Name) != null)
                            ResponseObject.GetType().GetProperty(propertyName.Name).SetValue(ResponseObject, SICreationObj[propertyName.Name].ToString());
                    }

                    SIDetail = new SITransDetails(RequestObject.REQUEST_REF_NO, ProductCode, Newtonsoft.Json.JsonConvert.SerializeObject(RequestObject), Newtonsoft.Json.JsonConvert.SerializeObject(ResponseObject), "RDCreation");
                    LoggerImplementObj.InsertInternalTransDBDetails(TransDBObject, SIDetail, ListInternalDB, ListInternalDetails);

                    ResponseObject.ERROR_DESCRIPTION = TransDBObject.Error_Desc;

                    return ResponseObject;
                }

                foreach (JProperty propertyName in SICreationObj.Properties())
                {
                    if (ResponseObject.GetType().GetProperty(propertyName.Name) != null)
                        ResponseObject.GetType().GetProperty(propertyName.Name).SetValue(ResponseObject, SICreationObj[propertyName.Name].ToString());
                }

                TransDBObject.Res_Time = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                SIDetail1 = new SITransDetails(RequestObject.REQUEST_REF_NO, ProductCode, Newtonsoft.Json.JsonConvert.SerializeObject(RequestObject), JsonConvert.SerializeObject(ResponseObject), "RDCreation");

                LoggerImplementObj.InsertInternalTransDBDetails(TransDBObject, SIDetail1, ListInternalDB, ListInternalDetails);

                return ResponseObject;
            }
            catch (Exception ex)
            {
                LoggerImplementObj.LoggerObj.WriteExceptionLog(ex.Message, "RDCreationException");

                TransDBObject.Res_Time = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                TransDBObject.Error_Code = "SI001";
                TransDBObject.Error_Desc = ex.Message.Replace("'", "");
                ResponseObject.ERROR_DESCRIPTION = "Exception while processing the response";

                ResponseObject.REQUEST_REF_NO = RequestObject.REQUEST_REF_NO;

                SIDetail = new SITransDetails(RequestObject.REQUEST_REF_NO, ProductCode, Newtonsoft.Json.JsonConvert.SerializeObject(RequestObject), Newtonsoft.Json.JsonConvert.SerializeObject(ResponseObject), "RDCreation");
                LoggerImplementObj.InsertInternalTransDBDetails(TransDBObject, SIDetail, ListInternalDB, ListInternalDetails);
            }
            finally
            {
                LoggerImplementObj.LoggerObj.WriteLog(Newtonsoft.Json.JsonConvert.SerializeObject(ResponseObject), "INBChannelEnquiryResponse");
                LoggerImplementObj.InsertInternalTransDBDetails(TransDBObject, SIDetail, ListInternalDB, ListInternalDetails);
            }

            return ResponseObject;
        }
    }
}