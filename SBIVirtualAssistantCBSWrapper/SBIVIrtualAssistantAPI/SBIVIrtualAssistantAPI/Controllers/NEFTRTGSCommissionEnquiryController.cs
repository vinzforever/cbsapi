﻿using LotusWebAPI.Models;
using LotusWebAPI.Provider;
using SBISI.CoreBanking.Common.Provider;
using SBISI.CoreBanking.Transaction.Services.Models;
using SBISI.CoreBanking.Transaction.Services.Provider;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace LotusWebAPI.Controllers
{
    public class NEFTRTGSCommissionEnquiryController : ApiController
    {
        [HttpPost]
        public async Task<CommissionEnqResponse> CommissionEnquiryAction(CommissionEnqRequest RequestObject)
        {
            if (RequestObject == null)
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound) { Content = new StringContent("SI011^Input string was not in a correct format."), ReasonPhrase = "SI011^Input string was not in a correct format." });

            if (string.IsNullOrEmpty(RequestObject.ReferenceNumber) || string.IsNullOrEmpty(RequestObject.Amount) || string.IsNullOrEmpty(RequestObject.Channel))
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound) { Content = new StringContent("SI011^Input string was not in a correct format."), ReasonPhrase = "SI011^Input string was not in a correct format." });

            if (RequestObject.ReferenceNumber.Length != 25)
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound) { Content = new StringContent("SI004^INVALID/NOT-UNIQUE REFERENCE NUMBER"), ReasonPhrase = "SI004^INVALID/NOT-UNIQUE REFERENCE NUMBER" });

            if (RequestObject.Channel != "RTGS" && RequestObject.Channel != "NEFT")
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound) { Content = new StringContent("SI005^Invalid Transaction Type"), ReasonPhrase = "SI005^Invalid Transaction Type" });

            if (RequestObject.Amount.Length != 17)
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound) { Content = new StringContent("SI009^Amount Not of 17 Digits"), ReasonPhrase = "SI009^Amount Not of 17 Digits" });

            if (RequestObject.ReferenceNumber.Substring(0, 5) != "SBILT")
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound) { Content = new StringContent("SI094^REFERENCE NUMBER INVALID. REFER DOCUMENT."), ReasonPhrase = "SI094^REFERENCE NUMBER INVALID. REFER DOCUMENT." });

            string ProductCode = "LOTUS";
            LoggerImplement LoggerImplementObj = new LoggerImplement(RequestObject.ReferenceNumber, ProductCode);
            LoggerImplementObj.LoggerObj.WriteLog(Newtonsoft.Json.JsonConvert.SerializeObject(RequestObject), "CommissionEnqRequest");

            DatabaseOperation dbobject = new DatabaseOperation();
            string BankName = DatabaseOperation.GetBankName(0);
            CommissionEnqResponse ResponseObject = new CommissionEnqResponse();

            List<InternalTransDB> ListInternalDB = new List<InternalTransDB>();
            List<InternalTransDetails> ListInternalDetails = new List<InternalTransDetails>();

            SITransDetails SIDetail = null;
            SITransDetails SIDetail1 = null;
            SITransDB TransDBObject = null;

            ResponseObject.ReferenceNumber = RequestObject.ReferenceNumber;

            try
            {
                TransDBObject = new SITransDB(RequestObject.ReferenceNumber, ProductCode, DateTime.Now, "", "", "", "NEFTRTGSCommissionEnquiry", IPAddressInfo.GetIP());

                string DBResponse = LoggerImplementObj.InsertSITransDB(TransDBObject);
                if (DBResponse != "SUCCESS")
                {
                    string ERROR_DESCRIPTION = string.Empty;
                    switch (DBResponse)
                    {
                        case "SI007": ERROR_DESCRIPTION = "DATABASE ERROR";
                            break;

                        case "SI087": ERROR_DESCRIPTION = "REFERENCE NUMBER MUST BE UNIQUE";
                            break;
                    }
                    ResponseObject.Error_desc = ERROR_DESCRIPTION;
                    ResponseObject.ReferenceNumber = RequestObject.ReferenceNumber;
                    return ResponseObject;
                }

                RequestObject.Amount = (Convert.ToDouble(RequestObject.Amount) / 1000).ToString();

                List<string> ThreadData = new List<string>();

                ResponseObject.ReferenceNumber = RequestObject.ReferenceNumber;

                if (RequestObject.Channel.Trim().ToUpper() == "RTGS")
                {
                    if (Convert.ToInt64(RequestObject.Amount) < 200000)
                    {
                        ResponseObject.Error_desc = "AMOUNT CANNOT BE LESS THAN 2,00,000 FOR RTGS";
                        return ResponseObject;
                    }
                }

                CoreBankingTransactionServices coreService = new CoreBankingTransactionServices();
                string MRReferenceNumber = dbobject.generateSBISIRRN(BankName + "MR", "");

                CommissionRequest EnqRequest = new CommissionRequest(BankName, RequestObject.Amount, RequestObject.Channel);

                var CommInfo = await coreService.CommissionEnquiry(EnqRequest);
                InternalTransDB InternalDB = new InternalTransDB("MR", RequestObject.ReferenceNumber, MRReferenceNumber, CommInfo.Item2.RequestDateTime, CommInfo.Item2.ResponseDateTime, CommInfo.Item2.ErrorCode, CommInfo.Item2.ErrorDescription, "CommissionEnquiry");
                InternalTransDetails InternalDetail = new InternalTransDetails(MRReferenceNumber, CommInfo.Item2.RequestString, CommInfo.Item2.ResponseString);

                ListInternalDB.Add(InternalDB);
                ListInternalDetails.Add(InternalDetail);

                if (CommInfo.Item1 == null || !string.IsNullOrEmpty(CommInfo.Item2.ErrorDescription))
                {
                    TransDBObject.Error_Code = CommInfo.Item2.ErrorCode;
                    TransDBObject.Error_Desc = CommInfo.Item2.ErrorDescription;

                    ResponseObject.Error_desc = CommInfo.Item2.ErrorDescription;
                    TransDBObject.Res_Time = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                    SIDetail = new SITransDetails(RequestObject.ReferenceNumber, ProductCode, RequestObject.TostringContent(), Newtonsoft.Json.JsonConvert.SerializeObject(CommInfo.Item1), "NEFTRTGSCommissionEnquiry");
                    LoggerImplementObj.InsertInternalTransDBDetails(TransDBObject, SIDetail, ListInternalDB, ListInternalDetails);

                    LoggerImplementObj.LoggerObj.WriteLog(Newtonsoft.Json.JsonConvert.SerializeObject(ResponseObject), "CommissionEnqResponse");

                    return ResponseObject;
                }

                ResponseObject.Commission = CommInfo.Item1.Commission;

                TransDBObject.Res_Time = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                SIDetail1 = new SITransDetails(RequestObject.ReferenceNumber, ProductCode, RequestObject.TostringContent(), Newtonsoft.Json.JsonConvert.SerializeObject(CommInfo.Item1), "NEFTRTGSCommissionEnquiry");
                LoggerImplementObj.InsertInternalTransDBDetails(TransDBObject, SIDetail1, ListInternalDB, ListInternalDetails);

                LoggerImplementObj.LoggerObj.WriteLog(Newtonsoft.Json.JsonConvert.SerializeObject(ResponseObject), "CommissionEnqResponse");

                return ResponseObject;
            }
            catch (Exception ex)
            {
                LoggerImplementObj.LoggerObj.WriteExceptionLog(ex.Message, "NEFTRTGSCommissionEnquiryException");

                TransDBObject.Res_Time = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                TransDBObject.Error_Code = "SI001";
                TransDBObject.Error_Desc = ex.Message.Replace("'", "");
                ResponseObject.Error_desc = "Exception while processing the response";

                SIDetail1 = new SITransDetails(RequestObject.ReferenceNumber, ProductCode, Newtonsoft.Json.JsonConvert.SerializeObject(RequestObject), Newtonsoft.Json.JsonConvert.SerializeObject(ResponseObject), "LoanAccountInformation");
                LoggerImplementObj.InsertInternalTransDBDetails(TransDBObject, SIDetail1, ListInternalDB, ListInternalDetails);
            }
            finally
            {
                LoggerImplementObj.LoggerObj.WriteLog(Newtonsoft.Json.JsonConvert.SerializeObject(ResponseObject), "CommissionEnqResponse");
                LoggerImplementObj.InsertInternalTransDBDetails(TransDBObject, SIDetail, ListInternalDB, ListInternalDetails);
            }

            return ResponseObject;
        }
    }
}