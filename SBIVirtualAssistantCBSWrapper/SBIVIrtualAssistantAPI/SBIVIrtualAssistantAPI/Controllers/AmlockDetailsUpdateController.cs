﻿using LotusWebAPI.Models;
using LotusWebAPI.Provider;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using SBISI.CoreBanking.Common.Provider;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace LotusWebAPI.Controllers
{
    public class AmlockDetailsUpdateController : ApiController
    {
        // [TokenSecurityFilter.TokenSecurityFilter]
        [HttpPost]
        public AmlockDetailsUpdateResponse AccountClosurePost(AmlockDetailsUpdateRequest RequestObject)
        {
            if (RequestObject == null)
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound) { Content = new StringContent("SI011^Input string was not in a correct format."), ReasonPhrase = "SI011^Input string was not in a correct format." });
            if (RequestObject.REFERENCE_NUMBER.Substring(0, 5) != "SBILT")
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound) { Content = new StringContent("SI094^REFERENCE NUMBER INVALID. REFER DOCUMENT."), ReasonPhrase = "SI094^REFERENCE NUMBER INVALID. REFER DOCUMENT." });

            string ProductCode = "LOTUS";
            LoggerImplement LoggerImplementObj = new LoggerImplement(RequestObject.REFERENCE_NUMBER, ProductCode);
            LoggerImplementObj.LoggerObj.WriteLog(Newtonsoft.Json.JsonConvert.SerializeObject(RequestObject), "AmlockDetailsUpdateRequest");

            AmlockDetailsUpdateResponse ResponseObject = new AmlockDetailsUpdateResponse();

            List<InternalTransDB> ListInternalDB = new List<InternalTransDB>();
            List<InternalTransDetails> ListInternalDetails = new List<InternalTransDetails>();

            SITransDetails SIDetail = null;
            SITransDetails SIDetail1 = null;
            SITransDB TransDBObject = null;

            CBSTransaction GenericObject = new CBSTransaction();

            DatabaseOperation DBObject = new DatabaseOperation();
            string BankName = DatabaseOperation.GetBankName(0);

            AmlockDetailsUpdateRequestInhrt AmlockDetailsUpdateRequestInhrt_InheritObj = new AmlockDetailsUpdateRequestInhrt();
            AmlockDetailsUpdateRequestInhrt_InheritObj.BRANCHCODE = RequestObject.BRANCHCODE;
            AmlockDetailsUpdateRequestInhrt_InheritObj.ACTION = RequestObject.ACTION;

            AmlockDetailsUpdateRequestInhrt_InheritObj.CIF = RequestObject.CIF;
            AmlockDetailsUpdateRequestInhrt_InheritObj.LISTINFORMATION = RequestObject.LISTINFO;

            AmlockDetailsUpdateRequestInhrt_InheritObj.MATCHED = RequestObject.MATCHED;
            AmlockDetailsUpdateRequestInhrt_InheritObj.REQUESTID = RequestObject.REQUESTID;

            TransDBObject = new SITransDB(RequestObject.REFERENCE_NUMBER, ProductCode, DateTime.Now, "", "", "", "AmlockDetailsUpdate", IPAddressInfo.GetIP());
            ResponseObject.REFERENCE_NUMBER = RequestObject.REFERENCE_NUMBER;

            try
            {
                string DBResponse = LoggerImplementObj.InsertSITransDB(TransDBObject);
                if (DBResponse != "SUCCESS")
                {
                    string ERROR_DESCRIPTION = string.Empty;
                    switch (DBResponse)
                    {
                        case "SI007": ERROR_DESCRIPTION = "DATABASE ERROR";
                            break;

                        case "SI087": ERROR_DESCRIPTION = "REFERENCE NUMBER MUST BE UNIQUE";
                            break;
                    }
                    ResponseObject.ERRORDESCRIPTION = ERROR_DESCRIPTION;
                    ResponseObject.ERRORCODE = DBResponse;
                    ResponseObject.REFERENCE_NUMBER = RequestObject.REFERENCE_NUMBER;
                    return ResponseObject;
                }
                string MRReferenceNumber = DBObject.generateSBISIRRN(BankName + "TC", "");
                DateTime Request_date_time = DateTime.Now;

                var SICreationResponseString = GenericObject.sendJsonToSINewPost("0", Newtonsoft.Json.JsonConvert.SerializeObject(AmlockDetailsUpdateRequestInhrt_InheritObj), "AmlockDetailsUpdatePost");
                DateTime Response_Date_Time = DateTime.Now;

                JObject SICreationObj = JObject.Parse(SICreationResponseString);

                InternalTransDB InternalDB = new InternalTransDB("TC", RequestObject.REFERENCE_NUMBER, MRReferenceNumber, Request_date_time, Response_Date_Time, "", "", "AmlockDetailsUpdate");
                InternalTransDetails InternalDetail = new InternalTransDetails(MRReferenceNumber, Newtonsoft.Json.JsonConvert.SerializeObject(AmlockDetailsUpdateRequestInhrt_InheritObj), SICreationResponseString);

                ListInternalDB.Add(InternalDB);
                ListInternalDetails.Add(InternalDetail);

                ResponseObject.REFERENCE_NUMBER = RequestObject.REFERENCE_NUMBER;

                if (SICreationObj["RESPONSECODE"].ToString() != "00")
                {
                    TransDBObject.Error_Desc = SICreationObj["ERRORDESCRIPTION"].ToString();
                    TransDBObject.Res_Time = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");

                    foreach (JProperty propertyName in SICreationObj.Properties())
                    {
                        if (ResponseObject.GetType().GetProperty(propertyName.Name) != null)
                            ResponseObject.GetType().GetProperty(propertyName.Name).SetValue(ResponseObject, SICreationObj[propertyName.Name].ToString());
                    }

                    SIDetail = new SITransDetails(RequestObject.REFERENCE_NUMBER, ProductCode, Newtonsoft.Json.JsonConvert.SerializeObject(RequestObject), Newtonsoft.Json.JsonConvert.SerializeObject(ResponseObject), "AmlockDetailsUpdate");

                    LoggerImplementObj.InsertInternalTransDBDetails(TransDBObject, SIDetail, ListInternalDB, ListInternalDetails);
                    return ResponseObject;
                }

                foreach (JProperty propertyName in SICreationObj.Properties())
                {
                    if (ResponseObject.GetType().GetProperty(propertyName.Name) != null)
                        ResponseObject.GetType().GetProperty(propertyName.Name).SetValue(ResponseObject, SICreationObj[propertyName.Name].ToString());
                }

                TransDBObject.Res_Time = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                SIDetail1 = new SITransDetails(RequestObject.REFERENCE_NUMBER, ProductCode, Newtonsoft.Json.JsonConvert.SerializeObject(RequestObject), JsonConvert.SerializeObject(ResponseObject), "AmlockDetailsUpdate");

                LoggerImplementObj.InsertInternalTransDBDetails(TransDBObject, SIDetail1, ListInternalDB, ListInternalDetails);
                return ResponseObject;
            }
            catch (Exception ex)
            {
                LoggerImplementObj.LoggerObj.WriteExceptionLog(ex.Message, "AmlockDetailsUpdateException");

                TransDBObject.Res_Time = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                TransDBObject.Error_Code = "SI001";
                TransDBObject.Error_Desc = ex.Message.Replace("'", "");
                ResponseObject.ERRORDESCRIPTION = "Exception while processing the response";
                ResponseObject.ERRORCODE = "SI001";
                ResponseObject.REFERENCE_NUMBER = RequestObject.REFERENCE_NUMBER;

                SIDetail = new SITransDetails(RequestObject.REFERENCE_NUMBER, ProductCode, Newtonsoft.Json.JsonConvert.SerializeObject(RequestObject), Newtonsoft.Json.JsonConvert.SerializeObject(ResponseObject), "AmlockDetailsUpdate");

                LoggerImplementObj.InsertInternalTransDBDetails(TransDBObject, SIDetail, ListInternalDB, ListInternalDetails);
            }
            finally
            {
                LoggerImplementObj.LoggerObj.WriteLog(Newtonsoft.Json.JsonConvert.SerializeObject(ResponseObject), "AmlockDetailsUpdateResponse");
                LoggerImplementObj.InsertInternalTransDBDetails(TransDBObject, SIDetail, ListInternalDB, ListInternalDetails);
            }

            return ResponseObject;
        }
    }
}