﻿using LotusWebAPI.Models;
using LotusWebAPI.Provider;
using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace LotusWebAPI.Controllers
{
    public class ReferenceNumberEnquiryController : ApiController
    {
        [TokenSecurityFilter.TokenSecurityFilter]
        [HttpPost]
        public async Task<ReferenceEnquiryResponse> Post(ReferenceEnquiryRequest RequestObject)
        {
            if (RequestObject == null)
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound) { Content = new StringContent("SI011^Input string was not in a correct format."), ReasonPhrase = "SI011^Input string was not in a correct format." });

            if (string.IsNullOrEmpty(RequestObject.ReferenceNumber))
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound) { Content = new StringContent("SI011^Input string was not in a correct format."), ReasonPhrase = "SI011^Input string was not in a correct format." });

            if (RequestObject.ReferenceNumber.Length != 25)
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound) { Content = new StringContent("SI004^INVALID/NOT-UNIQUE REFERENCE NUMBER"), ReasonPhrase = "SI004^INVALID/NOT-UNIQUE REFERENCE NUMBER" });

            if (RequestObject.ReferenceNumber.Substring(0, 5) != "SBILT")
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound) { Content = new StringContent("SI094^REFERENCE NUMBER INVALID. REFER DOCUMENT."), ReasonPhrase = "SI094^REFERENCE NUMBER INVALID. REFER DOCUMENT." });

            string ProductCode = "LOTUS";
            LoggerImplement LoggerImplementObj = new LoggerImplement(RequestObject.ReferenceNumber, ProductCode);
            LoggerImplementObj.LoggerObj.WriteLog(Newtonsoft.Json.JsonConvert.SerializeObject(RequestObject), "ReferenceEnquiryRequest");

            ReferenceEnquiryResponse ResponseObject = new ReferenceEnquiryResponse();

            DatabaseOperation DBObject = new DatabaseOperation();

            string BankName = DatabaseOperation.GetBankName(0);

            ResponseObject.ReferenceNumber = RequestObject.ReferenceNumber;
            try
            {
                string output = DBObject.executeSITransDetails(RequestObject.ReferenceNumber);

                if (output == "")
                {
                    output = "No record found for the given Reference Number";
                }

                ResponseObject.Response = output.Replace("\\", "");

                return ResponseObject;
            }
            catch (Exception ex)
            {
                LoggerImplementObj.LoggerObj.WriteExceptionLog(ex.Message, "ReferenceNumberEnquiry : " + Newtonsoft.Json.JsonConvert.SerializeObject(RequestObject));
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound) { Content = new StringContent("SI001^EXCEPTION WHILE PROCESSING THE RESPONSE"), ReasonPhrase = "SI001^EXCEPTION WHILE PROCESSING THE RESPONSE" });
            }
        }
    }
}