﻿using LotusWebAPI.Models;
using LotusWebAPI.Provider;
using Newtonsoft.Json.Linq;
using SBISI.CoreBanking.Common.Provider;
using SBISI.CoreBanking.Services.Models;
using SBISI.CoreBanking.Services.Providers;
using System;
using System.Collections.Generic;
using System.Net;
using System.Linq;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http;
using Newtonsoft.Json;
using System.Reflection.Emit;
using System.Reflection;

namespace LotusWebAPI.Controllers
{
    public class CustomerGeneralAmmendmentController : ApiController
    {
        [TokenSecurityFilter.TokenSecurityFilter]
        [HttpPost]
        public CustomerGeneralAmmendmentResponse AccountClosurePost(CustomerGeneralAmmendmentRequest RequestObject)
        {
            if (RequestObject == null)
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound) { Content = new StringContent("SI011^Input string was not in a correct format."), ReasonPhrase = "SI011^Input string was not in a correct format." });

            if (RequestObject.REQUEST_REF_NO.Substring(0, 5) != "SBILT")
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound) { Content = new StringContent("SI094^REFERENCE NUMBER INVALID. REFER DOCUMENT."), ReasonPhrase = "SI094^REFERENCE NUMBER INVALID. REFER DOCUMENT." });

            string ProductCode = "LOTUS";
            LoggerImplement LoggerImplementObj = new LoggerImplement(RequestObject.REQUEST_REF_NO, ProductCode);
            LoggerImplementObj.LoggerObj.WriteLog(Newtonsoft.Json.JsonConvert.SerializeObject(RequestObject), "CustomerGeneralAmmendmentRequest");

            CustomerGeneralAmmendmentResponse ResponseObject = new CustomerGeneralAmmendmentResponse();

            List<InternalTransDB> ListInternalDB = new List<InternalTransDB>();
            List<InternalTransDetails> ListInternalDetails = new List<InternalTransDetails>();

            SITransDetails SIDetail = null;
            SITransDetails SIDetail1 = null;
            SITransDB TransDBObject = null;

            CBSTransaction GenericObject = new CBSTransaction();

            DatabaseOperation DBObject = new DatabaseOperation();
            string BankName = DatabaseOperation.GetBankName(0);

            CustomerGeneralAmmendmentRequestInherit CustomerGeneralAmmendmentRequestInherit_InheritObj = new CustomerGeneralAmmendmentRequestInherit();

            CustomerGeneralAmmendmentRequestInherit_InheritObj.BRANCH_CODE = RequestObject.BRANCH_CODE;
            CustomerGeneralAmmendmentRequestInherit_InheritObj.NON_NPA_TRADE = RequestObject.NON_NPA_TRADE;

            CustomerGeneralAmmendmentRequestInherit_InheritObj.REQUEST_TELLER_ID = RequestObject.REQUEST_TELLER_ID;
            CustomerGeneralAmmendmentRequestInherit_InheritObj.REQUEST_REF_NO = RequestObject.REQUEST_REF_NO;
            CustomerGeneralAmmendmentRequestInherit_InheritObj.CUSTOMER_NO = RequestObject.CUSTOMER_NO;
            CustomerGeneralAmmendmentRequestInherit_InheritObj.CUSTOMER_TYPE = RequestObject.CUSTOMER_TYPE;
            //    CustomerGeneralAmmendmentRequestInherit_InheritObj.RELATIONSHIP_MANAGER = RequestObject.RELATIONSHIP_MANAGER;
            CustomerGeneralAmmendmentRequestInherit_InheritObj.TITLE = RequestObject.TITLE;
            CustomerGeneralAmmendmentRequestInherit_InheritObj.FIRST_NAME = RequestObject.FIRST_NAME;
            CustomerGeneralAmmendmentRequestInherit_InheritObj.LAST_NAME = RequestObject.LAST_NAME;
            //  CustomerGeneralAmmendmentRequestInherit_InheritObj.BUSINESS_CUSTOMER_NAME = RequestObject.BUSINESS_CUSTOMER_NAME;
            CustomerGeneralAmmendmentRequestInherit_InheritObj.THRESHOLD_LIMIT = RequestObject.THRESHOLD_LIMIT;
            CustomerGeneralAmmendmentRequestInherit_InheritObj.PF_INDEX = RequestObject.PF_INDEX;
            CustomerGeneralAmmendmentRequestInherit_InheritObj.HNI_CODE = RequestObject.HNI_CODE;
            CustomerGeneralAmmendmentRequestInherit_InheritObj.CUSTOMER_ADD1 = RequestObject.CUSTOMER_ADD1;
            CustomerGeneralAmmendmentRequestInherit_InheritObj.CUSTOMER_ADD2 = RequestObject.CUSTOMER_ADD2;
            CustomerGeneralAmmendmentRequestInherit_InheritObj.CUSTOMER_ADD3 = RequestObject.CUSTOMER_ADD3;
            CustomerGeneralAmmendmentRequestInherit_InheritObj.DISTRICT_DETAILS = RequestObject.DISTRICT_DETAILS;
            CustomerGeneralAmmendmentRequestInherit_InheritObj.ISD_CODE = RequestObject.ISD_CODE;
            CustomerGeneralAmmendmentRequestInherit_InheritObj.TAN = RequestObject.TAN;
            CustomerGeneralAmmendmentRequestInherit_InheritObj.DISCTRICT_CODE = RequestObject.DISCTRICT_CODE;
            CustomerGeneralAmmendmentRequestInherit_InheritObj.SUB_DISTRICT_CODE = RequestObject.SUB_DISTRICT_CODE;
            CustomerGeneralAmmendmentRequestInherit_InheritObj.VILLAGE_CODE = RequestObject.VILLAGE_CODE;
            CustomerGeneralAmmendmentRequestInherit_InheritObj.PIN_CODE = RequestObject.PIN_CODE;
            CustomerGeneralAmmendmentRequestInherit_InheritObj.COUNTRY_CODE = RequestObject.COUNTRY_CODE;
            CustomerGeneralAmmendmentRequestInherit_InheritObj.FAX_NO = RequestObject.FAX_NO;
            CustomerGeneralAmmendmentRequestInherit_InheritObj.BUSINESS_CONTACT_NO = RequestObject.BUSINESS_CONTACT_NO;
            CustomerGeneralAmmendmentRequestInherit_InheritObj.MOBILE_NO = RequestObject.MOBILE_NO;
            CustomerGeneralAmmendmentRequestInherit_InheritObj.NATIONALITY = RequestObject.NATIONALITY;
            CustomerGeneralAmmendmentRequestInherit_InheritObj.OCCUPANCY_CODE = RequestObject.OCCUPANCY_CODE;
            //  CustomerGeneralAmmendmentRequestInherit_InheritObj.LANGUAGE_CODE = RequestObject.LANGUAGE_CODE;
            CustomerGeneralAmmendmentRequestInherit_InheritObj.IDENTIFICATION_NO = RequestObject.IDENTIFICATION_NO;
            CustomerGeneralAmmendmentRequestInherit_InheritObj.ISSUE_DATE = RequestObject.ISSUE_DATE;
            CustomerGeneralAmmendmentRequestInherit_InheritObj.ID_ISSUED_AT = RequestObject.ID_ISSUED_AT;
            CustomerGeneralAmmendmentRequestInherit_InheritObj.ID_TYPE = RequestObject.ID_TYPE;
            CustomerGeneralAmmendmentRequestInherit_InheritObj.DOMESTIC_RISK = RequestObject.DOMESTIC_RISK;
            CustomerGeneralAmmendmentRequestInherit_InheritObj.CROSS_BORDER_RISK = RequestObject.CROSS_BORDER_RISK;
            CustomerGeneralAmmendmentRequestInherit_InheritObj.VIP_CODE = RequestObject.VIP_CODE;
            CustomerGeneralAmmendmentRequestInherit_InheritObj.CIF_STATUS = RequestObject.CIF_STATUS;
            //    CustomerGeneralAmmendmentRequestInherit_InheritObj.SEGMENT_CODE = RequestObject.SEGMENT_CODE;
            //  CustomerGeneralAmmendmentRequestInherit_InheritObj.LOCKER_HOLDER = RequestObject.LOCKER_HOLDER;
            CustomerGeneralAmmendmentRequestInherit_InheritObj.TFN_INDICATOR = RequestObject.TFN_INDICATOR;
            CustomerGeneralAmmendmentRequestInherit_InheritObj.HOME_BRANCH = RequestObject.HOME_BRANCH;
            CustomerGeneralAmmendmentRequestInherit_InheritObj.CUSTOMER_LIMIT_FLAG = RequestObject.CUSTOMER_LIMIT_FLAG;
            CustomerGeneralAmmendmentRequestInherit_InheritObj.NPA_TRADE = RequestObject.NPA_TRADE;
            CustomerGeneralAmmendmentRequestInherit_InheritObj.INDUSTRY = RequestObject.INDUSTRY;
            CustomerGeneralAmmendmentRequestInherit_InheritObj.COUNTRY = RequestObject.COUNTRY;
            CustomerGeneralAmmendmentRequestInherit_InheritObj.SECTOR = RequestObject.SECTOR;
            CustomerGeneralAmmendmentRequestInherit_InheritObj.INDUSTRY_CLASSIFICATION = RequestObject.INDUSTRY_CLASSIFICATION;
            CustomerGeneralAmmendmentRequestInherit_InheritObj.COUNTRY_CODE_TWO = RequestObject.COUNTRY_CODE_TWO;
            CustomerGeneralAmmendmentRequestInherit_InheritObj.AFFILIATE_CODE = RequestObject.AFFILIATE_CODE;
            CustomerGeneralAmmendmentRequestInherit_InheritObj.BUSINESS_SECTOR_CODE = RequestObject.BUSINESS_SECTOR_CODE;
            CustomerGeneralAmmendmentRequestInherit_InheritObj.MIDDLE_NAME = RequestObject.MIDDLE_NAME;
            CustomerGeneralAmmendmentRequestInherit_InheritObj.FATHER_OR_SPOUSE_NAME = RequestObject.FATHER_OR_SPOUSE_NAME;
            CustomerGeneralAmmendmentRequestInherit_InheritObj.MOTHER_MAIDEN_NAME = RequestObject.MOTHER_MAIDEN_NAME;
            CustomerGeneralAmmendmentRequestInherit_InheritObj.RELATIVE_CODE = RequestObject.RELATIVE_CODE;
            CustomerGeneralAmmendmentRequestInherit_InheritObj.RESIDENT_STATUS = RequestObject.RESIDENT_STATUS;
            CustomerGeneralAmmendmentRequestInherit_InheritObj.CIS_ORGANIZATION_CODE = RequestObject.CIS_ORGANIZATION_CODE;
            CustomerGeneralAmmendmentRequestInherit_InheritObj.BSR_ORGANIZATION_CODE = RequestObject.BSR_ORGANIZATION_CODE;
            CustomerGeneralAmmendmentRequestInherit_InheritObj.DATE_OF_BIRTH = RequestObject.DATE_OF_BIRTH;
            CustomerGeneralAmmendmentRequestInherit_InheritObj.GENDER_CODE = RequestObject.GENDER_CODE;
            CustomerGeneralAmmendmentRequestInherit_InheritObj.MARITAL_STATUS = RequestObject.MARITAL_STATUS;
            CustomerGeneralAmmendmentRequestInherit_InheritObj.DATE_OF_ESTABLISHMENT = RequestObject.DATE_OF_ESTABLISHMENT;
            CustomerGeneralAmmendmentRequestInherit_InheritObj.DATE_OF_COMMENCEMENT = RequestObject.DATE_OF_COMMENCEMENT;
            CustomerGeneralAmmendmentRequestInherit_InheritObj.CUSTOMER_TYPE_TWO = RequestObject.CUSTOMER_TYPE_TWO;
            CustomerGeneralAmmendmentRequestInherit_InheritObj.VISA_ISSUED_BY = RequestObject.VISA_ISSUED_BY;
            CustomerGeneralAmmendmentRequestInherit_InheritObj.VISA_ISSUE_DATE = RequestObject.VISA_ISSUE_DATE;
            CustomerGeneralAmmendmentRequestInherit_InheritObj.VISA_EXPIRY_DATE = RequestObject.VISA_EXPIRY_DATE;
            CustomerGeneralAmmendmentRequestInherit_InheritObj.VISA_DETAILS = RequestObject.VISA_DETAILS;
            CustomerGeneralAmmendmentRequestInherit_InheritObj.DATE_OF_LIQUIDATION = RequestObject.DATE_OF_LIQUIDATION;
            CustomerGeneralAmmendmentRequestInherit_InheritObj.PAN_ID = RequestObject.PAN_ID;
            //     CustomerGeneralAmmendmentRequestInherit_InheritObj.NON_NPA_TRADE = RequestObject.NON_NPA_TRADE;
            CustomerGeneralAmmendmentRequestInherit_InheritObj.TRADE_CUSOTMER = RequestObject.TRADE_CUSOTMER;
            CustomerGeneralAmmendmentRequestInherit_InheritObj.COUNTRY_CODE_THREE = RequestObject.COUNTRY_CODE_THREE;
            CustomerGeneralAmmendmentRequestInherit_InheritObj.STATE_CODE = RequestObject.STATE_CODE;
            CustomerGeneralAmmendmentRequestInherit_InheritObj.CITY_CODE = RequestObject.CITY_CODE;
            CustomerGeneralAmmendmentRequestInherit_InheritObj.VOTER_ID = RequestObject.VOTER_ID;
            CustomerGeneralAmmendmentRequestInherit_InheritObj.APPLY_TDS_FLAG = RequestObject.APPLY_TDS_FLAG;
            CustomerGeneralAmmendmentRequestInherit_InheritObj.ID_ISSUE_REMARK = RequestObject.ID_ISSUE_REMARK;
            CustomerGeneralAmmendmentRequestInherit_InheritObj.CUST_EVALUATION_REQ_FLAG = RequestObject.CUST_EVALUATION_REQ_FLAG;
            //   CustomerGeneralAmmendmentRequestInherit_InheritObj.GREETINGS_REQUIRED_FLAG = RequestObject.GREETINGS_REQUIRED_FLAG;
            CustomerGeneralAmmendmentRequestInherit_InheritObj.SECOND_ID = RequestObject.SECOND_ID;
            CustomerGeneralAmmendmentRequestInherit_InheritObj.SECOND_ID_NO = RequestObject.SECOND_ID_NO;
            CustomerGeneralAmmendmentRequestInherit_InheritObj.RMPB_ID = RequestObject.RMPB_ID;
            CustomerGeneralAmmendmentRequestInherit_InheritObj.CUSTOMER_RISK = RequestObject.CUSTOMER_RISK;
            CustomerGeneralAmmendmentRequestInherit_InheritObj.KYC_UPDATED_DATE = RequestObject.KYC_UPDATED_DATE;
            CustomerGeneralAmmendmentRequestInherit_InheritObj.RISK_CATEGORY_UPFDATED_DATE = RequestObject.RISK_CATEGORY_UPFDATED_DATE;
            CustomerGeneralAmmendmentRequestInherit_InheritObj.PREMIER_BANKING_SERVICE_FLAG = RequestObject.PREMIER_BANKING_SERVICE_FLAG;
            CustomerGeneralAmmendmentRequestInherit_InheritObj.VISUALLY_IMPAIRED = RequestObject.VISUALLY_IMPAIRED;
            CustomerGeneralAmmendmentRequestInherit_InheritObj.ADHAAR_NO = RequestObject.ADHAAR_NO;

            TransDBObject = new SITransDB(RequestObject.REQUEST_REF_NO, ProductCode, DateTime.Now, "", "", "", "CustomerGeneralAmmend", IPAddressInfo.GetIP());
            ResponseObject.REQUEST_REF_NO = RequestObject.REQUEST_REF_NO;

            try
            {
                string DBResponse = LoggerImplementObj.InsertSITransDB(TransDBObject);
                if (DBResponse != "SUCCESS")
                {
                    string ERROR_DESCRIPTION = string.Empty;
                    switch (DBResponse)
                    {
                        case "SI007": ERROR_DESCRIPTION = "DATABASE ERROR";
                            break;

                        case "SI087": ERROR_DESCRIPTION = "REFERENCE NUMBER MUST BE UNIQUE";
                            break;
                    }
                    ResponseObject.ERROR_DESCRIPTION = ERROR_DESCRIPTION;
                    ResponseObject.REQUEST_REF_NO = RequestObject.REQUEST_REF_NO;
                    return ResponseObject;
                }
                string MRReferenceNumber = DBObject.generateSBISIRRN(BankName + "TC", "");
                DateTime Request_date_time = DateTime.Now;

                var SICreationResponseString = GenericObject.sendJsonToSIPost("0", Newtonsoft.Json.JsonConvert.SerializeObject(CustomerGeneralAmmendmentRequestInherit_InheritObj), "CustomerGeneralAmmendmentPost");
                DateTime Response_Date_Time = DateTime.Now;

                JObject SICreationObj = JObject.Parse(JObject.Parse(SICreationResponseString)["Response"].ToString());

                InternalTransDB InternalDB = new InternalTransDB("TC", RequestObject.REQUEST_REF_NO, MRReferenceNumber, Request_date_time, Response_Date_Time, SICreationObj["RESPONSE_STATUS"].ToString(), "", "CustomerGeneralAmmend");
                InternalTransDetails InternalDetail = new InternalTransDetails(MRReferenceNumber, Newtonsoft.Json.JsonConvert.SerializeObject(CustomerGeneralAmmendmentRequestInherit_InheritObj), SICreationResponseString);

                ListInternalDB.Add(InternalDB);
                ListInternalDetails.Add(InternalDetail);

                ResponseObject.REQUEST_REF_NO = RequestObject.REQUEST_REF_NO;

                if (SICreationObj["RESPONSE_STATUS"].ToString() == "1")
                {
                    TransDBObject.Error_Desc = SICreationObj["ERROR_DESCRIPTION"].ToString();
                    TransDBObject.Res_Time = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");

                    foreach (JProperty propertyName in SICreationObj.Properties())
                    {
                        if (ResponseObject.GetType().GetProperty(propertyName.Name) != null)
                            ResponseObject.GetType().GetProperty(propertyName.Name).SetValue(ResponseObject, SICreationObj[propertyName.Name].ToString());
                    }

                    SIDetail = new SITransDetails(RequestObject.REQUEST_REF_NO, ProductCode, Newtonsoft.Json.JsonConvert.SerializeObject(RequestObject), Newtonsoft.Json.JsonConvert.SerializeObject(ResponseObject), "CustomerGeneralAmmend");
                    LoggerImplementObj.InsertInternalTransDBDetails(TransDBObject, SIDetail, ListInternalDB, ListInternalDetails);

                    ResponseObject.ERROR_DESCRIPTION = TransDBObject.Error_Desc;

                    return ResponseObject;
                }

                foreach (JProperty propertyName in SICreationObj.Properties())
                {
                    if (ResponseObject.GetType().GetProperty(propertyName.Name) != null)
                        ResponseObject.GetType().GetProperty(propertyName.Name).SetValue(ResponseObject, SICreationObj[propertyName.Name].ToString());
                }

                TransDBObject.Res_Time = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                SIDetail1 = new SITransDetails(RequestObject.REQUEST_REF_NO, ProductCode, Newtonsoft.Json.JsonConvert.SerializeObject(RequestObject), JsonConvert.SerializeObject(ResponseObject), "CustomerGeneralAmmend");

                LoggerImplementObj.InsertInternalTransDBDetails(TransDBObject, SIDetail1, ListInternalDB, ListInternalDetails);

                return ResponseObject;
            }
            catch (Exception ex)
            {
                LoggerImplementObj.LoggerObj.WriteExceptionLog(ex.Message, "CustomerGeneralAmmendmentException");

                TransDBObject.Res_Time = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                TransDBObject.Error_Code = "SI001";
                TransDBObject.Error_Desc = ex.Message.Replace("'", "");
                ResponseObject.ERROR_DESCRIPTION = "Exception while processing the response";

                ResponseObject.REQUEST_REF_NO = RequestObject.REQUEST_REF_NO;

                SIDetail = new SITransDetails(RequestObject.REQUEST_REF_NO, ProductCode, Newtonsoft.Json.JsonConvert.SerializeObject(RequestObject), Newtonsoft.Json.JsonConvert.SerializeObject(ResponseObject), "CustomerGeneralAmmend");
                LoggerImplementObj.InsertInternalTransDBDetails(TransDBObject, SIDetail, ListInternalDB, ListInternalDetails);
            }
            finally
            {
                LoggerImplementObj.LoggerObj.WriteLog(Newtonsoft.Json.JsonConvert.SerializeObject(ResponseObject), "CustomerGeneralAmmendmentResponse");
                LoggerImplementObj.InsertInternalTransDBDetails(TransDBObject, SIDetail, ListInternalDB, ListInternalDetails);
            }

            return ResponseObject;
        }
    }
}