﻿using LotusWebAPI.Models;
using LotusWebAPI.Provider;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using SBISI.CoreBanking.Common.Provider;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace LotusWebAPI.Controllers
{
    public class INBWelcomeKitController : ApiController
    {
        [TokenSecurityFilter.TokenSecurityFilter]
        public INBWelcomeKitResponse INBWelcomeKitPost(INBWelcomeKitRequest RequestObject)
        {
            if (RequestObject == null)
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound) { Content = new StringContent("SI011^Input string was not in a correct format."), ReasonPhrase = "SI011^Input string was not in a correct format." });
            if (RequestObject.REQUEST_REF_NO.Substring(0, 5) != "SBILT")
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound) { Content = new StringContent("SI094^REFERENCE NUMBER INVALID. REFER DOCUMENT."), ReasonPhrase = "SI094^REFERENCE NUMBER INVALID. REFER DOCUMENT." });

            string ProductCode = "LOTUS";
            LoggerImplement LoggerImplementObj = new LoggerImplement(RequestObject.REQUEST_REF_NO, ProductCode);
            LoggerImplementObj.LoggerObj.WriteLog(Newtonsoft.Json.JsonConvert.SerializeObject(RequestObject), "INBWelcomeKitRequest");

            INBWelcomeKitResponse ResponseObject = new INBWelcomeKitResponse();

            List<InternalTransDB> ListInternalDB = new List<InternalTransDB>();
            List<InternalTransDetails> ListInternalDetails = new List<InternalTransDetails>();

            SITransDetails SIDetail = null;
            SITransDetails SIDetail1 = null;
            SITransDB TransDBObject = null;

            CBSTransaction GenericObject = new CBSTransaction();

            DatabaseOperation DBObject = new DatabaseOperation();
            string BankName = DatabaseOperation.GetBankName(Convert.ToInt32(RequestObject.BANK_CODE));

            INBWelcomeKitInherit INBWelcomeKitInherit_InheritObj = new INBWelcomeKitInherit();
            INBWelcomeKitInherit_InheritObj.BANK_CODE = RequestObject.BANK_CODE;
            INBWelcomeKitInherit_InheritObj.BRANCH_CODE = RequestObject.BRANCH_CODE;
            INBWelcomeKitInherit_InheritObj.REQUEST_REF_NO = RequestObject.REQUEST_REF_NO;
            INBWelcomeKitInherit_InheritObj.CIF_NUMBER = RequestObject.CIF_NUMBER;
            INBWelcomeKitInherit_InheritObj.NAME = RequestObject.NAME;
            INBWelcomeKitInherit_InheritObj.CUSTOMER_SHORT_NAME = RequestObject.CUSTOMER_SHORT_NAME;
            INBWelcomeKitInherit_InheritObj.INCOME_TAX_BASIS = RequestObject.INCOME_TAX_BASIS;
            INBWelcomeKitInherit_InheritObj.INCOME_TAX_BRANCH = RequestObject.INCOME_TAX_BRANCH;
            INBWelcomeKitInherit_InheritObj.COMMUNE_CODE = RequestObject.COMMUNE_CODE;
            INBWelcomeKitInherit_InheritObj.SCOPE_OF_FIRM = RequestObject.SCOPE_OF_FIRM;
            INBWelcomeKitInherit_InheritObj.E_MAIL_ADDRESS_1 = RequestObject.E_MAIL_ADDRESS_1;
            INBWelcomeKitInherit_InheritObj.E_MAIL_ADDRESS_2 = RequestObject.E_MAIL_ADDRESS_2;
            INBWelcomeKitInherit_InheritObj.CREDITOR_CODE = RequestObject.CREDITOR_CODE;
            INBWelcomeKitInherit_InheritObj.EDUCATION_CODE = RequestObject.EDUCATION_CODE;
            INBWelcomeKitInherit_InheritObj.ACCESS_CHANNEL_11 = RequestObject.ACCESS_CHANNEL_11;
            INBWelcomeKitInherit_InheritObj.ACCESS_CHANNEL_12 = RequestObject.ACCESS_CHANNEL_12;
            INBWelcomeKitInherit_InheritObj.ACCESS_CHANNEL_13 = RequestObject.ACCESS_CHANNEL_13;
            INBWelcomeKitInherit_InheritObj.ACCESS_CHANNEL_14 = RequestObject.ACCESS_CHANNEL_14;
            INBWelcomeKitInherit_InheritObj.ACCESS_CHANNEL_15 = RequestObject.ACCESS_CHANNEL_15;
            INBWelcomeKitInherit_InheritObj.ACCESS_CHANNEL_16 = RequestObject.ACCESS_CHANNEL_16;
            INBWelcomeKitInherit_InheritObj.ACCESS_CHANNEL_17 = RequestObject.ACCESS_CHANNEL_17;
            INBWelcomeKitInherit_InheritObj.ACCESS_CHANNEL_18 = RequestObject.ACCESS_CHANNEL_18;
            INBWelcomeKitInherit_InheritObj.ACCESS_CHANNEL_19 = RequestObject.ACCESS_CHANNEL_19;
            INBWelcomeKitInherit_InheritObj.ACCESS_CHANNEL_20 = RequestObject.ACCESS_CHANNEL_20;
            INBWelcomeKitInherit_InheritObj.ACCESS_CHANNEL_21 = RequestObject.ACCESS_CHANNEL_21;
            INBWelcomeKitInherit_InheritObj.ACCESS_CHANNEL_22 = RequestObject.ACCESS_CHANNEL_22;
            INBWelcomeKitInherit_InheritObj.ACCESS_CHANNEL_23 = RequestObject.ACCESS_CHANNEL_23;
            INBWelcomeKitInherit_InheritObj.ACCESS_CHANNEL_24 = RequestObject.ACCESS_CHANNEL_24;
            INBWelcomeKitInherit_InheritObj.ACCESS_CHANNEL_25 = RequestObject.ACCESS_CHANNEL_25;
            INBWelcomeKitInherit_InheritObj.ACCESS_CHANNEL_26 = RequestObject.ACCESS_CHANNEL_26;
            INBWelcomeKitInherit_InheritObj.ACCESS_CHANNEL_27 = RequestObject.ACCESS_CHANNEL_27;
            INBWelcomeKitInherit_InheritObj.ACCESS_CHANNEL_28 = RequestObject.ACCESS_CHANNEL_28;
            INBWelcomeKitInherit_InheritObj.ACCESS_CHANNEL_29 = RequestObject.ACCESS_CHANNEL_29;
            INBWelcomeKitInherit_InheritObj.ACCESS_CHANNEL_30 = RequestObject.ACCESS_CHANNEL_30;
            INBWelcomeKitInherit_InheritObj.STATEMENT_FREQUENCY = RequestObject.STATEMENT_FREQUENCY;
            INBWelcomeKitInherit_InheritObj.CYCLE = RequestObject.CYCLE;
            INBWelcomeKitInherit_InheritObj.DAY = RequestObject.DAY;
            INBWelcomeKitInherit_InheritObj.DELIVERY_THROUGH_E_MAIL = RequestObject.DELIVERY_THROUGH_E_MAIL;
            INBWelcomeKitInherit_InheritObj.VILLAGE_CODE = RequestObject.VILLAGE_CODE;
            INBWelcomeKitInherit_InheritObj.INTERNET_BANKING_KIT_NUMBER = RequestObject.INTERNET_BANKING_KIT_NUMBER;
            INBWelcomeKitInherit_InheritObj.ADDRESS_1 = RequestObject.ADDRESS_1;
            INBWelcomeKitInherit_InheritObj.ADDRESS_2 = RequestObject.ADDRESS_2;
            INBWelcomeKitInherit_InheritObj.ADDRESS_3 = RequestObject.ADDRESS_3;
            INBWelcomeKitInherit_InheritObj.ADDRESS_4 = RequestObject.ADDRESS_4;
            INBWelcomeKitInherit_InheritObj.STATE_NAME = RequestObject.STATE_NAME;
            INBWelcomeKitInherit_InheritObj.CITY_NAME = RequestObject.CITY_NAME;
            INBWelcomeKitInherit_InheritObj.COUNTRY_NAME = RequestObject.COUNTRY_NAME;
            INBWelcomeKitInherit_InheritObj.POST_CODE = RequestObject.POST_CODE;
            INBWelcomeKitInherit_InheritObj.PHONE_HOME = RequestObject.PHONE_HOME;
            INBWelcomeKitInherit_InheritObj.PHONE_BUSINESS = RequestObject.PHONE_BUSINESS;
            INBWelcomeKitInherit_InheritObj.MOBILE_NUMBER = RequestObject.MOBILE_NUMBER;
            INBWelcomeKitInherit_InheritObj.RESIDENT_STATUS = RequestObject.RESIDENT_STATUS;
            INBWelcomeKitInherit_InheritObj.CUSTOMER_TYPE = RequestObject.CUSTOMER_TYPE;

            TransDBObject = new SITransDB(RequestObject.REQUEST_REF_NO, ProductCode, DateTime.Now, "", "", "", "INBWelcomeKit", IPAddressInfo.GetIP());
            ResponseObject.REQUEST_REF_NO = RequestObject.REQUEST_REF_NO;

            try
            {
                string DBResponse = LoggerImplementObj.InsertSITransDB(TransDBObject);
                if (DBResponse != "SUCCESS")
                {
                    string ERROR_DESCRIPTION = string.Empty;
                    switch (DBResponse)
                    {
                        case "SI007": ERROR_DESCRIPTION = "DATABASE ERROR";
                            break;

                        case "SI087": ERROR_DESCRIPTION = "REFERENCE NUMBER MUST BE UNIQUE";
                            break;
                    }
                    ResponseObject.ERROR_DESCRIPTION = ERROR_DESCRIPTION;
                    ResponseObject.REQUEST_REF_NO = RequestObject.REQUEST_REF_NO;
                    return ResponseObject;
                }

                string MRReferenceNumber = DBObject.generateSBISIRRN(BankName + "TC", "");
                DateTime Request_date_time = DateTime.Now;

                var SICreationResponseString = GenericObject.sendJsonToSIPost("0", Newtonsoft.Json.JsonConvert.SerializeObject(INBWelcomeKitInherit_InheritObj), "INBWelcomeKitPost");
                DateTime Response_Date_Time = DateTime.Now;

                JObject SICreationObj = JObject.Parse(JObject.Parse(SICreationResponseString)["Response"].ToString());

                InternalTransDB InternalDB = new InternalTransDB("TC", RequestObject.REQUEST_REF_NO, MRReferenceNumber, Request_date_time, Response_Date_Time, SICreationObj["RESPONSE_STATUS"].ToString(), "", "INBWelcomeKit");
                InternalTransDetails InternalDetail = new InternalTransDetails(MRReferenceNumber, Newtonsoft.Json.JsonConvert.SerializeObject(INBWelcomeKitInherit_InheritObj), SICreationResponseString);

                ListInternalDB.Add(InternalDB);
                ListInternalDetails.Add(InternalDetail);

                ResponseObject.REQUEST_REF_NO = RequestObject.REQUEST_REF_NO;

                if (SICreationObj["RESPONSE_STATUS"].ToString() == "1")
                {
                    TransDBObject.Error_Desc = SICreationObj["ERROR_DESCRIPTION"].ToString();
                    TransDBObject.Res_Time = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");

                    foreach (JProperty propertyName in SICreationObj.Properties())
                    {
                        if (ResponseObject.GetType().GetProperty(propertyName.Name) != null)
                            ResponseObject.GetType().GetProperty(propertyName.Name).SetValue(ResponseObject, SICreationObj[propertyName.Name].ToString());
                    }

                    SIDetail = new SITransDetails(RequestObject.REQUEST_REF_NO, ProductCode, Newtonsoft.Json.JsonConvert.SerializeObject(RequestObject), Newtonsoft.Json.JsonConvert.SerializeObject(ResponseObject), "INBWelcomeKit");
                    LoggerImplementObj.InsertInternalTransDBDetails(TransDBObject, SIDetail, ListInternalDB, ListInternalDetails);

                    ResponseObject.ERROR_DESCRIPTION = TransDBObject.Error_Desc;

                    return ResponseObject;
                }

                foreach (JProperty propertyName in SICreationObj.Properties())
                {
                    if (ResponseObject.GetType().GetProperty(propertyName.Name) != null)
                        ResponseObject.GetType().GetProperty(propertyName.Name).SetValue(ResponseObject, SICreationObj[propertyName.Name].ToString());
                }

                TransDBObject.Res_Time = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                SIDetail1 = new SITransDetails(RequestObject.REQUEST_REF_NO, ProductCode, Newtonsoft.Json.JsonConvert.SerializeObject(RequestObject), JsonConvert.SerializeObject(ResponseObject), "INBWelcomeKit");
                LoggerImplementObj.InsertInternalTransDBDetails(TransDBObject, SIDetail1, ListInternalDB, ListInternalDetails);

                return ResponseObject;
            }
            catch (Exception ex)
            {
                LoggerImplementObj.LoggerObj.WriteExceptionLog(ex.Message, "INBWelcomeKitException");

                TransDBObject.Res_Time = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                TransDBObject.Error_Code = "SI001";
                TransDBObject.Error_Desc = ex.Message.Replace("'", "");
                ResponseObject.ERROR_DESCRIPTION = "Exception while processing the response";
                ResponseObject.REQUEST_REF_NO = RequestObject.REQUEST_REF_NO;

                SIDetail = new SITransDetails(RequestObject.REQUEST_REF_NO, ProductCode, Newtonsoft.Json.JsonConvert.SerializeObject(RequestObject), Newtonsoft.Json.JsonConvert.SerializeObject(ResponseObject), "INBWelcomeKit");
                LoggerImplementObj.InsertInternalTransDBDetails(TransDBObject, SIDetail, ListInternalDB, ListInternalDetails);
            }
            finally
            {
                LoggerImplementObj.LoggerObj.WriteLog(Newtonsoft.Json.JsonConvert.SerializeObject(ResponseObject), "INBWelcomeKitResponse");
                LoggerImplementObj.InsertInternalTransDBDetails(TransDBObject, SIDetail, ListInternalDB, ListInternalDetails);
            }

            return ResponseObject;
        }
    }
}