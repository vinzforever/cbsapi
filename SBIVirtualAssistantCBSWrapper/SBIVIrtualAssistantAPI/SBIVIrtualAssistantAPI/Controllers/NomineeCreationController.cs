﻿using LotusWebAPI.Models;
using LotusWebAPI.Provider;
using Newtonsoft.Json.Linq;
using SBISI.CoreBanking.Common.Provider;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace LotusWebAPI.Controllers
{
    public class NomineeCreationController : ApiController
    {
        [TokenSecurityFilter.TokenSecurityFilter]
        [HttpPost]
        public NomineeCreationResponse post(NomineeCreationRequest RequestObject)
        {
            if (RequestObject == null)
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound) { Content = new StringContent("SI011^Input string was not in a correct format."), ReasonPhrase = "SI011^Input string was not in a correct format." });

            if (RequestObject.REQUEST_REF_NO.Substring(0, 5) != "SBILT")
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound) { Content = new StringContent("SI094^REFERENCE NUMBER INVALID. REFER DOCUMENT."), ReasonPhrase = "SI094^REFERENCE NUMBER INVALID. REFER DOCUMENT." });

            string ProductCode = "LOTUS";
            LoggerImplement LoggerImplementObj = new LoggerImplement(RequestObject.REQUEST_REF_NO, ProductCode);
            LoggerImplementObj.LoggerObj.WriteLog(Newtonsoft.Json.JsonConvert.SerializeObject(RequestObject), "NomineeCreationRequest");

            NomineeCreationResponse ResponseObject = new NomineeCreationResponse();

            List<InternalTransDB> ListInternalDB = new List<InternalTransDB>();
            List<InternalTransDetails> ListInternalDetails = new List<InternalTransDetails>();

            SITransDetails SIDetail = null;
            SITransDetails SIDetail1 = null;
            SITransDB TransDBObject = null;

            CBSTransaction GenericObject = new CBSTransaction();

            DatabaseOperation DBObject = new DatabaseOperation();
            string BankName = DatabaseOperation.GetBankName(Convert.ToInt32(RequestObject.BANK_CODE));

            NomineeCreationRequest_Inherit NomineeCreationRequest_InheritObj = new NomineeCreationRequest_Inherit();
            NomineeCreationRequest_InheritObj.BRANCH_CODE = RequestObject.BRANCH_CODE;
            NomineeCreationRequest_InheritObj.BANK_CODE = RequestObject.BANK_CODE;
            NomineeCreationRequest_InheritObj.REQUEST_REF_NO = RequestObject.REQUEST_REF_NO;
            NomineeCreationRequest_InheritObj.ACCOUNT_NUMBER = RequestObject.ACCOUNT_NUMBER;
            NomineeCreationRequest_InheritObj.NOMINEE_NAME = RequestObject.NOMINEE_NAME;
            NomineeCreationRequest_InheritObj.NOMINEE_ADDRESS = RequestObject.NOMINEE_ADDRESS;
            NomineeCreationRequest_InheritObj.RELATIONSHIP = RequestObject.RELATIONSHIP;
            NomineeCreationRequest_InheritObj.MINOR_FLAG = RequestObject.MINOR_FLAG;
            NomineeCreationRequest_InheritObj.DOB_OF_NOMINEE = RequestObject.DOB_OF_NOMINEE;
            NomineeCreationRequest_InheritObj.ACTION = RequestObject.ACTION;
            NomineeCreationRequest_InheritObj.NAME_TO_BE_PRINTED_FLAG = RequestObject.NAME_TO_BE_PRINTED_FLAG;
            NomineeCreationRequest_InheritObj.GUARDIAN_NAME = RequestObject.GUARDIAN_NAME;
            NomineeCreationRequest_InheritObj.GUARDIAN_ADDRESS1 = RequestObject.GUARDIAN_ADDRESS1;
            NomineeCreationRequest_InheritObj.GUARDIAN_ADDRESS2 = RequestObject.GUARDIAN_ADDRESS2;
            NomineeCreationRequest_InheritObj.GUARDIAN_AGE = RequestObject.GUARDIAN_AGE;
            NomineeCreationRequest_InheritObj.NOMINEE_SEQ = RequestObject.NOMINEE_SEQ;

            TransDBObject = new SITransDB(RequestObject.REQUEST_REF_NO, ProductCode, DateTime.Now, "", "", "", "NomineeCreation", IPAddressInfo.GetIP());
            ResponseObject.REQUEST_REF_NO = RequestObject.REQUEST_REF_NO;

            try
            {
                string DBResponse = LoggerImplementObj.InsertSITransDB(TransDBObject);
                if (DBResponse != "SUCCESS")
                {
                    string ERROR_DESCRIPTION = string.Empty;
                    switch (DBResponse)
                    {
                        case "SI007": ERROR_DESCRIPTION = "DATABASE ERROR";
                            break;

                        case "SI087": ERROR_DESCRIPTION = "REFERENCE NUMBER MUST BE UNIQUE";
                            break;
                    }
                    ResponseObject.ERROR_DESCRIPTION = ERROR_DESCRIPTION;
                    ResponseObject.REQUEST_REF_NO = RequestObject.REQUEST_REF_NO;
                    return ResponseObject;
                }

                var SICreationResponseString = GenericObject.sendJsonToSIPost("0", Newtonsoft.Json.JsonConvert.SerializeObject(NomineeCreationRequest_InheritObj), "NomineeCreationPost");
                LoggerImplementObj.LoggerObj.WriteLog(Newtonsoft.Json.JsonConvert.SerializeObject(ResponseObject), "NomineeCreationResponse");

                JObject SICreationObj = JObject.Parse(JObject.Parse(SICreationResponseString)["Response"].ToString());
                ResponseObject.REQUEST_REF_NO = RequestObject.REQUEST_REF_NO;

                string MRReferenceNumber = DBObject.generateSBISIRRN(BankName + "TC", "");
                DateTime Request_date_time = DateTime.Now;
                DateTime Response_Date_Time = DateTime.Now;

                InternalTransDB InternalDB = new InternalTransDB("TC", RequestObject.REQUEST_REF_NO, MRReferenceNumber, Request_date_time, Response_Date_Time, SICreationObj["RESPONSE_STATUS"].ToString(), "", "NomineeCreationPost");
                InternalTransDetails InternalDetail = new InternalTransDetails(MRReferenceNumber, Newtonsoft.Json.JsonConvert.SerializeObject(NomineeCreationRequest_InheritObj), SICreationResponseString);

                ListInternalDB.Add(InternalDB);
                ListInternalDetails.Add(InternalDetail);

                if (SICreationObj["RESPONSE_STATUS"].ToString() == "1")
                {
                    TransDBObject.Error_Desc = SICreationObj["ERROR_DESCRIPTION"].ToString();
                    TransDBObject.Res_Time = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");

                    foreach (JProperty propertyName in SICreationObj.Properties())
                    {
                        if (ResponseObject.GetType().GetProperty(propertyName.Name) != null)
                            ResponseObject.GetType().GetProperty(propertyName.Name).SetValue(ResponseObject, SICreationObj[propertyName.Name].ToString());
                    }

                    SIDetail = new SITransDetails(RequestObject.REQUEST_REF_NO, ProductCode, Newtonsoft.Json.JsonConvert.SerializeObject(RequestObject), Newtonsoft.Json.JsonConvert.SerializeObject(ResponseObject), "NomineeCreation");
                    LoggerImplementObj.InsertInternalTransDBDetails(TransDBObject, SIDetail, ListInternalDB, ListInternalDetails);

                    ResponseObject.ERROR_DESCRIPTION = TransDBObject.Error_Desc;

                    return ResponseObject;
                }

                foreach (JProperty propertyName in SICreationObj.Properties())
                {
                    if (ResponseObject.GetType().GetProperty(propertyName.Name) != null)
                        ResponseObject.GetType().GetProperty(propertyName.Name).SetValue(ResponseObject, SICreationObj[propertyName.Name].ToString());
                }

                TransDBObject.Res_Time = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                SIDetail1 = new SITransDetails(RequestObject.REQUEST_REF_NO, ProductCode, Newtonsoft.Json.JsonConvert.SerializeObject(RequestObject), Newtonsoft.Json.JsonConvert.SerializeObject(ResponseObject), "NomineeCreation");
                LoggerImplementObj.InsertInternalTransDBDetails(TransDBObject, SIDetail1, ListInternalDB, ListInternalDetails);

                return ResponseObject;
            }
            catch (Exception ex)
            {
                LoggerImplementObj.LoggerObj.WriteExceptionLog(ex.Message, "NomineeCreationException");

                TransDBObject.Res_Time = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                TransDBObject.Error_Code = "SI001";
                TransDBObject.Error_Desc = ex.Message.Replace("'", "");
                ResponseObject.ERROR_DESCRIPTION = "Exception while processing the response";

                ResponseObject.REQUEST_REF_NO = RequestObject.REQUEST_REF_NO;

                SIDetail = new SITransDetails(RequestObject.REQUEST_REF_NO, ProductCode, Newtonsoft.Json.JsonConvert.SerializeObject(RequestObject), Newtonsoft.Json.JsonConvert.SerializeObject(ResponseObject), "NomineeCreation");
                LoggerImplementObj.InsertInternalTransDBDetails(TransDBObject, SIDetail, ListInternalDB, ListInternalDetails);
            }
            finally
            {
                LoggerImplementObj.LoggerObj.WriteLog(Newtonsoft.Json.JsonConvert.SerializeObject(ResponseObject), "NomineeCreationResponse");
                LoggerImplementObj.InsertInternalTransDBDetails(TransDBObject, SIDetail, ListInternalDB, ListInternalDetails);
            }

            return ResponseObject;
        }
    }
}