﻿using LotusWebAPI.Models;
using LotusWebAPI.Provider;
using Newtonsoft.Json.Linq;
using SBISI.CoreBanking.Common.Provider;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace LotusWebAPI.Controllers
{
    public class DedupOnDOBName_CIF_SearchController : ApiController
    {
        [TokenSecurityFilter.TokenSecurityFilter]
        [HttpPost]
        public DedupOnDOBName_CIF_SearchResponse DedupOnDOBName_CIF_Search(DedupOnDOBName_CIF_SearchRequest RequestObject)
        {
            if (RequestObject == null)
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound) { Content = new StringContent("SI011^Input string was not in a correct format."), ReasonPhrase = "SI011^Input string was not in a correct format." });

            if (RequestObject.REQUEST_REF_NO.Substring(0, 5) != "SBILT")
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound) { Content = new StringContent("SI094^REFERENCE NUMBER INVALID. REFER DOCUMENT."), ReasonPhrase = "SI094^REFERENCE NUMBER INVALID. REFER DOCUMENT." });

            string ProductCode = "LOTUS";
            LoggerImplement LoggerImplementObj = new LoggerImplement(RequestObject.REQUEST_REF_NO, ProductCode);
            LoggerImplementObj.LoggerObj.WriteLog(Newtonsoft.Json.JsonConvert.SerializeObject(RequestObject), "DedupOnDOBName_CIF_SearchRequest");

            DedupOnDOBName_CIF_SearchResponse ResponseObject = new DedupOnDOBName_CIF_SearchResponse();

            List<InternalTransDB> ListInternalDB = new List<InternalTransDB>();
            List<InternalTransDetails> ListInternalDetails = new List<InternalTransDetails>();

            SITransDetails SIDetail = null;
            SITransDetails SIDetail1 = null;
            SITransDB TransDBObject = null;

            CBSTransaction GenericObject = new CBSTransaction();

            DatabaseOperation DBObject = new DatabaseOperation();
            string BankName = DatabaseOperation.GetBankName(Convert.ToInt32(RequestObject.BANK_CODE));

            DedupOnDOBName_CIF_SearchRequest_Inherit DedupOnDOBName_CIF_SearchRequest_InheritObj = new DedupOnDOBName_CIF_SearchRequest_Inherit();
            DedupOnDOBName_CIF_SearchRequest_InheritObj.BANK_CODE = RequestObject.BANK_CODE;
            DedupOnDOBName_CIF_SearchRequest_InheritObj.BRANCH_CODE = RequestObject.BRANCH_CODE;
            DedupOnDOBName_CIF_SearchRequest_InheritObj.REQUEST_REF_NO = RequestObject.REQUEST_REF_NO;
            DedupOnDOBName_CIF_SearchRequest_InheritObj.DATE_OF_BIRTH = RequestObject.DATE_OF_BIRTH;
            DedupOnDOBName_CIF_SearchRequest_InheritObj.FIRST_NAME = RequestObject.FIRST_NAME;
            DedupOnDOBName_CIF_SearchRequest_InheritObj.LAST_NAME = RequestObject.LAST_NAME;

            TransDBObject = new SITransDB(RequestObject.REQUEST_REF_NO, ProductCode, DateTime.Now, "", "", "", "DedupOnDOBName_CIF_Search", IPAddressInfo.GetIP());
            ResponseObject.REQUEST_REF_NO = RequestObject.REQUEST_REF_NO;

            try
            {
                string DBResponse = LoggerImplementObj.InsertSITransDB(TransDBObject);
                if (DBResponse != "SUCCESS")
                {
                    string ERROR_DESCRIPTION = string.Empty;
                    switch (DBResponse)
                    {
                        case "SI007": ERROR_DESCRIPTION = "DATABASE ERROR";
                            break;

                        case "SI087": ERROR_DESCRIPTION = "REFERENCE NUMBER MUST BE UNIQUE";
                            break;
                    }
                    ResponseObject.ERROR_DESCRIPTION = ERROR_DESCRIPTION;
                    ResponseObject.REQUEST_REF_NO = RequestObject.REQUEST_REF_NO;
                    return ResponseObject;
                }

                var CIFDetailsString = GenericObject.sendJsonToSI("0", Newtonsoft.Json.JsonConvert.SerializeObject(DedupOnDOBName_CIF_SearchRequest_InheritObj), "DedupOnDOBName_CIF_Search");

                LoggerImplementObj.LoggerObj.WriteLog(CIFDetailsString, "CIFDetailsString");

                JObject CIFDetailsObj = JObject.Parse(JObject.Parse(CIFDetailsString)["Response"].ToString());
                ResponseObject.REQUEST_REF_NO = RequestObject.REQUEST_REF_NO;

                string MRReferenceNumber = DBObject.generateSBISIRRN(BankName + "TC", "");
                DateTime Request_date_time = DateTime.Now;
                DateTime Response_Date_Time = DateTime.Now;

                InternalTransDB InternalDB = new InternalTransDB("TC", RequestObject.REQUEST_REF_NO, MRReferenceNumber, Request_date_time, Response_Date_Time, CIFDetailsObj["RESPONSE_STATUS"].ToString(), "", "DedupOnDOBName_CIF_Search");
                InternalTransDetails InternalDetail = new InternalTransDetails(MRReferenceNumber, Newtonsoft.Json.JsonConvert.SerializeObject(DedupOnDOBName_CIF_SearchRequest_InheritObj), CIFDetailsString);

                ListInternalDB.Add(InternalDB);
                ListInternalDetails.Add(InternalDetail);

                if (CIFDetailsObj["RESPONSE_STATUS"].ToString() == "1")
                {
                    TransDBObject.Error_Desc = CIFDetailsObj["ERROR_DESCRIPTION"].ToString();
                    TransDBObject.Res_Time = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");

                    foreach (JProperty propertyName in CIFDetailsObj.Properties())
                    {
                        if (ResponseObject.GetType().GetProperty(propertyName.Name) != null)
                            ResponseObject.GetType().GetProperty(propertyName.Name).SetValue(ResponseObject, CIFDetailsObj[propertyName.Name].ToString());
                    }

                    SIDetail = new SITransDetails(RequestObject.REQUEST_REF_NO, ProductCode, Newtonsoft.Json.JsonConvert.SerializeObject(RequestObject), TransDBObject.Error_Desc, "DedupOnDOBName_CIF_Search");
                    LoggerImplementObj.InsertInternalTransDBDetails(TransDBObject, SIDetail, ListInternalDB, ListInternalDetails);

                    ResponseObject.ERROR_DESCRIPTION = TransDBObject.Error_Desc;

                    return ResponseObject;
                }

                foreach (JProperty propertyName in CIFDetailsObj.Properties())
                {
                    if (propertyName.Name == "CIF_DETAILS")
                    {
                        JArray CIF_DETAILSObj = JArray.Parse(CIFDetailsObj["CIF_DETAILS"]["DETAILS"].ToString());
                        ResponseObject.DETAILS = new List<CIF_DETAILS>();
                        foreach (JObject Jobj in CIF_DETAILSObj)
                        {
                            CIF_DETAILS CIF_DetailsObj = new CIF_DETAILS();
                            foreach (JProperty propName in Jobj.Properties())
                            {
                                if (CIF_DetailsObj.GetType().GetProperty(propName.Name) != null)
                                    CIF_DetailsObj.GetType().GetProperty(propName.Name).SetValue(CIF_DetailsObj, Jobj[propName.Name].ToString());
                            }
                            ResponseObject.DETAILS.Add(CIF_DetailsObj);
                        }
                    }
                    else
                    {
                        if (ResponseObject.GetType().GetProperty(propertyName.Name) != null)
                            ResponseObject.GetType().GetProperty(propertyName.Name).SetValue(ResponseObject, CIFDetailsObj[propertyName.Name].ToString());
                    }
                }

                TransDBObject.Res_Time = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                SIDetail1 = new SITransDetails(RequestObject.REQUEST_REF_NO, ProductCode, Newtonsoft.Json.JsonConvert.SerializeObject(RequestObject), Newtonsoft.Json.JsonConvert.SerializeObject(ResponseObject), "DedupOnDOBName_CIF_Search");

                LoggerImplementObj.InsertInternalTransDBDetails(TransDBObject, SIDetail1, ListInternalDB, ListInternalDetails);

                return ResponseObject;
            }
            catch (Exception ex)
            {
                LoggerImplementObj.LoggerObj.WriteExceptionLog(ex.Message, "DedupOnDOBName_CIF_SearchException");

                TransDBObject.Res_Time = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                TransDBObject.Error_Code = "SI001";
                TransDBObject.Error_Desc = ex.Message.Replace("'", "");
                ResponseObject.ERROR_DESCRIPTION = "Exception while processing the response";

                ResponseObject.REQUEST_REF_NO = RequestObject.REQUEST_REF_NO;

                SIDetail = new SITransDetails(RequestObject.REQUEST_REF_NO, ProductCode, Newtonsoft.Json.JsonConvert.SerializeObject(RequestObject), Newtonsoft.Json.JsonConvert.SerializeObject(ResponseObject), "DedupOnDOBName_CIF_Search");
                LoggerImplementObj.InsertInternalTransDBDetails(TransDBObject, SIDetail, ListInternalDB, ListInternalDetails);
            }
            finally
            {
                LoggerImplementObj.LoggerObj.WriteLog(Newtonsoft.Json.JsonConvert.SerializeObject(ResponseObject), "CustomerPersonalAmmendmentResponse");
                LoggerImplementObj.InsertInternalTransDBDetails(TransDBObject, SIDetail, ListInternalDB, ListInternalDetails);
            }

            return ResponseObject;
        }
    }
}