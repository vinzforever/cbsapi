﻿using LotusWebAPI.Models;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;


namespace LotusWebAPI.Provider
{
    public class DatabaseOperation
    {
        //Logger lg = new Logger();
        static string BankName;
        public string executeProcedureSITransDB(SITransDB SItransObject)
        {
            //lg.URN = "OracleException_" + SItransObject.Reference_No;
            try
            {
                using (var conn = new OracleConnection(ConfigurationManager.AppSettings.Get(BankName + "DatabaseConnection")))
                using (var objCmd = new OracleCommand("INSERT_SI_TRANS_DB", conn))
                {
                    objCmd.CommandType = CommandType.StoredProcedure;
                    objCmd.Parameters.Add("REFERENCE_NO", OracleDbType.Varchar2, 25).Value = SItransObject.Reference_No;
                    objCmd.Parameters.Add("PRODUCT_CODE", OracleDbType.Varchar2, 15).Value = SItransObject.Product_Code;
                    objCmd.Parameters.Add("REQ_TIME", OracleDbType.Date).Value = SItransObject.Req_Time;
                    objCmd.Parameters.Add("RES_TIME", OracleDbType.Date).Value = null;
                    objCmd.Parameters.Add("ERROR_CODE", OracleDbType.Varchar2, 30).Value = SItransObject.Error_Code;
                    objCmd.Parameters.Add("ERROR_DESC", OracleDbType.Varchar2, 200).Value = SItransObject.Error_Desc;
                    objCmd.Parameters.Add("FUNCTION_NAME", OracleDbType.Varchar2, 200).Value = SItransObject.Function_Name;
                    objCmd.Parameters.Add("REQ_IP", OracleDbType.Varchar2, 15).Value = SItransObject.Req_Ip;
                    objCmd.Parameters.Add("sql_error_msg", OracleDbType.Varchar2, 200).Direction = ParameterDirection.Output;
                    conn.Open();
                    objCmd.ExecuteNonQuery();
                    string error = objCmd.Parameters["sql_error_msg"].Value.ToString();

                    if (error == "SUCCESS")
                        return "SUCCESS";
                    //lg.write_log(error, "Procedure Exception");
                    throw new FieldAccessException(error);

                }
            }
            catch (FieldAccessException e)
            {
                //lg.write_log("Reference Number Is Not Unique", SItransObject.Reference_No);
                return "SI004^INVALID/NOT-UNIQUE REFERENCE NUMBER";
            }
            catch (Exception ex)
            {
                //lg.write_log(ex.StackTrace, "Oracle Main Exception");
                return "SI007^Database Error";
            }
        }

        public string insertDB(String sql)
        {

            int output;

            try
            {
                using (var conn = new OracleConnection(System.Configuration.ConfigurationManager.AppSettings.Get(BankName + "DatabaseConnection")))
                using (var objCmd = new OracleCommand(sql, conn))
                {
                    if (conn != null && conn.State == ConnectionState.Closed)
                    {
                        conn.Open();
                    }


                    output = 0;
                    output = objCmd.ExecuteNonQuery();
                }
                //conn.Close();


                return (output.ToString());
            }

            catch (Exception e)
            {

                return "false";
            }



        }

        public string generateSBISIRRN(string productCode, string BranchCode)
        {
            try
            {
                using (var conn = new OracleConnection(System.Configuration.ConfigurationManager.AppSettings.Get(BankName + "DatabaseConnection")))
                using (var cmd = conn.CreateCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "prod_reference";
                    cmd.BindByName = true;
                    cmd.Parameters.Add("product", OracleDbType.Varchar2, 4000).Value = productCode;
                    cmd.Parameters.Add("parameter1", OracleDbType.Varchar2, 4000).Value = BranchCode;
                    cmd.Parameters.Add("parameter2", OracleDbType.Varchar2, 4000).Value = "";
                    cmd.Parameters.Add("Return_Value", OracleDbType.Varchar2, 300).Direction = ParameterDirection.ReturnValue;
                    conn.Open();
                    cmd.ExecuteNonQuery();
                    return cmd.Parameters["Return_Value"].Value.ToString();
                }
            }
            catch (Exception ex)
            {

                //lg.URN = "OracleException";
                //lg.write_log(ex.Message, "Oracle Exception");
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound) { Content = new StringContent("SI007^Database Error"), ReasonPhrase = "SI007^Database Error" });
            }
        }

        public static string GetBankName(int BankCode)
        {

            switch (BankCode)
            {
                case 0: BankName = "SBI";
                    break;
                case 1: BankName = "SBJ";
                    break;
                case 2: BankName = "SBH";
                    break;
                case 3: BankName = "SBI";
                    break;
                case 4: BankName = "SBM";
                    break;
                case 5: BankName = "SBP";
                    break;
                case 6: BankName = "SBI";
                    break;
                case 7: BankName = "SBT";
                    break;
                default:
                    throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound) { Content = new StringContent("SI006^Bank Code Is Not Proper"), ReasonPhrase = "SI006^Bank Code Is Not Proper" });
            }
            return BankName;

        }

        public static string GetAccountType(string AccountType)
        {
            int acType;

            bool result = Int32.TryParse(AccountType, out acType);
            if (!result)
            {
                return "";
            }

            if (acType >= 1000 && acType < 2000)
                return "SB";
            else if (acType >= 2000 && acType < 4000)
                return "TD";
            else if (acType >= 4000 && acType < 5000)
                return "GA";
            else if (acType >= 5000 && acType < 6000)
                return "CA";
            else if (acType >= 6000 && acType < 6200)
                return "CC";
            else
                return "OT";


        }

        public string returnDB(String sql)
        {
            DataSet ds;


            using (var conn = new OracleConnection(ConfigurationManager.AppSettings.Get(BankName + "DatabaseConnection")))
            {
                try
                {

                    String dateis = "";

                    conn.Open();




                    using (OracleCommand cmd = new OracleCommand(sql, conn))
                    {
                        OracleDataAdapter adapter = new OracleDataAdapter(cmd);
                        ds = new DataSet();
                        adapter.Fill(ds);
                        adapter.Dispose();

                        DataTable dt = ds.Tables[0];

                        return (dt.Rows.Count > 0 ? dt.Rows[0][0].ToString() : "0");

                    }



                }
                catch (Exception e)
                {
                    return "0";
                }
            }
        }

        public string getDataER(string referenceNumber)
        {
            try
            {
                using (var conn = new OracleConnection(System.Configuration.ConfigurationManager.AppSettings.Get(BankName + "DatabaseConnection")))
                using (var cmd = conn.CreateCommand())
                {
                    cmd.CommandText = "select input from internal_trans_details where SI_REFERENCE_NO = '" + referenceNumber + "'";
                    conn.Open();
                    OracleDataReader odr = cmd.ExecuteReader();
                    if (odr.HasRows)
                    {
                        odr.Read();
                        string rowValue = odr.GetValue(0).ToString();
                        odr.Close();
                        return rowValue;
                    }
                    else
                    {
                        return "Error:No Record Found";

                    }
                }
            }
            catch (Exception ex)
            {
                //lg.URN = "OracleValidateException";
                //lg.write_log(referenceNumber, "ER Request");
                //lg.write_log(ex.Message, "Oracle Exception");
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound) { Content = new StringContent("SI007^Database Error"), ReasonPhrase = "SI007^Database Error" });
            }
        }

        public string ReturnData(string Reference)
        {
            string sql = "select si_reference_no from internal_trans_db where reference_no ='" + Reference + "'";
            try
            {
                using (var conn = new OracleConnection(System.Configuration.ConfigurationManager.AppSettings.Get(BankName + "DatabaseConnection")))
                using (var cmd = conn.CreateCommand())
                {
                    cmd.CommandText = sql;
                    conn.Open();
                    OracleDataReader odr = cmd.ExecuteReader();
                    if (odr.HasRows)
                    {
                        odr.Read();
                        string rowValue = odr.GetValue(0).ToString();
                        odr.Close();
                        return rowValue;
                    }
                    else
                    {
                        return "Error:No Record Found";

                    }
                }
            }
            catch (Exception ex)
            {
                //lg.URN = "OracleValidateException";
                //lg.write_log(sql, "GET CBS REFERENCE Request");
                //lg.write_log(ex.Message, "Oracle Exception");
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound) { Content = new StringContent("SI007^Database Error"), ReasonPhrase = "SI007^Database Error" });
            }
        }

        public string executeSITransDetails(string ReferenceNumber)
        {
            string response = "";
            string strCon = System.Configuration.ConfigurationManager.AppSettings.Get(BankName + "DatabaseConnection");
            OracleConnection con = new OracleConnection(strCon);
            try
            {


                string sql = "select output from si_trans_details where reference_no='" + ReferenceNumber + "'";


                OracleCommand com = new OracleCommand();
                com.CommandType = CommandType.Text;
                com.CommandText = sql;
                com.Connection = con;
                con.Open();
                OracleDataReader dataReader = com.ExecuteReader();
                while (dataReader.Read())
                {

                    response = (string)(dataReader["OUTPUT"]);
                }
            }
            catch (Exception ex)
            {

                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound) { Content = new StringContent(ex.Message), ReasonPhrase = "Internal error occured" });
            }
            finally
            {

                con.Close();

            }

            return response;

        }

        public DataSet returnDBDset(String sql)
        {
            DataSet ds;


            using (var conn = new OracleConnection(ConfigurationManager.AppSettings.Get(BankName + "DatabaseConnection")))
            {
                try
                {

                    String dateis = "";

                    conn.Open();




                    using (OracleCommand cmd = new OracleCommand(sql, conn))
                    {
                        OracleDataAdapter adapter = new OracleDataAdapter(cmd);
                        ds = new DataSet();
                        adapter.Fill(ds);
                        adapter.Dispose();

                        DataTable dt = ds.Tables[0];

                        return (dt.Rows.Count > 0 ? ds : null);

                    }



                }
                catch (Exception e)
                {
                    return null;
                }
            }
        }

    }
}