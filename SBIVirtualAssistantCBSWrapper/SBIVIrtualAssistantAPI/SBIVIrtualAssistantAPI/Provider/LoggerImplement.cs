﻿using LotusWebAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LotusWebAPI.Provider
{
    public class LoggerImplement
    {
        SBISI.SILOGGER.DLL.SILOGGERData SILOGGERDataObj = null;
        public SBISI.SILOGGER.DLL.LoggerClass LoggerObj = new SBISI.SILOGGER.DLL.LoggerClass();
        public LoggerImplement(string ReferenceNumber, string product)
        {
            SILOGGERDataObj = new SBISI.SILOGGER.DLL.SILOGGERData();
            SILOGGERDataObj.ReferenceNumber = ReferenceNumber;
            SILOGGERDataObj.ProductCode = product;
        }

        public string InsertSITransDB(SITransDB SITransDBObj)
        {
            SILOGGERDataObj.ObjTransDB = new SBISI.SILOGGER.DLL.Modal.TransDB(SITransDBObj.Req_Time, SITransDBObj.Res_Time, SITransDBObj.Error_Code, SITransDBObj.Error_Desc, SITransDBObj.Function_Name, SITransDBObj.Req_Ip);
            string Response = SILOGGERDataObj.InsertSITransDB();
            return Response;
        }

        public void InsertInternalTransDBDetails(SITransDB SITransDBObj, SITransDetails SITransDetailsObj, List<InternalTransDB> InternalTransDBObj, List<InternalTransDetails> InternalTransDetailsObj)
        {
            SILOGGERDataObj.ObjTransDetail = new SBISI.SILOGGER.DLL.Modal.TransDetails(SITransDetailsObj.Input, SITransDetailsObj.Output, SITransDetailsObj.Function_Name);
            SILOGGERDataObj.ObjSIInternalDB = new List<SBISI.SILOGGER.DLL.Modal.SIInternalDB>();

            foreach (InternalTransDB InternaltransSingle in InternalTransDBObj)
            {
                SBISI.SILOGGER.DLL.Modal.SIInternalDB internalObj = new SBISI.SILOGGER.DLL.Modal.SIInternalDB("VA", InternaltransSingle.SI_Reference_No, InternaltransSingle.ReqTime, InternaltransSingle.ResTime, InternaltransSingle.Error_Code, InternaltransSingle.Error_Desc, InternaltransSingle.Method_Name);
                SILOGGERDataObj.ObjSIInternalDB.Add(internalObj);
            }
            SILOGGERDataObj.ObjSIInternalDetails = new List<SBISI.SILOGGER.DLL.Modal.SIInternalDetails>();
            foreach (InternalTransDetails InternaltransDetailSingle in InternalTransDetailsObj)
            {
                SBISI.SILOGGER.DLL.Modal.SIInternalDetails internalDetailObj = new SBISI.SILOGGER.DLL.Modal.SIInternalDetails(InternaltransDetailSingle.SI_Reference_No, InternaltransDetailSingle.Input, InternaltransDetailSingle.Output);
                SILOGGERDataObj.ObjSIInternalDetails.Add(internalDetailObj);
            }
            SILOGGERDataObj.Put();
        }
    }
}