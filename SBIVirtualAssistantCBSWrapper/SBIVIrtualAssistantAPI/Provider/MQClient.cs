﻿using SBISI.CoreBanking.Common.Provider;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LotusWebAPI.Provider
{
    public class MQClient
    {
        public string ReferenceNumber;

        public void WriteToQueue(string MessageData, string QueueName)
        {
            try
            {

                MQOperation MqDllClient = new MQOperation();
                MqDllClient.ConnectMQ();
                MqDllClient.WriteLocalQMsg(MessageData, QueueName);


            }
            catch (Exception e)
            {
                //Logger lg = new Logger();
                //lg.URN = ReferenceNumber;
                //lg.write_log("Message: " + MessageData + "   $   Queue: " + QueueName, "MQ Put Error");
                //lg.write_log(e.StackTrace, "MQ Connection Error");
            }

        }

    }
}