﻿using LotusWebAPI.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Oracle.ManagedDataAccess.Client;
using SBISI.CoreBanking.Common.Provider;
using SBISI.CoreBanking.Transaction.Services.Models;
using SBISI.CoreBanking.Transaction.Services.Provider;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Script.Serialization;

namespace LotusWebAPI.Provider
{
    public class CBSTransaction
    {
       
        DatabaseOperation dbobject = new DatabaseOperation();
        string ProductCode = "SBIVA";
       
        public void ConvertObject(TransactionResponse trObject, ref TxnResponse txnObject)
        {

            txnObject.GeneratedReconNumber = trObject.GeneratedReconReference;
            txnObject.JournalNumber = trObject.JournalNumber;
            txnObject.Status  = "00";
        }

        public PanResponse sendTOSI(string BankCode, string PANnumber)
        {
            try
            {
                string oRequest = "";
                oRequest = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:npv=\"http://www.example.org/NPVNSDL\">";
                oRequest = oRequest + "<soapenv:Header/>";
                oRequest = oRequest + "<soapenv:Body>";
                oRequest = oRequest + "<npv:BancsLinktoSIRequest>";
                oRequest = oRequest + "<Request>" + BankCode + "^99988^2222222^000000^" + PANnumber + "</Request>";
                oRequest = oRequest + "</npv:BancsLinktoSIRequest>";
                oRequest = oRequest + "</soapenv:Body>";
                oRequest = oRequest + "</soapenv:Envelope>";

                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(ConfigurationManager.AppSettings.Get("TCSPAN_ApiUrl"));

                byte[] requestInFormOfBytes = System.Text.Encoding.ASCII.GetBytes(oRequest);
                request.Method = "POST";
                request.ContentType = "text/xml;charset=utf-8";
                request.ContentLength = requestInFormOfBytes.Length;
                Stream requestStream = request.GetRequestStream();
                requestStream.Write(requestInFormOfBytes, 0, requestInFormOfBytes.Length);
                requestStream.Close();

                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                StreamReader respStream = new StreamReader(response.GetResponseStream(), System.Text.Encoding.Default);
                string receivedResponse = respStream.ReadToEnd();

                respStream.Close();
                response.Close();

                string[] ParsedResponse = receivedResponse.Split('^');
                PanResponse PanObject = new PanResponse();
                PanObject.FirstName = ParsedResponse[4];
                PanObject.MiddleName = ParsedResponse[5];
                PanObject.LastName = ParsedResponse[3];
                return PanObject;

            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound) { Content = new StringContent("GN006^TCS SI Service Not Availbale"), ReasonPhrase = ex.Message });
            }
        }


        public ATMPinValidationResponse sendTOSIATM(string BankCode, string Request)
        {
            try
            {
                string oRequest = "";
                oRequest = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:npv=\"http://card.tcs.com/\">";
                oRequest = oRequest + "<soapenv:Header/>";
                oRequest = oRequest + "<soapenv:Body>";
                oRequest = oRequest + "<card:ENQ>";
                oRequest = oRequest + "<card:Teller_ID>0</card:Teller_ID><card:Checker_ID>0</card:Checker_ID><card:Bank_Code>0</card:Bank_Code><card:Branch_Code>0</card:Branch_Code><card:Request>" + Request + "</card:Request>";
                oRequest = oRequest + "</card:ENQ>";
                oRequest = oRequest + "</soapenv:Body>";
                oRequest = oRequest + "</soapenv:Envelope>";

                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(ConfigurationManager.AppSettings.Get("ATMPinValidationPost"));

                byte[] requestInFormOfBytes = System.Text.Encoding.ASCII.GetBytes(oRequest);
                request.Method = "POST";
                request.ContentType = "text/xml;charset=utf-8";
                request.ContentLength = requestInFormOfBytes.Length;
                Stream requestStream = request.GetRequestStream();
                requestStream.Write(requestInFormOfBytes, 0, requestInFormOfBytes.Length);
                requestStream.Close();

                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                StreamReader respStream = new StreamReader(response.GetResponseStream(), System.Text.Encoding.Default);
                string receivedResponse = respStream.ReadToEnd();

                respStream.Close();
                response.Close();
                ATMPinValidationResponse responseObj = new ATMPinValidationResponse();
                responseObj.Response = receivedResponse;
                //string[] ParsedResponse = receivedResponse.Split('^');
                //PanResponse PanObject = new PanResponse();
                //PanObject.FirstName = ParsedResponse[4];
                //PanObject.MiddleName = ParsedResponse[5];
                //PanObject.LastName = ParsedResponse[3];
                return responseObj;

            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound) { Content = new StringContent("GN006^TCS SI Service Not Availbale"), ReasonPhrase = ex.Message });
            }
        }


        public string sendJsonToSI(string BankCode, string Request, string URLMethod)
        {
            try
            {
                string URL = ConfigurationManager.AppSettings.Get(URLMethod)   + Request;
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(URL);

                byte[] requestInFormOfBytes = System.Text.Encoding.ASCII.GetBytes(Request);
                // request.Method = "GET";
                //  request.ContentType = "text/json;charset=utf-8";
                //  request.ContentLength = requestInFormOfBytes.Length;
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                StreamReader respStream = new StreamReader(response.GetResponseStream(), System.Text.Encoding.Default);

                //Stream requestStream = request.GetRequestStream();
                //requestStream.Write(requestInFormOfBytes, 0, requestInFormOfBytes.Length);
                //requestStream.Close();

                //  HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                //  StreamReader respStream = new StreamReader(response.GetResponseStream(), System.Text.Encoding.Default);
                string receivedResponse = respStream.ReadToEnd();

                respStream.Close();
                response.Close();


                return receivedResponse;

            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound) { Content = new StringContent("GN006^TCS SI Service Not Availbale"), ReasonPhrase = ex.Message });
            }
        }

        public string sendJsonToSIPost(string BankCode, string Request, string URLMethod)
        {
            try
            {
                string URL = ConfigurationManager.AppSettings.Get(URLMethod);
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(URL);

                byte[] requestInFormOfBytes = System.Text.Encoding.ASCII.GetBytes(Request);
                request.Method = "POST";
                //request.ContentType = "text/json;charset=utf-8";
                request.ContentType = "application/json";
                if (URLMethod == "SignatureUploadPost")
                {
                    request.ContentType = "application/json;charset=utf-8";
                }
                request.ContentLength = requestInFormOfBytes.Length;
                Stream requestStream = request.GetRequestStream();
                requestStream.Write(requestInFormOfBytes, 0, requestInFormOfBytes.Length);
                requestStream.Close();

                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                StreamReader respStream = new StreamReader(response.GetResponseStream(), System.Text.Encoding.Default);
                string receivedResponse = respStream.ReadToEnd();

                respStream.Close();
                response.Close();


                return receivedResponse;

            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound) { Content = new StringContent("GN006^TCS SI Service Not Availbale"), ReasonPhrase = ex.Message });
            }
        }


        public string sendJsonToSINewPost(string BankCode, string Request, string URLMethod)
        {
            try
            {
                string URL = ConfigurationManager.AppSettings.Get(URLMethod) +Request;
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(URL);

                ServicePointManager
      .ServerCertificateValidationCallback +=
      (sender, cert, chain, sslPolicyErrors) => true;
                request.Method = "POST";
                  //request.ContentType = "application/x-www-form-urlencoded";
                //  request.ContentLength = requestInFormOfBytes.Length;
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                StreamReader respStream = new StreamReader(response.GetResponseStream(), System.Text.Encoding.Default);

                //Stream requestStream = request.GetRequestStream();
                //requestStream.Write(requestInFormOfBytes, 0, requestInFormOfBytes.Length);
                //requestStream.Close();

                //  HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                //  StreamReader respStream = new StreamReader(response.GetResponseStream(), System.Text.Encoding.Default);
                string receivedResponse = respStream.ReadToEnd();

                respStream.Close();
                response.Close();


                return receivedResponse;

            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound) { Content = new StringContent("GN006^TCS SI Service Not Availbale"), ReasonPhrase = ex.Message });
            }
        }

        public async Task<TxnResponse> C2BPost(TxnRequest RequestObject)
        {
            string BankName = DatabaseOperation.GetBankName(RequestObject.BankCode);


            string ProductCode = "SBIVA";
            TxnResponse ResponseObject = new TxnResponse();
            SITransDB TransDBObject = new SITransDB(RequestObject.ReferenceNumber, ProductCode, DateTime.Now, "", "", "", "CustomerToBGLTransfer", IPAddressInfo.GetIP());

           string DBResponse= dbobject.executeProcedureSITransDB(TransDBObject);
            if (DBResponse != "SUCCESS")
            {
                ResponseObject.ReferenceNumber = RequestObject.ReferenceNumber;
                ResponseObject.ErrorDesc = DBResponse.Split('^').Last();
                ResponseObject.ErrorCode = DBResponse.Split('^').First();

                return ResponseObject;
            }
            List<string> ThreadData = new List<string>();
            try { 
            CoreBankingTransactionServices coreService = new CoreBankingTransactionServices();
            string MRReferenceNumber = dbobject.generateSBISIRRN(BankName + "MR", "");
            ResponseObject.CBSReferenceNumber = MRReferenceNumber;
            TransactionRequest TxnRequestObject = new TransactionRequest(BankName, RequestObject.BranchCode, "SI", MRReferenceNumber, "", RequestObject.DebitAccount, RequestObject.CreditAccount, RequestObject.Amount, "0", "INR", "", RequestObject.Narration, "", "", RequestObject.ReconReferenceNumber);
            var P1TxnInfo = await coreService.A1CustomerToBGLTransfer(TxnRequestObject);
            int count = 4;
            InternalTransDB InternalDB = new InternalTransDB("MR", RequestObject.ReferenceNumber, MRReferenceNumber, P1TxnInfo.Item2.RequestDateTime, P1TxnInfo.Item2.ResponseDateTime, P1TxnInfo.Item2.ErrorCode, P1TxnInfo.Item2.ErrorDescription, "A1CustomerToBGLTransfer");
            InternalTransDetails InternalDetail = new InternalTransDetails(MRReferenceNumber, P1TxnInfo.Item2.RequestString, P1TxnInfo.Item2.ResponseString);
            string MQinsertData = Newtonsoft.Json.JsonConvert.SerializeObject(new { ReferenceNumber = RequestObject.ReferenceNumber, TransDB = InternalDB.TostringContent(), TransDetails = InternalDetail.TostringContent(), Bank = BankName });
            ThreadData.Add(MQinsertData);

            if (P1TxnInfo.Item1 == null || !string.IsNullOrEmpty(P1TxnInfo.Item2.ErrorCode))
            {
                TransDBObject.Error_Code = P1TxnInfo.Item2.ErrorCode;
                TransDBObject.Error_Desc = P1TxnInfo.Item2.ErrorDescription;
                ResponseObject.ErrorCode = P1TxnInfo.Item2.ErrorCode;
                ResponseObject.ErrorDesc = P1TxnInfo.Item2.ErrorDescription;
                TransDBObject.Res_Time = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                SITransDetails SIDetail = new SITransDetails(RequestObject.ReferenceNumber, ProductCode, RequestObject.TostringContent(), P1TxnInfo.Item1.TostringContent(), "CustomerToBGLTransfer");
                MQinsertData = Newtonsoft.Json.JsonConvert.SerializeObject(new { SITransDB = TransDBObject.TostringContent(), SITransDetail = SIDetail.TostringContent(), Bank = BankName });
                ThreadData.Add(MQinsertData);
                while (!System.Threading.ThreadPool.QueueUserWorkItem(new WaitCallback(this.ThreadMQConnect), ThreadData))
                {
                    System.Threading.Thread.Sleep(10);
                    if (count > 0)
                        count = count - 1;
                    else
                    {
                        count = 4;
                        break;
                    }
                }

              

                return ResponseObject;
            }

            TransDBObject.Res_Time = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            SITransDetails SIDetail1 = new SITransDetails(RequestObject.ReferenceNumber, ProductCode, RequestObject.TostringContent(), P1TxnInfo.Item1.TostringContent(), "CustomerToBGLTransfer");
            MQinsertData = Newtonsoft.Json.JsonConvert.SerializeObject(new { SITransDB = TransDBObject.TostringContent(), SITransDetail = SIDetail1.TostringContent(), Bank = BankName });
            ThreadData.Add(MQinsertData);
            while (!System.Threading.ThreadPool.QueueUserWorkItem(new WaitCallback(this.ThreadMQConnect), ThreadData))
            {
                System.Threading.Thread.Sleep(10);
                if (count > 0)
                    count = count - 1;
                else
                {
                    count = 4;
                    break;
                }
            }
            ConvertObject(P1TxnInfo.Item1, ref ResponseObject);
            return ResponseObject;

           }catch(Exception ex)
            {
                TransDBObject = new SITransDB(RequestObject.ReferenceNumber, ProductCode, DateTime.Now, "", "", "", "CustomerToBGLTransfer", IPAddressInfo.GetIP());
                TransDBObject.Res_Time = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                ResponseObject.ReferenceNumber = RequestObject.ReferenceNumber;
                ResponseObject.ErrorCode = "SI011";
                ResponseObject.ErrorDesc = "Input string was not in a correct format.";
                SITransDetails SIDetail = new SITransDetails(RequestObject.ReferenceNumber, ProductCode, Newtonsoft.Json.JsonConvert.SerializeObject(RequestObject), Newtonsoft.Json.JsonConvert.SerializeObject(ResponseObject), "CustomerToBGLTransfer");
                string MQinsertData = Newtonsoft.Json.JsonConvert.SerializeObject(new { SITransDB = TransDBObject.TostringContent(), SITransDetail = SIDetail.TostringContent(), Bank = BankName });
                ThreadData.Add(MQinsertData);
                int count = 4;
                while (!System.Threading.ThreadPool.QueueUserWorkItem(new WaitCallback(this.ThreadMQConnect), ThreadData))
                {
                    System.Threading.Thread.Sleep(10);
                    if (count > 0)
                        count = count - 1;
                    else
                    {
                        count = 4;
                        break;
                    }
                }
                return ResponseObject;
            }
           
        }

        public async Task<TxnResponse> B2CPost(TxnRequest RequestObject)
        {
            TxnResponse ResponseObject = new TxnResponse();
            string BankName = DatabaseOperation.GetBankName(RequestObject.BankCode);
            string ProductCode = "SBIVA";
            SITransDB TransDBObject = new SITransDB(RequestObject.ReferenceNumber, ProductCode, DateTime.Now, "", "", "", "BGLToCustomerTransfer", IPAddressInfo.GetIP());

            string DBResponse = dbobject.executeProcedureSITransDB(TransDBObject);
            if (DBResponse != "SUCCESS")
            {
                ResponseObject.ReferenceNumber = RequestObject.ReferenceNumber;
                ResponseObject.ErrorDesc = DBResponse.Split('^').Last();
                ResponseObject.ErrorCode = DBResponse.Split('^').First();

                return ResponseObject;
            }
            List<string> ThreadData = new List<string>();


            try
            {
                CoreBankingTransactionServices coreService = new CoreBankingTransactionServices();
                string MRReferenceNumber = dbobject.generateSBISIRRN(BankName + "MR", "");
                ResponseObject.CBSReferenceNumber = MRReferenceNumber;
                TransactionRequest TxnRequestObject = new TransactionRequest(BankName, RequestObject.BranchCode, "SI", MRReferenceNumber, "", RequestObject.CreditAccount, RequestObject.DebitAccount, RequestObject.Amount, "0", "INR", "", RequestObject.Narration, "", "", RequestObject.ReconReferenceNumber);
                var A2TxnInfo = await coreService.A2BGLToCustomerTransfer(TxnRequestObject);
                int count = 4;
                InternalTransDB InternalDB = new InternalTransDB("MR", RequestObject.ReferenceNumber, MRReferenceNumber, A2TxnInfo.Item2.RequestDateTime, A2TxnInfo.Item2.ResponseDateTime, A2TxnInfo.Item2.ErrorCode, A2TxnInfo.Item2.ErrorDescription, "P2BGLToCustomerTransfer");
                InternalTransDetails InternalDetail = new InternalTransDetails(MRReferenceNumber, A2TxnInfo.Item2.RequestString, A2TxnInfo.Item2.ResponseString);
                string MQinsertData = Newtonsoft.Json.JsonConvert.SerializeObject(new { ReferenceNumber = RequestObject.ReferenceNumber, TransDB = InternalDB.TostringContent(), TransDetails = InternalDetail.TostringContent(), Bank = BankName });
                ThreadData.Add(MQinsertData);

                if (A2TxnInfo.Item1 == null || !string.IsNullOrEmpty(A2TxnInfo.Item2.ErrorCode))
                {
                    TransDBObject.Error_Code = A2TxnInfo.Item2.ErrorCode;
                    TransDBObject.Error_Desc = A2TxnInfo.Item2.ErrorDescription;
                    ResponseObject.ErrorCode = A2TxnInfo.Item2.ErrorCode;
                    ResponseObject.ErrorDesc = A2TxnInfo.Item2.ErrorDescription;
                    TransDBObject.Res_Time = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                    SITransDetails SIDetail = new SITransDetails(RequestObject.ReferenceNumber, ProductCode, RequestObject.TostringContent(), A2TxnInfo.Item1.TostringContent(), "BGLToCustomerTransfer");
                    MQinsertData = Newtonsoft.Json.JsonConvert.SerializeObject(new { SITransDB = TransDBObject.TostringContent(), SITransDetail = SIDetail.TostringContent(), Bank = BankName });

                    ThreadData.Add(MQinsertData);
                    while (!System.Threading.ThreadPool.QueueUserWorkItem(new WaitCallback(this.ThreadMQConnect), ThreadData))
                    {
                        System.Threading.Thread.Sleep(10);
                        if (count > 0)
                            count = count - 1;
                        else
                        {
                            count = 4;
                            break;
                        }
                    }

                 

                    return ResponseObject;

                }

                TransDBObject.Res_Time = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                SITransDetails SIDetail1 = new SITransDetails(RequestObject.ReferenceNumber, ProductCode, RequestObject.TostringContent(), A2TxnInfo.Item1.TostringContent(), "BGLToCustomerTransfer");
                MQinsertData = Newtonsoft.Json.JsonConvert.SerializeObject(new { SITransDB = TransDBObject.TostringContent(), SITransDetail = SIDetail1.TostringContent(), Bank = BankName });
                ThreadData.Add(MQinsertData);
                while (!System.Threading.ThreadPool.QueueUserWorkItem(new WaitCallback(this.ThreadMQConnect), ThreadData))
                {
                    System.Threading.Thread.Sleep(10);
                    if (count > 0)
                        count = count - 1;
                    else
                    {
                        count = 4;
                        break;
                    }
                }
                ConvertObject(A2TxnInfo.Item1, ref ResponseObject);
                return ResponseObject;
            }
            catch (Exception ex)
            {
                TransDBObject = new SITransDB(RequestObject.ReferenceNumber, ProductCode, DateTime.Now, "", "", "", "BGLToCustomerTransfer", IPAddressInfo.GetIP());
                TransDBObject.Res_Time = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                ResponseObject.ReferenceNumber = RequestObject.ReferenceNumber;
                ResponseObject.ErrorCode = "SI011";
                ResponseObject.ErrorDesc = "Input string was not in a correct format.";
                SITransDetails SIDetail = new SITransDetails(RequestObject.ReferenceNumber, ProductCode, Newtonsoft.Json.JsonConvert.SerializeObject(RequestObject), Newtonsoft.Json.JsonConvert.SerializeObject(ResponseObject), "BGLToCustomerTransfer");
                string MQinsertData = Newtonsoft.Json.JsonConvert.SerializeObject(new { SITransDB = TransDBObject.TostringContent(), SITransDetail = SIDetail.TostringContent(), Bank = BankName });
                ThreadData.Add(MQinsertData);
                int count = 4;
                while (!System.Threading.ThreadPool.QueueUserWorkItem(new WaitCallback(this.ThreadMQConnect), ThreadData))
                {
                    System.Threading.Thread.Sleep(10);
                    if (count > 0)
                        count = count - 1;
                    else
                    {
                        count = 4;
                        break;
                    }
                }
                return ResponseObject;
            }
        }

        public async Task<TxnResponse> B2BPost(TxnRequest RequestObject)
        {
            TxnResponse ResponseObject = new TxnResponse();
            string BankName = DatabaseOperation.GetBankName(RequestObject.BankCode);
            string ProductCode = "SBIVA";
            SITransDB TransDBObject = new SITransDB(RequestObject.ReferenceNumber, ProductCode, DateTime.Now, "", "", "", "BGLToBGLTransfer", IPAddressInfo.GetIP());

            string DBResponse = dbobject.executeProcedureSITransDB(TransDBObject);
            if (DBResponse != "SUCCESS")
            {
                ResponseObject.ReferenceNumber = RequestObject.ReferenceNumber;
                ResponseObject.ErrorDesc = DBResponse.Split('^').Last();
                ResponseObject.ErrorCode = DBResponse.Split('^').First();

                return ResponseObject;
            }
            List<string> ThreadData = new List<string>();
            try
            {

                CoreBankingTransactionServices coreService = new CoreBankingTransactionServices();
                string MRReferenceNumber = dbobject.generateSBISIRRN(BankName + "MR", "");
                ResponseObject.CBSReferenceNumber = MRReferenceNumber;
                P4TransactionRequest TxnRequestObject = new P4TransactionRequest(BankName, RequestObject.BranchCode, "SI", MRReferenceNumber, "", RequestObject.DebitAccount, RequestObject.CreditAccount, RequestObject.Amount, "INR", RequestObject.Narration, RequestObject.ReconReferenceNumber);
                var P4TxnInfo = await coreService.P4BGLToBGLTransfer(TxnRequestObject);
                int count = 4;
                InternalTransDB InternalDB = new InternalTransDB("MR", RequestObject.ReferenceNumber, MRReferenceNumber, P4TxnInfo.Item2.RequestDateTime, P4TxnInfo.Item2.ResponseDateTime, P4TxnInfo.Item2.ErrorCode, P4TxnInfo.Item2.ErrorDescription, "P4BGLToBGLTransfer");
                InternalTransDetails InternalDetail = new InternalTransDetails(MRReferenceNumber, P4TxnInfo.Item2.RequestString, P4TxnInfo.Item2.ResponseString);
                string MQinsertData = Newtonsoft.Json.JsonConvert.SerializeObject(new { ReferenceNumber = RequestObject.ReferenceNumber, TransDB = InternalDB.TostringContent(), TransDetails = InternalDetail.TostringContent(), Bank = BankName });
                ThreadData.Add(MQinsertData);

                if (P4TxnInfo.Item1 == null || !string.IsNullOrEmpty(P4TxnInfo.Item2.ErrorCode))
                {
                    TransDBObject.Error_Code = P4TxnInfo.Item2.ErrorCode;
                    TransDBObject.Error_Desc = P4TxnInfo.Item2.ErrorDescription;
                    ResponseObject.ErrorCode = P4TxnInfo.Item2.ErrorCode;
                    ResponseObject.ErrorDesc = P4TxnInfo.Item2.ErrorDescription;
                    TransDBObject.Res_Time = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                    SITransDetails SIDetail = new SITransDetails(RequestObject.ReferenceNumber, ProductCode, RequestObject.TostringContent(), P4TxnInfo.Item1.TostringContent(), "BGLToBGLTransfer");
                    MQinsertData = Newtonsoft.Json.JsonConvert.SerializeObject(new { SITransDB = TransDBObject.TostringContent(), SITransDetail = SIDetail.TostringContent(), Bank = BankName });

                    ThreadData.Add(MQinsertData);
                    while (!System.Threading.ThreadPool.QueueUserWorkItem(new WaitCallback(this.ThreadMQConnect), ThreadData))
                    {
                        System.Threading.Thread.Sleep(10);
                        if (count > 0)
                            count = count - 1;
                        else
                        {
                            count = 4;
                            break;
                        }
                    }

                   

                    return ResponseObject;
                }

                TransDBObject.Res_Time = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                SITransDetails SIDetail1 = new SITransDetails(RequestObject.ReferenceNumber, ProductCode, RequestObject.TostringContent(), P4TxnInfo.Item1.TostringContent(), "BGLToBGLTransfer");
                MQinsertData = Newtonsoft.Json.JsonConvert.SerializeObject(new { SITransDB = TransDBObject.TostringContent(), SITransDetail = SIDetail1.TostringContent(), Bank = BankName });
                ThreadData.Add(MQinsertData);
                while (!System.Threading.ThreadPool.QueueUserWorkItem(new WaitCallback(this.ThreadMQConnect), ThreadData))
                {
                    System.Threading.Thread.Sleep(10);
                    if (count > 0)
                        count = count - 1;
                    else
                    {
                        count = 4;
                        break;
                    }
                }

                ConvertObject(P4TxnInfo.Item1, ref ResponseObject);
                return ResponseObject;
            }
            catch (Exception ex)
            {
                TransDBObject = new SITransDB(RequestObject.ReferenceNumber, ProductCode, DateTime.Now, "", "", "", "BGLToBGLTransfer", IPAddressInfo.GetIP());
                TransDBObject.Res_Time = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                ResponseObject.ReferenceNumber = RequestObject.ReferenceNumber;
                ResponseObject.ErrorCode = "SI011";
                ResponseObject.ErrorDesc = "Input string was not in a correct format.";
                SITransDetails SIDetail = new SITransDetails(RequestObject.ReferenceNumber, ProductCode, Newtonsoft.Json.JsonConvert.SerializeObject(RequestObject), Newtonsoft.Json.JsonConvert.SerializeObject(ResponseObject), "BGLToBGLTransfer");
                string MQinsertData = Newtonsoft.Json.JsonConvert.SerializeObject(new { SITransDB = TransDBObject.TostringContent(), SITransDetail = SIDetail.TostringContent(), Bank = BankName });
                ThreadData.Add(MQinsertData);
                int count = 4;
                while (!System.Threading.ThreadPool.QueueUserWorkItem(new WaitCallback(this.ThreadMQConnect), ThreadData))
                {
                    System.Threading.Thread.Sleep(10);
                    if (count > 0)
                        count = count - 1;
                    else
                    {
                        count = 4;
                        break;
                    }
                }
                return ResponseObject;
            }
        }

        public async Task<TxnResponse> C2CPost(TxnRequest RequestObject)
        {
            TxnResponse ResponseObject = new TxnResponse();
            string BankName = DatabaseOperation.GetBankName(RequestObject.BankCode);
            string ProductCode = "SBIVA";
            SITransDB TransDBObject = new SITransDB(RequestObject.ReferenceNumber, ProductCode, DateTime.Now, "", "", "", "CustomerToCustomerTransfer", IPAddressInfo.GetIP());

            string DBResponse = dbobject.executeProcedureSITransDB(TransDBObject);
            if (DBResponse != "SUCCESS")
            {
                ResponseObject.ReferenceNumber = RequestObject.ReferenceNumber;
                ResponseObject.ErrorDesc = DBResponse.Split('^').Last();
                ResponseObject.ErrorCode = DBResponse.Split('^').First();

                return ResponseObject;
            }
            List<string> ThreadData = new List<string>();

            try
            {
                CoreBankingTransactionServices coreService = new CoreBankingTransactionServices();
                string MRReferenceNumber = dbobject.generateSBISIRRN(BankName + "MR", "");
                ResponseObject.CBSReferenceNumber = MRReferenceNumber;
                P9TransactionRequest TxnRequestObject = new P9TransactionRequest(BankName, RequestObject.BranchCode, "SI", MRReferenceNumber, RequestObject.DebitAccount, RequestObject.CreditAccount, RequestObject.Amount, RequestObject.Narration);
                var P1TxnInfo = await coreService.P9CustomerToCustomerTransfer(TxnRequestObject);
                int count = 4;
                InternalTransDB InternalDB = new InternalTransDB("MR", RequestObject.ReferenceNumber, MRReferenceNumber, P1TxnInfo.Item2.RequestDateTime, P1TxnInfo.Item2.ResponseDateTime, P1TxnInfo.Item2.ErrorCode, P1TxnInfo.Item2.ErrorDescription, "P9CustomerToCustomerTransferr");
                InternalTransDetails InternalDetail = new InternalTransDetails(MRReferenceNumber, P1TxnInfo.Item2.RequestString, P1TxnInfo.Item2.ResponseString);
                string MQinsertData = Newtonsoft.Json.JsonConvert.SerializeObject(new { ReferenceNumber = RequestObject.ReferenceNumber, TransDB = InternalDB.TostringContent(), TransDetails = InternalDetail.TostringContent(), Bank = BankName });
                ThreadData.Add(MQinsertData);

                if (P1TxnInfo.Item1 == null || !string.IsNullOrEmpty(P1TxnInfo.Item2.ErrorCode))
                {
                    TransDBObject.Error_Code = P1TxnInfo.Item2.ErrorCode;
                    TransDBObject.Error_Desc = P1TxnInfo.Item2.ErrorDescription;
                    ResponseObject.ErrorCode = P1TxnInfo.Item2.ErrorCode;
                    ResponseObject.ErrorDesc = P1TxnInfo.Item2.ErrorDescription;
                    TransDBObject.Res_Time = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                    SITransDetails SIDetail = new SITransDetails(RequestObject.ReferenceNumber, ProductCode, RequestObject.TostringContent(), P1TxnInfo.Item1.TostringContent(), "CustomerToCustomerTransfer");
                    MQinsertData = Newtonsoft.Json.JsonConvert.SerializeObject(new { SITransDB = TransDBObject.TostringContent(), SITransDetail = SIDetail.TostringContent(), Bank = BankName });
                    ThreadData.Add(MQinsertData);
                    while (!System.Threading.ThreadPool.QueueUserWorkItem(new WaitCallback(this.ThreadMQConnect), ThreadData))
                    {
                        System.Threading.Thread.Sleep(10);
                        if (count > 0)
                            count = count - 1;
                        else
                        {
                            count = 4;
                            break;
                        }
                    }

                    
                    return ResponseObject;
                }

                TransDBObject.Res_Time = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                SITransDetails SIDetail1 = new SITransDetails(RequestObject.ReferenceNumber, ProductCode, RequestObject.TostringContent(), P1TxnInfo.Item1.TostringContent(), "CustomerToCustomerTransfer");
                MQinsertData = Newtonsoft.Json.JsonConvert.SerializeObject(new { SITransDB = TransDBObject.TostringContent(), SITransDetail = SIDetail1.TostringContent(), Bank = BankName });
                ThreadData.Add(MQinsertData);
                while (!System.Threading.ThreadPool.QueueUserWorkItem(new WaitCallback(this.ThreadMQConnect), ThreadData))
                {
                    System.Threading.Thread.Sleep(10);
                    if (count > 0)
                        count = count - 1;
                    else
                    {
                        count = 4;
                        break;
                    }
                }

                ConvertObject(P1TxnInfo.Item1, ref ResponseObject);
                return ResponseObject;
            }
            catch (Exception ex)
            {
                TransDBObject = new SITransDB(RequestObject.ReferenceNumber, ProductCode, DateTime.Now, "", "", "", "CustomerToCustomerTransfer", IPAddressInfo.GetIP());
                TransDBObject.Res_Time = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                ResponseObject.ReferenceNumber = RequestObject.ReferenceNumber;
                ResponseObject.ErrorCode = "SI011";
                ResponseObject.ErrorDesc = "Input string was not in a correct format.";
                SITransDetails SIDetail = new SITransDetails(RequestObject.ReferenceNumber, ProductCode, Newtonsoft.Json.JsonConvert.SerializeObject(RequestObject), Newtonsoft.Json.JsonConvert.SerializeObject(ResponseObject), "CustomerToCustomerTransfer");
                string MQinsertData = Newtonsoft.Json.JsonConvert.SerializeObject(new { SITransDB = TransDBObject.TostringContent(), SITransDetail = SIDetail.TostringContent(), Bank = BankName });
                ThreadData.Add(MQinsertData);
                int count = 4;
                while (!System.Threading.ThreadPool.QueueUserWorkItem(new WaitCallback(this.ThreadMQConnect), ThreadData))
                {
                    System.Threading.Thread.Sleep(10);
                    if (count > 0)
                        count = count - 1;
                    else
                    {
                        count = 4;
                        break;
                    }
                }
                return ResponseObject;
            }
        }

        [NonAction]
        public void ThreadMQConnect(object stateInfo)
        {
            List<string> stateInfoList = (List<string>)stateInfo;
            MQClient clientToMQ = new MQClient();
            foreach (string threadData in stateInfoList)
            {
                JObject multiDataObject = (JObject)Newtonsoft.Json.JsonConvert.DeserializeObject(threadData.ToString());
                if (!string.IsNullOrEmpty((string)multiDataObject["ReferenceNumber"]))
                    clientToMQ.ReferenceNumber = (string)multiDataObject["ReferenceNumber"];
                if (!string.IsNullOrEmpty((string)multiDataObject["TransDB"]))
                    clientToMQ.WriteToQueue((string)multiDataObject["TransDB"], multiDataObject["Bank"].ToString() + "_INTERNAL_TRANS_DB");
                if (!string.IsNullOrEmpty((string)multiDataObject["TransDetails"]))
                    clientToMQ.WriteToQueue((string)multiDataObject["TransDetails"], multiDataObject["Bank"].ToString() + "_INTERNAL_TRANS_DETAILS");
                if (!string.IsNullOrEmpty((string)multiDataObject["SITransDB"]))
                    clientToMQ.WriteToQueue((string)multiDataObject["SITransDB"], multiDataObject["Bank"].ToString() + "_SI_TRANS_DB");
                if (!string.IsNullOrEmpty((string)multiDataObject["SITransDetail"]))
                    clientToMQ.WriteToQueue((string)multiDataObject["SITransDetail"], multiDataObject["Bank"].ToString() + "_SI_TRANS_DETAILS");
            }
        }
     
    }
}