﻿using LotusWebAPI.Models;
using LotusWebAPI.Provider;
using SBISI.CoreBanking.Common.Provider;
using SBISI.CoreBanking.Services.Providers;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace LotusWebAPI.Controllers
{
    public class LoanAccountStatementController : ApiController
    {
        //[TokenSecurityFilter.TokenSecurityFilter]
        [HttpPost]
        public async Task<LoanAccountStatementInfo> LoanAccountStatementPost(LoanAccountStatementRequest RequestObject)
        {
            if (RequestObject == null)
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound) { Content = new StringContent("SI011^Input string was not in a correct format."), ReasonPhrase = "SI011^Input string was not in a correct format." });

            if (string.IsNullOrEmpty(RequestObject.ReferenceNumber))
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound) { Content = new StringContent("SI011^Input string was not in a correct format."), ReasonPhrase = "SI011^Input string was not in a correct format." });

            if (RequestObject.ReferenceNumber.Length != 25)
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound) { Content = new StringContent("SI004^INVALID/NOT-UNIQUE REFERENCE NUMBER"), ReasonPhrase = "SI004^INVALID/NOT-UNIQUE REFERENCE NUMBER" });

            if ((string.IsNullOrEmpty(RequestObject.BankCode) || Convert.ToInt32(RequestObject.BankCode) > 7))
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound) { Content = new StringContent("SI006^Bank Code Is Not Proper"), ReasonPhrase = "SI006^Bank Code Is Not Proper" });

            if (RequestObject.ReferenceNumber.Substring(0, 5) != "SBILT")
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound) { Content = new StringContent("SI094^REFERENCE NUMBER INVALID. REFER DOCUMENT."), ReasonPhrase = "SI094^REFERENCE NUMBER INVALID. REFER DOCUMENT." });

            string ProductCode = "SBIVA";
            LoggerImplement LoggerImplementObj = new LoggerImplement(RequestObject.ReferenceNumber, ProductCode);
            LoggerImplementObj.LoggerObj.WriteLog(Newtonsoft.Json.JsonConvert.SerializeObject(RequestObject), "LoanAccountStatementRequest");

            string TransactionNo = RequestObject.TransactionFlag;
            if (RequestObject.TransactionFlag == "10")
                TransactionNo = "0";
            else
                TransactionNo = "1";
            LoanAccountStatementInfo ResponseObject = new LoanAccountStatementInfo();

            List<InternalTransDB> ListInternalDB = new List<InternalTransDB>();
            List<InternalTransDetails> ListInternalDetails = new List<InternalTransDetails>();

            SITransDetails SIDetail = null;
            SITransDetails SIDetail1 = null;
            SITransDB TransDBObject = null;

            CBSTransaction GenericObject = new CBSTransaction();

            DatabaseOperation DBObject = new DatabaseOperation();
            string BankName = DatabaseOperation.GetBankName(Convert.ToInt32(RequestObject.BankCode));
            string INBReferenceNumber = DBObject.generateSBISIRRN(BankName + "IN", "");

            TransDBObject = new SITransDB(RequestObject.ReferenceNumber, ProductCode, DateTime.Now, "", "", "", "LoanAccountStatement", IPAddressInfo.GetIP());
            ResponseObject.ReferenceNumber = RequestObject.ReferenceNumber;

            try
            {
                string DBResponse = LoggerImplementObj.InsertSITransDB(TransDBObject);
                if (DBResponse != "SUCCESS")
                {
                    string ERROR_DESCRIPTION = string.Empty;
                    switch (DBResponse)
                    {
                        case "SI007": ERROR_DESCRIPTION = "DATABASE ERROR";
                            break;

                        case "SI087": ERROR_DESCRIPTION = "REFERENCE NUMBER MUST BE UNIQUE";
                            break;
                    }
                    ResponseObject.ErrorDescription = ERROR_DESCRIPTION;
                    ResponseObject.ErrorCode = DBResponse;
                    ResponseObject.ReferenceNumber = RequestObject.ReferenceNumber;
                    return ResponseObject;
                }

                CoreBankingServices coreService = new CoreBankingServices();
                SBISI.CoreBanking.Services.Models.LoanAccountStatementRequest CBSReuest = new SBISI.CoreBanking.Services.Models.LoanAccountStatementRequest();
                CBSReuest.AccountNumber = RequestObject.AccountNumber;
                CBSReuest.BankName = BankName;
                CBSReuest.CBSReferenceNumber = INBReferenceNumber;
                CBSReuest.FromDate = "";
                CBSReuest.ToDate = "";
                CBSReuest.TransactionFlag = TransactionNo;

                var responseobj = await coreService.GetLoanAccountStatement(CBSReuest);

                InternalTransDB InternalDB = new InternalTransDB("IN", RequestObject.ReferenceNumber, INBReferenceNumber, responseobj.Item2.RequestDateTime, responseobj.Item2.ResponseDateTime, responseobj.Item2.ErrorCode, responseobj.Item2.ErrorDescription, "GetLoanAccountStatement");
                InternalTransDetails InternalDetail = new InternalTransDetails(INBReferenceNumber, responseobj.Item2.RequestString, responseobj.Item2.ResponseString);

                ListInternalDB.Add(InternalDB);
                ListInternalDetails.Add(InternalDetail);

                if (responseobj.Item1 == null || !string.IsNullOrEmpty(responseobj.Item2.ErrorCode))
                {
                    TransDBObject.Error_Code = responseobj.Item2.ErrorCode;
                    TransDBObject.Error_Desc = responseobj.Item2.ErrorDescription;
                    TransDBObject.Res_Time = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                    ResponseObject.ErrorCode = responseobj.Item2.ErrorCode;
                    ResponseObject.ErrorDescription = responseobj.Item2.ErrorDescription;

                    SIDetail = new SITransDetails(RequestObject.ReferenceNumber, ProductCode, Newtonsoft.Json.JsonConvert.SerializeObject(RequestObject), Newtonsoft.Json.JsonConvert.SerializeObject(ResponseObject), "LoanAccountStatement");
                    LoggerImplementObj.InsertInternalTransDBDetails(TransDBObject, SIDetail, ListInternalDB, ListInternalDetails);

                    LoggerImplementObj.LoggerObj.WriteLog(Newtonsoft.Json.JsonConvert.SerializeObject(ResponseObject), "GetLoanAccountInformationResponse");

                    return ResponseObject;
                }
                List<LoanAccountStatementResponse> lstLoanAccountStatement = new List<LoanAccountStatementResponse>();
                foreach (SBISI.CoreBanking.Services.Models.LoanAccountStatementResponse statementRespDLL in responseobj.Item1.StatementDetail)
                {
                    LoanAccountStatementResponse loanAccountStatementObj = new LoanAccountStatementResponse();
                    loanAccountStatementObj.Balance = statementRespDLL.Balance;
                    loanAccountStatementObj.Amount = statementRespDLL.Amount;
                    loanAccountStatementObj.BranchNumber = statementRespDLL.BranchNumber;
                    loanAccountStatementObj.Narration = statementRespDLL.Narration;
                    loanAccountStatementObj.PostDate = statementRespDLL.PostDate;
                    loanAccountStatementObj.Statement = statementRespDLL.Statement;
                    loanAccountStatementObj.ValueDate = statementRespDLL.ValueDate;
                    lstLoanAccountStatement.Add(loanAccountStatementObj);
                }
                ResponseObject.StatementDetail = lstLoanAccountStatement;
                LoggerImplementObj.LoggerObj.WriteLog(Newtonsoft.Json.JsonConvert.SerializeObject(ResponseObject), "GetLoanAccountInformationResponse");

                TransDBObject.Res_Time = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                SIDetail1 = new SITransDetails(RequestObject.ReferenceNumber, ProductCode, Newtonsoft.Json.JsonConvert.SerializeObject(RequestObject), Newtonsoft.Json.JsonConvert.SerializeObject(ResponseObject), "LoanAccountStatement");
                LoggerImplementObj.InsertInternalTransDBDetails(TransDBObject, SIDetail1, ListInternalDB, ListInternalDetails);

                return ResponseObject;
            }
            catch (Exception ex)
            {
                LoggerImplementObj.LoggerObj.WriteExceptionLog(ex.Message, "LoanAccountStatementException");

                TransDBObject.Res_Time = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                TransDBObject.Error_Code = "SI001";
                TransDBObject.Error_Desc = ex.Message.Replace("'", "");
                ResponseObject.ErrorCode = "SI001";
                ResponseObject.ErrorDescription = "Exception while processing the response";

                SIDetail1 = new SITransDetails(RequestObject.ReferenceNumber, ProductCode, Newtonsoft.Json.JsonConvert.SerializeObject(RequestObject), Newtonsoft.Json.JsonConvert.SerializeObject(ResponseObject), "LoanAccountStatement");
                LoggerImplementObj.InsertInternalTransDBDetails(TransDBObject, SIDetail1, ListInternalDB, ListInternalDetails);
            }
            finally
            {
                LoggerImplementObj.LoggerObj.WriteLog(Newtonsoft.Json.JsonConvert.SerializeObject(ResponseObject), "LoanAccountStatementInfo");
                LoggerImplementObj.InsertInternalTransDBDetails(TransDBObject, SIDetail, ListInternalDB, ListInternalDetails);
            }

            return ResponseObject;
        }
    }
}