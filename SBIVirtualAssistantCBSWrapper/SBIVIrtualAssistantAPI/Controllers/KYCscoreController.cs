﻿using LotusWebAPI.Models;
using LotusWebAPI.Provider;
using SBISI.CoreBanking.Common.Provider;
using SBISI.CoreBanking.Services.Models;
using SBISI.CoreBanking.Services.Providers;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace LotusWebAPI.Controllers
{
    public class KYCscoreController : ApiController
    {
        //[TokenSecurityFilter.TokenSecurityFilter]
        public async Task<KYCscoreResponse> post(CBSAccountRequest RequestObject)
        {
            if (RequestObject == null)
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound) { Content = new StringContent("SI011^Input string was not in a correct format."), ReasonPhrase = "SI011^Input string was not in a correct format." });

            if (string.IsNullOrEmpty(RequestObject.Branch_Code) || string.IsNullOrEmpty(RequestObject.ReferenceNumber) || string.IsNullOrEmpty(RequestObject.Branch_Code) || string.IsNullOrEmpty(RequestObject.Request))
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound) { Content = new StringContent("SI011^Input string was not in a correct format."), ReasonPhrase = "SI011^Input string was not in a correct format." });

            if (RequestObject.ReferenceNumber.Length != 25)
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound) { Content = new StringContent("SI004^INVALID/NOT-UNIQUE REFERENCE NUMBER"), ReasonPhrase = "SI004^INVALID/NOT-UNIQUE REFERENCE NUMBER" });

            if (RequestObject.Bank_Code > 7)
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound) { Content = new StringContent("SI006^Bank Code Is Not Proper"), ReasonPhrase = "SI006^Bank Code Is Not Proper" });
            if (RequestObject.ReferenceNumber.Substring(0, 5) != "SBILT")
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound) { Content = new StringContent("SI094^REFERENCE NUMBER INVALID. REFER DOCUMENT."), ReasonPhrase = "SI094^REFERENCE NUMBER INVALID. REFER DOCUMENT." });

            string ProductCode = "SBIVA";
            LoggerImplement LoggerImplementObj = new LoggerImplement(RequestObject.ReferenceNumber, ProductCode);
            LoggerImplementObj.LoggerObj.WriteLog(Newtonsoft.Json.JsonConvert.SerializeObject(RequestObject), "CBSAccountRequest");

            DatabaseOperation dbobject = new DatabaseOperation();
            string BankName = DatabaseOperation.GetBankName(RequestObject.Bank_Code);

            KYCscoreResponse ResponseObject = new KYCscoreResponse();

            List<InternalTransDB> ListInternalDB = new List<InternalTransDB>();
            List<InternalTransDetails> ListInternalDetails = new List<InternalTransDetails>();

            SITransDetails SIDetail = null;
            SITransDetails SIDetail1 = null;
            SITransDB TransDBObject = null;
            ResponseObject.ReferenceNumber = RequestObject.ReferenceNumber;
            TransDBObject = new SITransDB(RequestObject.ReferenceNumber, ProductCode, DateTime.Now, "", "", "", "KYCscore", IPAddressInfo.GetIP());
            CoreBankingServices coreService = new CoreBankingServices();

            ResponseObject.ReferenceNumber = RequestObject.ReferenceNumber;

            try
            {
                string DBResponse = LoggerImplementObj.InsertSITransDB(TransDBObject);
                if (DBResponse != "SUCCESS")
                {
                    string ERROR_DESCRIPTION = string.Empty;
                    switch (DBResponse)
                    {
                        case "SI007": ERROR_DESCRIPTION = "DATABASE ERROR";
                            break;

                        case "SI087": ERROR_DESCRIPTION = "REFERENCE NUMBER MUST BE UNIQUE";
                            break;
                    }
                    ResponseObject.ErrorDescription = ERROR_DESCRIPTION;
                    ResponseObject.ReferenceNumber = RequestObject.ReferenceNumber;
                    ResponseObject.ErrorCode = DBResponse;
                    return ResponseObject;
                }

                string MultipleRemittanceReferenceNumber = dbobject.generateSBISIRRN(BankName + "MR", "");
                MultipleRemittenceAccountRequest CBSReuest = new MultipleRemittenceAccountRequest(MultipleRemittanceReferenceNumber, long.Parse(RequestObject.Request), BankName);

                var KYCResponse = await coreService.GetKYCInformation(CBSReuest);

                InternalTransDB InternalDB = new InternalTransDB("MR", RequestObject.ReferenceNumber, MultipleRemittanceReferenceNumber, KYCResponse.Item2.RequestDateTime, KYCResponse.Item2.ResponseDateTime, KYCResponse.Item2.ErrorCode, KYCResponse.Item2.ErrorDescription, "GetKYCInformation");
                InternalTransDetails InternalDetail = new InternalTransDetails(MultipleRemittanceReferenceNumber, KYCResponse.Item2.RequestString, KYCResponse.Item2.ResponseString);

                ListInternalDB.Add(InternalDB);
                ListInternalDetails.Add(InternalDetail);

                if (KYCResponse.Item1 == null || !string.IsNullOrEmpty(KYCResponse.Item2.ErrorCode))
                {
                    TransDBObject.Error_Code = KYCResponse.Item2.ErrorCode;
                    TransDBObject.Error_Desc = KYCResponse.Item2.ErrorDescription;
                    ResponseObject.ErrorCode = KYCResponse.Item2.ErrorCode;
                    ResponseObject.ErrorDescription = KYCResponse.Item2.ErrorDescription;
                    TransDBObject.Res_Time = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                    SIDetail = new SITransDetails(RequestObject.ReferenceNumber, ProductCode, RequestObject.TostringContent(), Newtonsoft.Json.JsonConvert.SerializeObject(KYCResponse.Item1), "KYCscore");
                    LoggerImplementObj.InsertInternalTransDBDetails(TransDBObject, SIDetail, ListInternalDB, ListInternalDetails);

                    LoggerImplementObj.LoggerObj.WriteLog(Newtonsoft.Json.JsonConvert.SerializeObject(ResponseObject), "KYCscoreResponse");

                    return ResponseObject;
                }
                ResponseObject.AccountNumber = RequestObject.Request;
                ResponseObject.KYCPoints = KYCResponse.Item1.TotalPoints;

                LoggerImplementObj.LoggerObj.WriteLog(Newtonsoft.Json.JsonConvert.SerializeObject(ResponseObject), "KYCscoreResponse");

                TransDBObject.Res_Time = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                SIDetail1 = new SITransDetails(RequestObject.ReferenceNumber, ProductCode, RequestObject.TostringContent(), Newtonsoft.Json.JsonConvert.SerializeObject(KYCResponse.Item1), "KYCscore");
                LoggerImplementObj.InsertInternalTransDBDetails(TransDBObject, SIDetail1, ListInternalDB, ListInternalDetails);

                return ResponseObject;
            }
            catch (Exception ex)
            {
                LoggerImplementObj.LoggerObj.WriteExceptionLog(ex.Message, "KYCscoreException");

                TransDBObject = new SITransDB(RequestObject.ReferenceNumber, ProductCode, DateTime.Now, "", "", "", "KYCscore", IPAddressInfo.GetIP());
                TransDBObject.Res_Time = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                ResponseObject.ReferenceNumber = RequestObject.ReferenceNumber;
                ResponseObject.ErrorCode = "SI011";
                ResponseObject.ErrorDescription = "Input string was not in a correct format.";
                SIDetail = new SITransDetails(RequestObject.ReferenceNumber, ProductCode, Newtonsoft.Json.JsonConvert.SerializeObject(RequestObject), Newtonsoft.Json.JsonConvert.SerializeObject(ResponseObject), "KYCscore");
                LoggerImplementObj.InsertInternalTransDBDetails(TransDBObject, SIDetail, ListInternalDB, ListInternalDetails);
            }
            finally
            {
                LoggerImplementObj.LoggerObj.WriteLog(Newtonsoft.Json.JsonConvert.SerializeObject(ResponseObject), "KYCscoreResponse");
                LoggerImplementObj.InsertInternalTransDBDetails(TransDBObject, SIDetail, ListInternalDB, ListInternalDetails);
            }
            return ResponseObject;
        }
    }
}