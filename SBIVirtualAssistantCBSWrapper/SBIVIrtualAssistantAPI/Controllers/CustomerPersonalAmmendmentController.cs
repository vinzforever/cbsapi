﻿using LotusWebAPI.Models;
using LotusWebAPI.Provider;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using SBISI.CoreBanking.Common.Provider;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace LotusWebAPI.Controllers
{
    public class CustomerPersonalAmmendmentController : ApiController
    {
        //[TokenSecurityFilter.TokenSecurityFilter]
        [HttpPost]
        public CustomerPersonalAmmendmentResponse AccountClosurePost(CustomerPersonalAmmendmentRequest RequestObject)
        {
            if (RequestObject == null)
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound) { Content = new StringContent("SI011^Input string was not in a correct format."), ReasonPhrase = "SI011^Input string was not in a correct format." });

            if (RequestObject.REQUEST_REF_NO.Substring(0, 5) != "SBILT")
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound) { Content = new StringContent("SI094^REFERENCE NUMBER INVALID. REFER DOCUMENT."), ReasonPhrase = "SI094^REFERENCE NUMBER INVALID. REFER DOCUMENT." });

            string ProductCode = "SBIVA";
            LoggerImplement LoggerImplementObj = new LoggerImplement(RequestObject.REQUEST_REF_NO, ProductCode);
            LoggerImplementObj.LoggerObj.WriteLog(Newtonsoft.Json.JsonConvert.SerializeObject(RequestObject), "CustomerPersonalAmmendmentRequest");

            CustomerPersonalAmmendmentResponse ResponseObject = new CustomerPersonalAmmendmentResponse();

            List<InternalTransDB> ListInternalDB = new List<InternalTransDB>();
            List<InternalTransDetails> ListInternalDetails = new List<InternalTransDetails>();

            SITransDetails SIDetail = null;
            SITransDetails SIDetail1 = null;
            SITransDB TransDBObject = null;

            CBSTransaction GenericObject = new CBSTransaction();

            DatabaseOperation DBObject = new DatabaseOperation();
            string BankName = DatabaseOperation.GetBankName(0);

            CustomerPersonalAmmendmentRequestInherit CustomerPersonalAmmendmentRequestInherit_InheritObj = new CustomerPersonalAmmendmentRequestInherit();

            CustomerPersonalAmmendmentRequestInherit_InheritObj.BRANCH_CODE = RequestObject.BRANCH_CODE;
            CustomerPersonalAmmendmentRequestInherit_InheritObj.COUNTRY_CODE = RequestObject.COUNTRY_CODE;
            CustomerPersonalAmmendmentRequestInherit_InheritObj.CSP_FLAG = RequestObject.CSP_FLAG;
            CustomerPersonalAmmendmentRequestInherit_InheritObj.CUSTOMER_NAME = RequestObject.CUSTOMER_NAME;
            CustomerPersonalAmmendmentRequestInherit_InheritObj.REQUEST_REF_NO = RequestObject.REQUEST_REF_NO;
            CustomerPersonalAmmendmentRequestInherit_InheritObj.CUSTOMER_NO = RequestObject.CUSTOMER_NO;
            CustomerPersonalAmmendmentRequestInherit_InheritObj.DATE_OF_BIRTH = RequestObject.DATE_OF_BIRTH;
            CustomerPersonalAmmendmentRequestInherit_InheritObj.POSTAL_CODE = RequestObject.POSTAL_CODE;
            CustomerPersonalAmmendmentRequestInherit_InheritObj.DATE_OF_DEATH = RequestObject.DATE_OF_DEATH;
            CustomerPersonalAmmendmentRequestInherit_InheritObj.OCCUPATION_DESCRIPTION = RequestObject.OCCUPATION_DESCRIPTION;
            CustomerPersonalAmmendmentRequestInherit_InheritObj.MARITAL_STATUS = RequestObject.MARITAL_STATUS;
            CustomerPersonalAmmendmentRequestInherit_InheritObj.DEPENDENT_PERSON = RequestObject.DEPENDENT_PERSON;
            CustomerPersonalAmmendmentRequestInherit_InheritObj.ECONOMIC_NO = RequestObject.ECONOMIC_NO;
            CustomerPersonalAmmendmentRequestInherit_InheritObj.EMPLOYED_DATE = RequestObject.EMPLOYED_DATE;
            CustomerPersonalAmmendmentRequestInherit_InheritObj.EMPLOYER_ADD = RequestObject.EMPLOYER_ADD;
            CustomerPersonalAmmendmentRequestInherit_InheritObj.EMPLOYER_ADD2 = RequestObject.EMPLOYER_ADD2;
            CustomerPersonalAmmendmentRequestInherit_InheritObj.EMPLOYER_PAN = RequestObject.EMPLOYER_PAN;
            CustomerPersonalAmmendmentRequestInherit_InheritObj.GENDER = RequestObject.GENDER;
            CustomerPersonalAmmendmentRequestInherit_InheritObj.OCCUPATION_CODE = RequestObject.OCCUPATION_CODE;
            CustomerPersonalAmmendmentRequestInherit_InheritObj.PLACE_OF_BIRTH = RequestObject.PLACE_OF_BIRTH;
            CustomerPersonalAmmendmentRequestInherit_InheritObj.JOURNAL_NO = RequestObject.JOURNAL_NO;

            TransDBObject = new SITransDB(RequestObject.REQUEST_REF_NO, ProductCode, DateTime.Now, "", "", "", "CustomerPersonalAmmend", IPAddressInfo.GetIP());
            ResponseObject.REQUEST_REF_NO = RequestObject.REQUEST_REF_NO;

            try
            {
                string DBResponse = LoggerImplementObj.InsertSITransDB(TransDBObject);
                if (DBResponse != "SUCCESS")
                {
                    string ERROR_DESCRIPTION = string.Empty;
                    switch (DBResponse)
                    {
                        case "SI007": ERROR_DESCRIPTION = "DATABASE ERROR";
                            break;

                        case "SI087": ERROR_DESCRIPTION = "REFERENCE NUMBER MUST BE UNIQUE";
                            break;
                    }
                    ResponseObject.ERROR_DESCRIPTION = ERROR_DESCRIPTION;
                    ResponseObject.REQUEST_REF_NO = RequestObject.REQUEST_REF_NO;
                    return ResponseObject;
                }
                string MRReferenceNumber = DBObject.generateSBISIRRN(BankName + "TC", "");
                DateTime Request_date_time = DateTime.Now;

                var SICreationResponseString = GenericObject.sendJsonToSIPost("0", Newtonsoft.Json.JsonConvert.SerializeObject(CustomerPersonalAmmendmentRequestInherit_InheritObj), "CustomerPersonalAmmendmentPost");
                DateTime Response_Date_Time = DateTime.Now;

                JObject SICreationObj = JObject.Parse(JObject.Parse(SICreationResponseString)["Response"].ToString());

                InternalTransDB InternalDB = new InternalTransDB("TC", RequestObject.REQUEST_REF_NO, MRReferenceNumber, Request_date_time, Response_Date_Time, SICreationObj["RESPONSE_STATUS"].ToString(), "", "CustomerPersonalAmmend");
                InternalTransDetails InternalDetail = new InternalTransDetails(MRReferenceNumber, Newtonsoft.Json.JsonConvert.SerializeObject(CustomerPersonalAmmendmentRequestInherit_InheritObj), SICreationResponseString);

                ListInternalDB.Add(InternalDB);
                ListInternalDetails.Add(InternalDetail);

                ResponseObject.REQUEST_REF_NO = RequestObject.REQUEST_REF_NO;

                if (SICreationObj["RESPONSE_STATUS"].ToString() == "1")
                {
                    TransDBObject.Error_Desc = SICreationObj["ERROR_DESCRIPTION"].ToString();
                    TransDBObject.Res_Time = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");

                    foreach (JProperty propertyName in SICreationObj.Properties())
                    {
                        if (ResponseObject.GetType().GetProperty(propertyName.Name) != null)
                            ResponseObject.GetType().GetProperty(propertyName.Name).SetValue(ResponseObject, SICreationObj[propertyName.Name].ToString());
                    }

                    SIDetail = new SITransDetails(RequestObject.REQUEST_REF_NO, ProductCode, Newtonsoft.Json.JsonConvert.SerializeObject(RequestObject), Newtonsoft.Json.JsonConvert.SerializeObject(ResponseObject), "CustomerPersonalAmmend");
                    LoggerImplementObj.InsertInternalTransDBDetails(TransDBObject, SIDetail, ListInternalDB, ListInternalDetails);

                    ResponseObject.ERROR_DESCRIPTION = TransDBObject.Error_Desc;

                    return ResponseObject;
                }

                foreach (JProperty propertyName in SICreationObj.Properties())
                {
                    if (ResponseObject.GetType().GetProperty(propertyName.Name) != null)
                        ResponseObject.GetType().GetProperty(propertyName.Name).SetValue(ResponseObject, SICreationObj[propertyName.Name].ToString());
                }

                TransDBObject.Res_Time = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                SIDetail1 = new SITransDetails(RequestObject.REQUEST_REF_NO, ProductCode, Newtonsoft.Json.JsonConvert.SerializeObject(RequestObject), JsonConvert.SerializeObject(ResponseObject), "CustomerPersonalAmmend");

                LoggerImplementObj.InsertInternalTransDBDetails(TransDBObject, SIDetail1, ListInternalDB, ListInternalDetails);

                return ResponseObject;
            }
            catch (Exception ex)
            {
                LoggerImplementObj.LoggerObj.WriteExceptionLog(ex.Message, "CustomerPersonalAmmendmentException");

                TransDBObject.Res_Time = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                TransDBObject.Error_Code = "SI001";
                TransDBObject.Error_Desc = ex.Message.Replace("'", "");
                ResponseObject.ERROR_DESCRIPTION = "Exception while processing the response";

                ResponseObject.REQUEST_REF_NO = RequestObject.REQUEST_REF_NO;

                SIDetail = new SITransDetails(RequestObject.REQUEST_REF_NO, ProductCode, Newtonsoft.Json.JsonConvert.SerializeObject(RequestObject), Newtonsoft.Json.JsonConvert.SerializeObject(ResponseObject), "CustomerPersonalAmmend");
                LoggerImplementObj.InsertInternalTransDBDetails(TransDBObject, SIDetail, ListInternalDB, ListInternalDetails);
            }
            finally
            {
                LoggerImplementObj.LoggerObj.WriteLog(Newtonsoft.Json.JsonConvert.SerializeObject(ResponseObject), "CustomerPersonalAmmendmentResponse");
                LoggerImplementObj.InsertInternalTransDBDetails(TransDBObject, SIDetail, ListInternalDB, ListInternalDetails);
            }
            return ResponseObject;
        }
    }
}