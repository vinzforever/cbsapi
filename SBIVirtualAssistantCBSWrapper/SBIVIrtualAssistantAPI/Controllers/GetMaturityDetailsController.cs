﻿using LotusWebAPI.Models;
using LotusWebAPI.Provider;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using SBISI.CoreBanking.Common.Provider;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace LotusWebAPI.Controllers
{
    //[TokenSecurityFilter.TokenSecurityFilter]
    public class GetMaturityDetailsController : ApiController
    {
        [HttpPost]
        public MaturityResponse MaturityDetails(MaturityRequest RequestObject)
        {
            if (RequestObject == null)
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound) { Content = new StringContent("SI011^Input string was not in a correct format."), ReasonPhrase = "SI011^Input string was not in a correct format." });

            if (RequestObject.REQUEST_REF_NO.Substring(0, 5) != "SBILT")
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound) { Content = new StringContent("SI094^REFERENCE NUMBER INVALID. REFER DOCUMENT."), ReasonPhrase = "SI094^REFERENCE NUMBER INVALID. REFER DOCUMENT." });

            string ProductCode = "SBIVA";
            LoggerImplement LoggerImplementObj = new LoggerImplement(RequestObject.REQUEST_REF_NO, ProductCode);
            LoggerImplementObj.LoggerObj.WriteLog(Newtonsoft.Json.JsonConvert.SerializeObject(RequestObject), "MaturityRequest");

            MaturityResponse ResponseObject = new MaturityResponse();

            List<InternalTransDB> ListInternalDB = new List<InternalTransDB>();
            List<InternalTransDetails> ListInternalDetails = new List<InternalTransDetails>();

            SITransDetails SIDetail = null;
            SITransDetails SIDetail1 = null;
            SITransDB TransDBObject = null;

            CBSTransaction GenericObject = new CBSTransaction();

            DatabaseOperation DBObject = new DatabaseOperation();
            string BankName = DatabaseOperation.GetBankName(0);

            string Reference = RequestObject.REQUEST_REF_NO;

            MaturityRequest_Inherit MaturityRequest_InheritObj = new MaturityRequest_Inherit();
            MaturityRequest_InheritObj.BRANCH_CODE = RequestObject.BRANCH_CODE;
            MaturityRequest_InheritObj.BANK_CODE = RequestObject.BANK_CODE;
            MaturityRequest_InheritObj.REQUEST_REF_NO = RequestObject.REQUEST_REF_NO;
            MaturityRequest_InheritObj.TERM_LENGTH = RequestObject.TERM_LENGTH;
            MaturityRequest_InheritObj.DAYS = RequestObject.DAYS;
            MaturityRequest_InheritObj.INTEREST_FREQUENCY = RequestObject.INTEREST_FREQUENCY;
            MaturityRequest_InheritObj.PRODUCT = RequestObject.PRODUCT;
            MaturityRequest_InheritObj.INTEREST_CATEGORY = RequestObject.INTEREST_CATEGORY;
            MaturityRequest_InheritObj.INTEREST_PAYMENT_METHOD = RequestObject.INTEREST_PAYMENT_METHOD;
            MaturityRequest_InheritObj.MONTHS = RequestObject.MONTHS;
            MaturityRequest_InheritObj.AMOUNT = RequestObject.AMOUNT;
            MaturityRequest_InheritObj.RECURRING_FREQUENCY = RequestObject.RECURRING_FREQUENCY;
            MaturityRequest_InheritObj.TERM_BASIS = RequestObject.TERM_BASIS;
            MaturityRequest_InheritObj.STARTDATE = RequestObject.STARTDATE;
            MaturityRequest_InheritObj.MATURITY_DATE = RequestObject.MATURITY_DATE;
            MaturityRequest_InheritObj.YEARS = RequestObject.YEARS;
            MaturityRequest_InheritObj.RECCURING_AMOUNT = RequestObject.RECCURING_AMOUNT;
            MaturityRequest_InheritObj.TYPE = RequestObject.TYPE;

            TransDBObject = new SITransDB(Reference, ProductCode, DateTime.Now, "", "", "", "GetMaturityDetails", IPAddressInfo.GetIP());

            ResponseObject.REQUEST_REF_NO = RequestObject.REQUEST_REF_NO;
            try
            {
                string DBResponse = LoggerImplementObj.InsertSITransDB(TransDBObject);
                if (DBResponse != "SUCCESS")
                {
                    string ERROR_DESCRIPTION = string.Empty;
                    switch (DBResponse)
                    {
                        case "SI007": ERROR_DESCRIPTION = "DATABASE ERROR";
                            break;

                        case "SI087": ERROR_DESCRIPTION = "REFERENCE NUMBER MUST BE UNIQUE";
                            break;
                    }
                    ResponseObject.ERROR_DESC = ERROR_DESCRIPTION;
                    ResponseObject.REQUEST_REF_NO = RequestObject.REQUEST_REF_NO;
                    return ResponseObject;
                }

                var SICreationResponseString = GenericObject.sendJsonToSIPost("0", Newtonsoft.Json.JsonConvert.SerializeObject(MaturityRequest_InheritObj), "GetMaturityDetailsPost");
                RequestObject.REQUEST_REF_NO = Reference;
                JObject SICreationObj = JObject.Parse(JObject.Parse(SICreationResponseString)["Response"].ToString());

                string MRReferenceNumber = DBObject.generateSBISIRRN(BankName + "TC", "");
                DateTime Request_date_time = DateTime.Now;
                DateTime Response_Date_Time = DateTime.Now;

                InternalTransDB InternalDB = new InternalTransDB("TC", RequestObject.REQUEST_REF_NO, MRReferenceNumber, Request_date_time, Response_Date_Time, SICreationObj["RESPONSE_STATUS"].ToString(), "", "GetMaturityDetailsPost");
                InternalTransDetails InternalDetail = new InternalTransDetails(MRReferenceNumber, Newtonsoft.Json.JsonConvert.SerializeObject(MaturityRequest_InheritObj), SICreationResponseString);

                ListInternalDB.Add(InternalDB);
                ListInternalDetails.Add(InternalDetail);

                if (SICreationObj["RESPONSE_STATUS"].ToString() == "1")
                {
                    TransDBObject.Error_Desc = SICreationObj["ERROR_DESCRIPTION"].ToString();
                    TransDBObject.Res_Time = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");

                    foreach (JProperty propertyName in SICreationObj.Properties())
                    {
                        if (ResponseObject.GetType().GetProperty(propertyName.Name) != null)
                            ResponseObject.GetType().GetProperty(propertyName.Name).SetValue(ResponseObject, SICreationObj[propertyName.Name].ToString());
                    }

                    SIDetail = new SITransDetails(Reference, ProductCode, Newtonsoft.Json.JsonConvert.SerializeObject(RequestObject), JsonConvert.SerializeObject(ResponseObject), "GetMaturityDetails");
                    LoggerImplementObj.InsertInternalTransDBDetails(TransDBObject, SIDetail, ListInternalDB, ListInternalDetails);

                    ResponseObject.ERROR_DESC = TransDBObject.Error_Desc;

                    ResponseObject.REQUEST_REF_NO = Reference;
                    return ResponseObject;
                }

                foreach (JProperty propertyName in SICreationObj.Properties())
                {
                    if (ResponseObject.GetType().GetProperty(propertyName.Name) != null)
                        ResponseObject.GetType().GetProperty(propertyName.Name).SetValue(ResponseObject, SICreationObj[propertyName.Name].ToString());
                }

                ResponseObject.REQUEST_REF_NO = Reference;
                TransDBObject.Res_Time = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                SIDetail1 = new SITransDetails(Reference, ProductCode, Newtonsoft.Json.JsonConvert.SerializeObject(RequestObject), Newtonsoft.Json.JsonConvert.SerializeObject(ResponseObject), "GetMaturityDetails");
                LoggerImplementObj.InsertInternalTransDBDetails(TransDBObject, SIDetail1, ListInternalDB, ListInternalDetails);

                return ResponseObject;
            }
            catch (Exception ex)
            {
                LoggerImplementObj.LoggerObj.WriteExceptionLog(ex.Message, "GetMaturityDetailsException");

                TransDBObject.Res_Time = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                TransDBObject.Error_Code = "SI001";
                TransDBObject.Error_Desc = ex.Message.Replace("'", "");
                ResponseObject.ERROR_DESC = "Exception while processing the response";
                SIDetail = new SITransDetails(Reference, ProductCode, Newtonsoft.Json.JsonConvert.SerializeObject(RequestObject), Newtonsoft.Json.JsonConvert.SerializeObject(ResponseObject), "GetMaturityDetails");
                LoggerImplementObj.InsertInternalTransDBDetails(TransDBObject, SIDetail, ListInternalDB, ListInternalDetails);
            }
            finally
            {
                LoggerImplementObj.LoggerObj.WriteLog(Newtonsoft.Json.JsonConvert.SerializeObject(ResponseObject), "MaturityResponse");
                LoggerImplementObj.InsertInternalTransDBDetails(TransDBObject, SIDetail, ListInternalDB, ListInternalDetails);
            }

            return ResponseObject;
        }
    }
}