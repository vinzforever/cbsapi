﻿using LotusWebAPI.Models;
using LotusWebAPI.Provider;
using Newtonsoft.Json.Linq;
using SBISI.CoreBanking.Common.Provider;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace LotusWebAPI.Controllers
{
    public class FDCreationController : ApiController
    {
        //[TokenSecurityFilter.TokenSecurityFilter]
        [HttpPost]
        public FDCreationResponse FDCreationPost(FDCreationRequest RequestObject)
        {
            if (RequestObject == null)
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound) { Content = new StringContent("SI011^Input string was not in a correct format."), ReasonPhrase = "SI011^Input string was not in a correct format." });

            if (RequestObject.REQUEST_REF_NO.Substring(0, 5) != "SBILT")
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound) { Content = new StringContent("SI094^REFERENCE NUMBER INVALID. REFER DOCUMENT."), ReasonPhrase = "SI094^REFERENCE NUMBER INVALID. REFER DOCUMENT." });

            string ProductCode = "SBIVA";
            LoggerImplement LoggerImplementObj = new LoggerImplement(RequestObject.REQUEST_REF_NO, ProductCode);
            LoggerImplementObj.LoggerObj.WriteLog(Newtonsoft.Json.JsonConvert.SerializeObject(RequestObject), "FDCreationRequest");

            FDCreationResponse ResponseObject = new FDCreationResponse();

            List<InternalTransDB> ListInternalDB = new List<InternalTransDB>();
            List<InternalTransDetails> ListInternalDetails = new List<InternalTransDetails>();

            SITransDetails SIDetail = null;
            SITransDetails SIDetail1 = null;
            SITransDB TransDBObject = null;

            CBSTransaction GenericObject = new CBSTransaction();

            DatabaseOperation DBObject = new DatabaseOperation();
            string BankName = DatabaseOperation.GetBankName(Convert.ToInt32(RequestObject.BANK_CODE));

            FDCreationRequest_Inherit FDCreationRequest_InheritObj = new FDCreationRequest_Inherit();
            FDCreationRequest_InheritObj.REQUEST_REF_NO = RequestObject.REQUEST_REF_NO;
            FDCreationRequest_InheritObj.BANK_CODE = RequestObject.BANK_CODE;
            FDCreationRequest_InheritObj.BRANCH_CODE = RequestObject.BRANCH_CODE;
            FDCreationRequest_InheritObj.MATURITY_INSTRUCTION = RequestObject.MATURITY_INSTRUCTION;
            FDCreationRequest_InheritObj.RD_EXPECTED_INSTALLMENT = RequestObject.RD_EXPECTED_INSTALLMENT;
            FDCreationRequest_InheritObj.MATURITY_DATE = RequestObject.MATURITY_DATE;
            FDCreationRequest_InheritObj.TERM_YEARS = RequestObject.TERM_YEARS;
            FDCreationRequest_InheritObj.TERM_MONTHS = RequestObject.TERM_MONTHS;
            FDCreationRequest_InheritObj.Term_Days = RequestObject.Term_Days;
            FDCreationRequest_InheritObj.MOBILISER = RequestObject.MOBILISER;
            FDCreationRequest_InheritObj.ACCT_SGMT_CODE = RequestObject.ACCT_SGMT_CODE;
            FDCreationRequest_InheritObj.TIME_BAND = RequestObject.TIME_BAND;
            FDCreationRequest_InheritObj.SECURITY_INDICATOR = RequestObject.SECURITY_INDICATOR;
            FDCreationRequest_InheritObj.ACCOUNT_CROSS_BORDER_RISK = RequestObject.ACCOUNT_CROSS_BORDER_RISK;
            FDCreationRequest_InheritObj.CROSS_BORDER_RISK = RequestObject.CROSS_BORDER_RISK;
            FDCreationRequest_InheritObj.CUSTOMER_RISK_DOMESTIC_RISK = RequestObject.CUSTOMER_RISK_DOMESTIC_RISK;
            FDCreationRequest_InheritObj.TERM_VALUE_DEPOSITED = RequestObject.TERM_VALUE_DEPOSITED;
            FDCreationRequest_InheritObj.TERM_BASIS = RequestObject.TERM_BASIS;
            FDCreationRequest_InheritObj.INTEREST_FREQUENCY = RequestObject.INTEREST_FREQUENCY;
            FDCreationRequest_InheritObj.TRANSFER_ACCOUNT_NUMBER = RequestObject.TRANSFER_ACCOUNT_NUMBER;
            FDCreationRequest_InheritObj.Language_Code = RequestObject.Language_Code;
            FDCreationRequest_InheritObj.INTEREST_PAYMENT_METHOD = RequestObject.INTEREST_PAYMENT_METHOD;
            FDCreationRequest_InheritObj.CURRENCY_CODE = RequestObject.CURRENCY_CODE;
            FDCreationRequest_InheritObj.CUSTOMER_CATEGORY = RequestObject.CUSTOMER_CATEGORY;
            FDCreationRequest_InheritObj.TERM_LOCATION = RequestObject.TERM_LOCATION;
            FDCreationRequest_InheritObj.TYPE = RequestObject.TYPE;
            FDCreationRequest_InheritObj.PRODUCT = RequestObject.PRODUCT;
            FDCreationRequest_InheritObj.POST_CODE = RequestObject.POST_CODE;
            FDCreationRequest_InheritObj.FORM60_61 = RequestObject.FORM60_61;
            FDCreationRequest_InheritObj.NATIONALITY = RequestObject.NATIONALITY;
            FDCreationRequest_InheritObj.PAN_NO = RequestObject.PAN_NO;
            FDCreationRequest_InheritObj.CURRENCY = RequestObject.CURRENCY;
            FDCreationRequest_InheritObj.ADDRESS_2 = RequestObject.ADDRESS_2;
            FDCreationRequest_InheritObj.ADDRESS_1 = RequestObject.ADDRESS_1;
            FDCreationRequest_InheritObj.CUSTOMER_NAME = RequestObject.CUSTOMER_NAME;
            FDCreationRequest_InheritObj.CUSTOMER_NO = RequestObject.CUSTOMER_NO;
            FDCreationRequest_InheritObj.RD_INSTL_FREQ = RequestObject.RD_INSTL_FREQ;
            FDCreationRequest_InheritObj.TERM_LENGTH = RequestObject.TERM_LENGTH;

            TransDBObject = new SITransDB(RequestObject.REQUEST_REF_NO, ProductCode, DateTime.Now, "", "", "", "FDCreation", IPAddressInfo.GetIP());

            ResponseObject.REQUEST_REF_NO = RequestObject.REQUEST_REF_NO;
            try
            {
                string DBResponse = LoggerImplementObj.InsertSITransDB(TransDBObject);
                if (DBResponse != "SUCCESS")
                {
                    string ERROR_DESCRIPTION = string.Empty;
                    switch (DBResponse)
                    {
                        case "SI007": ERROR_DESCRIPTION = "DATABASE ERROR";
                            break;

                        case "SI087": ERROR_DESCRIPTION = "REFERENCE NUMBER MUST BE UNIQUE";
                            break;
                    }
                    ResponseObject.ERROR_DESCRIPTION = ERROR_DESCRIPTION;
                    ResponseObject.REQUEST_REF_NO = RequestObject.REQUEST_REF_NO;
                    return ResponseObject;
                }

                var SICreationResponseString = GenericObject.sendJsonToSIPost("0", Newtonsoft.Json.JsonConvert.SerializeObject(FDCreationRequest_InheritObj), "FDCreationPost");
                LoggerImplementObj.LoggerObj.WriteLog(Newtonsoft.Json.JsonConvert.SerializeObject(ResponseObject), "FDCreationResponse");
                string MRReferenceNumber = DBObject.generateSBISIRRN(BankName + "TC", "");
                DateTime Request_date_time = DateTime.Now;
                DateTime Response_Date_Time = DateTime.Now;
                JObject SICreationObj = JObject.Parse(JObject.Parse(SICreationResponseString)["Response"].ToString());
                InternalTransDB InternalDB = new InternalTransDB("TC", RequestObject.REQUEST_REF_NO, MRReferenceNumber, Request_date_time, Response_Date_Time, SICreationObj["RESPONSE_STATUS"].ToString(), "", "FDCreationPost");
                InternalTransDetails InternalDetail = new InternalTransDetails(MRReferenceNumber, Newtonsoft.Json.JsonConvert.SerializeObject(FDCreationRequest_InheritObj), SICreationResponseString);

                ListInternalDB.Add(InternalDB);
                ListInternalDetails.Add(InternalDetail);

                ResponseObject.REQUEST_REF_NO = RequestObject.REQUEST_REF_NO;

                if (SICreationObj["RESPONSE_STATUS"].ToString() == "1")
                {
                    TransDBObject.Error_Desc = SICreationObj["ERROR_DESCRIPTION"].ToString();
                    TransDBObject.Res_Time = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");

                    foreach (JProperty propertyName in SICreationObj.Properties())
                    {
                        if (ResponseObject.GetType().GetProperty(propertyName.Name) != null)
                            ResponseObject.GetType().GetProperty(propertyName.Name).SetValue(ResponseObject, SICreationObj[propertyName.Name].ToString());
                    }

                    SIDetail = new SITransDetails(RequestObject.REQUEST_REF_NO, ProductCode, Newtonsoft.Json.JsonConvert.SerializeObject(RequestObject), Newtonsoft.Json.JsonConvert.SerializeObject(ResponseObject), "FDCreation");
                    LoggerImplementObj.InsertInternalTransDBDetails(TransDBObject, SIDetail, ListInternalDB, ListInternalDetails);

                    ResponseObject.ERROR_DESCRIPTION = TransDBObject.Error_Desc;

                    return ResponseObject;
                }

                foreach (JProperty propertyName in SICreationObj.Properties())
                {
                    if (ResponseObject.GetType().GetProperty(propertyName.Name) != null)
                        ResponseObject.GetType().GetProperty(propertyName.Name).SetValue(ResponseObject, SICreationObj[propertyName.Name].ToString());
                }

                TransDBObject.Res_Time = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                SIDetail1 = new SITransDetails(RequestObject.REQUEST_REF_NO, ProductCode, Newtonsoft.Json.JsonConvert.SerializeObject(RequestObject), Newtonsoft.Json.JsonConvert.SerializeObject(ResponseObject), "FDCreation");

                LoggerImplementObj.InsertInternalTransDBDetails(TransDBObject, SIDetail1, ListInternalDB, ListInternalDetails);

                return ResponseObject;
            }
            catch (Exception ex)
            {
                LoggerImplementObj.LoggerObj.WriteExceptionLog(ex.Message, "FDCreationException");

                TransDBObject.Res_Time = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                TransDBObject.Error_Code = "SI001";
                TransDBObject.Error_Desc = ex.Message.Replace("'", "");
                ResponseObject.ERROR_DESCRIPTION = "Exception while processing the response";

                ResponseObject.REQUEST_REF_NO = RequestObject.REQUEST_REF_NO;

                SIDetail = new SITransDetails(RequestObject.REQUEST_REF_NO, ProductCode, Newtonsoft.Json.JsonConvert.SerializeObject(RequestObject), Newtonsoft.Json.JsonConvert.SerializeObject(ResponseObject), "FDCreation");
                LoggerImplementObj.InsertInternalTransDBDetails(TransDBObject, SIDetail, ListInternalDB, ListInternalDetails);
            }
            finally
            {
                //LoggerImplementObj.LoggerObj.WriteLog(Newtonsoft.Json.JsonConvert.SerializeObject(ResponseObject), "FDCreationResponse");
                //LoggerImplementObj.InsertInternalTransDBDetails(TransDBObject, SIDetail, ListInternalDB, ListInternalDetails);
            }

            return ResponseObject;
        }
    }
}