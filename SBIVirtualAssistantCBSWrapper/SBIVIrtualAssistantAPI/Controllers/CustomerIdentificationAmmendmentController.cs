﻿using LotusWebAPI.Models;
using LotusWebAPI.Provider;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using SBISI.CoreBanking.Common.Provider;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace LotusWebAPI.Controllers
{
    public class CustomerIdentificationAmmendmentController : ApiController
    {
        //[TokenSecurityFilter.TokenSecurityFilter]
        [HttpPost]
        public CustomerIdentificationAmmendmentResponse AccountClosurePost(CustomerIdentificationAmmendmentRequest RequestObject)
        {
            if (RequestObject == null)
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound) { Content = new StringContent("SI011^Input string was not in a correct format."), ReasonPhrase = "SI011^Input string was not in a correct format." });

            if (RequestObject.REQUEST_REF_NO.Substring(0, 5) != "SBILT")
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound) { Content = new StringContent("SI094^REFERENCE NUMBER INVALID. REFER DOCUMENT."), ReasonPhrase = "SI094^REFERENCE NUMBER INVALID. REFER DOCUMENT." });

            string ProductCode = "SBIVA";
            LoggerImplement LoggerImplementObj = new LoggerImplement(RequestObject.REQUEST_REF_NO, ProductCode);
            LoggerImplementObj.LoggerObj.WriteLog(Newtonsoft.Json.JsonConvert.SerializeObject(RequestObject), "CustomerIdentificationAmmendmentRequest");

            CustomerIdentificationAmmendmentResponse ResponseObject = new CustomerIdentificationAmmendmentResponse();

            CBSTransaction GenericObject = new CBSTransaction();

            List<InternalTransDB> ListInternalDB = new List<InternalTransDB>();
            List<InternalTransDetails> ListInternalDetails = new List<InternalTransDetails>();

            SITransDetails SIDetail = null;
            SITransDetails SIDetail1 = null;
            SITransDB TransDBObject = null;

            DatabaseOperation DBObject = new DatabaseOperation();
            string BankName = DatabaseOperation.GetBankName(0);

            CustomerIdentificationAmmendmentRequestInherit CustomerIdentificationAmmendmentRequestInherit_InheritObj = new CustomerIdentificationAmmendmentRequestInherit();

            CustomerIdentificationAmmendmentRequestInherit_InheritObj.BRANCH_CODE = RequestObject.BRANCH_CODE;
            CustomerIdentificationAmmendmentRequestInherit_InheritObj.CUSTOMER_NO = RequestObject.CUSTOMER_NO;
            CustomerIdentificationAmmendmentRequestInherit_InheritObj.EXPIRY_DATE1 = RequestObject.EXPIRY_DATE1;
            CustomerIdentificationAmmendmentRequestInherit_InheritObj.EXPIRY_DATE2 = RequestObject.EXPIRY_DATE2;
            CustomerIdentificationAmmendmentRequestInherit_InheritObj.EXPIRY_DATE3 = RequestObject.EXPIRY_DATE3;
            CustomerIdentificationAmmendmentRequestInherit_InheritObj.REQUEST_REF_NO = RequestObject.REQUEST_REF_NO;
            CustomerIdentificationAmmendmentRequestInherit_InheritObj.EXPIRY_DATE4 = RequestObject.EXPIRY_DATE4;
            CustomerIdentificationAmmendmentRequestInherit_InheritObj.EXPIRY_DATE5 = RequestObject.EXPIRY_DATE5;
            CustomerIdentificationAmmendmentRequestInherit_InheritObj.FUNCTION_1 = RequestObject.FUNCTION_1;
            CustomerIdentificationAmmendmentRequestInherit_InheritObj.FUNCTION_3 = RequestObject.FUNCTION_3;
            CustomerIdentificationAmmendmentRequestInherit_InheritObj.FUNCTION_4 = RequestObject.FUNCTION_4;
            CustomerIdentificationAmmendmentRequestInherit_InheritObj.FUNCTION_5 = RequestObject.FUNCTION_5;
            CustomerIdentificationAmmendmentRequestInherit_InheritObj.FUNCTION_2 = RequestObject.FUNCTION_2;
            CustomerIdentificationAmmendmentRequestInherit_InheritObj.ID_ISSUE_DATE3 = RequestObject.ID_ISSUE_DATE3;
            CustomerIdentificationAmmendmentRequestInherit_InheritObj.ID_NUMBER1 = RequestObject.ID_NUMBER1;
            CustomerIdentificationAmmendmentRequestInherit_InheritObj.ID_NUMBER2 = RequestObject.ID_NUMBER2;
            CustomerIdentificationAmmendmentRequestInherit_InheritObj.ID_NUMBER3 = RequestObject.ID_NUMBER3;
            CustomerIdentificationAmmendmentRequestInherit_InheritObj.ID_NUMBER4 = RequestObject.ID_NUMBER4;
            CustomerIdentificationAmmendmentRequestInherit_InheritObj.ID_NUMBER5 = RequestObject.ID_NUMBER5;
            CustomerIdentificationAmmendmentRequestInherit_InheritObj.ID_TYPE1 = RequestObject.ID_TYPE1;
            CustomerIdentificationAmmendmentRequestInherit_InheritObj.ID_TYPE2 = RequestObject.ID_TYPE2;
            CustomerIdentificationAmmendmentRequestInherit_InheritObj.ID_TYPE3 = RequestObject.ID_TYPE3;
            CustomerIdentificationAmmendmentRequestInherit_InheritObj.ID_TYPE4 = RequestObject.ID_TYPE4;
            CustomerIdentificationAmmendmentRequestInherit_InheritObj.ID_TYPE5 = RequestObject.ID_TYPE5;
            CustomerIdentificationAmmendmentRequestInherit_InheritObj.ISSUE_PLACE1 = RequestObject.ISSUE_PLACE1;
            CustomerIdentificationAmmendmentRequestInherit_InheritObj.ISSUE_PLACE2 = RequestObject.ISSUE_PLACE2;
            CustomerIdentificationAmmendmentRequestInherit_InheritObj.ISSUE_PLACE3 = RequestObject.ISSUE_PLACE3;
            CustomerIdentificationAmmendmentRequestInherit_InheritObj.ISSUE_PLACE4 = RequestObject.ISSUE_PLACE4;
            CustomerIdentificationAmmendmentRequestInherit_InheritObj.ISSUE_PLACE5 = RequestObject.ISSUE_PLACE5;
            CustomerIdentificationAmmendmentRequestInherit_InheritObj.REMARKS1 = RequestObject.REMARKS1;
            CustomerIdentificationAmmendmentRequestInherit_InheritObj.REMARKS2 = RequestObject.REMARKS2;
            CustomerIdentificationAmmendmentRequestInherit_InheritObj.REMARKS3 = RequestObject.REMARKS3;
            CustomerIdentificationAmmendmentRequestInherit_InheritObj.REMARKS4 = RequestObject.REMARKS4;
            CustomerIdentificationAmmendmentRequestInherit_InheritObj.REMARKS5 = RequestObject.REMARKS5;

            TransDBObject = new SITransDB(RequestObject.REQUEST_REF_NO, ProductCode, DateTime.Now, "", "", "", "CustomerIdentificationAmmend", IPAddressInfo.GetIP());
            ResponseObject.REQUEST_REF_NO = RequestObject.REQUEST_REF_NO;

            try
            {
                string DBResponse = LoggerImplementObj.InsertSITransDB(TransDBObject);
                if (DBResponse != "SUCCESS")
                {
                    string ERROR_DESCRIPTION = string.Empty;
                    switch (DBResponse)
                    {
                        case "SI007": ERROR_DESCRIPTION = "DATABASE ERROR";
                            break;

                        case "SI087": ERROR_DESCRIPTION = "REFERENCE NUMBER MUST BE UNIQUE";
                            break;
                    }
                    ResponseObject.ERROR_DESCRIPTION = ERROR_DESCRIPTION;
                    ResponseObject.REQUEST_REF_NO = RequestObject.REQUEST_REF_NO;
                    return ResponseObject;
                }
                string MRReferenceNumber = DBObject.generateSBISIRRN(BankName + "TC", "");
                DateTime Request_date_time = DateTime.Now;

                var SICreationResponseString = GenericObject.sendJsonToSIPost("0", Newtonsoft.Json.JsonConvert.SerializeObject(CustomerIdentificationAmmendmentRequestInherit_InheritObj), "CustomerIdentificationAmmendmentPost");
                DateTime Response_Date_Time = DateTime.Now;

                JObject SICreationObj = JObject.Parse(JObject.Parse(SICreationResponseString)["Response"].ToString());

                InternalTransDB InternalDB = new InternalTransDB("TC", RequestObject.REQUEST_REF_NO, MRReferenceNumber, Request_date_time, Response_Date_Time, SICreationObj["RESPONSE_STATUS"].ToString(), "", "CustomerIdentificationAmmend");
                InternalTransDetails InternalDetail = new InternalTransDetails(MRReferenceNumber, Newtonsoft.Json.JsonConvert.SerializeObject(CustomerIdentificationAmmendmentRequestInherit_InheritObj), SICreationResponseString);

                ListInternalDB.Add(InternalDB);
                ListInternalDetails.Add(InternalDetail);

                ResponseObject.REQUEST_REF_NO = RequestObject.REQUEST_REF_NO;

                if (SICreationObj["RESPONSE_STATUS"].ToString() == "1")
                {
                    TransDBObject.Error_Desc = SICreationObj["ERROR_DESCRIPTION"].ToString();
                    TransDBObject.Res_Time = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");

                    foreach (JProperty propertyName in SICreationObj.Properties())
                    {
                        if (ResponseObject.GetType().GetProperty(propertyName.Name) != null)
                            ResponseObject.GetType().GetProperty(propertyName.Name).SetValue(ResponseObject, SICreationObj[propertyName.Name].ToString());
                    }

                    SIDetail = new SITransDetails(RequestObject.REQUEST_REF_NO, ProductCode, Newtonsoft.Json.JsonConvert.SerializeObject(RequestObject), Newtonsoft.Json.JsonConvert.SerializeObject(ResponseObject), "CustomerIdentificationAmmend");
                    LoggerImplementObj.InsertInternalTransDBDetails(TransDBObject, SIDetail, ListInternalDB, ListInternalDetails);

                    ResponseObject.ERROR_DESCRIPTION = TransDBObject.Error_Desc;

                    return ResponseObject;
                }

                foreach (JProperty propertyName in SICreationObj.Properties())
                {
                    if (ResponseObject.GetType().GetProperty(propertyName.Name) != null)
                        ResponseObject.GetType().GetProperty(propertyName.Name).SetValue(ResponseObject, SICreationObj[propertyName.Name].ToString());
                }

                TransDBObject.Res_Time = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                SIDetail1 = new SITransDetails(RequestObject.REQUEST_REF_NO, ProductCode, Newtonsoft.Json.JsonConvert.SerializeObject(RequestObject), JsonConvert.SerializeObject(ResponseObject), "CustomerIdentificationAmmend");

                LoggerImplementObj.InsertInternalTransDBDetails(TransDBObject, SIDetail1, ListInternalDB, ListInternalDetails);

                return ResponseObject;
            }
            catch (Exception ex)
            {
                LoggerImplementObj.LoggerObj.WriteExceptionLog(ex.Message, "CustomerIdentificationAmmendmentException");

                TransDBObject.Res_Time = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                TransDBObject.Error_Code = "SI001";
                TransDBObject.Error_Desc = ex.Message.Replace("'", "");
                ResponseObject.ERROR_DESCRIPTION = "Exception while processing the response";

                ResponseObject.REQUEST_REF_NO = RequestObject.REQUEST_REF_NO;

                SIDetail = new SITransDetails(RequestObject.REQUEST_REF_NO, ProductCode, Newtonsoft.Json.JsonConvert.SerializeObject(RequestObject), Newtonsoft.Json.JsonConvert.SerializeObject(ResponseObject), "CustomerIdentificationAmmend");
                LoggerImplementObj.InsertInternalTransDBDetails(TransDBObject, SIDetail, ListInternalDB, ListInternalDetails);
            }
            finally
            {
                LoggerImplementObj.LoggerObj.WriteLog(Newtonsoft.Json.JsonConvert.SerializeObject(ResponseObject), "CustomerIdentificationAmmendmentResponse");
                LoggerImplementObj.InsertInternalTransDBDetails(TransDBObject, SIDetail, ListInternalDB, ListInternalDetails);
            }

            return ResponseObject;
        }
    }
}