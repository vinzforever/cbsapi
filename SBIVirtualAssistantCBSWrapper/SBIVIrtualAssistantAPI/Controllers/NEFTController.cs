﻿using LotusWebAPI.Models;
using LotusWebAPI.Provider;
using SBISI.CoreBanking.Common.Provider;
using SBISI.CoreBanking.Transaction.Services.Models;
using SBISI.CoreBanking.Transaction.Services.Provider;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace LotusWebAPI.Controllers
{
    public class NEFTController : ApiController
    {
        [HttpPost]
        public async Task<NEFTRTGS_Response> NEFTTransferAction(NEFTRTGS_Request RequestObject)
        {
            if (RequestObject == null)
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound) { Content = new StringContent("SI011^Input string was not in a correct format."), ReasonPhrase = "SI011^Input string was not in a correct format." });

            if (string.IsNullOrEmpty(RequestObject.BranchCode) || string.IsNullOrEmpty(RequestObject.ReferenceNumber) || string.IsNullOrEmpty(RequestObject.TXNAMT) || string.IsNullOrEmpty(RequestObject.REMTACCTNO) || string.IsNullOrEmpty(RequestObject.BENFACCTNO) || string.IsNullOrEmpty(RequestObject.RECBNKIFSC))
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound) { Content = new StringContent("SI011^Input string was not in a correct format."), ReasonPhrase = "SI011^Input string was not in a correct format." });

            if (RequestObject.ReferenceNumber.Length != 25)
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound) { Content = new StringContent("SI004^INVALID/NOT-UNIQUE REFERENCE NUMBER"), ReasonPhrase = "SI004^INVALID/NOT-UNIQUE REFERENCE NUMBER" });

            if (RequestObject.BankCode > 7)
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound) { Content = new StringContent("SI006^Bank Code Is Not Proper"), ReasonPhrase = "SI006^Bank Code Is Not Proper" });

            if (RequestObject.ReferenceNumber.Substring(0, 5) != "SBILT")
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound) { Content = new StringContent("SI094^REFERENCE NUMBER INVALID. REFER DOCUMENT."), ReasonPhrase = "SI094^REFERENCE NUMBER INVALID. REFER DOCUMENT." });

            string ProductCode = "SBIVA";
            LoggerImplement LoggerImplementObj = new LoggerImplement(RequestObject.ReferenceNumber, ProductCode);
            LoggerImplementObj.LoggerObj.WriteLog(Newtonsoft.Json.JsonConvert.SerializeObject(RequestObject), "NEFTRTGS_Request");

            DatabaseOperation dbobject = new DatabaseOperation();
            string BankName = DatabaseOperation.GetBankName(RequestObject.BankCode);
            NEFTRTGS_Response ResponseObject = new NEFTRTGS_Response();

            List<InternalTransDB> ListInternalDB = new List<InternalTransDB>();
            List<InternalTransDetails> ListInternalDetails = new List<InternalTransDetails>();

            SITransDetails SIDetail = null;
            SITransDetails SIDetail1 = null;
            SITransDB TransDBObject = null;

            ResponseObject.ReferenceNumber = RequestObject.ReferenceNumber;

            try
            {
                TransDBObject = new SITransDB(RequestObject.ReferenceNumber, ProductCode, DateTime.Now, "", "", "", "NEFTTransferAction", IPAddressInfo.GetIP());

                string DBResponse = LoggerImplementObj.InsertSITransDB(TransDBObject);
                if (DBResponse != "SUCCESS")
                {
                    string ERROR_DESCRIPTION = string.Empty;
                    switch (DBResponse)
                    {
                        case "SI007": ERROR_DESCRIPTION = "DATABASE ERROR";
                            break;

                        case "SI087": ERROR_DESCRIPTION = "REFERENCE NUMBER MUST BE UNIQUE";
                            break;
                    }
                    ResponseObject.ErrorDesc = ERROR_DESCRIPTION;
                    ResponseObject.ErrorCode = DBResponse;
                    ResponseObject.ReferenceNumber = RequestObject.ReferenceNumber;
                    return ResponseObject;
                }

                //RequestObject.TXNAMT = (Convert.ToDouble(RequestObject.TXNAMT) / 1000).ToString();
                //RequestObject.COMMAMT = (Convert.ToDouble(RequestObject.COMMAMT) / 1000).ToString();

                CoreBankingTransactionServices coreService = new CoreBankingTransactionServices();
                string MRReferenceNumber = dbobject.generateSBISIRRN(BankName + "MR", "");
                ResponseObject.CBSReferenceNumber = MRReferenceNumber;
                NeftRequest TxnRequestObject = new NeftRequest(RequestObject.BranchCode, BankName, MRReferenceNumber, RequestObject.TXNAMT, RequestObject.COMMAMT, RequestObject.REMTACCTNO,
                    RequestObject.REMNAME, RequestObject.REMADD1, RequestObject.REMADD2, RequestObject.REMADD3, RequestObject.BENFACCTNO, RequestObject.BENFNAME,
                    RequestObject.BENFADD1, RequestObject.BENFADD2, RequestObject.BENFADD3, RequestObject.RECBNKIFSC, RequestObject.EMAILID, RequestObject.MOBNUMBER,
                    RequestObject.SNDIFSC, RequestObject.InstructionPriority, RequestObject.PurposeCode);

                var NeftTxnInfo = await coreService.NeftTransaction(TxnRequestObject);
                InternalTransDB InternalDB = new InternalTransDB("MR", RequestObject.ReferenceNumber, MRReferenceNumber, NeftTxnInfo.Item2.RequestDateTime, NeftTxnInfo.Item2.ResponseDateTime, NeftTxnInfo.Item2.ErrorCode, NeftTxnInfo.Item2.ErrorDescription, "NeftTransaction");
                InternalTransDetails InternalDetail = new InternalTransDetails(MRReferenceNumber, NeftTxnInfo.Item2.RequestString, NeftTxnInfo.Item2.ResponseString);

                ListInternalDB.Add(InternalDB);
                ListInternalDetails.Add(InternalDetail);

                if (NeftTxnInfo.Item1 == null || !string.IsNullOrEmpty(NeftTxnInfo.Item2.ErrorCode))
                {
                    TransDBObject.Error_Code = NeftTxnInfo.Item2.ErrorCode;
                    TransDBObject.Error_Desc = NeftTxnInfo.Item2.ErrorDescription;
                    ResponseObject.ErrorCode = NeftTxnInfo.Item2.ErrorCode;
                    ResponseObject.ErrorDesc = NeftTxnInfo.Item2.ErrorDescription;
                    TransDBObject.Res_Time = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                    SIDetail = new SITransDetails(RequestObject.ReferenceNumber, ProductCode, RequestObject.TostringContent(), Newtonsoft.Json.JsonConvert.SerializeObject(NeftTxnInfo.Item1), "NEFTTransferAction");
                    LoggerImplementObj.InsertInternalTransDBDetails(TransDBObject, SIDetail, ListInternalDB, ListInternalDetails);

                    LoggerImplementObj.LoggerObj.WriteLog(Newtonsoft.Json.JsonConvert.SerializeObject(ResponseObject), "NEFTRTGS_Response");

                    return ResponseObject;
                }

                ResponseObject.DateOfTransaction = NeftTxnInfo.Item1.DateOfTransaction;
                ResponseObject.JournalNumber = NeftTxnInfo.Item1.JournalNumber;
                ResponseObject.UTRno = NeftTxnInfo.Item1.UTRno;
                ResponseObject.ReferenceNumber = RequestObject.ReferenceNumber;

                LoggerImplementObj.LoggerObj.WriteLog(Newtonsoft.Json.JsonConvert.SerializeObject(ResponseObject), "NEFTRTGS_Response");

                TransDBObject.Res_Time = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                SIDetail1 = new SITransDetails(RequestObject.ReferenceNumber, ProductCode, RequestObject.TostringContent(), Newtonsoft.Json.JsonConvert.SerializeObject(ResponseObject), "NEFTTransferAction");
                LoggerImplementObj.InsertInternalTransDBDetails(TransDBObject, SIDetail1, ListInternalDB, ListInternalDetails);

                return ResponseObject;
            }
            catch (Exception ex)
            {
                LoggerImplementObj.LoggerObj.WriteExceptionLog(ex.Message, "NEFTException");

                TransDBObject.Res_Time = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                TransDBObject.Error_Code = "SI001";
                TransDBObject.Error_Desc = ex.Message.Replace("'", "");
                ResponseObject.ErrorDesc = "Exception while processing the response";

                SIDetail1 = new SITransDetails(RequestObject.ReferenceNumber, ProductCode, Newtonsoft.Json.JsonConvert.SerializeObject(RequestObject), Newtonsoft.Json.JsonConvert.SerializeObject(ResponseObject), "LoanAccountInformation");
                LoggerImplementObj.InsertInternalTransDBDetails(TransDBObject, SIDetail1, ListInternalDB, ListInternalDetails);
            }
            finally
            {
                //LoggerImplementObj.LoggerObj.WriteLog(Newtonsoft.Json.JsonConvert.SerializeObject(ResponseObject), "NEFTRTGS_Response");
                //LoggerImplementObj.InsertInternalTransDBDetails(TransDBObject, SIDetail, ListInternalDB, ListInternalDetails);
            }

            return ResponseObject;
        }
    }
}