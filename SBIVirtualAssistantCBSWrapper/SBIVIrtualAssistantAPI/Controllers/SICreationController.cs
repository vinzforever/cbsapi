﻿using LotusWebAPI.Models;
using LotusWebAPI.Provider;
using Newtonsoft.Json.Linq;
using SBISI.CoreBanking.Common.Provider;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace LotusWebAPI.Controllers
{
    public class SICreationController : ApiController
    {
        //[TokenSecurityFilter.TokenSecurityFilter]
        [HttpPost]
        public SICreateResponse SICreationPost(SICreateRequest RequestObject)
        {
            if (RequestObject == null)
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound) { Content = new StringContent("SI011^Input string was not in a correct format."), ReasonPhrase = "SI011^Input string was not in a correct format." });
            if (RequestObject.REQUEST_REF_NO.Substring(0, 5) != "SBILT")
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound) { Content = new StringContent("SI094^REFERENCE NUMBER INVALID. REFER DOCUMENT."), ReasonPhrase = "SI094^REFERENCE NUMBER INVALID. REFER DOCUMENT." });

            string ProductCode = "SBIVA";
            LoggerImplement LoggerImplementObj = new LoggerImplement(RequestObject.REQUEST_REF_NO, ProductCode);
            LoggerImplementObj.LoggerObj.WriteLog(Newtonsoft.Json.JsonConvert.SerializeObject(RequestObject), "SICreateRequest");

            SICreateResponse ResponseObject = new SICreateResponse();

            List<InternalTransDB> ListInternalDB = new List<InternalTransDB>();
            List<InternalTransDetails> ListInternalDetails = new List<InternalTransDetails>();

            SITransDetails SIDetail = null;
            SITransDetails SIDetail1 = null;
            SITransDB TransDBObject = null;

            CBSTransaction GenericObject = new CBSTransaction();

            DatabaseOperation DBObject = new DatabaseOperation();
            string BankName = DatabaseOperation.GetBankName(Convert.ToInt32(RequestObject.BANK_CODE));

            SICreateRequest_Inherit SICreateRequest_InheritObj = new SICreateRequest_Inherit();
            SICreateRequest_InheritObj.REQUEST_REF_NO = RequestObject.REQUEST_REF_NO;
            SICreateRequest_InheritObj.BANK_CODE = RequestObject.BANK_CODE;
            SICreateRequest_InheritObj.BRANCH_CODE = RequestObject.BRANCH_CODE;
            SICreateRequest_InheritObj.Account_Number = RequestObject.Account_Number;
            SICreateRequest_InheritObj.Amount = RequestObject.Amount;
            SICreateRequest_InheritObj.Frequency = RequestObject.Frequency;
            SICreateRequest_InheritObj.Frequency_Code = RequestObject.Frequency_Code;
            SICreateRequest_InheritObj.To_Account = RequestObject.To_Account;
            SICreateRequest_InheritObj.From_Account_Comments = RequestObject.From_Account_Comments;
            SICreateRequest_InheritObj.Start_Date = RequestObject.Start_Date;
            SICreateRequest_InheritObj.Currency = RequestObject.Currency;
            SICreateRequest_InheritObj.Security_Code = RequestObject.Security_Code;
            SICreateRequest_InheritObj.Priority_Code = RequestObject.Priority_Code;
            SICreateRequest_InheritObj.To_Account_Comments = RequestObject.To_Account_Comments;
            SICreateRequest_InheritObj.System = RequestObject.System;
            SICreateRequest_InheritObj.End_Date = RequestObject.End_Date;
            SICreateRequest_InheritObj.Auto_Chase_Days = RequestObject.Auto_Chase_Days;
            SICreateRequest_InheritObj.Payment_Type = RequestObject.Payment_Type;
            SICreateRequest_InheritObj.Purpose = RequestObject.Purpose;
            SICreateRequest_InheritObj.Hold_Required = RequestObject.Hold_Required;
            SICreateRequest_InheritObj.Apply_Fees = RequestObject.Apply_Fees;
            SICreateRequest_InheritObj.Purpose_Of_SI = RequestObject.Purpose_Of_SI;
            SICreateRequest_InheritObj.Lon_Chase_Days = RequestObject.Lon_Chase_Days;
            SICreateRequest_InheritObj.Other_Chase_Days = RequestObject.Other_Chase_Days;

            TransDBObject = new SITransDB(RequestObject.REQUEST_REF_NO, ProductCode, DateTime.Now, "", "", "", "SICreation", IPAddressInfo.GetIP());
            ResponseObject.REQUEST_REF_NO = RequestObject.REQUEST_REF_NO;

            try
            {
                string DBResponse = LoggerImplementObj.InsertSITransDB(TransDBObject);
                if (DBResponse != "SUCCESS")
                {
                    string ERROR_DESCRIPTION = string.Empty;
                    switch (DBResponse)
                    {
                        case "SI007": ERROR_DESCRIPTION = "DATABASE ERROR";
                            break;

                        case "SI087": ERROR_DESCRIPTION = "REFERENCE NUMBER MUST BE UNIQUE";
                            break;
                    }
                    ResponseObject.ERROR_DESCRIPTION = ERROR_DESCRIPTION;
                    ResponseObject.REQUEST_REF_NO = RequestObject.REQUEST_REF_NO;
                    return ResponseObject;
                }
                var SICreationResponseString = GenericObject.sendJsonToSIPost("0", Newtonsoft.Json.JsonConvert.SerializeObject(SICreateRequest_InheritObj), "SICreationPost");
                LoggerImplementObj.LoggerObj.WriteLog(SICreationResponseString, "SICreationResponseString");

                JObject SICreationObj = JObject.Parse(JObject.Parse(SICreationResponseString)["Response"].ToString());
                ResponseObject.REQUEST_REF_NO = RequestObject.REQUEST_REF_NO;

                string MRReferenceNumber = DBObject.generateSBISIRRN(BankName + "TC", "");
                DateTime Request_date_time = DateTime.Now;
                DateTime Response_Date_Time = DateTime.Now;

                InternalTransDB InternalDB = new InternalTransDB("TC", RequestObject.REQUEST_REF_NO, MRReferenceNumber, Request_date_time, Response_Date_Time, SICreationObj["RESPONSE_STATUS"].ToString(), "", "SICreationPost");
                InternalTransDetails InternalDetail = new InternalTransDetails(MRReferenceNumber, Newtonsoft.Json.JsonConvert.SerializeObject(SICreateRequest_InheritObj), SICreationResponseString);

                ListInternalDB.Add(InternalDB);
                ListInternalDetails.Add(InternalDetail);

                if (SICreationObj["RESPONSE_STATUS"].ToString() == "1")
                {
                    TransDBObject.Error_Desc = SICreationObj["ERROR_DESCRIPTION"].ToString();
                    TransDBObject.Res_Time = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");

                    foreach (JProperty propertyName in SICreationObj.Properties())
                    {
                        if (ResponseObject.GetType().GetProperty(propertyName.Name) != null)
                            ResponseObject.GetType().GetProperty(propertyName.Name).SetValue(ResponseObject, SICreationObj[propertyName.Name].ToString());
                    }

                    SIDetail = new SITransDetails(RequestObject.REQUEST_REF_NO, ProductCode, Newtonsoft.Json.JsonConvert.SerializeObject(RequestObject), Newtonsoft.Json.JsonConvert.SerializeObject(ResponseObject), "SICreation");
                    LoggerImplementObj.InsertInternalTransDBDetails(TransDBObject, SIDetail, ListInternalDB, ListInternalDetails);

                    ResponseObject.ERROR_DESCRIPTION = TransDBObject.Error_Desc;

                    return ResponseObject;
                }

                foreach (JProperty propertyName in SICreationObj.Properties())
                {
                    if (ResponseObject.GetType().GetProperty(propertyName.Name) != null)
                        ResponseObject.GetType().GetProperty(propertyName.Name).SetValue(ResponseObject, SICreationObj[propertyName.Name].ToString());
                }

                TransDBObject.Res_Time = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                SIDetail1 = new SITransDetails(RequestObject.REQUEST_REF_NO, ProductCode, Newtonsoft.Json.JsonConvert.SerializeObject(RequestObject), Newtonsoft.Json.JsonConvert.SerializeObject(ResponseObject), "SICreation");

                LoggerImplementObj.InsertInternalTransDBDetails(TransDBObject, SIDetail1, ListInternalDB, ListInternalDetails);

                return ResponseObject;
            }
            catch (Exception ex)
            {
                LoggerImplementObj.LoggerObj.WriteExceptionLog(ex.Message, "SICreationException");

                TransDBObject.Res_Time = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                TransDBObject.Error_Code = "SI001";
                TransDBObject.Error_Desc = ex.Message.Replace("'", "");
                ResponseObject.ERROR_DESCRIPTION = "Exception while processing the response";

                ResponseObject.REQUEST_REF_NO = RequestObject.REQUEST_REF_NO;

                SIDetail = new SITransDetails(RequestObject.REQUEST_REF_NO, ProductCode, Newtonsoft.Json.JsonConvert.SerializeObject(RequestObject), Newtonsoft.Json.JsonConvert.SerializeObject(ResponseObject), "SICreation");
                LoggerImplementObj.InsertInternalTransDBDetails(TransDBObject, SIDetail, ListInternalDB, ListInternalDetails);
            }
            finally
            {
                LoggerImplementObj.LoggerObj.WriteLog(Newtonsoft.Json.JsonConvert.SerializeObject(ResponseObject), "SICreateResponse");
                LoggerImplementObj.InsertInternalTransDBDetails(TransDBObject, SIDetail, ListInternalDB, ListInternalDetails);
            }

            return ResponseObject;
        }
    }
}