﻿using LotusWebAPI.Models;
using LotusWebAPI.Provider;
using SBISI.CoreBanking.Common.Provider;
using SBISI.CoreBanking.Services.Models;
using SBISI.CoreBanking.Services.Providers;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace LotusWebAPI.Controllers
{
    public class AccountListController : ApiController
    {
        ////[TokenSecurityFilter.TokenSecurityFilter]
        [HttpPost]
        public async Task<AccountListResponse> GetAccountDetails(CBSAccountRequest RequestObject)
        {
            if (RequestObject == null)
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound) { Content = new StringContent("SI011^Input string was not in a correct format."), ReasonPhrase = "SI011^Input string was not in a correct format." });

            if (string.IsNullOrEmpty(RequestObject.Branch_Code) || string.IsNullOrEmpty(RequestObject.ReferenceNumber) || string.IsNullOrEmpty(RequestObject.Request))
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound) { Content = new StringContent("SI011^Input string was not in a correct format."), ReasonPhrase = "SI011^Input string was not in a correct format." });

            if (RequestObject.ReferenceNumber.Length != 25)
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound) { Content = new StringContent("SI004^INVALID/NOT-UNIQUE REFERENCE NUMBER"), ReasonPhrase = "SI004^INVALID/NOT-UNIQUE REFERENCE NUMBER" });

            if (RequestObject.Bank_Code > 7)
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound) { Content = new StringContent("SI006^Bank Code Is Not Proper"), ReasonPhrase = "SI006^Bank Code Is Not Proper" });

            if (RequestObject.ReferenceNumber.Substring(0, 5) != "SBILT")
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound) { Content = new StringContent("SI094^REFERENCE NUMBER INVALID. REFER DOCUMENT."), ReasonPhrase = "SI094^REFERENCE NUMBER INVALID. REFER DOCUMENT." });

            string ProductCode = "SBIVA";
            LoggerImplement LoggerImplementObj = new LoggerImplement(RequestObject.ReferenceNumber, ProductCode);
            LoggerImplementObj.LoggerObj.WriteLog(Newtonsoft.Json.JsonConvert.SerializeObject(RequestObject), "CBSAccountRequest");

            DatabaseOperation DBObject = new DatabaseOperation();
            string BankName = DatabaseOperation.GetBankName(RequestObject.Bank_Code);

            AccountListResponse ResponseObject = new AccountListResponse();

            List<InternalTransDB> ListInternalDB = new List<InternalTransDB>();
            List<InternalTransDetails> ListInternalDetails = new List<InternalTransDetails>();

            SITransDetails SIDetail = null;
            SITransDetails SIDetail1 = null;
            SITransDB TransDBObject = null;

            TransDBObject = new SITransDB(RequestObject.ReferenceNumber, ProductCode, DateTime.Now, "", "", "", "GetAccountDetails", IPAddressInfo.GetIP());
            ResponseObject.ReferenceNumber = RequestObject.ReferenceNumber;
            ResponseObject.AccDetailsList = new List<AccountDetail>();
            try
            {
                string DBResponse = LoggerImplementObj.InsertSITransDB(TransDBObject);
                if (DBResponse != "SUCCESS")
                {
                    string ERROR_DESCRIPTION = string.Empty;
                    switch (DBResponse)
                    {
                        case "SI007": ERROR_DESCRIPTION = "DATABASE ERROR";
                            break;

                        case "SI087": ERROR_DESCRIPTION = "REFERENCE NUMBER MUST BE UNIQUE";
                            break;
                    }
                    ResponseObject.ErrorDesc = ERROR_DESCRIPTION;
                    ResponseObject.ErrorCode = DBResponse;
                    return ResponseObject;
                }

                CoreBankingServices coreService = new CoreBankingServices();
                string BancsReferenceNumber = DBObject.generateSBISIRRN("SBIBA", "");
                BancsPortRequest BancsRequestObject = new BancsPortRequest(BankName, RequestObject.Request.PadLeft(17, '0'));
                var AllAccountInformation = await coreService.GetALLAccountsInformation(BancsRequestObject);
                InternalTransDB InternalDB = new InternalTransDB("BANCS", RequestObject.ReferenceNumber, BancsReferenceNumber, AllAccountInformation.Item2.RequestDateTime, AllAccountInformation.Item2.ResponseDateTime, AllAccountInformation.Item2.ErrorCode, AllAccountInformation.Item2.ErrorDescription, "GetALLAccountsInformation");
                InternalTransDetails InternalDetail = new InternalTransDetails(BancsReferenceNumber, AllAccountInformation.Item2.RequestString, AllAccountInformation.Item2.ResponseString);

                ListInternalDB.Add(InternalDB);
                ListInternalDetails.Add(InternalDetail);

                ResponseObject.ReferenceNumber = RequestObject.ReferenceNumber;

                if (AllAccountInformation.Item1 == null || !string.IsNullOrEmpty(AllAccountInformation.Item2.ErrorCode))
                {
                    TransDBObject.Error_Code = AllAccountInformation.Item2.ErrorCode;
                    TransDBObject.Error_Desc = AllAccountInformation.Item2.ErrorDescription;
                    TransDBObject.Res_Time = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");

                    ResponseObject.ReferenceNumber = RequestObject.ReferenceNumber;
                    ResponseObject.ErrorDesc = TransDBObject.Error_Desc;
                    ResponseObject.ErrorCode = TransDBObject.Error_Code;

                    SIDetail = new SITransDetails(RequestObject.ReferenceNumber, ProductCode, Newtonsoft.Json.JsonConvert.SerializeObject(RequestObject), Newtonsoft.Json.JsonConvert.SerializeObject(ResponseObject), "GetAccountDetails");

                    LoggerImplementObj.InsertInternalTransDBDetails(TransDBObject, SIDetail, ListInternalDB, ListInternalDetails);
                    LoggerImplementObj.LoggerObj.WriteLog(Newtonsoft.Json.JsonConvert.SerializeObject(ResponseObject), "AccountListResponse");
                    return ResponseObject;
                }

                ResponseObject.Address1 = AllAccountInformation.Item1.main_ac.Address1;
                ResponseObject.Address2 = AllAccountInformation.Item1.main_ac.Address2;
                ResponseObject.Address3 = AllAccountInformation.Item1.main_ac.Address3;
                ResponseObject.Address4 = AllAccountInformation.Item1.main_ac.Address4;
                ResponseObject.CIFname = AllAccountInformation.Item1.main_ac.CIFname;
                ResponseObject.CIFnumber = AllAccountInformation.Item1.main_ac.CIFnumber;
                ResponseObject.EmailID = AllAccountInformation.Item1.main_ac.EmailID;
                ResponseObject.MobileNumber = AllAccountInformation.Item1.main_ac.MobileNumber;
                ResponseObject.PFId = AllAccountInformation.Item1.main_ac.PFId;
                ResponseObject.PinCode = AllAccountInformation.Item1.main_ac.PinCode;
                ResponseObject.SomeDate = AllAccountInformation.Item1.main_ac.SomeDate;

                foreach (AccountsDetail ac in AllAccountInformation.Item1.ac_details)
                {
                    AccountDetail AccountObj = new AccountDetail();
                    AccountObj.AccountCategory = ac.AccountCategory;
                    AccountObj.AccountNumber = ac.AccountNumber;
                    AccountObj.AccountOpeningDate = ac.AccountOpeningDate;
                    AccountObj.AccountStatus = ac.AccountStatus;
                    AccountObj.AccountType = ac.AccountType;
                    AccountObj.AccountTypeCode = ac.AccountTypeCode;
                    AccountObj.ApprovedSanctionedAmount = ac.ApprovedSanctionedAmount;
                    AccountObj.AvailableBalance = ac.AvailableBalance;
                    AccountObj.Currency = ac.Currency;
                    AccountObj.DPAvailable = ac.DPAvailable;
                    AccountObj.HomeBranch = ac.HomeBranch;
                    AccountObj.IntCategory = ac.IntCategory;
                    AccountObj.InterestRate = ac.InterestRate;
                    AccountObj.Link = ac.Link;
                    AccountObj.MaturityAmount = ac.MaturityAmount;
                    AccountObj.MaturityDate = ac.MaturityDate;
                    AccountObj.PrincipleAmount = ac.PrincipleAmount;
                    AccountObj.TermPeriod = ac.TermPeriod;
                    AccountObj.TotalBalance = ac.TotalBalance;
                    ResponseObject.AccDetailsList.Add(AccountObj);
                }

                TransDBObject.Res_Time = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                SITransDetails SIDetailNew = new SITransDetails(RequestObject.ReferenceNumber, ProductCode, Newtonsoft.Json.JsonConvert.SerializeObject(RequestObject), Newtonsoft.Json.JsonConvert.SerializeObject(ResponseObject), "GetAccountDetails");
                SIDetail = SIDetailNew;
                LoggerImplementObj.InsertInternalTransDBDetails(TransDBObject, SIDetail, ListInternalDB, ListInternalDetails);
                LoggerImplementObj.LoggerObj.WriteLog(Newtonsoft.Json.JsonConvert.SerializeObject(ResponseObject), "AccountListResponse");
                return ResponseObject;
            }
            catch (Exception ex)
            {
                LoggerImplementObj.LoggerObj.WriteExceptionLog(ex.Message, "AccountListException");

                TransDBObject = new SITransDB(RequestObject.ReferenceNumber, ProductCode, DateTime.Now, "", "", "", "GetAccountDetails", IPAddressInfo.GetIP());
                TransDBObject.Res_Time = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                ResponseObject.ReferenceNumber = RequestObject.ReferenceNumber;
                ResponseObject.ErrorCode = "SI011";
                ResponseObject.ErrorDesc = "Input string was not in a correct format.";
                SIDetail = new SITransDetails(RequestObject.ReferenceNumber, ProductCode, Newtonsoft.Json.JsonConvert.SerializeObject(RequestObject), Newtonsoft.Json.JsonConvert.SerializeObject(ResponseObject), "GetAccountDetails");

                LoggerImplementObj.InsertInternalTransDBDetails(TransDBObject, SIDetail1, ListInternalDB, ListInternalDetails);
            }
            finally
            {
                //LoggerImplementObj.LoggerObj.WriteLog(Newtonsoft.Json.JsonConvert.SerializeObject(ResponseObject), "AccountListResponse");
                //LoggerImplementObj.InsertInternalTransDBDetails(TransDBObject, SIDetail, ListInternalDB, ListInternalDetails);
            }

            return ResponseObject;
        }
    }
}