﻿using LotusWebAPI.Models;
using LotusWebAPI.Provider;
using SBISI.CoreBanking.Common.Provider;
using SBISI.CoreBanking.Services.Models;
using SBISI.CoreBanking.Services.Providers;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace LotusWebAPI.Controllers
{
    public class AccountInformationController : ApiController
    {
        //[TokenSecurityFilter.TokenSecurityFilter]
        [HttpPost]
        public async Task<BalanceEnquiryResponse> AccountInformation(GenericEnquiryRequest RequestObject)
        {

            if (RequestObject == null)
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound) { Content = new StringContent("SI011^Input string was not in a correct format."), ReasonPhrase = "SI011^Input string was not in a correct format." });

            if (string.IsNullOrEmpty(RequestObject.ReferenceNumber) || string.IsNullOrEmpty(RequestObject.RequestData))
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound) { Content = new StringContent("SI011^Input string was not in a correct format."), ReasonPhrase = "SI011^Input string was not in a correct format." });

            if (RequestObject.ReferenceNumber.Length != 25)
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound) { Content = new StringContent("SI004^INVALID/NOT-UNIQUE REFERENCE NUMBER"), ReasonPhrase = "SI004^INVALID/NOT-UNIQUE REFERENCE NUMBER" });

            if (RequestObject.Bank_Code > 7)
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound) { Content = new StringContent("SI006^Bank Code Is Not Proper"), ReasonPhrase = "SI006^Bank Code Is Not Proper" });

            if (RequestObject.ReferenceNumber.Substring(0, 5) != "SBILT")
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound) { Content = new StringContent("SI094^REFERENCE NUMBER INVALID. REFER DOCUMENT."), ReasonPhrase = "SI094^REFERENCE NUMBER INVALID. REFER DOCUMENT." });

            string ProductCode = "SBIVA";
            LoggerImplement LoggerImplementObj = new LoggerImplement(RequestObject.ReferenceNumber, ProductCode);
            LoggerImplementObj.LoggerObj.WriteLog(Newtonsoft.Json.JsonConvert.SerializeObject(RequestObject), "GenericEnquiryRequest");

            BalanceEnquiryResponse ResponseObject = new BalanceEnquiryResponse();

            List<InternalTransDB> ListInternalDB = new List<InternalTransDB>();
            List<InternalTransDetails> ListInternalDetails = new List<InternalTransDetails>();

            SITransDetails SIDetail = null;
            SITransDetails SIDetail1 = null;
            SITransDB TransDBObject = null;

            DatabaseOperation DBObject = new DatabaseOperation();
            string BankName = DatabaseOperation.GetBankName(RequestObject.Bank_Code);
            string MRReferenceNumber = DBObject.generateSBISIRRN(BankName + "MR", "");

            TransDBObject = new SITransDB(RequestObject.ReferenceNumber, ProductCode, DateTime.Now, "", "", "", "AccountInformation", IPAddressInfo.GetIP());
            ResponseObject.ReferenceNumber = RequestObject.ReferenceNumber;

            try
            {
                string DBResponse = LoggerImplementObj.InsertSITransDB(TransDBObject);
                if (DBResponse != "SUCCESS")
                {
                    string ERROR_DESCRIPTION = string.Empty;
                    switch (DBResponse)
                    {
                        case "SI007": ERROR_DESCRIPTION = "DATABASE ERROR";
                            break;

                        case "SI087": ERROR_DESCRIPTION = "REFERENCE NUMBER MUST BE UNIQUE";
                            break;
                    }
                    ResponseObject.ERROR_DESC = ERROR_DESCRIPTION;
                    return ResponseObject;
                }
                CoreBankingServices coreService = new CoreBankingServices();
                MultipleRemittenceAccountRequest CBSReuest = new MultipleRemittenceAccountRequest(MRReferenceNumber, Convert.ToInt64(RequestObject.RequestData), BankName);

                var responseobj = await coreService.GetCustomerAccountInformation(CBSReuest);

                InternalTransDB InternalDB = new InternalTransDB("MR", RequestObject.ReferenceNumber, MRReferenceNumber, responseobj.Item2.RequestDateTime, responseobj.Item2.ResponseDateTime, responseobj.Item2.ErrorCode, responseobj.Item2.ErrorDescription, "GetCustomerAccountInformation");
                InternalTransDetails InternalDetail = new InternalTransDetails(MRReferenceNumber, responseobj.Item2.RequestString, responseobj.Item2.ResponseString);

                ListInternalDB.Add(InternalDB);
                ListInternalDetails.Add(InternalDetail);

                if (responseobj.Item1 == null || !string.IsNullOrEmpty(responseobj.Item2.ErrorCode))
                {
                    TransDBObject.Error_Code = responseobj.Item2.ErrorCode;
                    TransDBObject.Error_Desc = responseobj.Item2.ErrorDescription;
                    TransDBObject.Res_Time = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");

                    SIDetail = new SITransDetails(RequestObject.ReferenceNumber, ProductCode, Newtonsoft.Json.JsonConvert.SerializeObject(RequestObject), Newtonsoft.Json.JsonConvert.SerializeObject(ResponseObject), "AccountInformation");

                    ResponseObject.AccountNumber = RequestObject.RequestData;
                    ResponseObject.ERROR_DESC = TransDBObject.Error_Desc;
                    ResponseObject.ERROR_CODE = TransDBObject.Error_Code;

                    LoggerImplementObj.InsertInternalTransDBDetails(TransDBObject, SIDetail, ListInternalDB, ListInternalDetails);
                    LoggerImplementObj.LoggerObj.WriteLog(Newtonsoft.Json.JsonConvert.SerializeObject(ResponseObject), "BalanceEnquiryResponse");
                    return ResponseObject;
                }

                ResponseObject.AccountNumber = RequestObject.RequestData;
                ResponseObject.AccountDescription = responseobj.Item1.AccountDescription;
                ResponseObject.AccountHomeBranch = responseobj.Item1.AccountHomeBranch;
                ResponseObject.AccountName = responseobj.Item1.AccountName;
                ResponseObject.AccountNumber = responseobj.Item1.AccountNumber;
                ResponseObject.AccountStatus = responseobj.Item1.AccountStatus;
                ResponseObject.AccountType = responseobj.Item1.AccountType;
                ResponseObject.Acctopendate = responseobj.Item1.AccountOpenDate;
                ResponseObject.ACCTYPE = responseobj.Item1.AccountType;
                ResponseObject.AvailableBalance = responseobj.Item1.AvailableBalance;
                ResponseObject.Currency = responseobj.Item1.Currency;
                ResponseObject.CustomerName = responseobj.Item1.CustomerName;
                ResponseObject.CustomerNumber = responseobj.Item1.CustomerNumber;
                ResponseObject.CustomerStatus = responseobj.Item1.CustomerStatus;
                ResponseObject.Interest_Category = responseobj.Item1.Interest_Category;
                ResponseObject.Micrcode = responseobj.Item1.MICRCode;
                ResponseObject.ModeofOperation = responseobj.Item1.ModeofOperation;
                ResponseObject.ReferenceNumber = RequestObject.ReferenceNumber;
                ResponseObject.Shadowfilebal = responseobj.Item1.ShadowFileBalance;
                ResponseObject.vipcode = responseobj.Item1.VIPCode;
                ResponseObject.Visheshflag = responseobj.Item1.VisheshFlag;

                LoggerImplementObj.LoggerObj.WriteLog(Newtonsoft.Json.JsonConvert.SerializeObject(ResponseObject), "BalanceEnquiryResponse");

                TransDBObject.Res_Time = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                SIDetail1 = new SITransDetails(RequestObject.ReferenceNumber, ProductCode, Newtonsoft.Json.JsonConvert.SerializeObject(RequestObject), Newtonsoft.Json.JsonConvert.SerializeObject(ResponseObject), "AccountInformation");

                LoggerImplementObj.InsertInternalTransDBDetails(TransDBObject, SIDetail1, ListInternalDB, ListInternalDetails);
                return ResponseObject;
            }
            catch (Exception ex)
            {
                LoggerImplementObj.LoggerObj.WriteExceptionLog(ex.Message, "AcntClosureThroughTrnsfrException");

                TransDBObject.Res_Time = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                TransDBObject.Error_Code = "SI001";
                TransDBObject.Error_Desc = ex.Message.Replace("'", "");
                ResponseObject.ERROR_DESC = "Exception while processing the response";
                ResponseObject.AccountNumber = RequestObject.RequestData;

                SIDetail1 = new SITransDetails(RequestObject.ReferenceNumber, ProductCode, Newtonsoft.Json.JsonConvert.SerializeObject(RequestObject), Newtonsoft.Json.JsonConvert.SerializeObject(ResponseObject), "AccountInformation");

                LoggerImplementObj.InsertInternalTransDBDetails(TransDBObject, SIDetail1, ListInternalDB, ListInternalDetails);
            }
            finally
            {
                //LoggerImplementObj.LoggerObj.WriteLog(Newtonsoft.Json.JsonConvert.SerializeObject(ResponseObject), "BalanceEnquiryResponse");
                //LoggerImplementObj.InsertInternalTransDBDetails(TransDBObject, SIDetail, ListInternalDB, ListInternalDetails);
            }

            return ResponseObject;
        }
    }
}