﻿using LotusWebAPI.Models;
using LotusWebAPI.Provider;
using SBISI.CoreBanking.Common.Provider;
using SBISI.CoreBanking.Services.Models;
using SBISI.CoreBanking.Services.Providers;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace LotusWebAPI.Controllers
{
    public class AccountStatementController : ApiController
    {
        //[TokenSecurityFilter.TokenSecurityFilter]
        public async Task<INBAccountStatementDetails> post(AccountStatementRequest RequestObject)
        {
            if (RequestObject == null)
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound) { Content = new StringContent("SI011^Input string was not in a correct format."), ReasonPhrase = "SI011^Input string was not in a correct format." });

            if (string.IsNullOrEmpty(RequestObject.FromAmount) || string.IsNullOrEmpty(RequestObject.ReferenceNumber) || string.IsNullOrEmpty(RequestObject.ToAmount) || string.IsNullOrEmpty(RequestObject.FromDate) || string.IsNullOrEmpty(RequestObject.ToDate) || string.IsNullOrEmpty(RequestObject.TransactionNumber))
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound) { Content = new StringContent("SI011^Input string was not in a correct format."), ReasonPhrase = "SI011^Input string was not in a correct format." });

            if (RequestObject.ReferenceNumber.Length != 25)
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound) { Content = new StringContent("SI004^INVALID/NOT-UNIQUE REFERENCE NUMBER"), ReasonPhrase = "SI004^INVALID/NOT-UNIQUE REFERENCE NUMBER" });

            if (RequestObject.Bank_Code > 7)
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound) { Content = new StringContent("SI006^Bank Code Is Not Proper"), ReasonPhrase = "SI006^Bank Code Is Not Proper" });

            if (RequestObject.TransactionNumber != "10" && RequestObject.TransactionNumber != "150")
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound) { Content = new StringContent("SI008^Invalid Transaction Number"), ReasonPhrase = "SI008^Invalid Transaction Number" });

            if (RequestObject.ReferenceNumber.Substring(0, 5) != "SBILT")
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound) { Content = new StringContent("SI094^REFERENCE NUMBER INVALID. REFER DOCUMENT."), ReasonPhrase = "SI094^REFERENCE NUMBER INVALID. REFER DOCUMENT." });

            string ProductCode = "SBIVA";
            LoggerImplement LoggerImplementObj = new LoggerImplement(RequestObject.ReferenceNumber, ProductCode);
            LoggerImplementObj.LoggerObj.WriteLog(Newtonsoft.Json.JsonConvert.SerializeObject(RequestObject), "AccountStatementRequest");

            DatabaseOperation dbobject = new DatabaseOperation();
            string BankName = DatabaseOperation.GetBankName(RequestObject.Bank_Code);
            string TransactionNo = string.Empty;
            if (RequestObject.TransactionNumber == "10")
                TransactionNo = "0";
            else
                TransactionNo = "1";

            INBAccountStatementDetails ResponseObject = new INBAccountStatementDetails();
            List<InternalTransDB> ListInternalDB = new List<InternalTransDB>();
            List<InternalTransDetails> ListInternalDetails = new List<InternalTransDetails>();

            SITransDetails SIDetail = null;
            SITransDetails SIDetail1 = null;
            SITransDB TransDBObject = null;

            ResponseObject.ReferenceNumber = RequestObject.ReferenceNumber;
            ResponseObject.AccountNumber = RequestObject.AccountNumber;
            try
            {
                TransDBObject = new SITransDB(RequestObject.ReferenceNumber, ProductCode, DateTime.Now, "", "", "", "AccountStatement", IPAddressInfo.GetIP());
                string DBResponse = LoggerImplementObj.InsertSITransDB(TransDBObject);
                if (DBResponse != "SUCCESS")
                {
                    string ERROR_DESCRIPTION = string.Empty;
                    switch (DBResponse)
                    {
                        case "SI007": ERROR_DESCRIPTION = "DATABASE ERROR";
                            break;

                        case "SI087": ERROR_DESCRIPTION = "REFERENCE NUMBER MUST BE UNIQUE";
                            break;
                    }
                    ResponseObject.ErrorDescription = ERROR_DESCRIPTION;
                    ResponseObject.ErrorCode = DBResponse;
                    return ResponseObject;
                }
                CoreBankingServices coreService = new CoreBankingServices();
                string INBReferenceNumber = dbobject.generateSBISIRRN(BankName + "IN", "");

                INBStatementRequest INBRequestObject = new INBStatementRequest();
                INBRequestObject.AccountNumber = RequestObject.AccountNumber;
                INBRequestObject.CBSReferenceNumber = INBReferenceNumber;
                INBRequestObject.BankName = BankName;
                INBRequestObject.FromAmount = RequestObject.FromAmount;
                INBRequestObject.FromDate = RequestObject.FromDate;
                INBRequestObject.ToDate = RequestObject.ToDate;
                INBRequestObject.ToAmount = RequestObject.ToAmount;
                INBRequestObject.TransactionNumber = TransactionNo;

                var StatementResponse = await coreService.Get450INBStatement(INBRequestObject);

                InternalTransDB InternalDB = new InternalTransDB("IN", RequestObject.ReferenceNumber, INBReferenceNumber, StatementResponse.Item2.RequestDateTime, StatementResponse.Item2.ResponseDateTime, StatementResponse.Item2.ErrorCode, StatementResponse.Item2.ErrorDescription, "Get450INBStatement");
                InternalTransDetails InternalDetail = new InternalTransDetails(INBReferenceNumber, StatementResponse.Item2.RequestString, StatementResponse.Item2.ResponseString);

                ListInternalDB.Add(InternalDB);
                ListInternalDetails.Add(InternalDetail);

                if (StatementResponse.Item1 == null || !string.IsNullOrEmpty(StatementResponse.Item2.ErrorCode))
                {
                    TransDBObject.Error_Code = StatementResponse.Item2.ErrorCode;
                    TransDBObject.Error_Desc = StatementResponse.Item2.ErrorDescription;
                    ResponseObject.ErrorCode = StatementResponse.Item2.ErrorCode;
                    ResponseObject.ErrorDescription = StatementResponse.Item2.ErrorDescription;
                    TransDBObject.Res_Time = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                    SIDetail = new SITransDetails(RequestObject.ReferenceNumber, ProductCode, Newtonsoft.Json.JsonConvert.SerializeObject(RequestObject), Newtonsoft.Json.JsonConvert.SerializeObject(ResponseObject), "AccountStatement");

                    LoggerImplementObj.InsertInternalTransDBDetails(TransDBObject, SIDetail, ListInternalDB, ListInternalDetails);
                    LoggerImplementObj.LoggerObj.WriteLog(Newtonsoft.Json.JsonConvert.SerializeObject(ResponseObject), "INBAccountStatementDetailsResponse");
                    return ResponseObject;
                }

                ResponseObject.AccountNumber = RequestObject.AccountNumber;
                ResponseObject.AccountStatement = new List<INBAccountStatement>();
                foreach (INBStatementDetails ac in StatementResponse.Item1.Statementdetail)
                {
                    INBAccountStatement StatementObject = new INBAccountStatement();
                    StatementObject.Amount = ac.Amount;
                    StatementObject.Balance = ac.Balance;
                    StatementObject.BranchNumber = ac.BranchNumber;
                    StatementObject.ChequeNumber = ac.ChequeNumber;
                    StatementObject.INBStatement = ac.INBStatement;
                    StatementObject.Narration = ac.Narration;
                    StatementObject.PostTime = ac.PostTime;
                    StatementObject.Statement = ac.Statement;
                    StatementObject.TodaysDate = ac.TodaysDate;
                    StatementObject.TransferStatement = ac.TransferStatement;
                    StatementObject.ValueDate = ac.ValueDate;
                    ResponseObject.AccountStatement.Add(StatementObject);
                }

                LoggerImplementObj.LoggerObj.WriteLog(Newtonsoft.Json.JsonConvert.SerializeObject(ResponseObject), "INBAccountStatementDetailsResponse");
                TransDBObject.Res_Time = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                SIDetail1 = new SITransDetails(RequestObject.ReferenceNumber, ProductCode, Newtonsoft.Json.JsonConvert.SerializeObject(RequestObject), Newtonsoft.Json.JsonConvert.SerializeObject(ResponseObject), "AccountStatement");
                LoggerImplementObj.InsertInternalTransDBDetails(TransDBObject, SIDetail1, ListInternalDB, ListInternalDetails);
                return ResponseObject;
            }
            catch (Exception ex)
            {
                LoggerImplementObj.LoggerObj.WriteExceptionLog(ex.Message, "AccountStatementException");
                throw;
            }
            finally
            {
                //LoggerImplementObj.LoggerObj.WriteLog(Newtonsoft.Json.JsonConvert.SerializeObject(ResponseObject), "INBAccountStatementDetailsResponse");
               // LoggerImplementObj.InsertInternalTransDBDetails(TransDBObject, SIDetail, ListInternalDB, ListInternalDetails);
            }
        }
    }
}