﻿using LotusWebAPI.Models;
using LotusWebAPI.Provider;
using Newtonsoft.Json.Linq;
using SBISI.CoreBanking.Common.Provider;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace LotusWebAPI.Controllers
{
    public class NomineeDetailsController : ApiController
    {
        //[TokenSecurityFilter.TokenSecurityFilter]
        [HttpPost]
        public NomineeDetailsResponse GetNomineeDetails(NomineeDetailsRequest RequestObject)
        {
            if (RequestObject == null)
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound) { Content = new StringContent("SI011^Input string was not in a correct format."), ReasonPhrase = "SI011^Input string was not in a correct format." });

            if (RequestObject.REQUEST_REF_NO.Substring(0, 5) != "SBILT")
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound) { Content = new StringContent("SI094^REFERENCE NUMBER INVALID. REFER DOCUMENT."), ReasonPhrase = "SI094^REFERENCE NUMBER INVALID. REFER DOCUMENT." });

            string ProductCode = "SBIVA";
            LoggerImplement LoggerImplementObj = new LoggerImplement(RequestObject.REQUEST_REF_NO, ProductCode);
            LoggerImplementObj.LoggerObj.WriteLog(Newtonsoft.Json.JsonConvert.SerializeObject(RequestObject), "NomineeDetailsRequest");

            NomineeDetailsResponse ResponseObject = new NomineeDetailsResponse();

            List<InternalTransDB> ListInternalDB = new List<InternalTransDB>();
            List<InternalTransDetails> ListInternalDetails = new List<InternalTransDetails>();

            SITransDetails SIDetail = null;
            SITransDetails SIDetail1 = null;
            SITransDB TransDBObject = null;

            CBSTransaction GenericObject = new CBSTransaction();

            DatabaseOperation DBObject = new DatabaseOperation();
            string BankName = DatabaseOperation.GetBankName(Convert.ToInt32(RequestObject.BANK_CODE));

            NomineeDetailsRequest_Inherit NomineeDetailsRequest_InheritObj = new NomineeDetailsRequest_Inherit();
            NomineeDetailsRequest_InheritObj.ACCOUNT_NO = RequestObject.ACCOUNT_NO;
            NomineeDetailsRequest_InheritObj.BANK_CODE = RequestObject.BANK_CODE;
            NomineeDetailsRequest_InheritObj.BRANCH_CODE = RequestObject.BRANCH_CODE;
            NomineeDetailsRequest_InheritObj.REQUEST_REF_NO = RequestObject.REQUEST_REF_NO;
            NomineeDetailsRequest_InheritObj.STATUS = RequestObject.STATUS;

            TransDBObject = new SITransDB(RequestObject.REQUEST_REF_NO, ProductCode, DateTime.Now, "", "", "", "GetNomineeDetails", IPAddressInfo.GetIP());
            ResponseObject.REQUEST_REF_NO = RequestObject.REQUEST_REF_NO;

            try
            {
                string DBResponse = LoggerImplementObj.InsertSITransDB(TransDBObject);
                if (DBResponse != "SUCCESS")
                {
                    string ERROR_DESCRIPTION = string.Empty;
                    switch (DBResponse)
                    {
                        case "SI007": ERROR_DESCRIPTION = "DATABASE ERROR";
                            break;

                        case "SI087": ERROR_DESCRIPTION = "REFERENCE NUMBER MUST BE UNIQUE";
                            break;
                    }
                    ResponseObject.ERROR_DESCRIPTION = ERROR_DESCRIPTION;
                    ResponseObject.REQUEST_REF_NO = RequestObject.REQUEST_REF_NO;
                    return ResponseObject;
                }

                var NomineeDetailsString = GenericObject.sendJsonToSI("0", Newtonsoft.Json.JsonConvert.SerializeObject(NomineeDetailsRequest_InheritObj), "NomineeDetails");

                // NomineeDetailsString = "{   \"Response\": {      \"NOMINEE_DETAILS\": {         \"NOMINEE\": [            {               \"MINOR\": \"N\",               \"ACCOUNT_NUMBER\": 30002516065,               \"NOMINEE_NUMBER\": 1699,               \"STATUS\": 0,               \"NAME_OF_NOMINEE\": \"S\",               \"GUARDIAN_NAME\": \"\"            },            {               \"MINOR\": \"N\",               \"ACCOUNT_NUMBER\": 30002516065,               \"NOMINEE_NUMBER\": 1730,               \"STATUS\": 0,               \"NAME_OF_NOMINEE\": \"MANAN\",               \"GUARDIAN_NAME\": \"\"            }         ]      },      \"SOURCE_ID\": \"CMP\",      \"RESPONSE_STATUS\": 0,      \"REQUEST_TELLER_ID\": 0,      \"REQUEST_CODE\": \"CBS.IT004\",      \"BRANCH_CODE\": 0,      \"BANK_CODE\": 0,      \"REQUEST_REF_NO\": \"API 2016-11-17T07:39:21\",      \"RESP_DATE\": \"17-11-2016 13:03:08\",      \"REQUEST_AUTH_ID\": 0,      \"REQUEST_DATE_TIME\": \"2016-11-17T07:39:21\"   }}";
                LoggerImplementObj.LoggerObj.WriteLog(Newtonsoft.Json.JsonConvert.SerializeObject(ResponseObject), "NomineeDetailsResponse");

                JObject NomineeDetailsObj = JObject.Parse(JObject.Parse(NomineeDetailsString)["Response"].ToString());
                ResponseObject.REQUEST_REF_NO = RequestObject.REQUEST_REF_NO;

                string MRReferenceNumber = DBObject.generateSBISIRRN(BankName + "TC", "");
                DateTime Request_date_time = DateTime.Now;
                DateTime Response_Date_Time = DateTime.Now;

                InternalTransDB InternalDB = new InternalTransDB("TC", RequestObject.REQUEST_REF_NO, MRReferenceNumber, Request_date_time, Response_Date_Time, "", "", "NomineeDetails");
                InternalTransDetails InternalDetail = new InternalTransDetails(MRReferenceNumber, Newtonsoft.Json.JsonConvert.SerializeObject(NomineeDetailsRequest_InheritObj), NomineeDetailsString);

                ListInternalDB.Add(InternalDB);
                ListInternalDetails.Add(InternalDetail);

                if (NomineeDetailsObj["RESPONSE_STATUS"].ToString() == "1")
                {
                    TransDBObject.Error_Desc = NomineeDetailsObj["ERROR_DESCRIPTION"].ToString();
                    TransDBObject.Res_Time = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");

                    foreach (JProperty propertyName in NomineeDetailsObj.Properties())
                    {
                        if (ResponseObject.GetType().GetProperty(propertyName.Name) != null)
                            ResponseObject.GetType().GetProperty(propertyName.Name).SetValue(ResponseObject, NomineeDetailsObj[propertyName.Name].ToString());
                    }

                    SIDetail = new SITransDetails(RequestObject.REQUEST_REF_NO, ProductCode, Newtonsoft.Json.JsonConvert.SerializeObject(RequestObject), TransDBObject.Error_Desc, "GetNomineeDetails");
                    LoggerImplementObj.InsertInternalTransDBDetails(TransDBObject, SIDetail, ListInternalDB, ListInternalDetails);

                    ResponseObject.ERROR_DESCRIPTION = TransDBObject.Error_Desc;

                    return ResponseObject;
                }

                foreach (JProperty propertyName in NomineeDetailsObj.Properties())
                {
                    if (propertyName.Name == "NOMINEE_DETAILS")
                    {
                        JArray NOMINEE_DETAILSObj = JArray.Parse(NomineeDetailsObj["NOMINEE_DETAILS"]["NOMINEE"].ToString());
                        ResponseObject.NOMINEE_DETAILS = new List<NOMINEE>();
                        foreach (JObject Jobj in NOMINEE_DETAILSObj)
                        {
                            NOMINEE NomineeObj = new NOMINEE();
                            foreach (JProperty propName in Jobj.Properties())
                            {
                                if (ResponseObject.GetType().GetProperty(propertyName.Name) != null)
                                    NomineeObj.GetType().GetProperty(propName.Name).SetValue(NomineeObj, Jobj[propName.Name].ToString());
                            }
                            ResponseObject.NOMINEE_DETAILS.Add(NomineeObj);
                        }
                    }
                    else
                    {
                        if (ResponseObject.GetType().GetProperty(propertyName.Name) != null)
                            ResponseObject.GetType().GetProperty(propertyName.Name).SetValue(ResponseObject, NomineeDetailsObj[propertyName.Name].ToString());
                    }
                }

                TransDBObject.Res_Time = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                SIDetail1 = new SITransDetails(RequestObject.REQUEST_REF_NO, ProductCode, Newtonsoft.Json.JsonConvert.SerializeObject(RequestObject), Newtonsoft.Json.JsonConvert.SerializeObject(ResponseObject), "GetNomineeDetails");
                LoggerImplementObj.InsertInternalTransDBDetails(TransDBObject, SIDetail1, ListInternalDB, ListInternalDetails);

                return ResponseObject;
            }
            catch (Exception ex)
            {
                LoggerImplementObj.LoggerObj.WriteExceptionLog(ex.Message, "NomineeDetailsResponse");

                TransDBObject.Res_Time = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                TransDBObject.Error_Code = "SI001";
                TransDBObject.Error_Desc = ex.Message.Replace("'", "");
                ResponseObject.ERROR_DESCRIPTION = "Exception while processing the response";

                ResponseObject.REQUEST_REF_NO = RequestObject.REQUEST_REF_NO;

                SIDetail = new SITransDetails(RequestObject.REQUEST_REF_NO, ProductCode, Newtonsoft.Json.JsonConvert.SerializeObject(RequestObject), Newtonsoft.Json.JsonConvert.SerializeObject(ResponseObject), "GetNomineeDetails");
                LoggerImplementObj.InsertInternalTransDBDetails(TransDBObject, SIDetail, ListInternalDB, ListInternalDetails);
            }
            finally
            {
                LoggerImplementObj.LoggerObj.WriteLog(Newtonsoft.Json.JsonConvert.SerializeObject(ResponseObject), "NomineeDetailsResponse");
                LoggerImplementObj.InsertInternalTransDBDetails(TransDBObject, SIDetail, ListInternalDB, ListInternalDetails);
            }

            return ResponseObject;
        }
    }
}