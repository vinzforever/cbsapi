﻿using LotusWebAPI.Models;
using LotusWebAPI.Provider;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using SBISI.CoreBanking.Common.Provider;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace LotusWebAPI.Controllers
{
    public class CustomerMiscAmmendmentController : ApiController
    {
        //[TokenSecurityFilter.TokenSecurityFilter]
        [HttpPost]
        public CustomerMiscAmmendmentResponse AccountClosurePost(CustomerMiscAmmendmentRequest RequestObject)
        {
            if (RequestObject == null)
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound) { Content = new StringContent("SI011^Input string was not in a correct format."), ReasonPhrase = "SI011^Input string was not in a correct format." });

            if (RequestObject.REQUEST_REF_NO.Substring(0, 5) != "SBILT")
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound) { Content = new StringContent("SI094^REFERENCE NUMBER INVALID. REFER DOCUMENT."), ReasonPhrase = "SI094^REFERENCE NUMBER INVALID. REFER DOCUMENT." });

            string ProductCode = "SBIVA";
            LoggerImplement LoggerImplementObj = new LoggerImplement(RequestObject.REQUEST_REF_NO, ProductCode);
            LoggerImplementObj.LoggerObj.WriteLog(Newtonsoft.Json.JsonConvert.SerializeObject(RequestObject), "CustomerMiscAmmendmentRequest");

            CustomerMiscAmmendmentResponse ResponseObject = new CustomerMiscAmmendmentResponse();

            List<InternalTransDB> ListInternalDB = new List<InternalTransDB>();
            List<InternalTransDetails> ListInternalDetails = new List<InternalTransDetails>();

            SITransDetails SIDetail = null;
            SITransDetails SIDetail1 = null;
            SITransDB TransDBObject = null;

            CBSTransaction GenericObject = new CBSTransaction();

            DatabaseOperation DBObject = new DatabaseOperation();
            string BankName = DatabaseOperation.GetBankName(0);

            CustomerMiscAmmendmentRequestInherit CustomerMiscAmmendmentRequestInherit_InheritObj = new CustomerMiscAmmendmentRequestInherit();

            CustomerMiscAmmendmentRequestInherit_InheritObj.REQUEST_TELLER_ID = RequestObject.REQUEST_TELLER_ID;
            CustomerMiscAmmendmentRequestInherit_InheritObj.BRANCH_CODE = RequestObject.BRANCH_CODE;
            CustomerMiscAmmendmentRequestInherit_InheritObj.REQUEST_REF_NO = RequestObject.REQUEST_REF_NO;
            CustomerMiscAmmendmentRequestInherit_InheritObj.CUSTOMER_NO = RequestObject.CUSTOMER_NO;
            CustomerMiscAmmendmentRequestInherit_InheritObj.CUSTOMER_NAME = RequestObject.CUSTOMER_NAME;
            CustomerMiscAmmendmentRequestInherit_InheritObj.CUSTOMER_SHORT_NAME = RequestObject.CUSTOMER_SHORT_NAME;
            CustomerMiscAmmendmentRequestInherit_InheritObj.INCOME_TAX_BASIS = RequestObject.INCOME_TAX_BASIS;
            CustomerMiscAmmendmentRequestInherit_InheritObj.INCOME_TAX_BRANCH = RequestObject.INCOME_TAX_BRANCH;
            CustomerMiscAmmendmentRequestInherit_InheritObj.COMMUNICATION_CODE = RequestObject.COMMUNICATION_CODE;
            CustomerMiscAmmendmentRequestInherit_InheritObj.SCOPE_OF_FIRM = RequestObject.SCOPE_OF_FIRM;
            CustomerMiscAmmendmentRequestInherit_InheritObj.EMAIL_ID1 = RequestObject.EMAIL_ID1;
            CustomerMiscAmmendmentRequestInherit_InheritObj.EMAIL_ID2 = RequestObject.EMAIL_ID2;
            CustomerMiscAmmendmentRequestInherit_InheritObj.CREDITOR_CODE = RequestObject.CREDITOR_CODE;
            CustomerMiscAmmendmentRequestInherit_InheritObj.EDUCATION_CODE = RequestObject.EDUCATION_CODE;
            CustomerMiscAmmendmentRequestInherit_InheritObj.ACCESS_CHANNEL1 = RequestObject.ACCESS_CHANNEL1;
            CustomerMiscAmmendmentRequestInherit_InheritObj.ACCESS_CHANNEL2 = RequestObject.ACCESS_CHANNEL2;
            CustomerMiscAmmendmentRequestInherit_InheritObj.ACCESS_CHANNEL3 = RequestObject.ACCESS_CHANNEL3;
            CustomerMiscAmmendmentRequestInherit_InheritObj.ACCESS_CHANNEL4 = RequestObject.ACCESS_CHANNEL4;
            CustomerMiscAmmendmentRequestInherit_InheritObj.ACCESS_CHANNEL5 = RequestObject.ACCESS_CHANNEL5;
            CustomerMiscAmmendmentRequestInherit_InheritObj.ACCESS_CHANNEL6 = RequestObject.ACCESS_CHANNEL6;
            CustomerMiscAmmendmentRequestInherit_InheritObj.ACCESS_CHANNEL7 = RequestObject.ACCESS_CHANNEL7;
            CustomerMiscAmmendmentRequestInherit_InheritObj.ACCESS_CHANNEL8 = RequestObject.ACCESS_CHANNEL8;
            CustomerMiscAmmendmentRequestInherit_InheritObj.ACCESS_CHANNEL9 = RequestObject.ACCESS_CHANNEL9;
            CustomerMiscAmmendmentRequestInherit_InheritObj.ACCESS_CHANNEL10 = RequestObject.ACCESS_CHANNEL10;
            CustomerMiscAmmendmentRequestInherit_InheritObj.ACCESS_CHANNEL11 = RequestObject.ACCESS_CHANNEL11;
            CustomerMiscAmmendmentRequestInherit_InheritObj.ACCESS_CHANNEL12 = RequestObject.ACCESS_CHANNEL12;
            CustomerMiscAmmendmentRequestInherit_InheritObj.ACCESS_CHANNEL13 = RequestObject.ACCESS_CHANNEL13;
            CustomerMiscAmmendmentRequestInherit_InheritObj.ACCESS_CHANNEL14 = RequestObject.ACCESS_CHANNEL14;
            CustomerMiscAmmendmentRequestInherit_InheritObj.ACCESS_CHANNEL15 = RequestObject.ACCESS_CHANNEL15;
            CustomerMiscAmmendmentRequestInherit_InheritObj.ACCESS_CHANNEL16 = RequestObject.ACCESS_CHANNEL16;
            CustomerMiscAmmendmentRequestInherit_InheritObj.ACCESS_CHANNEL17 = RequestObject.ACCESS_CHANNEL17;
            CustomerMiscAmmendmentRequestInherit_InheritObj.ACCESS_CHANNEL18 = RequestObject.ACCESS_CHANNEL18;
            CustomerMiscAmmendmentRequestInherit_InheritObj.ACCESS_CHANNEL19 = RequestObject.ACCESS_CHANNEL19;
            CustomerMiscAmmendmentRequestInherit_InheritObj.ACCESS_CHANNEL20 = RequestObject.ACCESS_CHANNEL20;
            CustomerMiscAmmendmentRequestInherit_InheritObj.STATEMENT_REQ_FLAG = RequestObject.STATEMENT_REQ_FLAG;
            CustomerMiscAmmendmentRequestInherit_InheritObj.STATEMENT_CYCLE = RequestObject.STATEMENT_CYCLE;
            CustomerMiscAmmendmentRequestInherit_InheritObj.STATEMENT_DAY = RequestObject.STATEMENT_DAY;
            CustomerMiscAmmendmentRequestInherit_InheritObj.MODE_OF_DELIVERY = RequestObject.MODE_OF_DELIVERY;
            CustomerMiscAmmendmentRequestInherit_InheritObj.VILLAGE_CODE = RequestObject.VILLAGE_CODE;
            CustomerMiscAmmendmentRequestInherit_InheritObj.INB_KIT_NO = RequestObject.INB_KIT_NO;
            CustomerMiscAmmendmentRequestInherit_InheritObj.CUSTOMER_ADD1 = RequestObject.CUSTOMER_ADD1;
            CustomerMiscAmmendmentRequestInherit_InheritObj.CUSTOMER_ADD2 = RequestObject.CUSTOMER_ADD2;
            CustomerMiscAmmendmentRequestInherit_InheritObj.CUSTOMER_ADD3 = RequestObject.CUSTOMER_ADD3;
            CustomerMiscAmmendmentRequestInherit_InheritObj.CUSTOMER_ADD4 = RequestObject.CUSTOMER_ADD4;
            CustomerMiscAmmendmentRequestInherit_InheritObj.STATE_CODE = RequestObject.STATE_CODE;
            CustomerMiscAmmendmentRequestInherit_InheritObj.CITY_CODE = RequestObject.CITY_CODE;
            CustomerMiscAmmendmentRequestInherit_InheritObj.COUNTRY_CODE = RequestObject.COUNTRY_CODE;
            CustomerMiscAmmendmentRequestInherit_InheritObj.POSTAL_CODE = RequestObject.POSTAL_CODE;
            CustomerMiscAmmendmentRequestInherit_InheritObj.HOME_PHONE_NO = RequestObject.HOME_PHONE_NO;
            CustomerMiscAmmendmentRequestInherit_InheritObj.BUSINESS_PHONE_NO = RequestObject.BUSINESS_PHONE_NO;
            CustomerMiscAmmendmentRequestInherit_InheritObj.MOBILE_NO = RequestObject.MOBILE_NO;
            CustomerMiscAmmendmentRequestInherit_InheritObj.CUSTOMER_TYPE = RequestObject.CUSTOMER_TYPE;

            TransDBObject = new SITransDB(RequestObject.REQUEST_REF_NO, ProductCode, DateTime.Now, "", "", "", "CustomerMiscAmmend", IPAddressInfo.GetIP());
            ResponseObject.REQUEST_REF_NO = RequestObject.REQUEST_REF_NO;

            try
            {
                string DBResponse = LoggerImplementObj.InsertSITransDB(TransDBObject);
                if (DBResponse != "SUCCESS")
                {
                    string ERROR_DESCRIPTION = string.Empty;
                    switch (DBResponse)
                    {
                        case "SI007": ERROR_DESCRIPTION = "DATABASE ERROR";
                            break;

                        case "SI087": ERROR_DESCRIPTION = "REFERENCE NUMBER MUST BE UNIQUE";
                            break;
                    }
                    ResponseObject.ERROR_DESCRIPTION = ERROR_DESCRIPTION;
                    ResponseObject.REQUEST_REF_NO = RequestObject.REQUEST_REF_NO;
                    return ResponseObject;
                }
                string MRReferenceNumber = DBObject.generateSBISIRRN(BankName + "TC", "");
                DateTime Request_date_time = DateTime.Now;

                var SICreationResponseString = GenericObject.sendJsonToSIPost("0", Newtonsoft.Json.JsonConvert.SerializeObject(CustomerMiscAmmendmentRequestInherit_InheritObj), "CustomerMiscAmmendmentPost");
                DateTime Response_Date_Time = DateTime.Now;

                JObject SICreationObj = JObject.Parse(JObject.Parse(SICreationResponseString)["Response"].ToString());

                InternalTransDB InternalDB = new InternalTransDB("TC", RequestObject.REQUEST_REF_NO, MRReferenceNumber, Request_date_time, Response_Date_Time, SICreationObj["RESPONSE_STATUS"].ToString(), "", "CustomerMiscAmmend");
                InternalTransDetails InternalDetail = new InternalTransDetails(MRReferenceNumber, Newtonsoft.Json.JsonConvert.SerializeObject(CustomerMiscAmmendmentRequestInherit_InheritObj), SICreationResponseString);

                ListInternalDB.Add(InternalDB);
                ListInternalDetails.Add(InternalDetail);

                ResponseObject.REQUEST_REF_NO = RequestObject.REQUEST_REF_NO;

                if (SICreationObj["RESPONSE_STATUS"].ToString() == "1")
                {
                    TransDBObject.Error_Desc = SICreationObj["ERROR_DESCRIPTION"].ToString();
                    TransDBObject.Res_Time = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");

                    foreach (JProperty propertyName in SICreationObj.Properties())
                    {
                        if (ResponseObject.GetType().GetProperty(propertyName.Name) != null)
                            ResponseObject.GetType().GetProperty(propertyName.Name).SetValue(ResponseObject, SICreationObj[propertyName.Name].ToString());
                    }

                    SIDetail = new SITransDetails(RequestObject.REQUEST_REF_NO, ProductCode, Newtonsoft.Json.JsonConvert.SerializeObject(RequestObject), Newtonsoft.Json.JsonConvert.SerializeObject(ResponseObject), "CustomerMiscAmmend");
                    LoggerImplementObj.InsertInternalTransDBDetails(TransDBObject, SIDetail, ListInternalDB, ListInternalDetails);

                    ResponseObject.ERROR_DESCRIPTION = TransDBObject.Error_Desc;

                    return ResponseObject;
                }

                foreach (JProperty propertyName in SICreationObj.Properties())
                {
                    if (ResponseObject.GetType().GetProperty(propertyName.Name) != null)
                        ResponseObject.GetType().GetProperty(propertyName.Name).SetValue(ResponseObject, SICreationObj[propertyName.Name].ToString());
                }

                TransDBObject.Res_Time = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                SIDetail1 = new SITransDetails(RequestObject.REQUEST_REF_NO, ProductCode, Newtonsoft.Json.JsonConvert.SerializeObject(RequestObject), JsonConvert.SerializeObject(ResponseObject), "CustomerMiscAmmend");

                LoggerImplementObj.InsertInternalTransDBDetails(TransDBObject, SIDetail1, ListInternalDB, ListInternalDetails);

                return ResponseObject;
            }
            catch (Exception ex)
            {
                LoggerImplementObj.LoggerObj.WriteExceptionLog(ex.Message, "CustomerIdentificationAmmendmentException");

                TransDBObject.Res_Time = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                TransDBObject.Error_Code = "SI001";
                TransDBObject.Error_Desc = ex.Message.Replace("'", "");
                ResponseObject.ERROR_DESCRIPTION = "Exception while processing the response";

                ResponseObject.REQUEST_REF_NO = RequestObject.REQUEST_REF_NO;

                SIDetail = new SITransDetails(RequestObject.REQUEST_REF_NO, ProductCode, Newtonsoft.Json.JsonConvert.SerializeObject(RequestObject), Newtonsoft.Json.JsonConvert.SerializeObject(ResponseObject), "CustomerMiscAmmend");
                LoggerImplementObj.InsertInternalTransDBDetails(TransDBObject, SIDetail, ListInternalDB, ListInternalDetails);
            }
            finally
            {
                LoggerImplementObj.LoggerObj.WriteLog(Newtonsoft.Json.JsonConvert.SerializeObject(ResponseObject), "CustomerMiscAmmendmentResponse");
                LoggerImplementObj.InsertInternalTransDBDetails(TransDBObject, SIDetail, ListInternalDB, ListInternalDetails);
            }

            return ResponseObject;
        }
    }
}