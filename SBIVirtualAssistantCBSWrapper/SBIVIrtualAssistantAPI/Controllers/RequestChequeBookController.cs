﻿using LotusWebAPI.Models;
using LotusWebAPI.Provider;
using SBISI.CoreBanking.Common.Provider;
using SBISI.CoreBanking.Intouch.Providers;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace LotusWebAPI.Controllers
{
    public class RequestChequeBookController : ApiController
    {
        //[TokenSecurityFilter.TokenSecurityFilter]
        public async Task<BancsResponse> post(ChequeBookRequest RequestObject)
        {
            if (RequestObject == null)
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound) { Content = new StringContent("SI011^Input string was not in a correct format."), ReasonPhrase = "SI011^Input string was not in a correct format." });

            if (string.IsNullOrEmpty(RequestObject.ReferenceNumber) || string.IsNullOrEmpty(RequestObject.Branch_Code) || string.IsNullOrEmpty(RequestObject.Checker_ID) || string.IsNullOrEmpty(RequestObject.Teller_ID))
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound) { Content = new StringContent("SI011^Input string was not in a correct format."), ReasonPhrase = "SI011^Input string was not in a correct format." });

            if (RequestObject.ReferenceNumber.Length != 25)
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound) { Content = new StringContent("SI004^INVALID/NOT-UNIQUE REFERENCE NUMBER"), ReasonPhrase = "SI004^INVALID/NOT-UNIQUE REFERENCE NUMBER" });

            if (RequestObject.Bank_Code > 7)
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound) { Content = new StringContent("SI006^Bank Code Is Not Proper"), ReasonPhrase = "SI006^Bank Code Is Not Proper" });

            if (RequestObject.ReferenceNumber.Substring(0, 5) != "SBILT")
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound) { Content = new StringContent("SI094^REFERENCE NUMBER INVALID. REFER DOCUMENT."), ReasonPhrase = "SI094^REFERENCE NUMBER INVALID. REFER DOCUMENT." });

            string ProductCode = "SBIVA";
            LoggerImplement LoggerImplementObj = new LoggerImplement(RequestObject.ReferenceNumber, ProductCode);
            LoggerImplementObj.LoggerObj.WriteLog(Newtonsoft.Json.JsonConvert.SerializeObject(RequestObject), "ChequeBookRequest");

            DatabaseOperation dbobject = new DatabaseOperation();
            string BankName = DatabaseOperation.GetBankName(RequestObject.Bank_Code);

            BancsResponse ResponseObject = new BancsResponse();

            List<InternalTransDB> ListInternalDB = new List<InternalTransDB>();
            List<InternalTransDetails> ListInternalDetails = new List<InternalTransDetails>();

            SITransDetails SIDetail = null;
            SITransDetails SIDetail1 = null;
            SITransDB TransDBObject = null;

            ResponseObject.ReferenceNumber = RequestObject.ReferenceNumber;
            try
            {
                TransDBObject = new SITransDB(RequestObject.ReferenceNumber, ProductCode, DateTime.Now, "", "", "", "RequestChequeBook", IPAddressInfo.GetIP());

                string DBResponse = LoggerImplementObj.InsertSITransDB(TransDBObject);
                if (DBResponse != "SUCCESS")
                {
                    string ERROR_DESCRIPTION = string.Empty;
                    switch (DBResponse)
                    {
                        case "SI007": ERROR_DESCRIPTION = "DATABASE ERROR";
                            break;

                        case "SI087": ERROR_DESCRIPTION = "REFERENCE NUMBER MUST BE UNIQUE";
                            break;
                    }
                    ResponseObject.ErrorDescription = ERROR_DESCRIPTION;
                    ResponseObject.ErrorCode = DBResponse;
                    ResponseObject.ReferenceNumber = RequestObject.ReferenceNumber;
                    return ResponseObject;
                }

                RequestObject.ChequeBookAmount = (Convert.ToDouble(RequestObject.ChequeBookAmount) * 1000).ToString() + "+";
                string Request = RequestObject.AccountNumber.PadLeft(17, '0') + "      " + RequestObject.InstrumentType.PadLeft(2, '0') + RequestObject.InstrumentSubCategory.PadLeft(2, '0') + RequestObject.LeavesPerBook.PadLeft(4, '0') + RequestObject.PickupBranch.PadLeft(5, '0') + RequestObject.NumberofChequeBooks.PadLeft(2, '0') + RequestObject.SANType.PadLeft(1, '0') + RequestObject.ChequeBookAmount.PadLeft(18, '0') + RequestObject.ChequeBookType.PadLeft(2, ' ') + "     " + RequestObject.PsegFlag.PadLeft(1, '0');

                CoreBankingIntouchServices coreService = new CoreBankingIntouchServices();
                string BancsReferenceNumber = dbobject.generateSBISIRRN(BankName + "BA", "");
                SBISI.CoreBanking.Intouch.Models.CBSGenericRequest CBSReuest = new SBISI.CoreBanking.Intouch.Models.CBSGenericRequest(BankName, RequestObject.ReferenceNumber.Substring(19), RequestObject.Branch_Code, RequestObject.Teller_ID, RequestObject.Checker_ID, Request);

                var NewAccountResponse = await coreService.ChequeBookRequest(CBSReuest);

                InternalTransDB InternalDB = new InternalTransDB("BA", RequestObject.ReferenceNumber, BancsReferenceNumber, NewAccountResponse.Item2.RequestDateTime, NewAccountResponse.Item2.ResponseDateTime, NewAccountResponse.Item2.ErrorCode, NewAccountResponse.Item2.ErrorDescription, "ChequeBookRequest");
                InternalTransDetails InternalDetail = new InternalTransDetails(BancsReferenceNumber, NewAccountResponse.Item2.RequestString, NewAccountResponse.Item2.ResponseString);

                ListInternalDB.Add(InternalDB);
                ListInternalDetails.Add(InternalDetail);

                if (NewAccountResponse.Item1 == null || !string.IsNullOrEmpty(NewAccountResponse.Item2.ErrorCode))
                {
                    TransDBObject.Error_Code = NewAccountResponse.Item2.ErrorCode;
                    TransDBObject.Error_Desc = NewAccountResponse.Item2.ErrorDescription;
                    ResponseObject.ErrorCode = NewAccountResponse.Item2.ErrorCode;
                    ResponseObject.ErrorDescription = NewAccountResponse.Item2.ErrorDescription;
                    TransDBObject.Res_Time = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                    SIDetail = new SITransDetails(RequestObject.ReferenceNumber, ProductCode, RequestObject.TostringContent(), Newtonsoft.Json.JsonConvert.SerializeObject(ResponseObject), "RequestChequeBook");
                    LoggerImplementObj.InsertInternalTransDBDetails(TransDBObject, SIDetail, ListInternalDB, ListInternalDetails);

                    LoggerImplementObj.LoggerObj.WriteLog(Newtonsoft.Json.JsonConvert.SerializeObject(ResponseObject), "BancsResponse");

                    return ResponseObject;
                }
                ResponseObject.AccountNumber = NewAccountResponse.Item1.MainData;
                ResponseObject.JournalNumber = NewAccountResponse.Item1.JournalNumber;
                ResponseObject.BranchCode = NewAccountResponse.Item1.BranchCode;
                ResponseObject.Status = "O.K.";
                LoggerImplementObj.LoggerObj.WriteLog(Newtonsoft.Json.JsonConvert.SerializeObject(ResponseObject), "BancsResponse");

                TransDBObject.Res_Time = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                SIDetail1 = new SITransDetails(RequestObject.ReferenceNumber, ProductCode, RequestObject.TostringContent(), Newtonsoft.Json.JsonConvert.SerializeObject(ResponseObject), "RequestChequeBook");
                LoggerImplementObj.InsertInternalTransDBDetails(TransDBObject, SIDetail1, ListInternalDB, ListInternalDetails);

                return ResponseObject;
            }
            catch (Exception ex)
            {
                LoggerImplementObj.LoggerObj.WriteExceptionLog(ex.Message, "RequestChequeBookException");

                TransDBObject.Res_Time = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                TransDBObject.Error_Code = "SI001";
                TransDBObject.Error_Desc = ex.Message.Replace("'", "");
                ResponseObject.ErrorDescription = "Exception while processing the response";

                SIDetail1 = new SITransDetails(RequestObject.ReferenceNumber, ProductCode, Newtonsoft.Json.JsonConvert.SerializeObject(RequestObject), Newtonsoft.Json.JsonConvert.SerializeObject(ResponseObject), "LoanAccountInformation");
                LoggerImplementObj.InsertInternalTransDBDetails(TransDBObject, SIDetail1, ListInternalDB, ListInternalDetails);
            }
            finally
            {
                LoggerImplementObj.LoggerObj.WriteLog(Newtonsoft.Json.JsonConvert.SerializeObject(ResponseObject), "BancsResponse");
                LoggerImplementObj.InsertInternalTransDBDetails(TransDBObject, SIDetail, ListInternalDB, ListInternalDetails);
            }

            return ResponseObject;
        }
    }
}