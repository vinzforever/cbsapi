﻿using LotusWebAPI.Models;
using LotusWebAPI.Provider;
using SBISI.CoreBanking.Common.Provider;
using SBISI.CoreBanking.Transaction.Services.Models;
using SBISI.CoreBanking.Transaction.Services.Provider;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace LotusWebAPI.Controllers
{
    public class FundTransferEnquiryController : ApiController
    {
        //[TokenSecurityFilter.TokenSecurityFilter]
        [HttpPost]
        public async Task<TxnResponse> TransferAction(TxnEnquiryRequest RequestObject)
        {
            if (RequestObject == null)
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound) { Content = new StringContent("SI011^Input string was not in a correct format."), ReasonPhrase = "SI011^Input string was not in a correct format." });

            if (string.IsNullOrEmpty(RequestObject.ReferenceNumber))
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound) { Content = new StringContent("SI011^Input string was not in a correct format."), ReasonPhrase = "SI011^Input string was not in a correct format." });

            if (string.IsNullOrEmpty(RequestObject.CBSReferenceNumber))
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound) { Content = new StringContent("SI010^CBS REFERENCE NUMBER IS NULL"), ReasonPhrase = "SI010^CBS REFERENCE NUMBER IS NULL" });

            if (RequestObject.ReferenceNumber.Length != 25)
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound) { Content = new StringContent("SI004^INVALID/NOT-UNIQUE REFERENCE NUMBER"), ReasonPhrase = "SI004^INVALID/NOT-UNIQUE REFERENCE NUMBER" });

            if (RequestObject.BankCode > 7)
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound) { Content = new StringContent("SI006^Bank Code Is Not Proper"), ReasonPhrase = "SI006^Bank Code Is Not Proper" });

            if (RequestObject.ReferenceNumber.Substring(0, 5) != "SBILT")
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound) { Content = new StringContent("SI094^REFERENCE NUMBER INVALID. REFER DOCUMENT."), ReasonPhrase = "SI094^REFERENCE NUMBER INVALID. REFER DOCUMENT." });

            string ProductCode = "SBIVA";
            LoggerImplement LoggerImplementObj = new LoggerImplement(RequestObject.ReferenceNumber, ProductCode);
            LoggerImplementObj.LoggerObj.WriteLog(Newtonsoft.Json.JsonConvert.SerializeObject(RequestObject), "TxnEnquiryRequest");

            DatabaseOperation dbobject = new DatabaseOperation();
            string BankName = DatabaseOperation.GetBankName(RequestObject.BankCode);
            CBSTransaction cbsObject = new CBSTransaction();
            TxnResponse ResponseObject = new TxnResponse();

            List<InternalTransDB> ListInternalDB = new List<InternalTransDB>();
            List<InternalTransDetails> ListInternalDetails = new List<InternalTransDetails>();

            SITransDetails SIDetail = null;
            SITransDetails SIDetail1 = null;
            SITransDB TransDBObject = null;

            ResponseObject.ReferenceNumber = RequestObject.ReferenceNumber;

            try
            {
                ResponseObject.ReferenceNumber = RequestObject.ReferenceNumber;
                ResponseObject.CBSReferenceNumber = RequestObject.CBSReferenceNumber;
                TransDBObject = new SITransDB(RequestObject.ReferenceNumber, ProductCode, DateTime.Now, "", "", "", "FundTransferEnquiry", IPAddressInfo.GetIP());

                string DBResponse = LoggerImplementObj.InsertSITransDB(TransDBObject);
                if (DBResponse != "SUCCESS")
                {
                    string ERROR_DESCRIPTION = string.Empty;
                    switch (DBResponse)
                    {
                        case "SI007": ERROR_DESCRIPTION = "DATABASE ERROR";
                            break;

                        case "SI087": ERROR_DESCRIPTION = "REFERENCE NUMBER MUST BE UNIQUE";
                            break;
                    }
                    ResponseObject.ErrorDesc = ERROR_DESCRIPTION;
                    ResponseObject.ErrorCode = DBResponse;
                    ResponseObject.ReferenceNumber = RequestObject.ReferenceNumber;
                    return ResponseObject;
                }

                CoreBankingTransactionServices coreService = new CoreBankingTransactionServices();
                string MRReferenceNumber = dbobject.generateSBISIRRN(BankName + "MR", "");

                string InputRequest = dbobject.getDataER(RequestObject.CBSReferenceNumber);
                if (InputRequest.Contains("No Record Found"))
                {
                    TxnResponse TxnObj = new TxnResponse();
                    TxnObj.ErrorDesc = InputRequest;
                    TxnObj.ReferenceNumber = RequestObject.ReferenceNumber;
                    return TxnObj;
                }
                string ReqBranchCode = InputRequest.Substring(9, 5);
                string ReqTransactionType = InputRequest.Substring(5, 2);

                if (!RequestObject.CBSReferenceNumber.StartsWith("SBIMR"))
                {
                    RequestObject.CBSReferenceNumber = dbobject.ReturnData(RequestObject.ReferenceNumber);
                    if (RequestObject.CBSReferenceNumber.Contains("No Record Found"))
                    {
                        TxnResponse TxnObj = new TxnResponse();
                        TxnObj.ErrorDesc = RequestObject.CBSReferenceNumber;
                        TxnObj.ReferenceNumber = RequestObject.ReferenceNumber;
                        return TxnObj;
                    }
                }

                ERTransactionRequest TxnRequestObject = new ERTransactionRequest(BankName, ReqBranchCode, "SI", MRReferenceNumber, ReqTransactionType, "SI", RequestObject.CBSReferenceNumber);

                var ERTxnInfo = await coreService.EREnquiryTransaction(TxnRequestObject);

                InternalTransDB InternalDB = new InternalTransDB("MR", RequestObject.ReferenceNumber, MRReferenceNumber, ERTxnInfo.Item2.RequestDateTime, ERTxnInfo.Item2.ResponseDateTime, ERTxnInfo.Item2.ErrorCode, ERTxnInfo.Item2.ErrorDescription, "EREnquiryTransaction");
                InternalTransDetails InternalDetail = new InternalTransDetails(MRReferenceNumber, ERTxnInfo.Item2.RequestString, ERTxnInfo.Item2.ResponseString);

                ListInternalDB.Add(InternalDB);
                ListInternalDetails.Add(InternalDetail);

                if (ERTxnInfo.Item1 == null || !string.IsNullOrEmpty(ERTxnInfo.Item2.ErrorCode))
                {
                    TransDBObject.Error_Code = ERTxnInfo.Item2.ErrorCode;
                    TransDBObject.Error_Desc = ERTxnInfo.Item2.ErrorDescription;
                    ResponseObject.ErrorCode = ERTxnInfo.Item2.ErrorCode;
                    ResponseObject.ErrorDesc = ERTxnInfo.Item2.ErrorDescription;
                    TransDBObject.Res_Time = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                    SIDetail = new SITransDetails(RequestObject.ReferenceNumber, ProductCode, RequestObject.TostringContent(), ResponseObject.TostringContent(), "FundTransferEnquiry");
                    LoggerImplementObj.InsertInternalTransDBDetails(TransDBObject, SIDetail, ListInternalDB, ListInternalDetails);

                    LoggerImplementObj.LoggerObj.WriteLog(Newtonsoft.Json.JsonConvert.SerializeObject(ResponseObject), "TxnResponse");

                    return ResponseObject;
                }

                cbsObject.ConvertObject(ERTxnInfo.Item1, ref ResponseObject);
                TransDBObject.Res_Time = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                SIDetail1 = new SITransDetails(RequestObject.ReferenceNumber, ProductCode, RequestObject.TostringContent(), ResponseObject.TostringContent(), "FundTransferEnquiry");
                LoggerImplementObj.InsertInternalTransDBDetails(TransDBObject, SIDetail1, ListInternalDB, ListInternalDetails);

                LoggerImplementObj.LoggerObj.WriteLog(Newtonsoft.Json.JsonConvert.SerializeObject(ResponseObject), "TxnResponse");

                return ResponseObject;
            }
            catch (Exception ex)
            {
                LoggerImplementObj.LoggerObj.WriteExceptionLog(ex.Message, "FundTransferEnquiryException");

                TransDBObject.Res_Time = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                TransDBObject.Error_Code = "SI001";
                TransDBObject.Error_Desc = ex.Message.Replace("'", "");
                ResponseObject.ErrorDesc = "Exception while processing the response";

                ResponseObject.ReferenceNumber = RequestObject.ReferenceNumber;

                SIDetail = new SITransDetails(RequestObject.ReferenceNumber, ProductCode, Newtonsoft.Json.JsonConvert.SerializeObject(RequestObject), Newtonsoft.Json.JsonConvert.SerializeObject(ResponseObject), "FDPreMatureClosure");
                LoggerImplementObj.InsertInternalTransDBDetails(TransDBObject, SIDetail, ListInternalDB, ListInternalDetails);
            }
            finally
            {
                LoggerImplementObj.LoggerObj.WriteLog(Newtonsoft.Json.JsonConvert.SerializeObject(ResponseObject), "TxnResponse");
                LoggerImplementObj.InsertInternalTransDBDetails(TransDBObject, SIDetail, ListInternalDB, ListInternalDetails);
            }
            return ResponseObject;
        }
    }
}