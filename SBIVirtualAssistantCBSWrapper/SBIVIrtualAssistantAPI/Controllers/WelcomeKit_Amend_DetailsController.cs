﻿using LotusWebAPI.Models;
using LotusWebAPI.Provider;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using SBISI.CoreBanking.Common.Provider;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace LotusWebAPI.Controllers
{
    public class WelcomeKit_Amend_DetailsController : ApiController
    {
        //[TokenSecurityFilter.TokenSecurityFilter]
        [HttpPost]
        public WK_AmendDetailsResponse WelcomeKit_Amend_DetailsPost(WK_AmendDetailsRequest RequestObject)
        {
            if (RequestObject == null)
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound) { Content = new StringContent("SI011^Input string was not in a correct format."), ReasonPhrase = "SI011^Input string was not in a correct format." });

            if (string.IsNullOrEmpty(RequestObject.REQUEST_REF_NO))
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound) { Content = new StringContent("SI011^Input string was not in a correct format."), ReasonPhrase = "SI011^Input string was not in a correct format." });

            if (RequestObject.REQUEST_REF_NO.Length != 25)
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound) { Content = new StringContent("SI004^INVALID/NOT-UNIQUE REFERENCE NUMBER"), ReasonPhrase = "SI004^INVALID/NOT-UNIQUE REFERENCE NUMBER" });

            if ((!string.IsNullOrEmpty(RequestObject.BANK_CODE) && Convert.ToInt32(RequestObject.BANK_CODE) > 7))
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound) { Content = new StringContent("SI006^Bank Code Is Not Proper"), ReasonPhrase = "SI006^Bank Code Is Not Proper" });
            if (RequestObject.REQUEST_REF_NO.Substring(0, 5) != "SBILT")
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound) { Content = new StringContent("SI094^REFERENCE NUMBER INVALID. REFER DOCUMENT."), ReasonPhrase = "SI094^REFERENCE NUMBER INVALID. REFER DOCUMENT." });

            string ProductCode = "SBIVA";
            LoggerImplement LoggerImplementObj = new LoggerImplement(RequestObject.REQUEST_REF_NO, ProductCode);
            LoggerImplementObj.LoggerObj.WriteLog(Newtonsoft.Json.JsonConvert.SerializeObject(RequestObject), "WK_AmendDetailsRequest");

            WK_AmendDetailsResponse ResponseObject = new WK_AmendDetailsResponse();

            List<InternalTransDB> ListInternalDB = new List<InternalTransDB>();
            List<InternalTransDetails> ListInternalDetails = new List<InternalTransDetails>();

            SITransDetails SIDetail = null;
            SITransDetails SIDetail1 = null;
            SITransDB TransDBObject = null;

            CBSTransaction GenericObject = new CBSTransaction();

            DatabaseOperation DBObject = new DatabaseOperation();
            string BankName = DatabaseOperation.GetBankName(Convert.ToInt32(RequestObject.BANK_CODE));

            WK_AmendDetailsRequest_Inherit WK_AmendDetailsRequest_InheritObj = new WK_AmendDetailsRequest_Inherit();
            WK_AmendDetailsRequest_InheritObj.REQUEST_TELLER_ID = RequestObject.REQUEST_TELLER_ID;
            WK_AmendDetailsRequest_InheritObj.REQUEST_REF_NO = RequestObject.REQUEST_REF_NO;
            WK_AmendDetailsRequest_InheritObj.BANK_CODE = RequestObject.BANK_CODE;
            WK_AmendDetailsRequest_InheritObj.BRANCH_CODE = RequestObject.BRANCH_CODE;
            //WK_AmendDetailsRequest_InheritObj.REQUEST_CODE = RequestObject.REQUEST_CODE;
            WK_AmendDetailsRequest_InheritObj.ACCOUNT_NUMBER = RequestObject.ACCOUNT_NUMBER;
            WK_AmendDetailsRequest_InheritObj.CUSTOMER_NUMBER = RequestObject.CUSTOMER_NUMBER;
            WK_AmendDetailsRequest_InheritObj.ID_NO = RequestObject.ID_NO;
            WK_AmendDetailsRequest_InheritObj.CUSTOMER_NAME = RequestObject.CUSTOMER_NAME;
            WK_AmendDetailsRequest_InheritObj.ADDRESS_LINE1 = RequestObject.ADDRESS_LINE1;
            WK_AmendDetailsRequest_InheritObj.ADDRESS_LINE2 = RequestObject.ADDRESS_LINE2;
            WK_AmendDetailsRequest_InheritObj.PAN_NUMBER = RequestObject.PAN_NUMBER;
            WK_AmendDetailsRequest_InheritObj.FORM_60_61 = RequestObject.FORM_60_61;
            WK_AmendDetailsRequest_InheritObj.TAX_FILE_NUMBER_INDICATOR = RequestObject.TAX_FILE_NUMBER_INDICATOR;
            WK_AmendDetailsRequest_InheritObj.ADDRESS_LINE3 = RequestObject.ADDRESS_LINE3;
            WK_AmendDetailsRequest_InheritObj.NATIONALITY = RequestObject.NATIONALITY;
            WK_AmendDetailsRequest_InheritObj.ADDRESS_LINE4 = RequestObject.ADDRESS_LINE4;
            WK_AmendDetailsRequest_InheritObj.POST_CODE = RequestObject.POST_CODE;
            WK_AmendDetailsRequest_InheritObj.PRODUCT = RequestObject.PRODUCT;
            WK_AmendDetailsRequest_InheritObj.TYPE = RequestObject.TYPE;
            WK_AmendDetailsRequest_InheritObj.INTEREST_CATEGORY = RequestObject.INTEREST_CATEGORY;
            WK_AmendDetailsRequest_InheritObj.LIMITED_ACCESS = RequestObject.LIMITED_ACCESS;
            WK_AmendDetailsRequest_InheritObj.INTEREST_PAYMENT_METHOD = RequestObject.INTEREST_PAYMENT_METHOD;
            WK_AmendDetailsRequest_InheritObj.TRANSFER_ACCOUNT_NUMBER = RequestObject.TRANSFER_ACCOUNT_NUMBER;
            WK_AmendDetailsRequest_InheritObj.CHARGE_EXEMPTION_PROFILE = RequestObject.CHARGE_EXEMPTION_PROFILE;
            WK_AmendDetailsRequest_InheritObj.LANGUAGE_CODE = RequestObject.LANGUAGE_CODE;
            WK_AmendDetailsRequest_InheritObj.FREQUENCY = RequestObject.FREQUENCY;
            WK_AmendDetailsRequest_InheritObj.CONSOLIDATED_STATEMENT_PROCESSING = RequestObject.CONSOLIDATED_STATEMENT_PROCESSING;
            WK_AmendDetailsRequest_InheritObj.DATE_INDICATOR = RequestObject.DATE_INDICATOR;
            WK_AmendDetailsRequest_InheritObj.MAIL_INDICATOR = RequestObject.MAIL_INDICATOR;
            WK_AmendDetailsRequest_InheritObj.NOTICE_INDICATOR = RequestObject.NOTICE_INDICATOR;
            WK_AmendDetailsRequest_InheritObj.NOTICE_CUSTOMER = RequestObject.NOTICE_CUSTOMER;
            WK_AmendDetailsRequest_InheritObj.VARIABLE_INTEREST_RATE = RequestObject.VARIABLE_INTEREST_RATE;
            WK_AmendDetailsRequest_InheritObj.STOP_CHQ_PRES_NFT = RequestObject.STOP_CHQ_PRES_NFT;
            WK_AmendDetailsRequest_InheritObj.STOP_CHQ_PRES_AMT_VALIDATION = RequestObject.STOP_CHQ_PRES_AMT_VALIDATION;
            WK_AmendDetailsRequest_InheritObj.INTEREST_FREQUENCY = RequestObject.INTEREST_FREQUENCY;
            WK_AmendDetailsRequest_InheritObj.CYCLE = RequestObject.CYCLE;
            WK_AmendDetailsRequest_InheritObj.DAY = RequestObject.DAY;
            WK_AmendDetailsRequest_InheritObj.AGENT_CODE = RequestObject.AGENT_CODE;
            WK_AmendDetailsRequest_InheritObj.INVESTMENT_TYPE = RequestObject.INVESTMENT_TYPE;
            WK_AmendDetailsRequest_InheritObj.DISTRICT = RequestObject.DISTRICT;
            WK_AmendDetailsRequest_InheritObj.NEXT_DR_INT_APPLN_DATE = RequestObject.NEXT_DR_INT_APPLN_DATE;
            WK_AmendDetailsRequest_InheritObj.GROUP_IND = RequestObject.GROUP_IND;
            WK_AmendDetailsRequest_InheritObj.FACILITY_NUMBER = RequestObject.FACILITY_NUMBER;
            WK_AmendDetailsRequest_InheritObj.APPLICATION_IDENTIFIER = RequestObject.APPLICATION_IDENTIFIER;
            WK_AmendDetailsRequest_InheritObj.DATE = RequestObject.DATE;
            WK_AmendDetailsRequest_InheritObj.CURRENCY = RequestObject.CURRENCY;
            WK_AmendDetailsRequest_InheritObj.ACCOUNT_MGMT_GRP = RequestObject.ACCOUNT_MGMT_GRP;
            WK_AmendDetailsRequest_InheritObj.VISA_FLAG = RequestObject.VISA_FLAG;
            WK_AmendDetailsRequest_InheritObj.VISA_SECURITY_CODE = RequestObject.VISA_SECURITY_CODE;
            WK_AmendDetailsRequest_InheritObj.VISA_CREDIT_LIMIT = RequestObject.VISA_CREDIT_LIMIT;
            WK_AmendDetailsRequest_InheritObj.CREDIT_RATING = RequestObject.CREDIT_RATING;
            WK_AmendDetailsRequest_InheritObj.TERM_LENGTH = RequestObject.TERM_LENGTH;
            WK_AmendDetailsRequest_InheritObj.TERM_BASIS = RequestObject.TERM_BASIS;
            WK_AmendDetailsRequest_InheritObj.TERM_VALUE_DEPOSITED = RequestObject.TERM_VALUE_DEPOSITED;
            WK_AmendDetailsRequest_InheritObj.ACCOUNT_SUB_TYPE = RequestObject.ACCOUNT_SUB_TYPE;
            WK_AmendDetailsRequest_InheritObj.CUSTOMER_RISK_DOMESTIC_RISK = RequestObject.CUSTOMER_RISK_DOMESTIC_RISK;
            WK_AmendDetailsRequest_InheritObj.CROSS_BORDER_RISK = RequestObject.CROSS_BORDER_RISK;
            WK_AmendDetailsRequest_InheritObj.ACCOUNT_CROSS_BORDER_RISK = RequestObject.ACCOUNT_CROSS_BORDER_RISK;
            WK_AmendDetailsRequest_InheritObj.SECURITY_INDICATOR = RequestObject.SECURITY_INDICATOR;
            WK_AmendDetailsRequest_InheritObj.TIME_BAND = RequestObject.TIME_BAND;
            WK_AmendDetailsRequest_InheritObj.VOSTRO_ACCOUNT_BANK_ID = RequestObject.VOSTRO_ACCOUNT_BANK_ID;
            WK_AmendDetailsRequest_InheritObj.GL_CLASS_CODE = RequestObject.GL_CLASS_CODE;
            WK_AmendDetailsRequest_InheritObj.COPIES = RequestObject.COPIES;
            WK_AmendDetailsRequest_InheritObj.NOTICE_PROCESS_INDICATOR = RequestObject.NOTICE_PROCESS_INDICATOR;
            WK_AmendDetailsRequest_InheritObj.ACCOUNT_EXTRACTION_IND = RequestObject.ACCOUNT_EXTRACTION_IND;
            WK_AmendDetailsRequest_InheritObj.FUNDS_TRANSFER_PRICING_RATE = RequestObject.FUNDS_TRANSFER_PRICING_RATE;
            WK_AmendDetailsRequest_InheritObj.FCY_CLAUSE = RequestObject.FCY_CLAUSE;
            WK_AmendDetailsRequest_InheritObj.NOMINATED_HEDGE_CURRENCY = RequestObject.NOMINATED_HEDGE_CURRENCY;
            WK_AmendDetailsRequest_InheritObj.RATE_CHANGE_NOTICE_ON_STATEMENT = RequestObject.RATE_CHANGE_NOTICE_ON_STATEMENT;
            WK_AmendDetailsRequest_InheritObj.OVERDRAFT_REVIEW_INDICATOR = RequestObject.OVERDRAFT_REVIEW_INDICATOR;
            WK_AmendDetailsRequest_InheritObj.PRIMARY_ACCOUNT_NO = RequestObject.PRIMARY_ACCOUNT_NO;
            WK_AmendDetailsRequest_InheritObj.BOOKING_NO = RequestObject.BOOKING_NO;
            WK_AmendDetailsRequest_InheritObj.TPR = RequestObject.TPR;
            WK_AmendDetailsRequest_InheritObj.SMS_REQD = RequestObject.SMS_REQD;
            WK_AmendDetailsRequest_InheritObj.SAVING_PLUS_TERM_LENGTH_DAYS = RequestObject.SAVING_PLUS_TERM_LENGTH_DAYS;
            WK_AmendDetailsRequest_InheritObj.CIRCLE_CODE = RequestObject.CIRCLE_CODE;
            WK_AmendDetailsRequest_InheritObj.ADF = RequestObject.ADF;
            WK_AmendDetailsRequest_InheritObj.PAL = RequestObject.PAL;
            WK_AmendDetailsRequest_InheritObj.RMPB = RequestObject.RMPB;
            WK_AmendDetailsRequest_InheritObj.AGENT_ID = RequestObject.AGENT_ID;
            WK_AmendDetailsRequest_InheritObj.VENDOR_ID = RequestObject.VENDOR_ID;
            WK_AmendDetailsRequest_InheritObj.TERM_DAYS = RequestObject.TERM_DAYS;
            WK_AmendDetailsRequest_InheritObj.TERM_MONTHS = RequestObject.TERM_MONTHS;
            WK_AmendDetailsRequest_InheritObj.TERM_YEARS = RequestObject.TERM_YEARS;
            WK_AmendDetailsRequest_InheritObj.MATURITY_DATE = RequestObject.MATURITY_DATE;
            WK_AmendDetailsRequest_InheritObj.SAVINGS_PLUS = RequestObject.SAVINGS_PLUS;
            WK_AmendDetailsRequest_InheritObj.SAVING_PLUS_ACCT_TYPE = RequestObject.SAVING_PLUS_ACCT_TYPE;
            WK_AmendDetailsRequest_InheritObj.SAVING_PLUS_TERM_LENGTH_MONTH = RequestObject.SAVING_PLUS_TERM_LENGTH_MONTH;
            WK_AmendDetailsRequest_InheritObj.SAVING_PLUS_TERM_BASIS = RequestObject.SAVING_PLUS_TERM_BASIS;
            WK_AmendDetailsRequest_InheritObj.SAVING_PLUS_INTEREST_FREQ = RequestObject.SAVING_PLUS_INTEREST_FREQ;
            WK_AmendDetailsRequest_InheritObj.RD_EXPECTED_INSTL = RequestObject.RD_EXPECTED_INSTL;
            WK_AmendDetailsRequest_InheritObj.RD_INSTL_FREQ = RequestObject.RD_INSTL_FREQ;
            WK_AmendDetailsRequest_InheritObj.RD_INSTALLMENT_DUE_DAY = RequestObject.RD_INSTALLMENT_DUE_DAY;
            WK_AmendDetailsRequest_InheritObj.MOD = RequestObject.MOD;
            WK_AmendDetailsRequest_InheritObj.LINK_ACTYPE = RequestObject.LINK_ACTYPE;
            WK_AmendDetailsRequest_InheritObj.LIFO_FIFO = RequestObject.LIFO_FIFO;
            WK_AmendDetailsRequest_InheritObj.TAG_NO = RequestObject.TAG_NO;
            WK_AmendDetailsRequest_InheritObj.DDO_CODE = RequestObject.DDO_CODE;
            WK_AmendDetailsRequest_InheritObj.MATURITY_INSTRUCTION = RequestObject.MATURITY_INSTRUCTION;
            WK_AmendDetailsRequest_InheritObj.BENEFICIARY_NAME = RequestObject.BENEFICIARY_NAME;
            WK_AmendDetailsRequest_InheritObj.BENEFICIARY_LAST_NAME = RequestObject.BENEFICIARY_LAST_NAME;
            WK_AmendDetailsRequest_InheritObj.ADDRESS1 = RequestObject.ADDRESS1;
            WK_AmendDetailsRequest_InheritObj.ADDRESS2 = RequestObject.ADDRESS2;
            WK_AmendDetailsRequest_InheritObj.PAYABLE_AT = RequestObject.PAYABLE_AT;
            WK_AmendDetailsRequest_InheritObj.PRIMARY_ACCT_NO = RequestObject.PRIMARY_ACCT_NO;
            WK_AmendDetailsRequest_InheritObj.SAVINGS_PLUS_SUB_CATEGORY = RequestObject.SAVINGS_PLUS_SUB_CATEGORY;
            WK_AmendDetailsRequest_InheritObj.BKDT_INT_RECALC_REQ = RequestObject.BKDT_INT_RECALC_REQ;
            WK_AmendDetailsRequest_InheritObj.LINKED_BRANCH_CODE = RequestObject.LINKED_BRANCH_CODE;
            WK_AmendDetailsRequest_InheritObj.CENTRAL_GOVT_CODE = RequestObject.CENTRAL_GOVT_CODE;
            WK_AmendDetailsRequest_InheritObj.CENTGOVT2 = RequestObject.CENTGOVT2;
            WK_AmendDetailsRequest_InheritObj.CENTGOVT3 = RequestObject.CENTGOVT3;
            WK_AmendDetailsRequest_InheritObj.CENTGOVT4 = RequestObject.CENTGOVT4;
            WK_AmendDetailsRequest_InheritObj.STATE_GOVT_CODE = RequestObject.STATE_GOVT_CODE;
            WK_AmendDetailsRequest_InheritObj.INSTALLMENT_COUNT = RequestObject.INSTALLMENT_COUNT;
            WK_AmendDetailsRequest_InheritObj.AMOUNT_DEPOSITED = RequestObject.AMOUNT_DEPOSITED;
            WK_AmendDetailsRequest_InheritObj.IRREGULAR_COUNT = RequestObject.IRREGULAR_COUNT;
            WK_AmendDetailsRequest_InheritObj.NO_OF_EXTNS = RequestObject.NO_OF_EXTNS;
            WK_AmendDetailsRequest_InheritObj.RECEIPT_TYPE = RequestObject.RECEIPT_TYPE;
            WK_AmendDetailsRequest_InheritObj.PRIMARY_ACCT = RequestObject.PRIMARY_ACCT;
            WK_AmendDetailsRequest_InheritObj.ADVICE_REQUIRED = RequestObject.ADVICE_REQUIRED;
            WK_AmendDetailsRequest_InheritObj.TYPE_OF_GOLD = RequestObject.TYPE_OF_GOLD;
            WK_AmendDetailsRequest_InheritObj.BACKDATED_RECALCULATION_IND = RequestObject.BACKDATED_RECALCULATION_IND;
            WK_AmendDetailsRequest_InheritObj.WELCOME_KIT_ISSUED = RequestObject.WELCOME_KIT_ISSUED;
            WK_AmendDetailsRequest_InheritObj.NOMINATION_REQD = RequestObject.NOMINATION_REQD;

            TransDBObject = new SITransDB(RequestObject.REQUEST_REF_NO, ProductCode, DateTime.Now, "", "", "", "WelcomeKit_Amend_Details", IPAddressInfo.GetIP());
            ResponseObject.REQUEST_REF_NO = RequestObject.REQUEST_REF_NO;

            try
            {
                string DBResponse = LoggerImplementObj.InsertSITransDB(TransDBObject);
                if (DBResponse != "SUCCESS")
                {
                    string ERROR_DESCRIPTION = string.Empty;
                    switch (DBResponse)
                    {
                        case "SI007": ERROR_DESCRIPTION = "DATABASE ERROR";
                            break;

                        case "SI087": ERROR_DESCRIPTION = "REFERENCE NUMBER MUST BE UNIQUE";
                            break;
                    }
                    ResponseObject.ERROR_DESCRIPTION = ERROR_DESCRIPTION;
                    ResponseObject.REQUEST_REF_NO = RequestObject.REQUEST_REF_NO;
                    return ResponseObject;
                }
                string MRReferenceNumber = DBObject.generateSBISIRRN(BankName + "TC", "");
                DateTime Request_date_time = DateTime.Now;

                var SICreationResponseString = GenericObject.sendJsonToSIPost("0", Newtonsoft.Json.JsonConvert.SerializeObject(WK_AmendDetailsRequest_InheritObj), "WelcomeKitAmendDetailsPost");
                DateTime Response_Date_Time = DateTime.Now;

                JObject SICreationObj = JObject.Parse(JObject.Parse(SICreationResponseString)["Response"].ToString());

                InternalTransDB InternalDB = new InternalTransDB("TC", RequestObject.REQUEST_REF_NO, MRReferenceNumber, Request_date_time, Response_Date_Time, SICreationObj["RESPONSE_STATUS"].ToString(), "", "WelcomeKit_Amend_Details");
                InternalTransDetails InternalDetail = new InternalTransDetails(MRReferenceNumber, Newtonsoft.Json.JsonConvert.SerializeObject(WK_AmendDetailsRequest_InheritObj), SICreationResponseString);

                ListInternalDB.Add(InternalDB);
                ListInternalDetails.Add(InternalDetail);

                ResponseObject.REQUEST_REF_NO = RequestObject.REQUEST_REF_NO;

                if (SICreationObj["RESPONSE_STATUS"].ToString() == "1")
                {
                    TransDBObject.Error_Desc = SICreationObj["ERROR_DESCRIPTION"].ToString();
                    TransDBObject.Res_Time = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");

                    foreach (JProperty propertyName in SICreationObj.Properties())
                    {
                        if (ResponseObject.GetType().GetProperty(propertyName.Name) != null)
                            ResponseObject.GetType().GetProperty(propertyName.Name).SetValue(ResponseObject, SICreationObj[propertyName.Name].ToString());
                    }

                    SIDetail = new SITransDetails(RequestObject.REQUEST_REF_NO, ProductCode, Newtonsoft.Json.JsonConvert.SerializeObject(RequestObject), Newtonsoft.Json.JsonConvert.SerializeObject(ResponseObject), "WelcomeKit_Amend_Details");
                    LoggerImplementObj.InsertInternalTransDBDetails(TransDBObject, SIDetail, ListInternalDB, ListInternalDetails);

                    ResponseObject.ERROR_DESCRIPTION = TransDBObject.Error_Desc;

                    return ResponseObject;
                }

                foreach (JProperty propertyName in SICreationObj.Properties())
                {
                    if (ResponseObject.GetType().GetProperty(propertyName.Name) != null)
                        ResponseObject.GetType().GetProperty(propertyName.Name).SetValue(ResponseObject, SICreationObj[propertyName.Name].ToString());
                }

                TransDBObject.Res_Time = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                SIDetail1 = new SITransDetails(RequestObject.REQUEST_REF_NO, ProductCode, Newtonsoft.Json.JsonConvert.SerializeObject(RequestObject), JsonConvert.SerializeObject(ResponseObject), "WelcomeKit_Amend_Details");
                LoggerImplementObj.InsertInternalTransDBDetails(TransDBObject, SIDetail1, ListInternalDB, ListInternalDetails);

                return ResponseObject;
            }
            catch (Exception ex)
            {
                LoggerImplementObj.LoggerObj.WriteExceptionLog(ex.Message, "WelcomeKit_Amend_DetailsException");

                TransDBObject.Res_Time = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                TransDBObject.Error_Code = "SI001";
                TransDBObject.Error_Desc = ex.Message.Replace("'", "");
                ResponseObject.ERROR_DESCRIPTION = "Exception while processing the response";

                ResponseObject.REQUEST_REF_NO = RequestObject.REQUEST_REF_NO;

                SIDetail = new SITransDetails(RequestObject.REQUEST_REF_NO, ProductCode, Newtonsoft.Json.JsonConvert.SerializeObject(RequestObject), Newtonsoft.Json.JsonConvert.SerializeObject(ResponseObject), "WelcomeKit_Amend_Details");
                LoggerImplementObj.InsertInternalTransDBDetails(TransDBObject, SIDetail, ListInternalDB, ListInternalDetails);
            }
            finally
            {
                LoggerImplementObj.LoggerObj.WriteLog(Newtonsoft.Json.JsonConvert.SerializeObject(ResponseObject), "WK_AmendDetailsResponse");
                LoggerImplementObj.InsertInternalTransDBDetails(TransDBObject, SIDetail, ListInternalDB, ListInternalDetails);
            }

            return ResponseObject;
        }
    }
}