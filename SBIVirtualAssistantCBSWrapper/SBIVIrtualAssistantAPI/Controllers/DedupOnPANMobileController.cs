﻿using LotusWebAPI.Models;
using LotusWebAPI.Provider;
using SBISI.CoreBanking.Common.Provider;
using SBISI.CoreBanking.Services.Models;
using SBISI.CoreBanking.Services.Providers;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace LotusWebAPI.Controllers
{
    public class DedupOnPANMobileController : ApiController
    {
        //[TokenSecurityFilter.TokenSecurityFilter]
        [HttpPost]
        public async Task<Cif_MobilePanResponse> DedupOnPANMobile(Cif_MobilePanRequest RequestObject)
        {
            if (RequestObject == null)
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound) { Content = new StringContent("SI011^Input string was not in a correct format."), ReasonPhrase = "SI011^Input string was not in a correct format." });

            if (string.IsNullOrEmpty(RequestObject.ReferenceNumber) || string.IsNullOrEmpty(RequestObject.RequestData))
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound) { Content = new StringContent("SI011^Input string was not in a correct format."), ReasonPhrase = "SI011^Input string was not in a correct format." });

            if (RequestObject.ReferenceNumber.Length != 25)
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound) { Content = new StringContent("SI004^INVALID/NOT-UNIQUE REFERENCE NUMBER"), ReasonPhrase = "SI004^INVALID/NOT-UNIQUE REFERENCE NUMBER" });

            if (RequestObject.Bank_Code > 7)
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound) { Content = new StringContent("SI006^Bank Code Is Not Proper"), ReasonPhrase = "SI006^Bank Code Is Not Proper" });

            if (RequestObject.ReferenceNumber.Substring(0, 5) != "SBILT")
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound) { Content = new StringContent("SI094^REFERENCE NUMBER INVALID. REFER DOCUMENT."), ReasonPhrase = "SI094^REFERENCE NUMBER INVALID. REFER DOCUMENT." });

            string ProductCode = "SBIVA";
            LoggerImplement LoggerImplementObj = new LoggerImplement(RequestObject.ReferenceNumber, ProductCode);
            LoggerImplementObj.LoggerObj.WriteLog(Newtonsoft.Json.JsonConvert.SerializeObject(RequestObject), "Cif_MobilePanRequest");

            Cif_MobilePanResponse ResponseObject = new Cif_MobilePanResponse();
            ResponseObject.Cif_Details_List = new List<Individual_CIf_Details>();

            List<InternalTransDB> ListInternalDB = new List<InternalTransDB>();
            List<InternalTransDetails> ListInternalDetails = new List<InternalTransDetails>();

            SITransDetails SIDetail = null;
            SITransDetails SIDetail1 = null;
            SITransDB TransDBObject = null;

            CBSTransaction GenericObject = new CBSTransaction();

            DatabaseOperation DBObject = new DatabaseOperation();
            string BankName = DatabaseOperation.GetBankName(RequestObject.Bank_Code);
            string MRReferenceNumber = DBObject.generateSBISIRRN(BankName + "MR", "");

            TransDBObject = new SITransDB(RequestObject.ReferenceNumber, ProductCode, DateTime.Now, "", "", "", "DedupOnPANMobile", IPAddressInfo.GetIP());

            ResponseObject.ReferenceNumber = RequestObject.ReferenceNumber;
            try
            {
                ResponseObject.ReferenceNumber = RequestObject.ReferenceNumber;

                string DBResponse = LoggerImplementObj.InsertSITransDB(TransDBObject);
                if (DBResponse != "SUCCESS")
                {
                    string ERROR_DESCRIPTION = string.Empty;
                    switch (DBResponse)
                    {
                        case "SI007": ERROR_DESCRIPTION = "DATABASE ERROR";
                            break;

                        case "SI087": ERROR_DESCRIPTION = "REFERENCE NUMBER MUST BE UNIQUE";
                            break;
                    }
                    ResponseObject.ErrorDesc = ERROR_DESCRIPTION;
                    ResponseObject.ReferenceNumber = RequestObject.ReferenceNumber;
                    ResponseObject.ErrorCode = DBResponse;
                    return ResponseObject;
                }
                CoreBankingServices coreService = new CoreBankingServices();
                CIf_MobilePan CBSReuest = new CIf_MobilePan(RequestObject.RequestType, BankName, RequestObject.RequestData);

                var responseobj = await coreService.GetCIF_MobilePan(CBSReuest);

                InternalTransDB InternalDB = new InternalTransDB("MR", RequestObject.ReferenceNumber, MRReferenceNumber, responseobj.Item2.RequestDateTime, responseobj.Item2.ResponseDateTime, responseobj.Item2.ErrorCode, responseobj.Item2.ErrorDescription, "GetCIF_MobilePan");
                InternalTransDetails InternalDetail = new InternalTransDetails(MRReferenceNumber, responseobj.Item2.RequestString, responseobj.Item2.ResponseString);

                ListInternalDB.Add(InternalDB);
                ListInternalDetails.Add(InternalDetail);

                if (responseobj.Item1 == null || !string.IsNullOrEmpty(responseobj.Item2.ErrorCode))
                {
                    TransDBObject.Error_Code = responseobj.Item2.ErrorCode;
                    TransDBObject.Error_Desc = responseobj.Item2.ErrorDescription;
                    TransDBObject.Res_Time = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");

                    SIDetail = new SITransDetails(RequestObject.ReferenceNumber, ProductCode, Newtonsoft.Json.JsonConvert.SerializeObject(RequestObject.RequestData), responseobj.Item2.ErrorCode + "^" + responseobj.Item2.ErrorDescription, "DedupOnPANMobile");
                    LoggerImplementObj.InsertInternalTransDBDetails(TransDBObject, SIDetail, ListInternalDB, ListInternalDetails);

                    ResponseObject.ErrorDesc = TransDBObject.Error_Desc;
                    ResponseObject.ErrorCode = TransDBObject.Error_Code;
                    LoggerImplementObj.LoggerObj.WriteLog(Newtonsoft.Json.JsonConvert.SerializeObject(ResponseObject), "Cif_MobilePanResponse");

                    return ResponseObject;
                }

                foreach (CIf_Details cif_Details_obj in responseobj.Item1.Cif_Details_List)
                {
                    Individual_CIf_Details Cif_Obj = new Individual_CIf_Details();

                    Cif_Obj.CifNumber = cif_Details_obj.CifNumber;
                    Cif_Obj.CustomerName = cif_Details_obj.CustomerName;

                    ResponseObject.Cif_Details_List.Add(Cif_Obj);
                }
                LoggerImplementObj.LoggerObj.WriteLog(Newtonsoft.Json.JsonConvert.SerializeObject(ResponseObject), "Cif_MobilePanResponse");

                TransDBObject.Res_Time = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                SIDetail1 = new SITransDetails(RequestObject.ReferenceNumber, ProductCode, Newtonsoft.Json.JsonConvert.SerializeObject(RequestObject), Newtonsoft.Json.JsonConvert.SerializeObject(ResponseObject), "DedupOnPANMobile");

                LoggerImplementObj.InsertInternalTransDBDetails(TransDBObject, SIDetail1, ListInternalDB, ListInternalDetails);

                return ResponseObject;
            }
            catch (Exception ex)
            {
                LoggerImplementObj.LoggerObj.WriteExceptionLog(ex.Message, "DedupOnPANMobileException");

                TransDBObject.Res_Time = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                TransDBObject.Error_Code = "SI001";
                TransDBObject.Error_Desc = ex.Message.Replace("'", "");
                ResponseObject.ErrorDesc = "Exception while processing the response";

                SIDetail1 = new SITransDetails(RequestObject.ReferenceNumber, ProductCode, Newtonsoft.Json.JsonConvert.SerializeObject(RequestObject), Newtonsoft.Json.JsonConvert.SerializeObject(ResponseObject), "DedupOnPANMobile");
                LoggerImplementObj.InsertInternalTransDBDetails(TransDBObject, SIDetail1, ListInternalDB, ListInternalDetails);
            }
            finally
            {
                LoggerImplementObj.LoggerObj.WriteLog(Newtonsoft.Json.JsonConvert.SerializeObject(ResponseObject), "CustomerPersonalAmmendmentResponse");
                LoggerImplementObj.InsertInternalTransDBDetails(TransDBObject, SIDetail, ListInternalDB, ListInternalDetails);
            }

            return ResponseObject;
        }
    }
}