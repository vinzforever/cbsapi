﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBIVirtualAssistantCBSWrapper.Common.Utility
{
    public struct DBQueryID
    {
        public const char QuerySeparator = ';';
        public const string GETACCOUNTLIST = "GETACCOUNTLIST";
        public const string GETTESTDATA = "GETTESTDATA";
        public const string GETSEQUENCE = "GETSEQUENCE";
    }
}
