﻿using SBIVirtualAssistantCBSWrapper.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBIVirtualAssistantCBSWrapper.Domain.Contracts
{
    public interface ISBIVirtualAssistantCBSWrapperDomain
    {
        SMSResponse sendSMS(SMSData smsData);

        BalanceEnquiryResponse getAccountBalance(AccountBalanceRequest accountBalanceRequest);

        RTGSResponse RTGStransfer(RTGSRequest rtgsRequest);
        NEFTResponse NEFTtransfer(NEFTRequest neftRequest);
        SBITransferResponse SBITransfer(SBITransferRequest sbiTransferRequest);
                
        //Enquiry API
        AllAccountDetailsResponse AllAccountDetails(AllAccountDetailsRequest allAccountDetailsRequest);

        FDCreationResponse FDCreation(FDCreationRequest fdCreationRequest);

        RDCreationResponse RDCreation(RDCreationRequest rdCreationRequest);

        ChequeBookResponse RequestChequeBook(ChequeBookRequest chequeBookRequest);

        AccountListResponse GetAccountDetails(CBSAccountRequest cbsAccountRequest);

        INBAccountStatementDetails AccountStatement(AccountStatementRequest accountStatementRequest);

        CIFNumberResponse getCIFNumber(CIFNumberRequest cifNumberRequest);
    }
}
