﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SBIVirtualAssistantCBSWrapper.Domain.Contracts;
using SBIVirtualAssistantCBSWrapper.Client;
using SBIVirtualAssistantCBSWrapper.Common.Resources;
using System.Net.Http;
using Newtonsoft.Json;
using SBIVirtualAssistantCBSWrapper.Repository.Contracts;
using SBIVirtualAssistantCBSWrapper.Entities;

namespace SBIVirtualAssistantCBSWrapper.Domain
{
    class SBIVirtualAssistantCBSWrapperDomain : ISBIVirtualAssistantCBSWrapperDomain
    {


        private ISBIVirtualAssistantCBSWrapperRepository _Repository;

        public SBIVirtualAssistantCBSWrapperDomain(ISBIVirtualAssistantCBSWrapperRepository Repository)
        {
            _Repository = Repository;
        }

        public SMSResponse sendSMS(Entities.SMSData smsData)
        {

            SMSResponse smsResponse = new SMSResponse();

            smsData.MessageData = "Bank never calls you to share this" + smsData.MessageData;
            String request = JsonConvert.SerializeObject(smsData);



            HttpBaseClient clobj = new HttpBaseClient(System.Configuration.ConfigurationSettings.AppSettings.Get("CBSAPIURL"));
            System.Net.Http.HttpContent cl = new StringContent(request, Encoding.UTF8, "application/json");
            smsResponse.data = clobj.ExecuteHttpRequest(URLs.SMS_API, "POST", cl);

            return smsResponse;
        }

        public BalanceEnquiryResponse getAccountBalance(AccountBalanceRequest accountBalanceRequest)
        {
            BalanceEnquiryResponse accountBalanceResponse = new BalanceEnquiryResponse();


            String request = JsonConvert.SerializeObject(accountBalanceRequest);
            HttpBaseClient clobj = new HttpBaseClient(System.Configuration.ConfigurationSettings.AppSettings.Get("APIWRAPPERURL"));
            System.Net.Http.HttpContent cl = new StringContent(request, Encoding.UTF8, "application/json");
            string response = clobj.ExecuteHttpRequest(URLs.AccountInformation, "POST", cl);
            accountBalanceResponse = JsonConvert.DeserializeObject<BalanceEnquiryResponse>(response);

            string bal = accountBalanceResponse.AvailableBalance.Replace("+", "");
            accountBalanceResponse.SpeechEnglish = "Your Balance is " + bal;
            accountBalanceResponse.SpeechHindi = "आपकी शेष राशि " + bal + " है";
            accountBalanceResponse.DisplayEnglish = "Your balance in Account number " + accountBalanceRequest.RequestData + " is " + bal + " Rupees";
            accountBalanceResponse.DisplayHindi = "खाता संख्या " + accountBalanceRequest.RequestData + " में आपकी शेष राशि " + bal + " रुपये है";
            return accountBalanceResponse;
        }

        public string getReferenceNumber(string QueryID)
        {
            var resp = _Repository.getReferenceNumber("SBIUP" + DateTime.Now.ToString("ddMMyyyy"));
            return resp;
        }


        public RTGSResponse RTGStransfer(RTGSRequest rtgsRequest)
        {

            RTGSResponse rtgsResponse = new RTGSResponse();


            String request = JsonConvert.SerializeObject(rtgsRequest);
            HttpBaseClient clobj = new HttpBaseClient(System.Configuration.ConfigurationSettings.AppSettings.Get("APIWRAPPERURL"));
            System.Net.Http.HttpContent cl = new StringContent(request, Encoding.UTF8, "application/json");
            string response = clobj.ExecuteHttpRequest(URLs.RTGSTransaction, "POST", cl);
            rtgsResponse = JsonConvert.DeserializeObject<RTGSResponse>(response);


            rtgsResponse.DisplayEnglish = "Thank you for using SBI Chatboot. Your transaction of Rs. " + rtgsRequest.TXNAMT + " successful with UTR: " + rtgsResponse.CBSReferenceNumber;
            rtgsResponse.DisplayHindi = "भारतीय स्टेट बैंक की चैट बोट सेवा का उपयोग करने के लिए धन्यवाद. आपका  " + rtgsRequest.TXNAMT + "  रूपये का लेन देन लाभार्थी को दिनांक " + DateTime.Now + " को प्राप्त हो गया है।";
            rtgsResponse.SpeechEnglish = rtgsResponse.DisplayEnglish;
            rtgsResponse.SpeechHindi = rtgsResponse.DisplayHindi;


            return rtgsResponse;

        }

        public NEFTResponse NEFTtransfer(NEFTRequest neftRequest)
        {
            NEFTResponse neftResponse = new NEFTResponse();


            String request = JsonConvert.SerializeObject(neftRequest);
            HttpBaseClient clobj = new HttpBaseClient(System.Configuration.ConfigurationSettings.AppSettings.Get("APIWRAPPERURL"));
            System.Net.Http.HttpContent cl = new StringContent(request, Encoding.UTF8, "application/json");
            string response = clobj.ExecuteHttpRequest(URLs.NEFT, "POST", cl);
            neftResponse = JsonConvert.DeserializeObject<NEFTResponse>(response);


            neftResponse.DisplayEnglish = "Thank you for using SBI Chatboot. Your transaction of Rs. " + neftRequest.TXNAMT + " successful with UTR: " + neftResponse.CBSReferenceNumber;
            neftResponse.DisplayHindi = "भारतीय स्टेट बैंक की चैट बोट सेवा का उपयोग करने के लिए धन्यवाद. आपका  " + neftRequest.TXNAMT + "  रूपये का लेन देन लाभार्थी को दिनांक " + DateTime.Now + " को प्राप्त हो गया है।";
            neftResponse.SpeechEnglish = neftResponse.DisplayEnglish;
            neftResponse.SpeechHindi = neftResponse.DisplayHindi;


            return neftResponse;
        }


        public SBITransferResponse SBITransfer(SBITransferRequest sbiTransferRequest)
        {

            SBITransferResponse sbiTransferResponse = new SBITransferResponse();

            String request = JsonConvert.SerializeObject(sbiTransferRequest);
            HttpBaseClient clobj = new HttpBaseClient(System.Configuration.ConfigurationSettings.AppSettings.Get("APIWRAPPERURL"));
            System.Net.Http.HttpContent cl = new StringContent(request, Encoding.UTF8, "application/json");
            string response = clobj.ExecuteHttpRequest(URLs.FundTransfer, "POST", cl);
            sbiTransferResponse = JsonConvert.DeserializeObject<SBITransferResponse>(response);


            //sbiTransferResponse.CBSReferenceNumber = "SBISI12345678901234567890";
            //sbiTransferResponse.JournalNumber = "12456877";
            //sbiTransferResponse.ReferenceNumber = "RRNFF12345678901234567890";

            sbiTransferResponse.DisplayEnglish = "Thank you for using SBI Chatboot. Your transaction of Rs. " + sbiTransferRequest.Amount + " successful with UTR: " + sbiTransferResponse.CBSReferenceNumber;
            sbiTransferResponse.DisplayHindi = "भारतीय स्टेट बैंक की चैट बोट सेवा का उपयोग करने के लिए धन्यवाद. आपका  " + sbiTransferRequest.Amount + "  रूपये का लेन देन लाभार्थी को दिनांक 12/05/2017 11:30 को प्राप्त हो गया है।";
            sbiTransferResponse.SpeechEnglish = sbiTransferResponse.DisplayEnglish;
            sbiTransferResponse.SpeechHindi = sbiTransferResponse.DisplayHindi;



            return sbiTransferResponse;


        }


        public AllAccountDetailsResponse AllAccountDetails(AllAccountDetailsRequest allAccountDetailsRequest)
        {


           return AllAccountDetails1(allAccountDetailsRequest);

            AllAccountDetailsResponse allAccountDetailsResponse = new AllAccountDetailsResponse();


          var data = getReferenceNumber("GETSEQUENCE");


            String request = JsonConvert.SerializeObject(allAccountDetailsRequest);
            HttpBaseClient clobj = new HttpBaseClient(System.Configuration.ConfigurationSettings.AppSettings.Get("APIGATEWAYURL"));

            System.Net.Http.HttpContent cl = new StringContent(request, Encoding.UTF8, "application/json");


            string response = clobj.ExecuteHttpRequest(URLs.AccountList, "POST", cl);
            allAccountDetailsResponse = JsonConvert.DeserializeObject<AllAccountDetailsResponse>(response);


            allAccountDetailsResponse.calculateNetBalance();


            return allAccountDetailsResponse;

        }




        public AllAccountDetailsResponse AllAccountDetails1(AllAccountDetailsRequest AccountNumber)
        {
            AllAccountDetailsResponse allAccountDetailsResponse = new AllAccountDetailsResponse();


            //var data = getReferenceNumber("GETSEQUENCE");


            var request = JsonConvert.SerializeObject(
    new
    {
        AccountNumber = AccountNumber.Request
    });



            //String request = JsonConvert.SerializeObject(allAccountDetailsRequest);

            HttpBaseClient clobj = new HttpBaseClient(System.Configuration.ConfigurationSettings.AppSettings.Get("APIGATEWAYURL"));
            System.Net.Http.HttpContent cl = new StringContent(request, Encoding.UTF8, "application/json");
            string response = clobj.ExecuteHttpRequest("EnqBancsAccountsList", "POST", cl);
            allAccountDetailsResponse = JsonConvert.DeserializeObject<AllAccountDetailsResponse>(response);


           allAccountDetailsResponse.calculateNetBalance();


            return allAccountDetailsResponse;

        }






        public FDCreationResponse FDCreation(FDCreationRequest fdCreationRequest)
        {
            FDCreationResponse fdCreationResponse = new FDCreationResponse();

            fdCreationRequest.REQUEST_REF_NO = getReferenceNumber("GETSEQUENCE");

            String request = JsonConvert.SerializeObject(fdCreationRequest);
            HttpBaseClient clobj = new HttpBaseClient(System.Configuration.ConfigurationSettings.AppSettings.Get("APIWRAPPERURL"));
            System.Net.Http.HttpContent cl = new StringContent(request, Encoding.UTF8, "application/json");
            string response = clobj.ExecuteHttpRequest(URLs.FDCreation, "POST", cl);
            fdCreationResponse = JsonConvert.DeserializeObject<FDCreationResponse>(response);


            fdCreationResponse.DisplayEnglish = "Thank you for using SBI Chatboot. Your Fixed Deposit of Rs. " + fdCreationRequest.TERM_VALUE_DEPOSITED + " is created successfully";
            fdCreationResponse.DisplayHindi = "भारतीय स्टेट बैंक की चैट बोट सेवा का उपयोग करने के लिए धन्यवाद. आपकी " + fdCreationRequest.TERM_VALUE_DEPOSITED + " रुपये की सावधि जमा " + DateTime.Now + " को बन गई है";
            fdCreationResponse.SpeechEnglish = fdCreationResponse.DisplayEnglish;
            fdCreationResponse.SpeechHindi = fdCreationResponse.DisplayHindi;


            return fdCreationResponse;
        }


        public RDCreationResponse RDCreation(RDCreationRequest rdCreationRequest)
        {
            RDCreationResponse rdCreationResponse = new RDCreationResponse();

            String request = JsonConvert.SerializeObject(rdCreationRequest);
            HttpBaseClient clobj = new HttpBaseClient(System.Configuration.ConfigurationSettings.AppSettings.Get("APIWRAPPERURL"));
            System.Net.Http.HttpContent cl = new StringContent(request, Encoding.UTF8, "application/json");
            string response = clobj.ExecuteHttpRequest(URLs.RDCreation, "POST", cl);
            rdCreationResponse = JsonConvert.DeserializeObject<RDCreationResponse>(response);


            rdCreationResponse.DisplayEnglish = "Thank you for using SBI Chatboot. Your Recurrent Deposit of Rs. " + rdCreationRequest.TERM_VALUE_DEPOSITED + " is created successfully";
            rdCreationResponse.DisplayHindi = "भारतीय स्टेट बैंक की चैट बोट सेवा का उपयोग करने के लिए धन्यवाद. आपकी " + rdCreationRequest.TERM_VALUE_DEPOSITED + " रुपये की आवर्ती जमा " + DateTime.Now + " को बन गई है";
            rdCreationResponse.SpeechEnglish = rdCreationResponse.DisplayEnglish;
            rdCreationResponse.SpeechHindi = rdCreationResponse.DisplayHindi;

            return rdCreationResponse;

        }


        public ChequeBookResponse RequestChequeBook(ChequeBookRequest chequeBookRequest)
        {
            ChequeBookResponse chequeBookResponse = new ChequeBookResponse();

            // var data = getReferenceNumber("GETSEQUENCE");




            String request = JsonConvert.SerializeObject(chequeBookRequest);
            HttpBaseClient clobj = new HttpBaseClient(System.Configuration.ConfigurationSettings.AppSettings.Get("APIWRAPPERURL"));
            System.Net.Http.HttpContent cl = new StringContent(request, Encoding.UTF8, "application/json");
            string response = clobj.ExecuteHttpRequest(URLs.RequestChequeBook, "POST", cl);
            chequeBookResponse = JsonConvert.DeserializeObject<ChequeBookResponse>(response);


            return chequeBookResponse;

        }


        public AccountListResponse GetAccountDetails(CBSAccountRequest cbsAccountRequest)
        {
            AccountListResponse accountListResponse = new AccountListResponse();

            // var data = getReferenceNumber("GETSEQUENCE");




            String request = JsonConvert.SerializeObject(cbsAccountRequest);
            HttpBaseClient clobj = new HttpBaseClient(System.Configuration.ConfigurationSettings.AppSettings.Get("APIWRAPPERURL"));
            System.Net.Http.HttpContent cl = new StringContent(request, Encoding.UTF8, "application/json");
            string response = clobj.ExecuteHttpRequest(URLs.AccountList, "POST", cl);
            accountListResponse = JsonConvert.DeserializeObject<AccountListResponse>(response);


            return accountListResponse;
        }


        public INBAccountStatementDetails AccountStatement(AccountStatementRequest accountStatementRequest)
        {

            INBAccountStatementDetails inbAccountStatementDetails = new INBAccountStatementDetails();

            // var data = getReferenceNumber("GETSEQUENCE");

            String request = JsonConvert.SerializeObject(accountStatementRequest);
            HttpBaseClient clobj = new HttpBaseClient(System.Configuration.ConfigurationSettings.AppSettings.Get("APIGATEWAYURL"));
            System.Net.Http.HttpContent cl = new StringContent(request, Encoding.UTF8, "application/json");
            string response = clobj.ExecuteHttpRequest(URLs.APIGATEWAYAccountStatement, "POST", cl);
            inbAccountStatementDetails = JsonConvert.DeserializeObject<INBAccountStatementDetails>(response);

            return inbAccountStatementDetails;


        }


        public CIFNumberResponse getCIFNumber(CIFNumberRequest cifNumberRequest)
        {

            CIFNumberResponse cifNumberResponse = new CIFNumberResponse();

            String request = JsonConvert.SerializeObject(cifNumberRequest);
            HttpBaseClient clobj = new HttpBaseClient(System.Configuration.ConfigurationSettings.AppSettings.Get("APIWRAPPERURL"));
            System.Net.Http.HttpContent cl = new StringContent(request, Encoding.UTF8, "application/json");
            string response = clobj.ExecuteHttpRequest(URLs.CIF_from_ACCOUNT, "POST", cl);
            cifNumberResponse = JsonConvert.DeserializeObject<CIFNumberResponse>(response);

            return cifNumberResponse;

        }
    }
}
